/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')
const dotenv = require('dotenv');

dotenv.config({
  path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV}`),
});

/** @type {import('next').NextConfig} */

// Remove this if you're not using Fullcalendar features
const publicRuntimeConfig = {
  SLUG_CUSTOMER: process.env.SLUG_API_SERVICE_CUSTOMER || "customers",
  SLUG_REF_DATA: process.env.SLUG_API_SERVICE_REFDATA || "ref-data",
  SLUG_VEHICLE: process.env.SLUG_API_SERVICE_VEHICLE || "vehicles",
  SLUG_WARRANTY: process.env.SLUG_API_SERVICE_WARRANTY || "warranty",
  SLUG_AUTHEN: process.env.SLUG_API_SERVICE_AUTHEN || "authen",
  SLUG_ORGANIZATION: process.env.SLUG_API_SERVICE_ORGANIZATION || "organization",
  SLUG_NOTIFICATION: process.env.SLUG_API_SERVICE_NOTIFICATIONS || "notifications",
  SLUG_MEDIA_URL: process.env.SLUG_API_MEDIA || "media",
  SLUG_DOCUMENTS: process.env.SLUG_API_SERVICE_DOCUMENTS || "documents",
  SLUG_APPOINTMENT: process.env.SLUG_API_SERVICE_APPOINTMENT || "appointment",
  DEFAULT_USERNAME: process.env.NODE_ENV === "production" ? "" : "admin",
  DEFAULT_PASSWORD: process.env.NODE_ENV === "production" ? "" : "Thaco@123",
  DEFAULT_RECAPTCHA: process.env.NODE_ENV === "production" ? "" : "TRUE",
  WWW_SERVICE_IMAGE: process.env.WWW_API_SERVICE_HOST + "/" + process.env.SLUG_API_SERVICE_REFDATA + "/",
  X_API_KEY: process.env.X_API_KEY,
  RECAPTCHA_SITE_KEY: process.env.RECAPTCHA_SITE_KEY
};
module.exports = {
  trailingSlash: true,
  reactStrictMode: false,
  publicRuntimeConfig,
  webpack: config => {
    config.resolve.alias = {
      ...config.resolve.alias,
      apexcharts: path.resolve(__dirname, './node_modules/apexcharts-clevision')
    }

    return config
  }
}
