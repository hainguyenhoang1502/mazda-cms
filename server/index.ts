require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
})

const express = require('express')
const next = require('next')
const url = require('url')
const port = process.env.PORT
const dev = process.env.NODE_ENV === 'development'
const app = next({ dev })
const handle = app.getRequestHandler()
const proxy = require('express-http-proxy')

app.prepare().then(() => {
  const server = express()

  // ** Proxy Service Customer
  server.use('/api-customer',
    proxy(process.env.WWW_API_SERVICE_HOST, {
      proxyReqPathResolver: req => {

        return `/${process.env.SLUG_API_SERVICE_CUSTOMER}/api${url.parse(req.url).path}`
      },
      limit: '5mb'
    })
  )

  // ** Proxy Service Ref Data
  server.use('/api-ref-data',
    proxy(process.env.WWW_API_SERVICE_HOST, {
      proxyReqPathResolver: req => {
        return `/${process.env.SLUG_API_SERVICE_REFDATA}/api${url.parse(req.url).path}`
      },
      limit: '5mb'
    })
  )

  // ** Proxy Service Vehicle
  server.use('/api-vehicle',
    proxy(process.env.WWW_API_SERVICE_HOST, {
      proxyReqPathResolver: req => {

        return `/${process.env.SLUG_API_SERVICE_VEHICLE}/api${url.parse(req.url).path}`
      },
      limit: '5mb'
    })
  )

  // ** Proxy Service Warranty
  server.use('/api-warranty',
    proxy(process.env.WWW_API_SERVICE_HOST, {
      proxyReqPathResolver: req => {

        return `/${process.env.SLUG_API_SERVICE_WARRANTY}/api${url.parse(req.url).path}`
      },
      limit: '5mb'
    })
  )

  // ** Proxy Service Authen
  server.use('/api-authen',
    proxy(process.env.WWW_API_SERVICE_HOST, {
      proxyReqPathResolver: req => {

        return `/${process.env.SLUG_API_SERVICE_AUTHEN}/api${url.parse(req.url).path}`
      },
      limit: '5mb'
    })
  )

  // ** Proxy Service Organization
  server.use('/api-organization',
    proxy(process.env.WWW_API_SERVICE_HOST, {
      proxyReqPathResolver: req => {

        return `/${process.env.SLUG_API_SERVICE_ORGANIZATION}/api${url.parse(req.url).path}`
      },
      limit: '5mb'
    })
  )

  // ** Proxy Api Google
  server.use('/api-google',
    proxy("https://www.google.com", {
      proxyReqPathResolver: req => {
        return `/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET_KEY}&${url.parse(req.url).path.substring(1)}`
      },
      limit: '5mb'
    })
  )

  // ** Proxy Service documents
  server.use('/api-document',
    proxy(process.env.WWW_API_SERVICE_HOST, {
      proxyReqPathResolver: req => {

        return `/${process.env.SLUG_API_SERVICE_DOCUMENTS}/api${url.parse(req.url).path}`
      },
      limit: '5mb'
    })
  )

  server.all('*', (req, res) => {
    return handle(req, res)
  })
  server.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})
