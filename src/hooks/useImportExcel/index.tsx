import React, { useEffect } from 'react'
import XLSX from 'xlsx'

function useImportExcel() {
    const [data, setData]= React.useState([]);
    const [file, setFile]= React.useState<File>(null);

    const hanldeImportFile=React.useCallback(async ()=>{
        const data= await file.arrayBuffer();
        const workbook= XLSX.read(data);
        const worksheet= workbook.Sheets[workbook.SheetNames[0]];
        const jsonData= XLSX.utils.sheet_to_json(worksheet);
        setData(jsonData);
    },[file])

    useEffect(()=>{
        if(file){
            hanldeImportFile()
        }
    },[hanldeImportFile])

  return{
    data,
    setFile,
    setData
  }
}

export default useImportExcel