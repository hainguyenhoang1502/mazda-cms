import { AsyncThunk } from '@reduxjs/toolkit';
import { useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import { IParamFilter } from 'src/api/type'
import { AppDispatch } from 'src/store'

interface IUseFilterProps {
    fetchDataGetList: AsyncThunk<
        any, // return typeof action
        IParamFilter, // typeof param
        {}
    >;
}

const useFilter = ({ fetchDataGetList }: IUseFilterProps) => {
    const filterRef = useRef<boolean>(false)
    const dispatch = useDispatch<AppDispatch>()

    const [valuesFilter, setValuesFilter] = useState<IParamFilter>({
        keyword: '',
        pageIndex: 1,
        pageSize: 10,
        status: null
    })

    const handleChangeInput = (value: string, field: string) => {
        const body = { ...valuesFilter }
        body[field] = value
        setValuesFilter(body)
    }


    const handleFilter = (initKeyword: string, initStatus: string) => {

        if (valuesFilter.keyword !== initKeyword || valuesFilter.status !== initStatus) {
            dispatch(fetchDataGetList(valuesFilter))
            filterRef.current = true
        }
    }

    const handleClearFilter = () => {
        const param = {
            keyword: '',
            pageIndex: 1,
            pageSize: 10,
            status: null
        }
        if (filterRef.current) {
            setValuesFilter(param)
            dispatch(fetchDataGetList(param))
            filterRef.current = false
        }

    }

    return (
        {
            valuesFilter,
            handleChangeInput,
            handleFilter,
            handleClearFilter
        }
    )
}

export default useFilter