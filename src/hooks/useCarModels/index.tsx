import React, { useEffect } from 'react'
import { locationsApi } from 'src/api-client'

export const carModelsDUmmy = [
    {
        id: 1,
        attributes: {
            car_id: "1001",
            car_name: "MAZDA-CX5",
            createdAt: "2023-05-08T07:47:34.439Z",
            updatedAt: "2023-05-08T07:47:35.463Z",
            publishedAt: "2023-05-08T07:47:35.459Z"
        }
    },
    {
        id: 2,
        attributes: {
            car_id: "1002",
            car_name: "MAZDA-CX8",
            createdAt: "2023-05-08T07:47:50.533Z",
            updatedAt: "2023-05-08T07:47:53.636Z",
            publishedAt: "2023-05-08T07:47:53.633Z"
        }
    },
    {
        id: 3,
        attributes: {
            car_id: "1003",
            car_name: "MAZDA-CX2",
            createdAt: "2023-05-08T07:48:04.659Z",
            updatedAt: "2023-05-08T07:48:05.864Z",
            publishedAt: "2023-05-08T07:48:05.861Z"
        }
    },
    {
        id: 4,
        attributes: {
            car_id: "1004",
            car_name: "MAZDA-CX10",
            createdAt: "2023-05-08T07:48:20.415Z",
            updatedAt: "2023-05-08T07:48:21.195Z",
            publishedAt: "2023-05-08T07:48:21.192Z"
        }
    },
    {
        id: 5,
        attributes: {
            car_id: "MAZDA-CX8",
            car_name: "MAZDA-CX8",
            createdAt: "2023-05-08T07:47:50.533Z",
            updatedAt: "2023-05-08T07:47:53.636Z",
            publishedAt: "2023-05-08T07:47:53.633Z"
        }
    },
]

export default function useCarmodels() {
    
const [carModels, setCarModels] = React.useState([]);

const getCarModels = React.useCallback(async () => {
    const res = await locationsApi.getListProvinces()
    if (res && res.data && res.data.length) {
      const newCarModel = carModelsDUmmy.map((item) => {
          return {
              value: item.attributes.car_id,
              label: item.attributes.car_name
          }
      })
      setCarModels(newCarModel)
    }
  },[])

  useEffect(() => {
    getCarModels()
  }, [getCarModels])
  
return {
    carModels
  }
}