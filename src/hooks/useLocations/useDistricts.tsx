import axiosClient from 'src/api-client/axios-client'
import useSWR from 'swr'

type Props = {
    id:string|string[]
}
const fetcher=url=> axiosClient.get(url)
export default function useDistricts({id}: Props) {
    const {data, error,isLoading }=useSWR(`/ref-data/api/locations/get-district-by-province-id?provinceId=${id}`,fetcher)
  
return {
    data, error, isLoading
  }
}