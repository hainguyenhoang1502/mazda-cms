import React from 'react'
import { useEffect, useState } from 'react'
import { locationsApi } from 'src/api-client'

export default function useProvinces() {
  const [provinces, setProvinces] = useState([])

  const getProvinces = React.useCallback(async () => {
    const res = await locationsApi.getListProvinces()
    if (res && res.data && res.data.length) {
      const provincesData = res.data.map((item) => {
        
        return {
          label: item.locationName,
          value: item.code
        }
      })
      setProvinces(provincesData)
    }
  },[])

  useEffect(() => {
    
    getProvinces()
  }, [getProvinces])
  
return {
    provinces
  }
}