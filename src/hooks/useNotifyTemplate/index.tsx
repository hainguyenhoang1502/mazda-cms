import React from 'react';
import { useEffect, useState } from 'react'
import { notificationTemplateApi } from 'src/api-client/notification-template'

export default function useNotifyTemplate() {
  const [notifyTemplate, setNotifyTemplate] = useState<any>([])
  const [notifyTemplateContent, setNotifyTemplateContent]= useState<any>([])
  const [loading, setLoading]=useState(false);

  const getNotifyTemplate = React.useCallback(async () => {
    setLoading(true);
  const res = await notificationTemplateApi.getListTemplate({keyword:'', pageIndex:1, pageSize:9999, status:'ACTIVE'})
  if (res && res.data && res.data.result) {
    const notifyTemplateData = res.data.result.map((item) => {
      
      return {
        label: item.name,
        value: item.id
      }
    })
    const notifyTemplateContent=res.data.result.map(item=>{
        return {
            value:item.id,
            content: item.content
        }
    })
    setNotifyTemplate(notifyTemplateData)
    setNotifyTemplateContent(notifyTemplateContent)
    setLoading(false);
  }
},[])

  useEffect(() => {
    
    getNotifyTemplate()
  }, [getNotifyTemplate])
  
return {
    notifyTemplate,
    notifyTemplateContent,
    loading
  }
}