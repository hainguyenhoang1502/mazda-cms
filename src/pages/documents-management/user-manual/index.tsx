
// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Components Imports
import ModalNotify from 'src/views/component/modalNotify'
import ModalConfirm from 'src/views/component/modalConfirm'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { ListDataTable, TableHeader } from 'src/views/pages/user-manual/list'

// ** Config Imports
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Redux Store Imports
import { AppDispatch, RootState } from 'src/store'
import { useDispatch, useSelector } from 'react-redux'
import { setValueBackrop } from 'src/store/utils/loadingBackdrop'
import { fetchDataGetUserManual } from 'src/store/apps/user-manual'

// ** Api Imports
import { ApiGetDeleteUserManualCMS } from 'src/api/document'


const UserManualPage = () => {
  // ** States
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.userManual)

  useEffect(() => {
    if ((store && !store.data) || (store && store.data.length === 0) || (store && store.isNeedReloadData)) {
      dispatch(fetchDataGetUserManual(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const getDeleteData = async (id: string) => {
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    dispatch(setValueBackrop(true))
    const res = await ApiGetDeleteUserManualCMS(id)
    dispatch(setValueBackrop(false))
    if (res.code == 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Xóa thành công",
      })
      dispatch(fetchDataGetUserManual(store.param))
    } else {
      setModalNotify({
        isOpen: true,
        title: "Xóa thất bại",
        isError: true,
        message: res.message
      })
    }
  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Danh sách hướng dẫn sử dụng' />}
        />
      </Grid>
      <Grid item xs={12}>
        <Card>
          <TableHeader />
          <ListDataTable getDeleteData={getDeleteData} setModalConfirm={setModalConfirm} />
        </Card>
      </Grid>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </Grid>
  )
}

export default UserManualPage
