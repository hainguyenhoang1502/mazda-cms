// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store
import { RootState } from 'src/store'
import { ApiGetDetailUserManualCMS } from 'src/api/document'
import { CreateUserManualMain } from 'src/views/pages/user-manual/create'
import { IResponeDetailUserManual } from 'src/api/document/type'

// ** Interface

interface EditUserManualProps {
  data: IResponeDetailUserManual["data"]
  id: number
}
interface EditUserManualContextProps {
  data: IResponeDetailUserManual["data"]
  isEdit: boolean,
  id: number,
  handleGetDetail: () => void
}
export const EditUserManualContext = createContext<EditUserManualContextProps>(null)


const EditUserManual = (props: EditUserManualProps) => {
  const store = useSelector((state: RootState) => state.profile)
  const [data, setData] = useState<IResponeDetailUserManual["data"]>(null)


  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission("")
      if (!permission) {
        router.replace('/401')
      }
    }
  }, [store.grantedAuthorities])

  const handleGetDetail = async () => {
    const res = await ApiGetDetailUserManualCMS(props.id)
    if (res && res.code == 200 && res.result) {
      setData(res.data)
    } else {
      window.location.reload()
    }
  }
  
  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Chỉnh Sửa hướng dẫn sử dụng' />} />
      </Grid>
      <EditUserManualContext.Provider
        value={{
          data: data || props.data,
          isEdit: true,
          id: props.id,
          handleGetDetail: handleGetDetail
        }}>
        <CreateUserManualMain />
      </EditUserManualContext.Provider>
    </Grid>
  )
}

export default EditUserManual

export const getServerSideProps = async ctx => {
  const data = await ApiGetDetailUserManualCMS(ctx.query.slug, ctx.req)
  if (data && data.code == 200 && data.result) {
    return {
      props: {
        data: data.data,
        id: ctx.query.slug
      }
    }
  } else {
    return {
      redirect: {
        destination: '/404',
        permanent: false
      }
    }
  }
}
