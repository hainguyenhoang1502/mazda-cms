// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store
import { RootState } from 'src/store'
import { ApiGetDetailPointUserManualCMS } from 'src/api/document'
import { CreateUserManualLayout } from 'src/views/pages/user-manual/create'
import { IResponeDetailPointUserManual } from 'src/api/document/type'

// ** Interface

interface EditPointUserManualProps {
  data: IResponeDetailPointUserManual["data"]
  id: number
}
interface EditPointUserManualContextProps {
  data: IResponeDetailPointUserManual["data"]
  isEdit: boolean,
  id: number,
  handleGetDetail: () => void
}
export const EditPointUserManualContext = createContext<EditPointUserManualContextProps>(null)
const EditPointUserManual = (props: EditPointUserManualProps) => {

  const store = useSelector((state: RootState) => state.profile)
  const storeUserManual = useSelector((state: RootState) => state.userManual)
  const [data, setData] = useState<IResponeDetailPointUserManual["data"]>(null)

  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission("")
      if (!permission) {
        router.replace('/401')
      }
    }
  }, [store.grantedAuthorities])

  useEffect(() => {
    if (!storeUserManual || !storeUserManual.id) {
      const fragment = window.location.hash.substring(1);
      router.push(`/documents-management/user-manual/edit/${fragment}`)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  const handleGetDetail = async () => {
    const res = await ApiGetDetailPointUserManualCMS(props.id)
    if (res && res.code == 200 && res.result) {
      setData(res.data)
    } else {
      const fragment = window.location.hash.substring(1);
      router.push(`/documents-management/user-manual/edit/${fragment}`)
    }
  }
  
  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Chỉnh Sửa bố cục hiển thị' />} />
      </Grid>
      <EditPointUserManualContext.Provider
        value={{
          data: data || props.data,
          isEdit: true,
          id: props.id,
          handleGetDetail: handleGetDetail
        }}>
        <CreateUserManualLayout />
      </EditPointUserManualContext.Provider>
    </Grid>
  )
}

export default EditPointUserManual

export const getServerSideProps = async ctx => {
  const data = await ApiGetDetailPointUserManualCMS(ctx.query.slug, ctx.req)
  if (data && data.code == 200 && data.result) {
    return {
      props: {
        data: data.data,
        id: ctx.query.slug
      }
    }
  } else {
    return {
      redirect: {
        destination: '/404',
        permanent: false
      }
    }
  }
}
