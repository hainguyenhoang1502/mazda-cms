
// ** MUI Imports
import Grid from '@mui/material/Grid'
import router from 'next/router';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header';
import { RootState } from 'src/store';
import { CreateUserManualLayout } from 'src/views/pages/user-manual/create/';

function CreateUserManualLayoutPage() {

  // ** Store
  const store = useSelector((state: RootState) => state.userManual)

  useEffect(() => {
    if (!store || !store.id) {
      const fragment = window.location.hash.substring(1);
      router.push(`/documents-management/user-manual/edit/${fragment}`)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Thêm mới bố cục hiển thị' />}
        />
      </Grid>
      <Grid item xs={12}>
        <CreateUserManualLayout />
      </Grid>
    </Grid>
  );
}

export default CreateUserManualLayoutPage;