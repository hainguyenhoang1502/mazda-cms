import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import FormCreateSignalLight from 'src/components/document-management/signal-lights/Form/create'
import SignalLightsCreateProvider from 'src/context/document-management/signal-lights/FormContext/create'


export default function CreateSignalLights() {
    return (
        <ModalsProvider>
            <SignalLightsCreateProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Thêm Đèn cảnh báo và chỉ báo' />}
                />
                <FormCreateSignalLight
                />
            </SignalLightsCreateProvider>

        </ModalsProvider>
    )
}