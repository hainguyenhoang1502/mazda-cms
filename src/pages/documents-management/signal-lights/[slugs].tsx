import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import SignalLightUpdateProvider from 'src/context/document-management/signal-lights/FormContext/update'
import FormUpdateSignalLight from 'src/components/document-management/signal-lights/Form/update'


export default function CreateNotify() {

    return (

            <>
                <ModalsProvider>
                    <SignalLightUpdateProvider>
                        <PageHeader
                            title={<PageHeaderTitle title='Thêm Đèn cảnh báo và chỉ báo' />}
                        />
                        <FormUpdateSignalLight/>
                    </SignalLightUpdateProvider>

                </ModalsProvider>
            </>
    )
}
