import React from 'react'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import TableSignalLights from 'src/components/document-management/signal-lights/Table'
import ModalProvider from 'src/context/ModalContext'
import SignalLightsListProvider from 'src/context/document-management/signal-lights/TableContext'

type Props = {}

export default function SignalLinePage({ }: Props) {
    return (
        <ModalProvider>
            <SignalLightsListProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Danh sách đèn cảnh báo' />}
                />
                <TableSignalLights />
            </SignalLightsListProvider>
        </ModalProvider>
    )
}