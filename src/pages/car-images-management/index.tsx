import React from 'react'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import TableCarImage from 'src/components/car-images-management/Table'
import ModalProvider from 'src/context/ModalContext'
import CarImageListProvider from 'src/context/car-images-management/TableContext'


export default function CarImagePage() {
    return (
        <ModalProvider>
            <CarImageListProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Danh sách hình ảnh xe' />}
                />
                <TableCarImage />
            </CarImageListProvider>
        </ModalProvider>
    )
}