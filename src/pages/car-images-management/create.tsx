import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import FormCreateProvider from 'src/context/car-images-management/FormContext/create'
import FormCreate from 'src/components/car-images-management/Form/create'
import CarImageListProvider from 'src/context/car-images-management/TableContext'


export default function CreateCarImage() {

    return (

        <>
            <ModalsProvider>
                <CarImageListProvider>
                    <FormCreateProvider>
                        <PageHeader
                            title={<PageHeaderTitle title='Thêm hình ảnh xe' />}
                        />
                        <FormCreate />
                    </FormCreateProvider>
                </CarImageListProvider>

            </ModalsProvider>
        </>
    )
}
