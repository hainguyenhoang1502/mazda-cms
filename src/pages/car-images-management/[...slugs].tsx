import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import CarImageListProvider from 'src/context/car-images-management/TableContext'
import FormUpdateProvider from 'src/context/car-images-management/FormContext/update'
import FormUpdate from 'src/components/car-images-management/Form/update'


export default function CreateAppointment() {

    return (
        <>
            <ModalsProvider>
                <CarImageListProvider>
                    <FormUpdateProvider>
                        <PageHeader
                            title={<PageHeaderTitle title='Chỉnh sửa hình ảnh xe' />}
                        />
                        <FormUpdate />
                    </FormUpdateProvider>
                </CarImageListProvider>

            </ModalsProvider>
        </>
    )
}
