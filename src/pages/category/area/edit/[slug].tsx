
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'


// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store
import { RootState } from 'src/store'
import { ApiEditAreaById } from 'src/api/refData'
import { CreateArea } from 'src/views/pages/category/area/create'
import { IResponseAreaEdit } from 'src/api/refData/type'

// ** Interface

interface EditAreaProps {
  dataArea: IResponseAreaEdit['data'] // edit
}
interface EditAreaContextProps {
  data: IResponseAreaEdit['data'];
  isEdit: boolean
}

export const EditAreaContext = createContext<EditAreaContextProps>(null);

const EditAreaPage = (props: EditAreaProps) => {
  const store = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission('')
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh Sửa Khu Vực' />}
        />
      </Grid>
      <EditAreaContext.Provider value={{ data: props.dataArea, isEdit: true }}>
        <CreateArea />
      </EditAreaContext.Provider>
    </Grid >
  )
}



export default EditAreaPage

export const getServerSideProps = async (ctx) => {
  const [dataArea] = await Promise.all([ApiEditAreaById(ctx.query.slug, ctx.req)])
  if (dataArea && dataArea.code == 200 && dataArea.result) {

    return {
      props: {
        dataArea: dataArea.data
      }
    }
  }
  else {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}