// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Next Imports
import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { ListDataTable, TableHeader } from 'src/views/pages/category/area/list'
import ModalConfirm from 'src/views/component/modalConfirm'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'
import { ModalConfirmState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM } from 'src/configs/initValueConfig'

// ** Store Redux Imports
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetListArea } from 'src/store/apps/category/area'

// ** Util Imports
import { checkUserPermission } from 'src/@core/utils/permission'


const CategoryProvincePage = () => {
  // const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.area)
  const storeProfile = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (storeProfile.grantedAuthorities && storeProfile.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.LOCATION_LIST_VIEW)
      if (!permission) {
        router.replace("/401")
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [storeProfile.grantedAuthorities])

  useEffect(() => {
    if ((store && !store.data) || (store && store.data.length === 0) || (store && store.isNeedReloadData)) {
      dispatch(fetchDataGetListArea(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Khu vực' />} />
      </Grid>

      <Grid item xs={12}>
        <Card>
          <TableHeader />
          <ListDataTable />
        </Card>
      </Grid>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />

    </Grid>
  )
}

export default CategoryProvincePage
