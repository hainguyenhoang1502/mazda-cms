// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
// import { ApiDetailUserCms } from 'src/api/authen'
import { checkUserPermission } from 'src/@core/utils/permission'
import { permissonConfig } from 'src/configs/roleConfig'

// ** Store
import { RootState } from 'src/store'

import { CreateProvinceMain } from 'src/views/pages/category/province/create'

// ** Interface

interface EditDistrictProps {
  dataUser: any
}
interface EditDistrictContextProps {
  dataUser: any
  isEdit: boolean
}

export const EditDistrictContext = createContext<EditDistrictContextProps>(null)

const EditProvince = (props: EditDistrictProps) => {
  const store = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.USER_EDIT)
      if (!permission) {
        router.replace('/401')
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Chỉnh Sửa Quận / Huyện' />} />
      </Grid>
      <EditDistrictContext.Provider value={{ dataUser: props.dataUser, isEdit: true }}>
        <CreateProvinceMain />
      </EditDistrictContext.Provider>
    </Grid>
  )
}

export default EditProvince

// export const getServerSideProps = async ctx => {
//   const dataUser = await ApiDetailUserCms(ctx.query.slug, ctx.req)
//   if (dataUser && dataUser.code == 200 && dataUser.result) {
//     return {
//       props: {
//         dataUser: dataUser.data
//       }
//     }
//   } else {
//     return {
//       redirect: {
//         destination: '/404',
//         permanent: false
//       }
//     }
//   }
// }
