
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** Next Import
import router from 'next/router'

// ** React Imports
import { useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { CreateDealerMain } from 'src/views/pages/category/company/dealer/create'

// ** Util Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store Imports
import { RootState } from 'src/store'

const CreateDealerPage = () => {

  const store = useSelector((state: RootState) => state.profile)
  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission("")
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Thêm điểm bán hàng ' />}
        />
      </Grid>
      <CreateDealerMain />
    </Grid>
  )
}

export default CreateDealerPage
