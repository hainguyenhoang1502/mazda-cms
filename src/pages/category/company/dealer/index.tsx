
//** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Import
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import SidebarLeftCompany from 'src/views/pages/category/company/SideBarLeft'
import { TableHeader, ListDataTable } from 'src/views/pages/category/company/dealer/list'
import ModalConfirm from 'src/views/component/modalConfirm'

// ** Store Imports
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetListDealers } from 'src/store/apps/category/company-dealer/dealer'

// ** Config Imports
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'

// ** Api Imports
import { ApiGetDeleteDealerCms } from 'src/api/organization'

const CategoryDealerPage = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.dealer)

  // ** States
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  useEffect(() => {
    if ((store && !store.data) || (store && store.data.length === 0) || (store && store.isNeedReloadData)) {
      dispatch(fetchDataGetListDealers(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const getDelete = async (id: string) => {
    const res = await ApiGetDeleteDealerCms(id)
    if (res.code == 200 && res.result) {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa công ty thành công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(fetchDataGetListDealers(store.param))
    } else {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa công ty thất bại",
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }

  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title="Điểm bán hàng" />}
        />
      </Grid>
      <Grid item xs={12} height="auto">
        <Grid container spacing={6} >
          <Grid item xs={2} height="auto">
            <Card sx={{ height: "100%" }}>
              <SidebarLeftCompany label={"dealer"} />
            </Card>
          </Grid>
          <Grid item xs={10}>
            <Card>
              <TableHeader />
              <ListDataTable
                getDelete={getDelete}
                setModalConfirm={setModalConfirm}
              />
              <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
            </Card>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default CategoryDealerPage