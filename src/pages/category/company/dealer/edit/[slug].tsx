// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store
import { RootState } from 'src/store'
import { CreateDealerMain } from 'src/views/pages/category/company/dealer/create'
import { IResponeDetailDealer } from 'src/api/organization/type'
import { ApiGetDetailDealerCms } from 'src/api/organization'

// ** Interface
interface EditDealerProps {
  datadealer: IResponeDetailDealer["data"]
}
interface EditDealerContextProps {
  data: IResponeDetailDealer["data"]
  isEdit: boolean
}

export const EditDealerContext = createContext<EditDealerContextProps>(null);

const EditDealerPage = (props: EditDealerProps) => {
  const store = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission('')
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh sửa điểm bán hàng' />}
        />
      </Grid>
      <EditDealerContext.Provider value={{ data: props.datadealer, isEdit: true }}>
        <CreateDealerMain />
      </EditDealerContext.Provider>
    </Grid >
  )
}



export default EditDealerPage

export const getServerSideProps = async (ctx) => {

  const datadealer = await ApiGetDetailDealerCms(ctx.query.slug, ctx.req)
  if (datadealer && datadealer.code == 200 && datadealer.result) {

    return {
      props: {
        datadealer: datadealer.data
      }
    }
  }
  else {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}
