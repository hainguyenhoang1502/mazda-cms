
//** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Import
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import SidebarLeftCompany from 'src/views/pages/category/company/SideBarLeft'
import { TableHeader, ListDataTable } from 'src/views/pages/category/company/company-agency/list'
import ModalConfirm from 'src/views/component/modalConfirm'

// ** Store Imports
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetListCompany } from 'src/store/apps/category/company-dealer/company'

// ** Config Imports
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'

// ** Api Imports
import { ApiGetDeleteCompanyCms } from 'src/api/organization'

const CategoryCompanyPage = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.company)

  // ** States
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  useEffect(() => {
    if ((store && !store.data) || (store && store.data.length === 0) || (store && store.isNeedReloadData)) {
      dispatch(fetchDataGetListCompany(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const getDelete = async (id: string) => {
    const res = await ApiGetDeleteCompanyCms(id)
    if (res.code == 200 && res.result) {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa công ty thành công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(fetchDataGetListCompany(store.param))
    } else {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa công ty thất bại",
        isError:true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }

  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title="Công ty / Đại lý" />}
        />
      </Grid>
      <Grid item xs={12} height="auto">
        <Grid container spacing={6} >
          <Grid item xs={2} height="auto">
            <Card sx={{ height: "100%" }}>
              <SidebarLeftCompany label={"company"} />
            </Card>
          </Grid>
          <Grid item xs={10}>
            <Card>
              <TableHeader />
              <ListDataTable
                getDelete={getDelete}
                setModalConfirm={setModalConfirm}
              />
            </Card>
          </Grid>
        </Grid>
      </Grid>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />

    </Grid>
  )
}

export default CategoryCompanyPage