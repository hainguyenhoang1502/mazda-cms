
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'


// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store
import { RootState } from 'src/store'
import { CreateCompanyAgencyMain } from 'src/views/pages/category/company/company-agency/create'
import { IResponeDetailComanyAgency } from 'src/api/organization/type'
import { ApGetCompanyDetail } from 'src/api/organization'

// ** Interface

interface EditCompanyProps {
  dataCompany: IResponeDetailComanyAgency["data"]
}
interface EditCompanyContextProps {
  data: IResponeDetailComanyAgency["data"]
  isEdit: boolean
}

export const EditCompanyContext = createContext<EditCompanyContextProps>(null);

const EditCompanyPage = (props: EditCompanyProps) => {
  const store = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission('')
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh Sửa công ty / đại lý ' />}
        />
      </Grid>
      <EditCompanyContext.Provider value={{ data: props.dataCompany, isEdit: true }}>
        <CreateCompanyAgencyMain />
      </EditCompanyContext.Provider>
    </Grid >
  )
}



export default EditCompanyPage

export const getServerSideProps = async (ctx) => {

  const dataCompany = await ApGetCompanyDetail(ctx.query.slug, ctx.req)
  if (dataCompany && dataCompany.code == 200 && dataCompany.result) {

    return {
      props: {
        dataCompany: dataCompany.data
      }
    }
  }
  else {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}

