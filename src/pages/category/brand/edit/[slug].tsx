
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store
import { RootState } from 'src/store'
import { ApiOrganizationBrandDetail } from 'src/api/organization'
import { CreateBrand } from 'src/views/pages/category/brand/create'

// ** Interface

interface EditBrandProps {
  dataBrand: any // edit
}
interface EditBrandContextProps {
  data: any
  isEdit: boolean
}

export const EditBrandContext = createContext<EditBrandContextProps>(null);

const EditBrandPage = (props: EditBrandProps) => {
  const store = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.BRAND_EDIT)
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh Sửa Thương Hiệu' />}
        />
      </Grid>
      <EditBrandContext.Provider value={{ data: props.dataBrand, isEdit: true }}>
        <CreateBrand />
      </EditBrandContext.Provider>
    </Grid >
  )
}



export default EditBrandPage

export const getServerSideProps = async (ctx) => {

  const [dataBrand] = await Promise.all([ApiOrganizationBrandDetail(ctx.query.slug, ctx.req)])
  if (dataBrand && dataBrand.code == 200 && dataBrand.result) {

    return {
      props: {
        dataBrand: dataBrand.data
      }
    }
  }
  else {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}