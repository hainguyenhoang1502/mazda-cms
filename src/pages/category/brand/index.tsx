// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Next Imports
import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { ListDataTable, TableHeader } from 'src/views/pages/category/brand/list'
import ModalConfirm from 'src/views/component/modalConfirm'

// ** Api Imports
import { ApiOrganizationBrandDelete } from 'src/api/organization'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'

// ** Store Redux Imports
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetListBrand } from 'src/store/apps/category/brand'

// ** Util Imports
import { checkUserPermission } from 'src/@core/utils/permission'

const CategoryProvincePage = () => {
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux Store
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.brand)
  const storeProfile = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (storeProfile.grantedAuthorities && storeProfile.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.BRAND_LIST_VIEW)
      if (!permission) {
        router.replace("/401")
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [storeProfile.grantedAuthorities])

  useEffect(() => {
    if ((store && !store.data) || (store && store.data.length === 0) || (store && store.isNeedReloadData)) {
      dispatch(fetchDataGetListBrand(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  const getDeleteBrand = async (code: string) => {

    const res = await ApiOrganizationBrandDelete(code)
    if (res.code == 200 && res.result) {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Thương Hiệu Thành Công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(fetchDataGetListBrand(store.param))

    } else {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Thương Hiệu Thất Bại",
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }

  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Thương hiệu' />} />
      </Grid>

      <Grid item xs={12}>
        <Card>
          <TableHeader />
          <ListDataTable setModalConfirm={setModalConfirm} getDeleteBrand={getDeleteBrand} />
        </Card>
      </Grid>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Grid>
  )
}

export default CategoryProvincePage
