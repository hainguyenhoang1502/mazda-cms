
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'


// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store
import { RootState } from 'src/store'
import { CreateRegion } from 'src/views/pages/category/region/create'
import { ApiEditRegionById } from 'src/api/refData'
import { IResponseRegionEdit } from 'src/api/refData/type'

// ** Interface

interface EditRegionProps {
  dataRegion: IResponseRegionEdit["data"] // edit
}

interface EditRegionContextProps {
  data: IResponseRegionEdit["data"]
  isEdit: boolean
}
export const EditRegionContext = createContext<EditRegionContextProps>(null);

const EditRegion = (props: EditRegionProps) => {
  const store = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission('')
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh Sửa Vùng Miền' />}
        />
      </Grid>
      <EditRegionContext.Provider value={{ data: props.dataRegion, isEdit: true }}>
        <CreateRegion />
      </EditRegionContext.Provider>
    </Grid >
  )
}



export default EditRegion

export const getServerSideProps = async (ctx) => {
  const [dataRegion] = await Promise.all([ApiEditRegionById(ctx.query.slug, ctx.req)])
  if (dataRegion && dataRegion.code == 200 && dataRegion.result) {

    return {
      props: {
        dataRegion: dataRegion.data
      }
    }
  }
  else {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}