// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'
import { permissonConfig } from 'src/configs/roleConfig'

// ** Store
import { RootState } from 'src/store'

import { CreateProvinceMain } from 'src/views/pages/category/province/create'
import { ApiGetProvinceById } from 'src/api/refData'
import { IResponseProvinceDetailByID } from 'src/api/refData/type'

// ** Interface

interface EditProvinceProps {
  dataProvince: IResponseProvinceDetailByID['data'];
}
interface EditProvinceContextProps {
  dataProvince: IResponseProvinceDetailByID['data'];
  isEdit: boolean
}

export const EditProvinceContext = createContext<EditProvinceContextProps>(null)

const EditProvince = (props: EditProvinceProps) => {
  const store = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.USER_EDIT)
      if (!permission) {
        router.replace('/401')
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Chỉnh Sửa Tỉnh Thành' />} />
      </Grid>
      <EditProvinceContext.Provider value={{ dataProvince: props.dataProvince, isEdit: true }}>
        <CreateProvinceMain />
      </EditProvinceContext.Provider>
    </Grid>
  )
}

export default EditProvince

export const getServerSideProps = async ctx => {
  const dataProvince = await ApiGetProvinceById(ctx.query.slug, ctx.req)
  if (dataProvince && dataProvince.code === 200 && dataProvince.result) {
    return {
      props: {
        dataProvince: dataProvince.data
      }
    }
  } else {
    return {
      redirect: {
        destination: '/404',
        permanent: false
      }
    }
  }
}
