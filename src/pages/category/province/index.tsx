// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

import { ListDataTable, TableHeader } from 'src/views/pages/category/province/list'

import ModalConfirm from 'src/views/component/modalConfirm'

// ** Api Imports

// ** Config Imports
import { ModalConfirmState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM } from 'src/configs/initValueConfig'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetListProvince } from 'src/store/apps/category/province'

const CategoryProvincePage = () => {
  // const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.province)

  useEffect(() => {
    if ((store && !store.data) || (store && store.data.length === 0) || (store && store.isNeedReloadData)) {
      dispatch(fetchDataGetListProvince(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Quản Lý Tỉnh / Thành' />} />
      </Grid>

      <Grid item xs={12}>
        <Card>
          <TableHeader />
          <ListDataTable  />
        </Card>
      </Grid>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Grid>
  )
}

export default CategoryProvincePage
