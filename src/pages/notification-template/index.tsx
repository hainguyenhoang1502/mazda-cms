import React from 'react'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import Table from 'src/components/notification-template/Table'
import ModalProvider from 'src/context/ModalContext'
import NotificationTemplateListProvider from 'src/context/notification-template/Table'


export default function NotificaionTemplatePage() {
    return (
        <ModalProvider>

            <NotificationTemplateListProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Danh sách template' />}
                />
                <Table />
            </NotificationTemplateListProvider>
        </ModalProvider>
    )
}