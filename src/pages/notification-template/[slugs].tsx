import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import FormUpdateProvider from 'src/context/notification-template/Form/update'
import FormUpdate from 'src/components/notification-template/Form/update'


export default function CreateAppointment() {

    return (

        <>
            <ModalsProvider>
                    <FormUpdateProvider>
                        <PageHeader
                            title={<PageHeaderTitle title='Chỉnh sửa template' />}
                        />
                        <FormUpdate />
                    </FormUpdateProvider>
            </ModalsProvider>
        </>
    )
}
