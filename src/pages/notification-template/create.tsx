import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import FormCreate from 'src/components/notification-template/Form/create'
import FormCreateProvider from 'src/context/notification-template/Form/create'


export default function CreateNotificationTemplate() {

    return (

        <ModalsProvider>
            <FormCreateProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Thêm template' />}
                />
                <FormCreate />
            </FormCreateProvider>
        </ModalsProvider>
    )
}
