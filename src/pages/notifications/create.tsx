import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import FormCreateProvider from 'src/context/notifications/Form/create'
import CreateForm from 'src/components/notifications/Form/create'
import ListNotifyUserProvider from 'src/context/notifications/TableUser'
import NotificationProvider from 'src/context/notifications'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'


export default function CreateNotify() {

  return (
    <ModalsProvider>
      <NotificationProvider>
        <ListNotifyUserProvider>
          <FormCreateProvider>
            <PageHeader
              title={<PageHeaderTitle title='Thêm mới thông báo' />}
            />
            <CreateForm />
          </FormCreateProvider>
        </ListNotifyUserProvider>
      </NotificationProvider>
    </ModalsProvider>
  )
}


