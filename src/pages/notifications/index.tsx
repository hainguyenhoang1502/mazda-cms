import TableNotifications from 'src/components/notifications/Table'
import React from 'react'
import NotificationListProvider from 'src/context/notifications/Table'
import ModalProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

const NotificationPage = () => {

    return (
        <ModalProvider>

            <NotificationListProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Danh sách thông báo' />}
                />
                <TableNotifications />
            </NotificationListProvider>
        </ModalProvider>

    )
}

export default NotificationPage
