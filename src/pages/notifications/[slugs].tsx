/* eslint-disable */

import React from 'react'
import axiosClient from 'src/api-client/axios-client'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { checkIfArrayValid } from 'src/utils'
import { GetServerSidePropsContext } from 'next/types'
import FormUpdateProvider from 'src/context/notifications/Form/update'
import UpdateForm from 'src/components/notifications/Form/update'
import ListNotifyUserProvider from 'src/context/notifications/TableUser'
import ModalsProvider from 'src/context/ModalContext'
import NotificationProvider from 'src/context/notifications'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

export interface NoftificationDetailProps {
    notify_cate: string,
    notify_type: string,
    send_date: Date,
    notify_title: string,
    templateId: string,
    gender: any[],
    provinces: any[],
    carModel: any[],
    notify_template: string,
    editor: string,
    fileUpload: any,
    status: string
}
export default function NotificationDetail() {
    return (
        <ModalsProvider>
            <NotificationProvider>
                <ListNotifyUserProvider>
                    <FormUpdateProvider
                    >
                        <PageHeader
                            title={<PageHeaderTitle title='Chỉnh sửa thông báo' />}
                        />
                        <UpdateForm />
                    </FormUpdateProvider>
                </ListNotifyUserProvider>
            </NotificationProvider>
        </ModalsProvider >
    )
}
// export async function getServerSideProps(context: GetServerSidePropsContext) {
//     const slugs = context.params?.slugs
//     const notifyData = await axiosClient.post(`${process.env.WWW_API_SERVICE_HOST}/${process.env.SLUG_API_SERVICE_NOTIFICATIONS}/api/cms/notification-detail-by-id`, { id: slugs })
//     console.log(notifyData.data)

//     if (notifyData.data) {
//         const provinceValue = checkIfArrayValid(notifyData.data.sendTo?.province) ? notifyData.data.sendTo.province.split(',') : []
//         const carModel = checkIfArrayValid(notifyData.data.sendTo?.model) ? notifyData.data.sendTo.model.split(',') : []
//         const genderOption = checkIfArrayValid(notifyData.data.sendTo?.gender) ? notifyData.data.sendTo.gender.split(',') : []

//         return {
//             props: {
//                 notify_title: notifyData.data.title,
//                 notify_cate: notifyData.data.notificationTypeId,
//                 notify_type: notifyData.data.schedule ? 'SCHEDULE' : 'SENDNOW',
//                 editor: notifyData.data.content,
//                 send_date: notifyData.data.schedule,
//                 gender: genderOption,
//                 carModel: carModel,
//                 templateId: notifyData.data.templateId,
//                 provinces: provinceValue,
//                 fileUpload: notifyData.data.image,
//                 status: notifyData.data.status
//             }
//         }
//     } else {
//         return {
//             notFound: true
//         }
//     }

// }
/* eslint-enable */