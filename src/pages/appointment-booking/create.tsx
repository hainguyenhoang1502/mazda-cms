import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import FormCreateProvider from 'src/context/appointment/FormContext/create'
import FormCreate from 'src/components/appointment-booking/Form/create'


export default function CreateAppointment() {

    return (

            <>
                <ModalsProvider>
                    <FormCreateProvider>
                        <PageHeader
                            title={<PageHeaderTitle title='Thêm lịch hẹn' />}
                        />
                        <FormCreate/>
                    </FormCreateProvider>

                </ModalsProvider>
            </>
    )
}
