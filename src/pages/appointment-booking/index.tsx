// ** React Imports

// ** MUI Imports

// ** ThirdParty Components

// ** Custom Components

// ** Types Imports

// ** Utils Import
import React from 'react'
import ModalProvider from 'src/context/ModalContext'
import TableAppointments from 'src/components/appointment-booking/Table'
import AppointmentListProvider from 'src/context/appointment/TableContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'


const AppointmentPage = () => {

    return (
        <ModalProvider>

            <AppointmentListProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Danh sách lịch đặt hẹn' />}
                />
                <TableAppointments />
            </AppointmentListProvider>
        </ModalProvider>

    )
}

export default AppointmentPage
