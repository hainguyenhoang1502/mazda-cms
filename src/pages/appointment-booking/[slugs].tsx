import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import FormUpdateProvider from 'src/context/appointment/FormContext/update'
import FormUpdate from 'src/components/appointment-booking/Form/update'


export default function CreateAppointment() {

    return (

            <>
                <ModalsProvider>
                    <FormUpdateProvider>
                        <PageHeader
                            title={<PageHeaderTitle title='Chỉnh sửa lịch hẹn' />}
                        />
                        <FormUpdate/>
                    </FormUpdateProvider>

                </ModalsProvider>
            </>
    )
}
