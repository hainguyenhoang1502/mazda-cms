import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import FormCreate from 'src/components/survey/survey-management/create'
import CreateProvider from 'src/context/survey/survey-management/create'
import TableQuestionModalProvider from 'src/context/survey/survey-management/tableQuestionModal'
import TableQuestionProvider from 'src/context/survey/survey-management/tableQuestion'
import FormProvider from 'src/context/survey/survey-management/formCtx'


export default function CreateQuestions() {
    return (
        <ModalsProvider>
            <FormProvider>
                <CreateProvider>
                    <TableQuestionProvider>
                        <TableQuestionModalProvider>

                            <PageHeader
                                title={<PageHeaderTitle title='Tạo câu hỏi' />}
                            />
                            <FormCreate />
                        </TableQuestionModalProvider>
                    </TableQuestionProvider>
                </CreateProvider>
            </FormProvider>
        </ModalsProvider>
    )
}