import React from 'react'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import Table from 'src/components/survey/survey-management/Table'
import ModalProvider from 'src/context/ModalContext'
import ListProvider from 'src/context/survey/survey-management'


export default function SurveyQuestionPage() {
    return (
        <ModalProvider>
            <ListProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Danh sách khảo sát' />}
                />
                <Table />
            </ListProvider>
        </ModalProvider>
    )
}