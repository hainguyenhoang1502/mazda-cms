import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import FormUpdate from 'src/components/survey/survey-management/update'
import UpdateProvider from 'src/context/survey/survey-management/update'
import TableQuestionModalProvider from 'src/context/survey/survey-management/tableQuestionModal'
import TableQuestionProvider from 'src/context/survey/survey-management/tableQuestion'
import FormProvider from 'src/context/survey/survey-management/formCtx'


export default function UpdateQuestions() {
    return (
        <ModalsProvider>
            <FormProvider>
                <UpdateProvider>
                    <TableQuestionProvider>
                        <TableQuestionModalProvider>
                            <PageHeader
                                title={<PageHeaderTitle title='Chỉnh sửa câu hỏi' />}
                            />
                            <FormUpdate />
                        </TableQuestionModalProvider>
                    </TableQuestionProvider>
                </UpdateProvider>
            </FormProvider>
        </ModalsProvider>
    )
}