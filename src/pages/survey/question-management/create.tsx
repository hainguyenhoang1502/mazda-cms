import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import CreateProvider from 'src/context/survey/question-management/create'
import FormCreate from 'src/components/survey/question-management/create'


export default function CreateQuestions() {
    return (
        <ModalsProvider>
            <CreateProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Tạo câu hỏi' />}
                />
                <FormCreate
                />
            </CreateProvider>

        </ModalsProvider>
    )
}
