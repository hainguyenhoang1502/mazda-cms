/* eslint-disable */

import React from 'react'
import axiosClient from 'src/api-client/axios-client'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { GetServerSidePropsContext } from 'next/types'
import ModalsProvider from 'src/context/ModalContext'
import UpdateProvider, { SurveyQuestionDetailProps } from 'src/context/survey/question-management/update'
import FormUpdate from 'src/components/survey/question-management/update'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'


export default function QuestionSurveyDetail() {
    return (
        <ModalsProvider>
            <UpdateProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Chỉnh sửa câu hỏi' />}
                />
                <FormUpdate />
            </UpdateProvider>
        </ModalsProvider >
    )
}

/* eslint-enable */