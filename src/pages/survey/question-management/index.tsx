import React from 'react'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import Table from 'src/components/survey/question-management/Table'
import ModalProvider from 'src/context/ModalContext'
import ListProvider from 'src/context/survey/question-management'


export default function SurveyQuestionPage() {
    return (
        <ModalProvider>
            <ListProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Danh sách câu hỏi' />}
                />
                <Table />
                
            </ListProvider>
        </ModalProvider>
    )
}