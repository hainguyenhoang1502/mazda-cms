

import Cookies from "cookies";
import httpProxy from "http-proxy";
import { NextApiRequest, NextApiResponse } from "next/types";


export const config = {
    api: {
        bodyParser: false,
    },
}

const proxy = httpProxy.createProxyServer()
export default function handler(req: NextApiRequest, res: NextApiResponse<any>) {

    return new Promise((resolve) => {
        req.url = req.url.replace(`/api/${process.env.SLUG_API_MEDIA}`, '')
        const cookies= new Cookies(req, res)
        const accessToken=cookies.get('accessToken')
        if(accessToken){
            req.headers.Authorization=`${accessToken}`
        }

        proxy.once('proxyRes', ()=>resolve(true))

        proxy.web(req, res, {
            target: `${process.env.WWW_API_SERVICE_HOST}/${process.env.SLUG_API_SERVICE_REFDATA}/`,
            changeOrigin: true,
            selfHandleResponse: false
        })
    })
}