import httpProxy, { ProxyResCallback } from "http-proxy";
import { NextApiRequest, NextApiResponse } from "next/types";



export const config = {
    api: {
        bodyParser: false,
    },
}

const proxy = httpProxy.createProxyServer()
export default function handler(req: NextApiRequest, res: NextApiResponse<any>) {

    
    return new Promise((resolve) => {
        req.url = req.url.replace('/api/customers/', '')

        req.headers.cookie=''
        const handleLoginResponse: ProxyResCallback = (proxyRes, req, res) => {
            let body = ''
            proxyRes.on('data', chunk => {
                body += chunk
            })
            proxyRes.on('end', () => {
                try {
                    const {data} = JSON.parse(body)
                        ; (res as NextApiResponse).status(200).json({ data })
                    
                } catch (e) {
                    ; (res as NextApiResponse).status(500).json({ message: 'Login failed!' })
                }
                resolve(true)
            })
        }

        proxy.once('proxyRes', handleLoginResponse)

        proxy.web(req, res, {
            target: `https://kong-gateway.toponseek.com/customers/api/`,
            changeOrigin: true,
            selfHandleResponse: true
        })
    })
}