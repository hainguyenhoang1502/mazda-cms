import { NextApiRequest, NextApiResponse } from "next/types";
import nextHttpProxyMiddleware, { NextHttpProxyMiddlewareOptions } from 'next-http-proxy-middleware';
import Cookies from "cookies";

export default async function proxy(req: NextApiRequest, res: NextApiResponse<any>) {
  try {
    const cookies = new Cookies(req, res);
    const accessToken = cookies.get('accessToken');

    if (accessToken) {
      req.headers.Authorization = accessToken;
      const proxyRes = await nextHttpProxyMiddleware(req, res, {
        target: process.env.WWW_API_SERVICE_HOST,
        pathRewrite: {
          '^/api': '', // Remove the '/api' prefix from the request URL
        },
      } as NextHttpProxyMiddlewareOptions);

      if (proxyRes.statusCode === 401) {
        res.writeHead(302, { Location: '/401' });
        res.end();

        return; // Stop execution after sending the response
      }
      
    } else {
      res.writeHead(302, { Location: '/500' });
      res.end();

      return; // Stop execution after sending the response
    }
  } catch (error) {
    console.error("An error occurred:", error);
    res.writeHead(500, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ message: 'Internal Server Error' }));

    return; // Stop execution after sending the response


  }
}
