
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'


// ** Components Imports
import { CreateCustomerMain } from 'src/views/pages/customer-information/create'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { ApiDetailCustomerCms } from 'src/api/customer'
import { IResultDataDetailCustomer } from 'src/api/customer/type'
import { checkUserPermission } from 'src/@core/utils/permission'
import { permissonConfig } from 'src/configs/roleConfig'

// ** Store
import { RootState } from 'src/store'

// ** Interface

interface EditCustomerProps {
  dataCustomer: IResultDataDetailCustomer
}
interface EditCustomerContextProps {
  dataCustomer: IResultDataDetailCustomer
  isEdit: boolean
}
export const EditCustomerContext = createContext<EditCustomerContextProps>(null);
const EditCustomer = (props: EditCustomerProps) => {

  const store = useSelector((state: RootState) => state.profile)
  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.CUSTOMER_EDIT)
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh Sửa Thông Tin Khách Hàng' />}
        />
      </Grid>
      <EditCustomerContext.Provider value={{ dataCustomer: props.dataCustomer, isEdit: true }}>
        <CreateCustomerMain />
      </EditCustomerContext.Provider>
    </Grid >
  )
}

export default EditCustomer

export const getServerSideProps = async (ctx) => {
  const param = {
    id: ctx.query.slug,
  }
  const [dataCustomer] = await Promise.all([ApiDetailCustomerCms(param, ctx.req)])
  if (dataCustomer && dataCustomer.code == 200 && dataCustomer.result) {

    return {
      props: {
        dataCustomer: dataCustomer.data
      }
    }
  }
  else {

    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}