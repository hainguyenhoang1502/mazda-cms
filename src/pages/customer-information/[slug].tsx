
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports 
import { createContext, useEffect, useState } from 'react'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { CustomerInformationDetail } from 'src/views/pages/customer-information/view'

// ** Api Imports 
import { ApiDetailCustomerCms } from 'src/api/customer'
import { IResultDataDetailCustomer } from 'src/api/customer/type'
import { ApiListVehicleByCustomerCms } from 'src/api/vehicles'
import { IParamListVehicleByCustomer, IResultDataItemVehicleByCustomer } from 'src/api/vehicles/type'

// ** Config Imports
import { INIT_VALUE_PAGINATION } from 'src/configs/initValueConfig'
import { IPagination } from 'src/configs/typeOption'

// ** Interface Imports
interface DetailCustomerProps {
  dataDetail: IResultDataDetailCustomer,
  id: number | string
}
interface IDetailCustomerContext {
  dataDetail: IResultDataDetailCustomer
  listVehicle: Array<IResultDataItemVehicleByCustomer>
  isLoading: boolean
  paginationItem: IPagination
  paramFilter: IParamListVehicleByCustomer
  getListVehicleByCustomer: Function
  customerId: number | string
}
export const DetailCustomerContext = createContext<IDetailCustomerContext>(null);

export const getServerSideProps = async (ctx) => {
  const param = {
    id: ctx.query.slug,
  }
  const [dataCustomer] = await Promise.all([ApiDetailCustomerCms(param, ctx.req)])
  if (dataCustomer && dataCustomer.code == 200 && dataCustomer.result) {
    return {
      props: {
        dataDetail: dataCustomer.data,
        id: ctx.query.slug,
      }
    }
  }
  else {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}

const CustomerInformationDetailPage = (props: DetailCustomerProps) => {

  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [paramFilter, setParamFilter] = useState<IParamListVehicleByCustomer>({
    keyword: '',
    pageIndex: 1,
    pageSize: 10,
    customerId: null
  })
  const [paginationItem, setPaginationItem] = useState<IPagination>(INIT_VALUE_PAGINATION)
  const [listVehicle, setListVehicle] = useState<Array<IResultDataItemVehicleByCustomer>>([])

  useEffect(() => {
    if (props.id) {
      getListVehicleByCustomer({
        keyword: '',
        pageIndex: 1,
        pageSize: 10,
        customerId: props.id
      })
    }
  }, [props.id])

  const getListVehicleByCustomer = async param => {
    setIsLoading(true)
    setParamFilter(param)
    const res = await ApiListVehicleByCustomerCms(param)
    if (res.code === 200 && res.result && res.data) {
      setListVehicle(res.data.result)
      setPaginationItem({
        pageIndex: res.data.pageIndex,
        pageSize: res.data.pageSize,
        totalPages: res.data.totalPages,
        totalRecords: res.data.totalRecords
      })
    } else {
      setListVehicle([])
    }
    setIsLoading(false)
  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title=' Chi Tiết Khách Hàng' />}
        />
      </Grid>
      <DetailCustomerContext.Provider
        value={{
          customerId: props.id,
          dataDetail: props.dataDetail,
          listVehicle: listVehicle,
          paginationItem: paginationItem,
          getListVehicleByCustomer: getListVehicleByCustomer,
          isLoading: isLoading,
          paramFilter: paramFilter,
        }}
      >
        <CustomerInformationDetail
        />
      </DetailCustomerContext.Provider>
    </Grid>
  )
}

export default CustomerInformationDetailPage

