// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'
import { GridRowId } from '@mui/x-data-grid'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Next Import
import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { ListDataTable, TableHeader } from 'src/views/pages/customer-information/list'
import ModalConfirm from 'src/views/component/modalConfirm'

// ** Api Imports 
import { ApiDeleteCustomerCms } from 'src/api/customer'


// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'

// ** Store Redux Imports
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetCustomer } from 'src/store/apps/customer-information'

// ** Util Imports
import { checkUserPermission } from 'src/@core/utils/permission'

const CustomerInformationPage = () => {

  // ** States
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.customer)
  const storeProfile = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (storeProfile.grantedAuthorities && storeProfile.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.BRAND_LIST_VIEW)
      if (!permission) {
        router.replace("/401")
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [storeProfile.grantedAuthorities])

  useEffect(() => {
    if (store && (!store.data || store.data.length === 0)) {
      dispatch(fetchDataGetCustomer(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const [arrRowId, setArrRowId] = useState<Array<GridRowId>>([])

  const getDeleteCustomer = async (id: string) => {
    const param = {
      listCustomerId: id
    }
    const res = await ApiDeleteCustomerCms(param)
    if (res.code == 200 && res.result) {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Khách Hàng Thành Công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(fetchDataGetCustomer(store.param))
    } else {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Khách Hàng Thất bại",
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }

  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Quản Lý Thông Tin Khách Hàng' />}

        />
      </Grid>
      <Grid item xs={12}>
        <Card>
          <TableHeader
            setModalConfirm={setModalConfirm}
            arrRowId={arrRowId}
            getDeleteCustomer={getDeleteCustomer}

          />
          <ListDataTable
            setModalConfirm={setModalConfirm}
            getDeleteCustomer={getDeleteCustomer}
            setArrRowId={setArrRowId}
          />
        </Card>
      </Grid>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Grid>
  )
}

export default CustomerInformationPage
