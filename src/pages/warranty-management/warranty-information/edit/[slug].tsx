
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'


// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
// import { ApiDetailWarrantyCms } from 'src/api/Warranty'
// import { IResultDataDetailWarranty } from 'src/api/Warranty/type'
import { checkUserPermission } from 'src/@core/utils/permission'

// import { permissonConfig } from 'src/configs/roleConfig'

// ** Store
import { RootState } from 'src/store'
import { CreateWrranty } from 'src/views/pages/warranty-information/create'

// ** Interface

interface EditWarrantyProps {
  dataWarranty: any
}
interface EditWarrantyContextProps {
  dataWarranty: any
  isEdit: boolean
}

export const EditWarrantyContext = createContext<EditWarrantyContextProps>(null);
const EditWarranty = (props: EditWarrantyProps) => {

  const store = useSelector((state: RootState) => state.profile)
  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission('')
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh sửa thông tin bảo hành xe ' />}
        />
      </Grid>
      <EditWarrantyContext.Provider value={{ dataWarranty: props.dataWarranty, isEdit: true }}>
        <CreateWrranty/>
      </EditWarrantyContext.Provider>
    </Grid >
  )
}

export default EditWarranty

// export const getServerSideProps = async (ctx) => {
//   const param = {
//     id: ctx.query.slug,
//   }
//   const [dataWarranty] = await Promise.all([ApiDetailWarrantyCms(param, ctx.req)])
//   if (dataWarranty && dataWarranty.code == 200 && dataWarranty.result) {

//     return {
//       props: {
//         dataWarranty: dataWarranty.data
//       }
//     }
//   }
//   else {

//     return {
//       redirect: {
//         destination: '/404',
//         permanent: false,
//       },
//     }
//   }
// }