// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { ListDataTable, TableHeader } from 'src/views/pages/warranty-information/list'
import ModalConfirm from 'src/views/component/modalConfirm'

// ** Api Imports 

// import { ApiDeleteCustomerCms } from 'src/api/customer'

// ** Config Imports
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import ModalNotify from 'src/views/component/modalNotify'
import { fetchDataGetWarrantyInformation } from 'src/store/apps/warranty-information'
import ModalFile from 'src/components/Modal/ModalFile'


const WarrantyInformationPage = () => {

  // ** States
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.warrantyInformation)

  useEffect(() => {
    if (store && (!store.data || store.data.length === 0)) {
      dispatch(fetchDataGetWarrantyInformation(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  // const getDelete = async (id: string) => {
  const getDelete = async () => {

    setModalNotify({
      ...modalNotify,
      isOpen: true,
      title: "Bạn đã xóa thông tin bảo hành thành công",
    })
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)

    // const param = {
    //   listCustomerId: id
    // }
    // const res = await ApiDeleteCustomerCms(param)
    // if (res.code == 200 && res.result) {
    //   setModalNotify({
    //     ...modalNotify,
    //     isOpen: true,
    //     title: "Xóa Khách Hàng Thành Công",
    //   })
    //   setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    //   dispatch(fetchDataGetCustomer(store.param))
    // } else {
    //   setModalNotify({
    //     ...modalNotify,
    //     isOpen: true,
    //     title: "Xóa Khách Hàng Thất bại",
    //   })
    //   setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    // }

  }

  return (
    <ModalFile.Group>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <PageHeader
            title={<PageHeaderTitle title='Danh sách thông tin xe bảo hành' />}

          />
        </Grid>
        <Grid item xs={12}>
          <Card>
            <TableHeader />
            <ListDataTable
              setModalConfirm={setModalConfirm}
              getDelete={getDelete}
            />
          </Card>
        </Grid>
        <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
        <ModalNotify {...modalNotify} toggle={setModalNotify} />
      </Grid>
    </ModalFile.Group>
  )
}

export default WarrantyInformationPage
