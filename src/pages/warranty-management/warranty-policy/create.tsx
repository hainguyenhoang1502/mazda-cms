import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import WarrantyCreateProvider from 'src/context/warranty-management/waranty-policy/FormContext/create'
import CreateFormWarrantyPolicy from 'src/components/warranty-management/warranty-policy/Form/CreateFormWarrantyPolicy'


export default function CreateSignalLights() {

    return (
        <ModalsProvider>
            <WarrantyCreateProvider>
                <PageHeader
                    title={<PageHeaderTitle title='Thêm chính sách bảo hành' />}
                />
                <CreateFormWarrantyPolicy />
            </WarrantyCreateProvider>
        </ModalsProvider>
    )
}