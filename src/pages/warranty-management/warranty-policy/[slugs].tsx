import React from 'react'
import ModalsProvider from 'src/context/ModalContext'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import UpdateFormWarrantyPolicy from 'src/components/warranty-management/warranty-policy/Form/UpdateFormWarrantyPolicy'
import WarrantyUpdateProvider from 'src/context/warranty-management/waranty-policy/FormContext/update'


export default function CreateNotify() {

    return (
            <>
                <ModalsProvider>
                    <WarrantyUpdateProvider>
                        <PageHeader
                            title={<PageHeaderTitle title='Chỉnh sửa chính sách bảo hành' />}
                        />
                        <UpdateFormWarrantyPolicy />
                    </WarrantyUpdateProvider>
                </ModalsProvider>
            </>
    )
}
