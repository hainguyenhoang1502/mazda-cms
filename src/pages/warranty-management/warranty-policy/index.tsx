import React from 'react'
import TableWarrantyPolicy from 'src/components/warranty-management/warranty-policy/Table'
import ModalProvider from 'src/context/ModalContext'
import WarrantyPolicyListProvider from 'src/context/warranty-management/waranty-policy/TableContext'


export default function SignalLinePage() {
    return (
        <ModalProvider>
            <WarrantyPolicyListProvider>
                <TableWarrantyPolicy />
            </WarrantyPolicyListProvider>
        </ModalProvider>
    )
}