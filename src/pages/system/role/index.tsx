
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { useEffect } from 'react'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Store Redux Imports
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetRole } from 'src/store/apps/role'

// ** Component Imports
import { RoleCreate, RoleList, RolePermissionList } from 'src/views/pages/role/new-layout'
import { checkUserPermission } from 'src/@core/utils/permission'
import { permissonConfig } from 'src/configs/roleConfig'
import router from 'next/router'

import ModalClonePermission from 'src/components/Modal/ModalClonePermission'

const RolePage = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.role)
  const storeProflie = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if ((store && !store.data) || (store && store.data.length === 0) || (store && store.isNeedReloadData)) {
      dispatch(fetchDataGetRole(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (storeProflie.grantedAuthorities && storeProflie.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.ROLE_VIEW)
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [storeProflie.grantedAuthorities])

  return (
    <Grid container>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Quản Lý Phân Quyền Người Dùng' />}
        />
      </Grid>
      <Grid container xs={12} spacing={6}>
        <Grid item xs={4}>
          <ModalClonePermission.Group>
            <RoleCreate />
            <RoleList />
          </ModalClonePermission.Group>
        </Grid>
        <Grid item xs={8}>
          <RolePermissionList />
        </Grid>
      </Grid>
    </Grid>
  )
}

export default RolePage
