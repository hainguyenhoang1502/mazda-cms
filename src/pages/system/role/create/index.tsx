
// ** MUI Imports
import Grid from '@mui/material/Grid'
import router from 'next/router'
import { useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { checkUserPermission } from 'src/@core/utils/permission'
import { permissonConfig } from 'src/configs/roleConfig'
import { RootState } from 'src/store'
import { CreateRoleMain } from 'src/views/pages/role/create'

const CreatePermission = () => {

  const store = useSelector((state: RootState) => state.profile)
  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.ROLE_CREATE)
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Thêm vai trò' />}
        />
      </Grid>
      <CreateRoleMain />
    </Grid>
  )
}


export default CreatePermission
