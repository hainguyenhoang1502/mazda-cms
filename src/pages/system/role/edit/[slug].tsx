
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'


// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { ApiDetailRoleCms } from 'src/api/authen'
import { checkUserPermission } from 'src/@core/utils/permission'
import { permissonConfig } from 'src/configs/roleConfig'

// ** Store
import { RootState } from 'src/store'
import { CreateRoleMain } from 'src/views/pages/role/create'
import { AnyAction } from '@reduxjs/toolkit'

// ** Interface

interface EditRoleProps {
  data: AnyAction
}
interface EditRoleContextProps {
  data: any
  isEdit: boolean
}
export const EditRoleContext = createContext<EditRoleContextProps>(null);
const EditRole = (props: EditRoleProps) => {

  const store = useSelector((state: RootState) => state.profile)
  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.ROLE_EDIT)
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh Sửa vai trò' />}
        />
      </Grid>
      <EditRoleContext.Provider value={{ data: props.data, isEdit: true }}>
        <CreateRoleMain />
      </EditRoleContext.Provider>
    </Grid >
  )
}

export default EditRole

export const getServerSideProps = async (ctx) => {
  const [data] = await Promise.all([ApiDetailRoleCms(ctx.query.slug, ctx.req)])
  if (data && data.code == 200 && data.result) {

    return {
      props: {
        data: data.data
      }
    }
  }
  else {

    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}