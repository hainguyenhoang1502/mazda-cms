import { Grid } from '@mui/material'
import React, { createContext, useContext, useState } from 'react'
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { ApiDetailSystemSetting } from 'src/api/refData'
import { IResponseSettingDetail } from 'src/api/refData/type'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import ModalConfirm from 'src/views/component/modalConfirm'
import ModalNotify from 'src/views/component/modalNotify'
import AppForm from 'src/views/pages/system/setting/app'
import EmailForm from 'src/views/pages/system/setting/email'
import SMSForm from 'src/views/pages/system/setting/sms'

interface IInitContext {
    data: IResponseSettingDetail['data'];
}

interface SettingProps {
    data: IResponseSettingDetail['data'];
}

export const SettingContext = createContext<IInitContext>(null)

const SettingPageSlug = (props: SettingProps) => {
    const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)
    const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)


    return (
        <SettingContext.Provider value={{ data: props.data }}>
            <Grid container>
                <Grid item xs={12}>
                    <PageHeader
                        title={<PageHeaderTitle title='Cài đặt' />}
                    />
                </Grid>
                <Grid container gap={8}>
                    <Grid item xs={12}>
                        <SMSForm setModalConfirm={setModalConfirm} setModalNotify={setModalNotify} />
                    </Grid>
                    <Grid item xs={12}>
                        <EmailForm setModalConfirm={setModalConfirm} setModalNotify={setModalNotify} />
                    </Grid>
                    <Grid item xs={12}>
                        <AppForm setModalConfirm={setModalConfirm} setModalNotify={setModalNotify} />
                    </Grid>
                </Grid>
            </Grid>

            <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
            <ModalNotify {...modalNotify} toggle={setModalNotify} />
        </SettingContext.Provider>
    )
}
export const useSettingSystem = () => {
    const context = useContext(SettingContext)
    if (typeof context === "undefined") throw new Error("Component must be used within SettingContextProvider")

    return context
}
export default SettingPageSlug

export const getServerSideProps = async (ctx) => {

    const dataDetail = await ApiDetailSystemSetting(ctx.query.slug, ctx.req)
    if (dataDetail && dataDetail.code == 200 && dataDetail.result && ctx.query.slug === 'mazda') {

        return {
            props: {
                data: dataDetail.data,
            }
        }
    }
    else {
        // return {
        //     props: {
        //         data: {
        //             smsConfig: {
        //                 id: null,
        //                 brandCode: ctx.query.slug,
        //                 brandName: null,
        //                 apiKey: null,
        //                 secretKey: null,
        //                 isActive: 0
        //             },
        //             emailConfig: {
        //                 id: null,
        //                 host: null,
        //                 port: null,
        //                 userName: null,
        //                 password: null,
        //                 displayName: null,
        //                 brandCode: ctx.query.slug,
        //                 isActive: 0
        //             },
        //             appConfig: {
        //                 id: null,
        //                 brandCode: ctx.query.slug,
        //                 logo: null,
        //                 splashcreen: null,
        //                 textIntro: null
        //             }
        //         },
        //     }
        // }
        return {
            redirect: {
                destination: '/404',
                permanent: false,
            },
        }
    }
}