// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext } from 'react'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { UserInformationDetail } from 'src/views/pages/user/view'

// ** Config Imports
import { IResponeDetailUser } from 'src/api/authen/type'

// ** Api Imports
import { ApiDetailUserCms } from 'src/api/authen'

// ** Interface Imports
interface DetailUserProps {
  dataDetail: IResponeDetailUser["data"]
  id: number | string
}
interface IDetailUserContext {
  dataDetail: IResponeDetailUser["data"]
}
export const DetailUserContext = createContext<IDetailUserContext>(null)

export const getServerSideProps = async ctx => {
  const [dataUser] = await Promise.all([ApiDetailUserCms(ctx.query.slug, ctx.req)])
  if (dataUser && dataUser.code == 200 && dataUser.result) {
    return {
      props: {
        dataDetail: dataUser.data,
        id: ctx.query.slug
      }
    }
  } else {
    return {
      redirect: {
        destination: '/404',
        permanent: false
      }
    }
  }
}

const UserInformationDetailPage = (props: DetailUserProps) => {

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title=' Chi Tiết Người Dùng' />} />
      </Grid>
      <DetailUserContext.Provider
        value={{
          dataDetail: props.dataDetail,
        }}
      >
        <UserInformationDetail />
      </DetailUserContext.Provider>
    </Grid>
  )
}

export default UserInformationDetailPage
