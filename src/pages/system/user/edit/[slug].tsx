// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'
import { permissonConfig } from 'src/configs/roleConfig'

// ** Store
import { RootState } from 'src/store'
import { CreateUserMain } from 'src/views/pages/user/create'
import { ApiDetailUserCms } from 'src/api/authen'

// ** Interface
import { IResultDataDetailUser } from 'src/api/authen/type'

interface EditUserProps {
  dataUser: IResultDataDetailUser
}
interface EditUserContextProps {
  dataUser: IResultDataDetailUser
  isEdit: boolean
}
export const EditUserContext = createContext<EditUserContextProps>(null)
const EditUser = (props: EditUserProps) => {
  const store = useSelector((state: RootState) => state.profile)
  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.USER_EDIT)
      if (!permission) {
        router.replace('/401')
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Chỉnh Sửa Tài Khoản Người Dùng' />} />
      </Grid>
      <EditUserContext.Provider value={{ dataUser: props.dataUser, isEdit: true }}>
        <CreateUserMain />
      </EditUserContext.Provider>
    </Grid>
  )
}

export default EditUser

export const getServerSideProps = async ctx => {
  const dataUser = await ApiDetailUserCms(ctx.query.slug, ctx.req)
  if (dataUser && dataUser.code == 200 && dataUser.result) {
    return {
      props: {
        dataUser: dataUser.data
      }
    }
  } else {
    return {
      redirect: {
        destination: '/404',
        permanent: false
      }
    }
  }
}
