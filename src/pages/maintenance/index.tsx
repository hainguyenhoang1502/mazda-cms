// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// import * as XLSX from 'xlsx'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { ListDataTable, TableHeader } from 'src/views/pages/maintenance/list'
import ModalConfirm from 'src/views/component/modalConfirm'

// ** Api Imports 


// ** Config Imports
import { ModalConfirmState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM } from 'src/configs/initValueConfig'

import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetMaintenanceData } from 'src/store/apps/maintenance'
import ModalFile from 'src/components/Modal/ModalFile'

// import axiosClient from 'src/api-client/axios-client'



const CustomerInformationPage = () => {


  // useEffect(() => {
  //   downloadFile()
  // },[])

  // const downloadFile = async () => {
  //   // Tải tệp Excel từ URL
  //   try{
  //     const response = await axiosClient.get('https://kong-gateway.toponseek.com/ref-data/files/2023/6/admin/ImportMaintenanceHistoryTemplate.xlsx', {
  //       responseType: 'arraybuffer',
  //     });
  //     console.log('response :>> ', response);
  //     const data = response.data;
  
  //     // Phân tích tệp Excel và lấy dữ liệu từ nó
  //     const workbook = XLSX.read(data, { type: 'array' });
  //     const sheetName = workbook.SheetNames[0]; // Lấy tên sheet đầu tiên
  //     const worksheet = workbook.Sheets[sheetName];
  //     const jsonData = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
  
  //     // In ra dữ liệu lấy được
  //     console.log(jsonData);
  //   } catch(error) {
  //     console.log('error :>> ', error);
  //   }
   
  // };

  // ** States
  // const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)
  
  // const [res, setRes] = useState()

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.maintenanceHistory)

  useEffect(() => {
    if ((store && !store.data) || (store && store.data.length === 0) || (store && store.isNeedReloadData)) {
      dispatch(fetchDataGetMaintenanceData(store.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <ModalFile.Group>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <PageHeader
            title={<PageHeaderTitle title='Lịch sử bảo dưỡng' />}
          />
        </Grid>
        <Grid item xs={12}>
          <Card>
            <TableHeader />
            <ListDataTable />
          </Card>
        </Grid>
        <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      </Grid>
    </ModalFile.Group>
  )
}

export default CustomerInformationPage


