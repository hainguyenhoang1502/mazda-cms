// ** React Imports
import { useState, ReactNode } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** MUI Components
import Button from '@mui/material/Button'
import IconButton from '@mui/material/IconButton'
import Box, { BoxProps } from '@mui/material/Box'
import FormControl from '@mui/material/FormControl'
import useMediaQuery from '@mui/material/useMediaQuery'
import { styled, useTheme } from '@mui/material/styles'

// ** Icon Imports
import Icon from 'src/@core/components/icon'

// ** Third Party Imports
import * as yup from 'yup'
import { Field, Form, Formik } from 'formik'

// ** Hooks
import { useAuth } from 'src/hooks/useAuth'
import { useSettings } from 'src/@core/hooks/useSettings'

// ** Layout Import
import BlankLayout from 'src/@core/layouts/BlankLayout'
import { InputRequiredMUI } from 'src/views/component/theme'
import InputAdornment from '@mui/material/InputAdornment'
import { ERROR_INVALID_PASSWORD, ERROR_NULL_VALUE_INPUT, ERROR_USERNAME_VALUE_INPUT, regexPassword, regexUserName } from 'src/configs/initValueConfig'
import { DEFAULT_PASSWORD, DEFAULT_RECAPTCHA, DEFAULT_USERNAME } from 'src/configs/envConfig'
import Recapcha from 'src/views/component/recapcha'
import { Typography } from '@mui/material'

// ** Styled Components
const LoginIllustration = styled('img')({
  height: '100%',
  maxWidth: '100%',
  objectFit: "cover",

})

export const LoginBoxImgLeft = styled(Box)({
  p: 0,
  flex: 1,
  display: 'flex',
  position: "relative",
  alignItems: 'center',
  justifyContent: 'center',
  "&::after": {
    top: 0,
    left: 0,
    zIndex: 1,
    content: '""',
    width: "100%",
    height: "100%",
    position: "absolute",
    background: "rgba(0, 0, 0, 0.4)"
  }
})

export const LoginBoxTextLeft = styled(Box)({
  left: "5%",
  zIndex: 2,
  bottom: "1%",
  position: "absolute",

})


const RightWrapper = styled(Box)<BoxProps>(({ theme }) => ({
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  padding: theme.spacing(6),
  backgroundColor: theme.palette.background.paper,
  [theme.breakpoints.up('lg')]: {
    maxWidth: 480
  },
  [theme.breakpoints.up('xl')]: {
    maxWidth: 500
  },
  [theme.breakpoints.up('sm')]: {
    padding: theme.spacing(12)
  }
}))

const LinkStyled = styled(Link)(({ theme }) => ({
  fontSize: '0.875rem',
  textDecoration: 'none',
  color: theme.palette.primary.main
}))

const schema = yup.object().shape({
  userName: yup.string().required(ERROR_NULL_VALUE_INPUT).matches(regexUserName, ERROR_USERNAME_VALUE_INPUT),
  password: yup.string().required(ERROR_NULL_VALUE_INPUT).min(6, ERROR_INVALID_PASSWORD).matches(regexPassword, ERROR_INVALID_PASSWORD),
  recapcha: yup.string().required("")
})

const defaultValues = {
  userName: DEFAULT_USERNAME,
  password: DEFAULT_PASSWORD,
  recapcha: DEFAULT_RECAPTCHA
}

interface FormData {
  userName: string
  password: string
}

const LoginPage = () => {
  const [showPassword, setShowPassword] = useState<boolean>(false)
  const [resetCaptcha, setResetCaptcha] = useState<number>(0)

  // ** Hooks
  const auth = useAuth()
  const theme = useTheme()
  const { settings } = useSettings()
  const hidden = useMediaQuery(theme.breakpoints.down('lg'))

  // ** Var
  const { skin } = settings

  const onSubmit = async (data: FormData) => {
    const { userName, password } = data
    auth.login({ userName, password },
      () => {
        if (resetCaptcha === 3) {
          setResetCaptcha(0)
        } else {
          setResetCaptcha(resetCaptcha + 1)
        }
      }
    )

  }

  return (
    <Box className='content-right'>
      {!hidden ? (
        <LoginBoxImgLeft>
          <LoginIllustration
            alt='login-illustration'
            src={`/images/pages/logo-login.png`}
          />
          <LoginBoxTextLeft>

            {/* <Typography sx={{ color: theme.palette.common.white, fontWeight: "500" }}>THACO AUTO</Typography> */}

            <Typography sx={{ color: theme.palette.common.white }}>Bản quyền 2023 @ Thaco Auto</Typography>
          </LoginBoxTextLeft>
        </LoginBoxImgLeft>
      ) : null}
      <RightWrapper
        sx={{ ...(skin === 'bordered' && !hidden && { borderLeft: `1px solid ${theme.palette.divider}` }) }}
      >
        <Box sx={{ mx: 'auto', maxWidth: 400, padding: "20px" }}>
          <Box sx={{ padding: "0 20px 20px" }}>
            <img src="/images/logothacoauto.png" width="100%" height={"100%"} alt="" />
          </Box>
          <Formik
            initialValues={defaultValues}
            onSubmit={(values) => onSubmit(values)}
            validationSchema={schema}
          >
            {(props) => {

              return (
                <Form>
                  <FormControl fullWidth sx={{ mb: 4 }}>
                    <Field
                      id="my-userName"
                      label="Tên đăng nhập"
                      name="userName"
                      component={InputRequiredMUI}
                      texterror={props.touched.userName && props.errors.userName}
                      texticon="*"
                    />
                  </FormControl>
                  <FormControl fullWidth sx={{ mb: 4 }}>
                    <Field
                      id="my-password"
                      label="Mật khẩu"
                      name="password"
                      component={InputRequiredMUI}
                      texterror={props.touched.password && props.errors.password}
                      texticon="*"
                      type={showPassword ? 'text' : 'password'}
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton
                            edge='end'
                            onMouseDown={e => e.preventDefault()}
                            onClick={() => setShowPassword(!showPassword)}
                          >
                            <Icon fontSize={20} icon={showPassword ? 'bx:show' : 'bx:hide'} />
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </FormControl>
                  <Box
                    sx={{ mb: 4, display: 'flex', alignItems: 'center', flexWrap: 'wrap', justifyContent: 'end' }}
                  >
                    <LinkStyled href='/forgot-password'>Quên mật khẩu?</LinkStyled>
                  </Box>
                  <FormControl fullWidth sx={{ mb: 4 }}>
                    <Field
                      id="my-Recapcha"
                      label="Recapcha"
                      name="recapcha"
                      component={Recapcha}
                      onChange={(e: string) => props.setFieldValue("recapcha", e)}
                      isReset={resetCaptcha == 3 ? true : false}
                    />
                  </FormControl>

                  <Button fullWidth size='large' type='submit' variant='contained' >
                    Đăng nhập
                  </Button>
                </Form>
              )
            }
            }
          </Formik>
        </Box>
      </RightWrapper>
    </Box>
  )
}

LoginPage.getLayout = (page: ReactNode) => <BlankLayout>{page}</BlankLayout>

LoginPage.guestGuard = true

export default LoginPage
