

const Home = () => {

  return <>Home Page</>
}

export const getServerSideProps = async () => {

  return {
    redirect: {
      destination: '/customer-information',
      permanent: false,
    },
  }

}

export default Home
