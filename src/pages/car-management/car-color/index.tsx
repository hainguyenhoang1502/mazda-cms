// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Next Imports
// import router from 'next/router'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { ListDataTable, TableHeader } from 'src/views/pages/car-management/car-color/list'
import ModalConfirm from 'src/views/component/modalConfirm'

// ** Config Imports
// import { permissonConfig } from 'src/configs/roleConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'

// ** Store Redux Imports
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'

// ** Util Imports
// import { checkUserPermission } from 'src/@core/utils/permission'
import ModalNotify from 'src/views/component/modalNotify'
import { ApiVehicleColorDelete } from 'src/api/vehicles'
import { fetchDataGetListVehicleColor } from 'src/store/apps/car-management/car-color'

const CarColorPage = () => {
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux Store
  const dispatch = useDispatch<AppDispatch>()
  const storeListVehicleColor = useSelector((state: RootState) => state.listVehicleColor)

  //CALL LIST
  useEffect(() => {
    if (storeListVehicleColor && !storeListVehicleColor.data || storeListVehicleColor && storeListVehicleColor.data.length === 0 || storeListVehicleColor.isNeedReloadData) {
      dispatch(fetchDataGetListVehicleColor(storeListVehicleColor.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const onDeleteCarColor = async (id: number) => {

    const res = await ApiVehicleColorDelete(id)
    if (res.code == 200 && res.result) {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Màu Xe Thành Công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(fetchDataGetListVehicleColor(storeListVehicleColor.param))

    } else {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Màu Xe Thất Bại",
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }

  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Màu xe' />} />
      </Grid>

      <Grid item xs={12}>
        <Card>
          <TableHeader />
          <ListDataTable setModalConfirm={setModalConfirm} onDeleteCarColor={onDeleteCarColor} />
        </Card>
      </Grid>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </Grid>
  )
}

export default CarColorPage
