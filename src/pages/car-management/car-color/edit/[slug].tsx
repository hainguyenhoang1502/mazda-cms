
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext } from 'react'

import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Store
import { CreateColorForm } from 'src/views/pages/car-management/car-color/create'
import { IResponseColorDetail } from 'src/api/vehicles/type'
import { ApiListVehicleColorDetail } from 'src/api/vehicles'

// ** Interface

interface EditVehicleColorProps {
  data: IResponseColorDetail['data']; // edit
}
interface EditVehicleColorProps {
  data: IResponseColorDetail['data'];
  isEdit: boolean;
}

export const EditVehicleColorContext = createContext<EditVehicleColorProps>(null);

const EditVehicleColorPage = (props: EditVehicleColorProps) => {

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh Sửa Màu Xe' />}
        />
      </Grid>
      <EditVehicleColorContext.Provider value={{ data: props.data, isEdit: true }}>
        <CreateColorForm />
      </EditVehicleColorContext.Provider>
    </Grid >
  )
}



export default EditVehicleColorPage

export const getServerSideProps = async (ctx) => {

  const data = await ApiListVehicleColorDetail(ctx.query.slug, ctx.req)
  if (data && data.code === 200 && data.result && data.data) {

    return {
      props: {
        data: data.data
      }
    }
  }

  else {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}