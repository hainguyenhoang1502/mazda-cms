
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext, useEffect } from 'react'
import { useSelector } from 'react-redux'

// ** Next Imports
import router from 'next/router'


// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Api Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store
import { RootState } from 'src/store'
import { ApiVehicleModelDetail } from 'src/api/vehicles'
import { CreateModel } from 'src/views/pages/car-management/model/create'

// ** Interface

interface EditModelProps {
  dataModel: any // edit
}
interface EditModelContextProps {
  data: any
  isEdit: boolean
}

export const EditModelContext = createContext<EditModelContextProps>(null);

const EditModelPage = (props: EditModelProps) => {
  const store = useSelector((state: RootState) => state.profile)

  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission('')
      if (!permission) {
        router.replace("/401")
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh Sửa Dòng Xe' />}
        />
      </Grid>
      <EditModelContext.Provider value={{ data: props.dataModel, isEdit: true }}>
        <CreateModel />
      </EditModelContext.Provider>
    </Grid >
  )
}



export default EditModelPage

export const getServerSideProps = async (ctx) => {
  
  const [dataModel] = await Promise.all([ApiVehicleModelDetail(ctx.query.slug, ctx.req)])
  if (dataModel && dataModel.code == 200 && dataModel.result) {

    return {
      props: {
        dataModel: dataModel.data
      }
    }
  }
  else {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}