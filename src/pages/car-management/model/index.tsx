// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

import { ListDataTable, TableHeader } from 'src/views/pages/car-management/model/list'

import ModalConfirm from 'src/views/component/modalConfirm'

// ** Config Imports
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'
import ModalNotify from 'src/views/component/modalNotify'

import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetVehicleData } from 'src/store/apps/vehicle/model'
import { ApiVehicleModelDelete } from 'src/api/vehicles'

// import { ApiOrganizationBrandDelete } from 'src/api/organization'

const CategoryModelPage = () => {
    const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
    const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

    // ** Redux
    const dispatch = useDispatch<AppDispatch>()
    const store = useSelector((state: RootState) => state.vehicleData)

    useEffect(() => {
        if ((store && !store.data) || (store && store.data.length === 0) || (store && store.isNeedReloadData)) {
            dispatch(fetchDataGetVehicleData(store.param))
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const getDeleteModel = async (code: string) => {

        const res = await ApiVehicleModelDelete(code)
        if (res.code == 200 && res.result) {
            setModalNotify({
                ...modalNotify,
                isOpen: true,
                title: "Xóa Dòng xe Thành Công",
            })
            setModalConfirm(INIT_STATE_MODAL_CONFIRM)

            dispatch(fetchDataGetVehicleData(store.param))

        } else {
            setModalNotify({
                ...modalNotify,
                isOpen: true,
                title: "Xóa Dòng xe Thất Bại",
                isError: true
            })
            setModalConfirm(INIT_STATE_MODAL_CONFIRM)
        }

    }

    return (
        <Grid container spacing={6}>
            <Grid item xs={12}>
                <PageHeader title={<PageHeaderTitle title='Dòng xe' />} />
            </Grid>

            <Grid item xs={12}>
                <Card>
                    <TableHeader />
                    <ListDataTable setModalConfirm={setModalConfirm} getDeleteModel={getDeleteModel} />
                </Card>
            </Grid>

            <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
            <ModalNotify {...modalNotify} toggle={setModalNotify} />
        </Grid>
    )
}

export default CategoryModelPage