import React from 'react'
import TableCarImage from 'src/components/car-images-management/Table'
import ModalProvider from 'src/context/ModalContext'
import CarImageListProvider from 'src/context/car-images-management/TableContext'


export default function CarImagePage() {
    return (
        <ModalProvider>
            <CarImageListProvider>
                <TableCarImage />
            </CarImageListProvider>
        </ModalProvider>
    )
}