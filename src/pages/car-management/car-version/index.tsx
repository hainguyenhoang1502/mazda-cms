// ** MUI Imports
import { Card } from '@mui/material'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useEffect, useState } from 'react'


// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'
import { ListDataTable, TableHeader } from 'src/views/pages/car-management/car-version/list'
import ModalConfirm from 'src/views/component/modalConfirm'

// ** Config Imports
// import { permissonConfig } from 'src/configs/roleConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'

// ** Store Redux Imports
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'

// ** Util Imports
// import { checkUserPermission } from 'src/@core/utils/permission'
import { getListModelCodeAction } from 'src/store/apps/vehicle/listModelCode'
import { fetchDataGetListVehicleVersion } from 'src/store/apps/car-management/car-version'
import ModalNotify from 'src/views/component/modalNotify'
import { ApiVehicleVersionDelete } from 'src/api/vehicles'

const CarVersionPage = () => {
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux Store
  const dispatch = useDispatch<AppDispatch>()
  const storeListModelCode = useSelector((state: RootState) => state.listModelCode)
  const storeListVehicleVersion = useSelector((state: RootState) => state.listVehicleVersion)

  //CALL LIST
  useEffect(() => {
    if (storeListVehicleVersion && !storeListVehicleVersion.data || storeListVehicleVersion && storeListVehicleVersion.data.length === 0 || storeListVehicleVersion.isNeedReloadData) {
      dispatch(fetchDataGetListVehicleVersion(storeListVehicleVersion.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  //LIST Model
  useEffect(() => {
    if ((storeListModelCode && !storeListModelCode.data) || (storeListModelCode && storeListModelCode.data.length === 0) || (storeListModelCode && storeListModelCode.isNeedReloadData)) {
      dispatch(getListModelCodeAction())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const onDeleteCarVerion = async (id: number) => {

    const res = await ApiVehicleVersionDelete(id)
    if (res.code == 200 && res.result) {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Phiên Bản Thành Công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(fetchDataGetListVehicleVersion(storeListVehicleVersion.param))

    } else {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Phiên Bản Thất Bại",
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }

  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Phiên bản' />} />
      </Grid>

      <Grid item xs={12}>
        <Card>
          <TableHeader />
          <ListDataTable setModalConfirm={setModalConfirm} onDeleteCarVerion={onDeleteCarVerion} />
        </Card>
      </Grid>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </Grid>
  )
}

export default CarVersionPage
