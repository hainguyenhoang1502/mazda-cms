
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Imports
import { createContext } from 'react'

import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Store
import { CreateVersionForm } from 'src/views/pages/car-management/car-version/create'
import { ApiListVehicleVersionDetail } from 'src/api/vehicles'
import { IResponseVehicleDetail } from 'src/api/vehicles/type'

// ** Interface

interface EditVehicleVersionProps {
  data: IResponseVehicleDetail['data']; // edit
}
interface EditVehicleVersionProps {
  data: IResponseVehicleDetail['data'];
  isEdit: boolean;
}

export const EditVehicleVersionContext = createContext<EditVehicleVersionProps>(null);

const EditVehicleVersionPage = (props: EditVehicleVersionProps) => {

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader
          title={<PageHeaderTitle title='Chỉnh Sửa Phiên Bản' />}
        />
      </Grid>
      <EditVehicleVersionContext.Provider value={{ data: props.data, isEdit: true }}>
        <CreateVersionForm />
      </EditVehicleVersionContext.Provider>
    </Grid >
  )
}



export default EditVehicleVersionPage

export const getServerSideProps = async (ctx) => {

  const data = await ApiListVehicleVersionDetail(ctx.query.slug, ctx.req)
  if (data && data.code === 200 && data.result && data.data) {

    return {
      props: {
        data: data.data
      }
    }
  }

  else {
    return {
      redirect: {
        destination: '/404',
        permanent: false,
      },
    }
  }
}