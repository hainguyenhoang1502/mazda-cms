// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** Next Imports
import router from 'next/router'

// ** React Imports
import { useEffect } from 'react'

// ** Store Imports
import { RootState } from 'src/store'
import { useSelector } from 'react-redux'

// ** Components Imports
import PageHeader, { PageHeaderTitle } from 'src/@core/components/page-header'

// ** Util Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'
import { CreateVersionForm } from 'src/views/pages/car-management/car-version/create'

const CreateVersionPage = () => {
  const store = useSelector((state: RootState) => state.profile)
  useEffect(() => {
    if (store.grantedAuthorities && store.grantedAuthorities.length) {
      const permission = checkUserPermission(permissonConfig.BRAND_CREATE)
      if (!permission) {
        router.replace('/401')
      }
    }
  }, [store.grantedAuthorities])

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <PageHeader title={<PageHeaderTitle title='Thêm phiên bản' />} />
      </Grid>
      <CreateVersionForm />
    </Grid>
  )
}

export default CreateVersionPage
