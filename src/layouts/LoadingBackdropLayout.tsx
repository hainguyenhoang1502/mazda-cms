import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'src/store';

export const LoadingBackdropLayout = ({ children }) => {
  
  const store = useSelector((state: RootState) => state.loading)

  return (
    <>
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={store.isLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {children}
    </>
  );
};
