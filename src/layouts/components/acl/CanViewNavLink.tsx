// src/layouts/components/acl/CanViewNavLink.tsx
import { ReactNode } from 'react'
import { checkUserPermission } from 'src/@core/utils/permission'

interface Props {
  children: ReactNode,
  permission: string
}

const CanViewNavLink = (props: Props) => {
  const { children } = props
  if (checkUserPermission(props.permission)) {

    return <>{children}</>
  } else {
    return
  }
}

export default CanViewNavLink