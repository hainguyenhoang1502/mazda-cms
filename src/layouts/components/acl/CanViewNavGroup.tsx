// src/layouts/components/acl/CanViewNavGroup.tsx
import { ReactNode } from 'react'
import { NavGroup } from 'src/@core/layouts/types'
import { checkUserPermission } from 'src/@core/utils/permission'

interface Props {
  children: ReactNode,
  item: NavGroup
}

const CanViewNavGroup = (props: Props) => {
  const { children } = props
  let isCanView = false
  props.item && props.item.children && props.item.children.length > 0 && props.item.children.forEach(child => {
    if (checkUserPermission(child.permisson)) {
      isCanView = true
    }
  })
  if (isCanView) {

    return <>{children}</>
  } else {
    return
  }
}

export default CanViewNavGroup