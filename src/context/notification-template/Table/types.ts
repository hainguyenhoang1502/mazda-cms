export interface NotificationTemplateProps {
    children: React.ReactNode
}
export interface ActiveStatus {
    'ACTIVE': {
        color: 'green',
        title: 'Đang hoạt động'
    },
    'INACTIVE': {
        color: 'red',
        title: 'Không hoạt động'
    }
}
export interface NotificationTemplateListRowsType {
    id: number,
    name: string
    content: string
    status: string
}
export interface INotificationTemplatePageState {
    total: number
    sort: any
    rows: any[]
    filter: {
        keyword:string
        status: string
    }
    sortColumn: string
    paginationModel: { page: number, pageSize: number }
    loading: boolean

}

export enum INotificationTemplatesActionType {
    SET_TOTAL = 'TOTAL',
    SET_SORT = 'SORT',
    SET_ROWS = 'ROWS',
    SET_SORT_COLUMN = 'SORT_COLUMN',
    SET_PAGINATION_MODEL = 'PAGINATION_MODEL',
    SET_LOADING = 'LOADING',
    SET_FILTER_STATUS = "SET_FILTER_STATUS",
    SET_FILTER_KEYWORD = "SET_FILTER_KEYWORD",
}
export interface INotificationTemplateAction {
    type: INotificationTemplatesActionType
    payload: any
}
