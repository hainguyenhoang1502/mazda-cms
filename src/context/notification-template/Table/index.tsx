import React, { useCallback } from 'react'
import { debounce } from 'src/utils'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { INotificationTemplatePageState, NotificationTemplateProps } from './types'
import { useModals } from 'src/context/ModalContext'
import { INotificationTemplatesActionType as type } from './types'
import { reducer } from './reducer'
import { notificationTemplateApi } from 'src/api-client/notification-template'

const defaultValues: INotificationTemplatePageState = {
    total: 0,
    sort: 'asc',
    rows: [],
    filter: {
        keyword: '',
        status: '',
    },
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    loading: false,
}

export const statusOptions=[{ label: 'Tất cả', value: '' }, { label: 'Đang hoạt động', value: 'ACTIVE' }, { label: 'Ngưng hoạt động', value: 'INACTIVE' }]

export const NotificationTemplateListContext = React.createContext<any>({})

export default function NotificationTemplateListProvider({ children }: NotificationTemplateProps) {
    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const {
        paginationModel,
        filter } = state
    const idRef = React.useRef(null);
    const { registerModal, openModal } = useModals();

    //register modal

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_delete_success_notification-template', <ModalSuccess message='Bạn đã thao tác xóa template thành công!' />)
    registerModal('modal_delete_error_notification-template', <ModalError message='Bạn đã thao tác xóa template thất bại!' />)

    //fetch data from api

    const handleSearch = useCallback((value: any) => {
        dispatch({ type: type.SET_FILTER_KEYWORD, payload: value });
    },[]);



    const handleClearFilter = useCallback(() => {
        dispatch({ type: type.SET_FILTER_KEYWORD, payload: '' });
        dispatch({ type: type.SET_FILTER_STATUS, payload: '' });
    },[])

    const fetchTableData =
        useCallback(async () => {
            dispatch({ type: type.SET_LOADING, payload: true })
            const params = {
                keyword: filter.keyword,
                pageIndex: paginationModel.page + 1,
                pageSize: paginationModel.pageSize,
                ...filter.status !== 'Tất cả' && filter.status.length > 0 ? { status: filter.status } : {},
            }
            await notificationTemplateApi.getListTemplate(params).then(res => {
                console.log(res);
                if (res) {
                    dispatch({ type: type.SET_TOTAL, payload: res.data.totalRecords })
                    dispatch({ type: type.SET_ROWS, payload: res.data?.result ? res.data.result : [] })
                    dispatch({ type: type.SET_LOADING, payload: false })
                }
            })
        }, [filter.keyword, filter.status, paginationModel.page, paginationModel.pageSize])

    // eslint-disable-next-line react-hooks/exhaustive-deps

    const debouncedFetchTableData = useCallback(
        debounce(fetchTableData, 600),
        [fetchTableData]
    )
    
    //debounce when search

    React.useEffect(() => {
        debouncedFetchTableData()
    }, [debouncedFetchTableData, filter.status, paginationModel])


    const handleDelete = useCallback( async () => {
        openModal('modal_loading')
        try {
            console.log(idRef.current)
            const res = await notificationTemplateApi.deleteTemplate(idRef.current)
            if (res.data) {
                openModal('modal_delete_success_notification-template')
                fetchTableData()
            }
            else {
                openModal('modal_delete_error_notification-template')
            }
        } catch (e) {
            openModal('modal_delete_error_notification-template')
            console.log(e)
        }
    }
,[fetchTableData, openModal])
    

    registerModal('modal_confirm_delete_notification-template', <ModalConfirm message='Bạn đang thao tác xóa template' handleConfirm={handleDelete} />)

    const handleOpenModal = useCallback((modalName: string) => {
        openModal(modalName)
    },[openModal])

    return (
        <NotificationTemplateListContext.Provider value={{ ...state, dispatch, handleClearFilter, handleSearch, handleDelete, handleOpenModal, idRef }}>{children}</NotificationTemplateListContext.Provider>
    )
}