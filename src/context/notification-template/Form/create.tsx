import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { notificationTemplateApi } from 'src/api-client/notification-template'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'


export const defaultValues = {
    name: '',
    content: '',
    status: '',
    image: [],
    type:'AUTO',
    isShowPopup:false,
    link:'',
    fromDate: null,
    endDate:null
}
export type FormValues = {
    name: string,
    content: string,
    status: string,
    image:File[],
    isShowPopup: boolean,
    type:string,
    link:string,
    fromDate:Date,
    endDate:Date
}

export const checkEditor = (field: string, value: string) => {
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(value, 'text/html');
    const textString = htmlDoc.body.textContent;
    console.log(textString);
    if (textString.trim() === '' || textString.trim().length > 0) return `Vui lòng nhập thông tin`;

    return '';
}
export const schema = yup.object().shape({
    name: yup.string().required('Vui lòng nhập thông tin!'),
    content: yup.string().required(obj => checkEditor('editor', obj.value)),
    status: yup.string().required('Vui lòng nhập thông tin!')
})


export const FormCreateContext = React.createContext<any>({})

export default function FormCreateProvider({ children }: { children: React.ReactNode }) {
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal, closeModal } = useModals();
    const [confirm, setConfirm] = React.useState(false)

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_create_error_notification-template', <ModalError message='Bạn đã tạo template thông báo thất bại!' />)

    const handleSave = React.useCallback(async () => {
        try {
            openModal('modal_loading')

            const params = {
                name: form.getValues('name'),
                content: form.getValues('content'),
                status: form.getValues('status'),
            }
            const result = await notificationTemplateApi.createTemplate(params)
            if (result.data) {
                openModal('modal_create_success_notification-template')
            }
            else {
                openModal('modal_create_error_notification-template')
            }

        } catch (e) {
            openModal('modal_create_error_notification-template')
        }

    }, [form, openModal])

    registerModal('modal_confirm_create_notification-template', <ModalConfirm message='Bạn đang thao tác tạo template thông báo!' handleConfirm={handleSave} />)
    const handleConfirm = React.useCallback(() => {
        openModal('modal_confirm_create_notification-template')
    }, [openModal]);

    const handleClearForm = React.useCallback(() => {
        form.reset(defaultValues)
        closeModal();
    }, [closeModal, form])

    console.log(form.getValues('type'))

    registerModal('modal_create_success_notification-template', <ModalSuccess message='Bạn đã tạo template thông báo thành công!' handleConfirm={handleClearForm} />)

    return (
        <FormCreateContext.Provider value={{ form, handleConfirm, handleSave }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />
        </FormCreateContext.Provider>
    )
}