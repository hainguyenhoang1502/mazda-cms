import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useRouter } from 'next/router'
import * as yup from 'yup'
import { notificationTemplateApi } from 'src/api-client/notification-template'
import { Box, CircularProgress } from '@mui/material'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'

export const defaultValues = {
    name: '',
    content: '',
    status: '',
    image: [],
    type:'ISHOWPOPUP',
    isShowPopup: false,
    link:'',
    fromDate: null,
    endDate:null
}
export type FormValues = {
    name: string,
    content: string,
    status: string,
    image:File[],
    type:string,
    link:string,
    isShowPopup: boolean,
    fromDate:Date,
    endDate:Date
}



export const checkEditor = (field: string, value: string) => {
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(value, 'text/html');
    const textString = htmlDoc.body.textContent;
    console.log(textString);
    if (textString.trim() === '' || textString.trim().length > 0) return `Vui lòng nhập thông tin`;

    return '';
}
export const schema = yup.object().shape({
    name: yup.string().required('Vui lòng nhập thông tin!'),
    content: yup.string().required(obj => checkEditor('editor', obj.value)),
    status:yup.string().required('Vui lòng nhập thông tin!')
})

export const FormUpdateContext = React.createContext<any>({})

export default function FormUpdateProvider({ children }: { children: React.ReactNode }) {
    //hooks 
    
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal } = useModals();
    const [loading, setLoading] = React.useState(false)
    const router = useRouter();
    const { slugs } = router.query
    const [confirm, setConfirm] = React.useState(false)

    // register modal

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_update_success_notification-template', <ModalSuccess message='Bạn đã cập nhật template thành công!' />)
    registerModal('modal_update_error_notification-template', <ModalError message='Bạn đã cập nhật template thất bại!' />)

    const getInitialValues = React.useCallback(async () => {
        setLoading(true)
        const res = await notificationTemplateApi.getDetailTemplate(slugs)
        if (res.data) {
            form.reset({
                name: res.data.name ? res.data.name : '',
                content: res.data.content ? res.data.content : '',
                status: res.data.status,
                image: [],
                type:'',
                link:'',
                fromDate: null,
                endDate:null
            })
            setLoading(false)
        }
        else {
            router.push('/404')
        }
        setLoading(false)
    },[form, router, slugs])

    const handleSave = React.useCallback(async () => {
        try {
            openModal('modal_loading')

            const params = {
                id: slugs[0],
                name: form.getValues('name'),
                content: form.getValues('content'),
                status: form.getValues('status'),
                
            }
            const result = await notificationTemplateApi.updateNotificationTemplate(params);
            if (result.data) {
                getInitialValues()
                openModal('modal_update_success_notification-template')
            }
            else {
                openModal('modal_update_error_notification-template')
            }
        } catch (e) {
            openModal('modal_update_error_notification-template')
        }
    },[form, getInitialValues, openModal, slugs])

    registerModal('modal_confirm_update_notification-template', <ModalConfirm message='Bạn đang thao tác cập nhật template' handleConfirm={handleSave} />)
    const handleConfirm = React.useCallback(() => {
        openModal('modal_confirm_update_notification-template')
    },[openModal])

    React.useEffect(() => {
        getInitialValues()
    }, [getInitialValues, slugs])


    return (
        <FormUpdateContext.Provider value={{ form, handleConfirm, handleSave, loading }}>
            {loading?<Box sx={{ margin: '0 auto', width: '100%', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><CircularProgress /></Box>:children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm}/>

        </FormUpdateContext.Provider>
    )
}