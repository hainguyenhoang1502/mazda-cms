import { IParamLogin } from "src/api/authen/type"

export type AuthValuesType = {
  token: string
  loading: boolean
  logout: () => void
  setLoading: (value: boolean) => void
  login: (params: IParamLogin, ErrorCallBack: () => void) => void
}

export type LoadingBackdropValuesType = {
  loading: boolean,
  setOnToggle: () => void
  setOffToggle: () => void

}