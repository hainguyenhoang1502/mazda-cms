import {
    IWarrantyPolicyPageState,
    IWarrantyPolicyAction
} from "./types"
import { IWarrantyPolicyActionType as type } from './types'

export const reducer = (state: IWarrantyPolicyPageState, action: IWarrantyPolicyAction) => {
    switch (action.type) {
        case type.SET_TOTAL: {
            return {
                ...state,
                total: action.payload
            }
        }
        case type.SET_SORT: {
            return {
                ...state,
                sort: action.payload
            }
        }
        case type.SET_ROWS: {
            return {
                ...state,
                rows: action.payload
            }
        }
        case type.SET_SEARCH_VALUE: {
            return {
                ...state,
                search: action.payload
            }
        }
        case type.SET_SORT_COLUMN: {
            return {
                ...state,
                sortColumn: action.payload
            }
        }
        case type.SET_PAGINATION_MODEL: {
            return {
                ...state,
                paginationModel: action.payload
            }
        }
        case type.SET_STATUS: {
            return {
                ...state,
                status: action.payload
            }
        }
        case type.SET_LOADING: {
            return {
                ...state,
                loading: action.payload
            }
        }
        default:
            return state
    }
}