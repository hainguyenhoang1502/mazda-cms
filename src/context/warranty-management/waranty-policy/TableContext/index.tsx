import React, { useCallback } from 'react'
import { debounce } from 'src/utils'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { IWarrantyPolicyPageState, WarrantyPolicyProps } from './types'
import { useModals } from 'src/context/ModalContext'
import {IWarrantyPolicyActionType as type} from './types'
import { reducer } from './reducer'
import { WarrantyPolicysApi } from 'src/api-client/warranty'


const defaultValues: IWarrantyPolicyPageState = {
    total: 0,
    sort: 'asc',
    rows: [],
    search: '',
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    status: '',
    loading: false,
}

export const WarrantyTypeObj=[
    {
        label:'Chính sách bảo hành xe mới',
        value:'NEWCAR'
    },
    {
        label:"Phụ tùng thay thế",
        value:'SPAREPART'
    }
]
    


export const WarrantyPolicyListContext = React.createContext<any>({})

export default function WarrantyPolicyListProvider({ children }: WarrantyPolicyProps) {
    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const {
        search,
        paginationModel,
        status } = state
    const idRef=React.useRef(null);
    const searchValue=React.useRef(null);
    const { registerModal, openModal } = useModals();

    //register modal
    registerModal('modal_loading', <ModalLoading/>)
    registerModal('modal_delete_success_warranty-policy', <ModalSuccess message='Bạn đã thao tác xóa chính sách bảo hành thành công!'  />)
    registerModal('modal_delete_error_warranty-policy', <ModalError message='Bạn đã thao tác xóa chính sách bảo hành thất bại!'  />)
    registerModal('modal_save_success_warranty-policy', <ModalSuccess message='Bạn đã lưu chính sách bảo hành thành công!'  />)
    registerModal('modal_save_error_warranty-policy', <ModalError message='Bạn đã lưu chính sách bảo hành thất bại!'  />)

    //fetch data from api
    const fetchTableData = useCallback(
        debounce(async () => {
            dispatch({ type: type.SET_LOADING, payload: true })
            const params={
                keyword: search,
                pageIndex: paginationModel.page + 1,
                pageSize: paginationModel.pageSize,
                ...status===""?{}:{status:status}
            }
            await WarrantyPolicysApi.getListWarrantyPolicys(params).then(res => {
                console.log(res);
                if (res) {
                    dispatch({ type: type.SET_TOTAL, payload: res.data.totalRecords })
                    dispatch({ type: type.SET_ROWS, payload: res.data&&res.data?.result?.length>0 ? res.data.result : [] })
                    dispatch({ type: type.SET_LOADING, payload: false })
                }
            })
        },600),
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [paginationModel, search, status]
    )

    //debounce when search


    React.useEffect(() => {
        fetchTableData()
    }, [fetchTableData])


    const handleDelete = async () => {
        openModal('modal_loading')
        try {
            console.log(idRef.current)
            const res = await WarrantyPolicysApi.deleteWarrantyPolicys(idRef.current)
            if (res.data) {
                openModal('modal_delete_success_warranty-policy')
                fetchTableData()
            }
            else {
                openModal('modal_delete_error_warranty-policy')
            }
        } catch (e) {
            openModal('modal_delete_error_warranty-policy')
            console.log(e)
        }
    }

    const handleSearch = () => {
        dispatch({ type: type.SET_SEARCH_VALUE, payload: searchValue.current.value });
    };


    const handleClearFilter = () => {
        dispatch({ type: type.SET_SEARCH_VALUE, payload: '' });
        searchValue.current.value="";
        dispatch({ type: type.SET_STATUS, payload: '' });
    }

    registerModal('modal_confirm_delete_warranty-policy', <ModalConfirm message='Bạn đang thao tác xóa chính sách bảo hành' handleConfirm={handleDelete}  />)
    
    const handleOpenModal = (modalName: string) => {
        openModal(modalName)
    }
    
return (
        <WarrantyPolicyListContext.Provider value={{ ...state, dispatch,searchValue, handleClearFilter, handleSearch,  handleDelete, handleOpenModal,idRef }}>{children}</WarrantyPolicyListContext.Provider>
    )
}