import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { mediaApi } from 'src/api-client'
import { WarrantyPolicysApi } from 'src/api-client/warranty'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'

export const defaultValues = {
    title: '',
    warrantyPolicyTypeId: '',
    content: '',
    status: '',
    thumbnail: []
}
export type FormValues = {
    title: string
    warrantyPolicyTypeId: string
    status: string
    content: string
    thumbnail: File[]
}

export const checkEditor = (field: string, value: string) => {
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(value, 'text/html');
    const textString = htmlDoc.body.textContent;
    console.log(textString);
    if (textString.trim() === '' || textString.trim().length > 0) return `Vui lòng nhập thông tin`;

    return '';
}
export const schema = yup.object().shape({
    title: yup.string().required('Vui lòng nhập thông tin!'),
    warrantyPolicyTypeId: yup.string().required('Vui lòng nhập thông tin!'),
    status: yup.string().required('Vui lòng nhập thông tin!'),
    content: yup.string().required(obj => checkEditor('editor', obj.value)),
    thumbnail: yup
        .mixed()
        .test('fileUpload', "Vui lòng chọn một file ảnh cho đèn cảnh báo!", (value) => { return value[0] })
        .test("fileSize", "Vui lòng tải hình ảnh có kích thước dưới 2mb!", (value) => {
            return value[0] && value[0].size <= 3000000;
        })
        .test("type", "Hệ thống chỉ hỗ trợ: .jpeg, .jpg, .bmp, .pdf and .doc", (value) => {
            return value[0] && (
                value[0].type === "image/jpeg" ||
                value[0].type === "image/bmp" ||
                value[0].type === "image/png" ||
                value[0].type === 'application/pdf' ||
                value[0].type === "application/msword"
            );
        }),

})


export const WarrantyCreateContext = React.createContext<any>({})

export default function WarrantyCreateProvider({ children }: { children: React.ReactNode }) {
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal, closeModal } = useModals();
    const [confirm, setConfirm] = React.useState(false)

    const handleClearForm = () => {
        form.reset(defaultValues)
        closeModal();
    }

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_create_success_warranty-policy', <ModalSuccess message='Bạn đã tạo chính sách bảo hành thành công!' handleConfirm={handleClearForm} />)
    registerModal('modal_create_error_warranty-policy', <ModalError message='Bạn đã tạo chính sách bảo hành thất bại!' />)

    const handleSave = async () => {
        try {
            openModal('modal_loading')
            const formData = new FormData();
            const imageData = form.getValues('thumbnail')[0]
            formData.append('Files', imageData);

            // upload image to server
            const newImg = await mediaApi.uploadImage(formData)
            if (newImg.data) {
                const params = {
                    title: form.getValues('title'),
                    warrantyPolicyTypeId: form.getValues('warrantyPolicyTypeId'),
                    status: form.getValues('status'),
                    content: form.getValues('content'),
                    thumbnail: newImg.data.filePath,
                }
                const result = await WarrantyPolicysApi.createWarrantyPolicys(params)
                if (result.data) {
                    openModal('modal_create_success_warranty-policy')
                }
                else {
                    openModal('modal_create_error_warranty-policy')
                }
            }
        } catch (e) {
            openModal('modal_create_error_warranty-policy')
        }
    }

    registerModal('modal_confirm_create_warranty-policy', <ModalConfirm message='Bạn đang thao tác tạo chính sách bảo hành' handleConfirm={handleSave} />)
    const handleConfirm = () => {
        openModal('modal_confirm_create_warranty-policy')
    }

    return (
        <WarrantyCreateContext.Provider value={{ form, handleConfirm, handleSave }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />
        </WarrantyCreateContext.Provider>
    )
}