import {
    IState,
    IAction
} from "./types"
import { IActionType as type } from './types'

export const reducer = (state: IState, action: IAction) => {
    switch (action.type) {
        case type.SET_TOTAL: {
            return {
                ...state,
                total: action.payload
            }
        }
        case type.SET_SORT: {
            return {
                ...state,
                sort: action.payload
            }
        }
        case type.SET_ROWS: {
            return {
                ...state,
                rows: action.payload
            }
        }
        case type.SET_FILTER_SEARCH: {
            return {
                ...state,
                filter: {
                    ...state.filter,
                    search: action.payload
                }
            }
        }
        case type.SET_SORT_COLUMN: {
            return {
                ...state,
                sortColumn: action.payload
            }
        }
        case type.SET_PAGINATION_MODEL: {
            return {
                ...state,
                paginationModel: action.payload
            }
        }

        case type.SET_FILTER_STATUS: {
            return {
                ...state,
                filter: {
                    ...state.filter,
                    status: action.payload
                }
            }
        }
        case type.SET_LOADING: {
            return {
                ...state,
                loading: action.payload
            }
        }
        case type.SET_FILTER_START_DATE:{
            return {
                ...state,
                filter: {
                    ...state.filter,
                    startDate: action.payload
                }
            }
        }
        case type.SET_FILTER_END_DATE:{
            return {
                ...state,
                filter: {
                    ...state.filter,
                    endDate: action.payload
                }
            }
        }

        default:
            return state
    }
}