import React from 'react'
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { GridRowSelectionModel } from '@mui/x-data-grid'
import { useRouter } from 'next/router';


export const FormContext = React.createContext<any>({})

export default function FormProvider({ children }: { children: React.ReactNode }) {

    //#region Table state
    const [tableQuestion, setTableQuestion] = React.useState([])
    const [tableQuestionOrigin, setTableQuestionOrigin] = React.useState([])
    const [questionsSelected, setQuestionSelected] = React.useState<GridRowSelectionModel>([]);
    const [openModalTable, setOpenModalTable] = React.useState(false);
    const [loading, setLoading]=React.useState(false);
    const [updateMode, setUpdateMode]=React.useState(false);
    const router = useRouter();
    const [originQuestion, setOriginQuestion]=React.useState([]);
    const { slugs } = router.query;
    const [questions, setQuestions]= React.useState([]);


    const onSelectedChange = React.useCallback(() => {
        const newQuestionTable = new Set(tableQuestionOrigin.filter(item => questionsSelected.includes(item.id)));
        setTableQuestion(Array.from(newQuestionTable));
    }, [questionsSelected, tableQuestionOrigin])

    React.useEffect(() => {
        onSelectedChange()
    }, [onSelectedChange])
    

    return (
        <FormContext.Provider value={{ questionsSelected,tableQuestionOrigin, setTableQuestionOrigin,questions, setQuestions,originQuestion, setOriginQuestion,slugs, updateMode, setUpdateMode, loading, setLoading, setQuestionSelected, tableQuestion, openModalTable, setOpenModalTable, setTableQuestion}}>{children}</FormContext.Provider>
    )
}