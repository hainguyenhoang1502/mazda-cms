import React, { useCallback } from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { IState, IProps } from '../question-management/types'
import { useModals } from 'src/context/ModalContext'
import { IActionType } from '../question-management/types'
import { reducer } from '../question-management/reducer'
import { surveysApi } from 'src/api-client/survey'
import { FormContext } from './formCtx'

const defaultValues: IState = {
    total: 0,
    sort: 'asc',
    rows: [],
    filter: {
        search: '',
        status: '',
    },
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    loading: false,
}

export const questionType = [
    {
        label: 'Chọn 1 đáp án',
        value: 1
    },
    {
        label: 'Chọn nhiều đáp án',
        value: 2
    },
    {
        label: 'Văn bản',
        value: 3
    }
]

export const statusOptions = [{ label: 'Tất cả', value: '' }, { label: 'Đang hoạt động', value: 'ACTIVE' }, { label: 'Ngưng hoạt động', value: 'INACTIVE' }]

export const TableQuestionModalCtx = React.createContext<any>({})

export default function TableQuestionModalProvider({ children }: IProps) {
    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const {
        paginationModel,
        filter
    } = state
    const idRef = React.useRef(null);
    const searchValue = React.useRef(null);
    const { registerModal, openModal } = useModals();

    const {
        // setTableQuestion,
        questionsSelected,
        setQuestionSelected,
        openModalTable,
        setOpenModalTable,
        handleDelete,
        updateMode,
        setOriginQuestion,
        slugs,
        questions,
        originQuestion,
        tableQuestionOrigin,
        setTableQuestionOrigin
    } = React.useContext(FormContext)

    //register modal
    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_delete_success_survey-management', <ModalSuccess message='Bạn đã thao tác xóa câu hỏi khảo sát thành công!' />)
    registerModal('modal_delete_error_survey-management', <ModalError message='Bạn đã thao tác xóa câu hỏi khảo sát thất bại!' />)
    registerModal('modal_create_success_survey-management', <ModalSuccess message='Bạn đã thêm câu hỏi khảo sát thành công!' />)
    registerModal('modal_create_error_survey-management', <ModalError message='Bạn đã thêm câu hỏi khảo sát thất bại!' />)


    //#region table modal
    //fetch data from api
    const fetchTableData = useCallback(
        async () => {
            dispatch({ type: IActionType.SET_LOADING, payload: true })
            await surveysApi.getListSurveysQuestion({
                keyword: filter.search,
                pageIndex: paginationModel.page + 1,
                pageSize: paginationModel.pageSize,
                ...filter.status === null ? {} : { status: filter.status }
            }).then(res => {
                if (res) {
                    dispatch({ type: IActionType.SET_TOTAL, payload: res.data.totalRecords })
                    const rows = res.data && res.data?.data?.length > 0 ? res.data.data : []
                    dispatch({ type: IActionType.SET_ROWS, payload: rows })
                    const newTableQuestions = [...questions,...tableQuestionOrigin, ...rows]
                    setTableQuestionOrigin(Array.from(new Set(newTableQuestions.map(obj => obj.id)))
                    .map(id => newTableQuestions.find(obj => obj.id === id)))

                    // setTableQuestion(Array.from(new Set(newTableQuestions.map(obj => obj.id)))
                    //     .map(id => newTableQuestions.find(obj => obj.id === id)))
                    dispatch({ type: IActionType.SET_LOADING, payload: false })
                }
            })
        },
        [paginationModel, filter]
    )

    React.useEffect(() => {
        fetchTableData()
    }, [fetchTableData])

    const handleSearch = () => {
        dispatch({ type: IActionType.SET_FILTER_SEARCH, payload: searchValue.current.value });
    };

    const handleClearFilter = () => {
        dispatch({ type: IActionType.SET_FILTER_SEARCH, payload: '' });
        searchValue.current.value="";
        dispatch({ type: IActionType.SET_FILTER_STATUS, payload: '' });
    }


    const handleOpenModal = (modalName: string) => {
        openModal(modalName)
    }

    //#endregion

    //#region table question 

    const handleAddQuestionAsync = React.useCallback(async () => {
        const newquestions = Array.from(new Set(questionsSelected.filter(item => !originQuestion.includes(item))));
        const res = await surveysApi.addSurveyQuestion({
            surveyId: slugs,
            questionIds: newquestions
        });
        if (res.data) {
            const newQuestions = Array.from(new Set([...originQuestion, ...newquestions]))
            setQuestionSelected(newQuestions)
            setOriginQuestion([...originQuestion, ...newQuestions])
            
        }
    }, [questionsSelected, state.rows])

    const handleAddQuestion = React.useCallback(async () => {
        const newquestions = Array.from(new Set(questionsSelected.filter(item => !originQuestion.includes(item))));

        if (updateMode) {
            handleAddQuestionAsync()
        }
        else {
            const newQuestions = Array.from(new Set([...originQuestion, ...newquestions]))
            setQuestionSelected(newQuestions)

            // setOriginQuestion([...originQuestion, ...newQuestions])
        }
        setOpenModalTable(false)

    }, [questionsSelected, state.rows])

    registerModal('modal_confirm_add_survey-question', <ModalConfirm message='Bạn đang thao tác thêm khảo sát' handleConfirm={handleAddQuestionAsync} />)

    return (
        <TableQuestionModalCtx.Provider value={{ ...state, questions,searchValue, openModalTable, handleAddQuestion, setOpenModalTable, dispatch, setQuestionSelected, questionsSelected, handleClearFilter, handleSearch, handleDelete, handleOpenModal, idRef }}>{children}</TableQuestionModalCtx.Provider>
    )
}