import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { IState, IProps } from '../question-management/types'
import { useModals } from 'src/context/ModalContext'
import { IActionType } from '../question-management/types'
import { reducer } from '../question-management/reducer'
import React from 'react'
import { FormContext } from './formCtx'
import { surveysApi } from 'src/api-client/survey'
import ModalConfirm from 'src/components/Modal/ModalConfirm'


const defaultValues: IState = {
    total: 0,
    sort: 'asc',
    rows: [],
    filter: {
        search: '',
        status: '',
    },
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    loading: false,
}

export const questionType = [
    {
        label: 'Chọn 1 đáp án',
        value: 1
    },
    {
        label: 'Chọn nhiều đáp án',
        value: 2
    },
    {
        label: 'Văn bản',
        value: 3
    }
]

export const statusOptions = [{ label: 'Tất cả', value: '' }, { label: 'Đang hoạt động', value: 'ACTIVE' }, { label: 'Ngưng hoạt động', value: 'INACTIVE' }]

export const TableQuestionCtx = React.createContext<any>({})

export default function TableQuestionProvider({ children }: IProps) {
    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const idRef = React.useRef(null);
    const { registerModal, openModal } = useModals();


    const {
        tableQuestion,
        setTableQuestion,
        questionsSelected,
        setQuestionSelected,
        openModalTable,
        setOpenModalTable,
        loading,
        updateMode,
        slugs
    } = React.useContext(FormContext);

    //register modal
    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_delete_success_survey-management', <ModalSuccess message='Bạn đã thao tác xóa câu hỏi khảo sát thành công!' />)
    registerModal('modal_delete_error_survey-management', <ModalError message='Bạn đã thao tác xóa câu hỏi khảo sát thất bại!' />)
    registerModal('modal_create_success_survey-management', <ModalSuccess message='Bạn đã thêm câu hỏi khảo sát thành công!' />)
    registerModal('modal_create_error_survey-management', <ModalError message='Bạn đã thêm câu hỏi khảo sát thất bại!' />)

    const handleSearch = (value: any) => {
        dispatch({ type: IActionType.SET_FILTER_SEARCH, payload: value });
    };

    const handleClearFilter = () => {
        dispatch({ type: IActionType.SET_FILTER_SEARCH, payload: '' });
        dispatch({ type: IActionType.SET_FILTER_STATUS, payload: '' });
    }


    const handleOpenModal = (modalName: string) => {
        openModal(modalName)
    }

    //#endregion

    //#region table question 
    const handleDelete = React.useCallback(async (id: number) => {
        console.log(idRef.current)
        if (updateMode) {
            openModal('modal_confirm_delete_survey-question')
        }
        else {
            setQuestionSelected(Array.from(new Set(questionsSelected.filter(item => item !== id))));
            setOpenModalTable(false)
        }
    }, [questionsSelected, state.rows])

    const handleDeleteAsync = React.useCallback(async () => {
        openModal('modal_loading')
        try{
            const res = await surveysApi.removeSurveysQuestion({
                surveyId: slugs,
                questionId: idRef.current as string
            });
            if (res.data) {
                setQuestionSelected(Array.from(new Set(questionsSelected.filter(item => item !== idRef.current))));
                openModal('modal_delete_success_survey-management')
                setOpenModalTable(false)
            }
            else{
                openModal('modal_error_success_survey-management')
            }
        }catch(error){
            openModal('modal_error_success_survey-management')
        }
    }, [questionsSelected, state.rows, idRef]);

    React.useEffect(()=>{
        registerModal('modal_confirm_delete_survey-question', <ModalConfirm message='Bạn đang thao tác xóa câu hỏi khảo sát' handleConfirm={handleDeleteAsync} />)
    },[handleDeleteAsync])

    //#endregion
    
    return (
            <TableQuestionCtx.Provider value={{ ...state, openModalTable, setTableQuestion, setOpenModalTable, dispatch, setQuestionSelected, tableQuestion, questionsSelected, handleClearFilter, handleSearch, handleDelete, handleOpenModal,loading, idRef }}>{children}</TableQuestionCtx.Provider>
    )
}