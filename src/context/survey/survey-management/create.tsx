import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { surveysApi } from 'src/api-client/survey'
import { checkEditor } from 'src/utils'
import { FormContext } from './formCtx'
import { useRouter } from 'next/router'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'

const defaultValues = {
    title: '',
    startDate: null,
    status: 'ACTIVE',
    endDate: null,
    questionIds: []
}
export type FormValues = {
    title: string,
    startDate: Date,
    endDate: Date,
    status: string
    questionIds: any[]
}



const schema = yup.object().shape({
    status: yup.string().required('Vui lòng chọn trường này!'),
    title: yup.string().required(obj => checkEditor('editor', obj.value)),
    startDate: yup.date().required('Vui lòng chọn ngày bắt đầu!').nullable(),
    endDate: yup.date().required('Vui lòng chọn ngày kết thúc!').nullable(),
    questionIds: yup.array().min(1).required('Vui lòng chọn ít nhất một câu hỏi!')
})

export const CreateContext = React.createContext<any>({})

export default function CreateProvider({ children }: { children: React.ReactNode }) {
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const router = useRouter()

    const { registerModal, openModal, closeModal } = useModals();
    const [answerCodes, setAnswerCodes] = React.useState([])
    const [confirm, setConfirm] = React.useState(false)
    const { questionsSelected } = React.useContext(FormContext);

    //#endregion
    const handleClearForm = () => {
        router.reload();
        closeModal();
    }

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_create_success_survey-question', <ModalSuccess message='Bạn đã tạo khảo sát thành công!' handleConfirm={handleClearForm} />)
    registerModal('modal_create_error_survey-question', <ModalError message='Bạn đã tạo khảo sát thất bại!' />)

    const handleAddQuestion = () => {
        openModal('modal_add_question_survey-management')
    }

    const handleSave = React.useCallback(async () => {
        try {
            openModal('modal_loading')
            const params = {
                title: form.getValues('title'),
                startDate: form.getValues('startDate'),
                status: form.getValues('status'),
                endDate: form.getValues('endDate'),
                questionIds: Array.from(new Set(form.getValues('questionIds'))),
            }
            const result = await surveysApi.createSurveys(params)
            if (result.data) {
                openModal('modal_create_success_survey-question')
            }
            else {
                openModal('modal_create_error_survey-question')
            }
        } catch (e) {
            openModal('modal_create_error_survey-question')
        }
    }, [openModal, form.watch(), answerCodes, setAnswerCodes])

    registerModal('modal_confirm_create_survey-question', <ModalConfirm message='Bạn đang thao tác tạo khảo sát' handleConfirm={handleSave} />)

    const handleConfirm = React.useCallback(() => {
        openModal('modal_confirm_create_survey-question')
    }, [registerModal])

    React.useEffect(() => {
        form.setValue('questionIds', questionsSelected)
    }, [questionsSelected])


    return (
        <CreateContext.Provider value={{ form, handleConfirm, handleSave, handleAddQuestion }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm}/>
        </CreateContext.Provider>
    )
}