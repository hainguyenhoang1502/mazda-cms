import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { surveysApi } from 'src/api-client/survey'
import { checkEditor } from 'src/utils'
import { FormContext } from './formCtx'
import { useRouter } from 'next/router'
import { Box, CircularProgress } from '@mui/material'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'

const defaultValues = {
    title: '',
    startDate: null,
    status: 'ACTIVE',
    endDate: null,
    questionIds: []
}
export type FormValues = {
    title: string,
    startDate: Date,
    endDate: Date,
    status: string
    questionIds: any[]
}



const schema = yup.object().shape({
    status: yup.string().required('Vui lòng chọn trường này!'),
    title: yup.string().required(obj => checkEditor('editor', obj.value)),
    startDate: yup.date().required('Vui lòng chọn ngày bắt đầu!').nullable(),
    endDate: yup.date().required('Vui lòng chọn ngày kết thúc!').nullable(),
    questionIds: yup.array().min(1).required('Vui lòng chọn ít nhất một câu hỏi!')
})

export const UpdateContext = React.createContext<any>({})

export default function UpdateProvider({ children }: { children: React.ReactNode }) {
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal, closeModal } = useModals();
    const [answerCodes, setAnswerCodes] = React.useState([]);
    const [confirm, setConfirm] = React.useState(false)
    const router = useRouter();
    const { slugs } = router.query;

    //#region Table state

    const { questionsSelected, setQuestions, setQuestionSelected, loading, setUpdateMode, setOriginQuestion, setLoading } = React.useContext(FormContext);

    //#endregion
    const handleClearForm = () => {
        form.reset(defaultValues)
        closeModal();
    }

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_update_success_survey-question', <ModalSuccess message='Bạn đã chỉnh sửa khảo sát thành công!' handleConfirm={handleClearForm} />)
    registerModal('modal_update_error_survey-question', <ModalError message='Bạn đã chỉnh sửa khảo sát thất bại!' />)

    const handleAddQuestion = () => {
        openModal('modal_add_question_survey-management')
    }

    const handleSave = React.useCallback(async () => {
        try {
            openModal('modal_loading')
            const params = {
                id: slugs,
                title: form.getValues('title'),
                startDate: form.getValues('startDate'),
                status: form.getValues('status'),
                endDate: form.getValues('endDate'),
                questionIds: form.getValues('questionIds'),
            }
            const result = await surveysApi.updateSurveys(params)
            if (result.data) {
                openModal('modal_update_success_survey-question')
            }
            else {
                openModal('modal_update_error_survey-question')
            }
        } catch (e) {
            openModal('modal_update_error_survey-question')
        }
    }, [openModal, form.watch(), answerCodes, setAnswerCodes])

    registerModal('modal_confirm_update_survey-question', <ModalConfirm message='Bạn đang thao tác chỉnh sửa khảo sát' handleConfirm={handleSave} />)

    const handleConfirm = React.useCallback(() => {
        openModal('modal_confirm_update_survey-question')
    }, [registerModal])

    React.useEffect(() => {
        form.setValue('questionIds', questionsSelected)
    }, [questionsSelected])

    const getInitialValues = React.useCallback(async () => {
        setUpdateMode(true)
        setLoading(true)
        const res = await surveysApi.getSurveysDetail(slugs);
        if (res.data) {
            form.reset({
                title: res.data.title,
                startDate: new Date(res.data.startDate),
                status: res.data.status,
                endDate: new Date(res.data.endDate),
                questionIds: res.data.questions.map(item => item.id)
            });
            setQuestionSelected(res.data.questions.map(item => item.id))
            const questions = res.data.questions.map(item => {
                return {
                    id: item.id,
                    name: item.name,
                    type: item.type,
                    isRequired: item.isRequired,
                    status: item.status,
                    answerNumber: item.answerNumber,
                    answers: []
                }
            })
            const uniqueQuestions = Array.from(new Set(questions.map(obj => obj.id)))
                .map(id => questions.find(obj => obj.id === id));
            setQuestions(Array.from(new Set(uniqueQuestions)));
            setOriginQuestion(res.data.questions.map(item => item.id))
            setLoading(false);
        }
    }, [slugs])

    React.useEffect(() => {
        getInitialValues()
    }, [getInitialValues, slugs])


    return (
        loading ?
            <Box sx={{ margin: '0 auto', width: '100%', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><CircularProgress /></Box> :
            <UpdateContext.Provider value={{ form, loading, setLoading, handleConfirm, handleSave, handleAddQuestion }}>
                {children}
                <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />

            </UpdateContext.Provider>
    )
}