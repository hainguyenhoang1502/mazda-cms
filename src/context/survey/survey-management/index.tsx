import React, { useCallback } from 'react'
import { debounce } from 'src/utils'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { IState, IProps } from './types'
import { useModals } from 'src/context/ModalContext'
import {IActionType} from './types'
import { reducer } from './reducer'
import { surveysApi } from 'src/api-client/survey'
import { format } from 'date-fns'


const defaultValues: IState = {
    total: 0,
    sort: 'asc',
    rows: [],
    filter: {
        search: '',
        status: '',
        startDate:null,
        endDate:null,
    },
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    loading: false,
}

export const questionType=[
    {
        label:'Chọn 1 đáp án',
        value:1
    },
    {
        label:'Chọn nhiều đáp án',
        value:2
    },
    {
        label:'Văn bản',
        value:3
    }
]

export const statusOptions=[{ label: 'Tất cả', value: '' }, { label: 'Đang hoạt động', value: 'ACTIVE' }, { label: 'Ngưng hoạt động', value: 'INACTIVE' }]

export const Context = React.createContext<any>({})

export default function ListProvider({ children }: IProps) {
    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const {
        paginationModel,
        filter 
    } = state
    const idRef=React.useRef(null);
    const searchValue=React.useRef(null);
    const { registerModal, openModal } = useModals();

    //register modal
    registerModal('modal_loading', <ModalLoading/>)
    registerModal('modal_delete_success_survey-management', <ModalSuccess message='Bạn đã thao tác xóa khảo sát thành công!'  />)
    registerModal('modal_delete_error_survey-management', <ModalError message='Bạn đã thao tác xóa khảo sát thất bại!'  />)

    //fetch data from api
    const fetchTableData = useCallback(
        async () => {
            console.log(filter.endDate)
            dispatch({ type: IActionType.SET_LOADING, payload: true })
            await surveysApi.getListSurveys({
                keyword: filter.search,
                pageIndex: paginationModel.page + 1,
                pageSize: paginationModel.pageSize,
                ...filter.status===null? {}:{status: filter.status},
                ...filter.startDate&&filter.startDate!==null? {fromDate:format(new Date(filter.startDate),"yyyy-MM-dd")}:{},
                ...filter.endDate&&filter.endDate!==null? {toDate: format(new Date(filter.endDate),"yyyy-MM-dd")}:{}
            }).then(res => {
                console.log(res);
                if (res) {
                    dispatch({ type: IActionType.SET_TOTAL, payload: res.data.totalRecords })
                    dispatch({ type: IActionType.SET_ROWS, payload: res.data&&res.data?.data?.length>0 ? res.data.data : [] })
                    dispatch({ type: IActionType.SET_LOADING, payload: false })
                }
            })
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [paginationModel, filter]
    )

    //debounce when search
    const debouncedFetchTableData = useCallback(
        debounce(fetchTableData, 500),
        [fetchTableData]
    )

    React.useEffect(() => {
        debouncedFetchTableData()
    }, [debouncedFetchTableData, filter])


    const handleDelete = async () => {
        openModal('modal_loading')
        try {
            console.log(idRef.current)
            const res = await surveysApi.deleteSurveys(idRef.current)
            if (res.data) {
                openModal('modal_delete_success_survey-management')
                fetchTableData()
            }
            else {
                openModal('modal_delete_error_survey-management')
            }
        } catch (e) {
            openModal('modal_delete_error_survey-management')
            console.log(e)
        }
    }

    const handleSearch = () => {
        dispatch({ type: IActionType.SET_FILTER_SEARCH, payload: searchValue.current.value });
    };


    const handleClearFilter = () => {
        dispatch({ type: IActionType.SET_FILTER_SEARCH, payload: '' });
        searchValue.current.value=""
        dispatch({ type: IActionType.SET_FILTER_STATUS, payload: '' });
    }

    registerModal('modal_confirm_delete_survey-management', <ModalConfirm message='Bạn đang thao tác xóa khảo sát' handleConfirm={handleDelete}  />)
    
    const handleOpenModal = (modalName: string) => {
        openModal(modalName)
    }
    
return (
        <Context.Provider value={{ ...state, dispatch,searchValue, handleClearFilter, handleSearch,  handleDelete, handleOpenModal,idRef }}>{children}</Context.Provider>
    )
}