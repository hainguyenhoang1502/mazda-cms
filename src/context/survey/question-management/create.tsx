import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { surveysApi } from 'src/api-client/survey'
import { checkEditor } from 'src/utils'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'

const defaultValues = {
    name: '',
    type: -1,
    status: 'ACTIVE',
    answerContent: '',
    answerCodes: [],
    required:'NOTREQUIRED',
    voteHighest:'',
    voteLowest:''
}
export type FormValues = {
    name: string,
    type: number,
    status: string,
    answerContent: string
    answerCodes: any[]
    required:string
    voteHighest:string
    voteLowest:string
}



const schema = yup.object().shape({
    type: yup.number().min(0,'Vui lòng nhập thông tin!').required('Vui lòng nhập thông tin!'),
    name: yup.string().required(obj => checkEditor('editor', obj.value)),
    status: yup.string().required('Vui lòng nhập thông tin!')
})

export const CreateContext = React.createContext<any>({})

export default function CreateProvider({ children }: { children: React.ReactNode }) {
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal, closeModal } = useModals();
    const [answerCodes, setAnswerCodes] = React.useState([])
    const [confirm, setConfirm] = React.useState(false)

    const handleClearForm = () => {
        form.reset(defaultValues)
        closeModal();
    }

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_create_success_survey-question', <ModalSuccess message='Bạn đã tạo câu hỏi khảo sát thành công!' handleConfirm={handleClearForm} />)
    registerModal('modal_create_error_survey-question', <ModalError message='Bạn đã tạo câu hỏi khảo sát thất bại!' />)

    const handleConfirm = () => {
        openModal('modal_confirm_create_survey-question')
    }
    const handleAddAnswer = () => {
        const answer = form.getValues('answerContent');
        if (answer.trim().length > 0) {
            const newArr = [...form.getValues('answerCodes'), answer.trim()];
            form.setValue('answerCodes', newArr)
        }
    }
    console.log(form.watch('required'))
    const handleRemoveAnswer = (id: number) => {
        const filteredAnswers = form.getValues('answerCodes').filter((item, index) => index !== id);
        form.setValue('answerCodes', filteredAnswers)
    }
    const handleSave = React.useCallback(async () => {
        try {
            openModal('modal_loading')
            const params = {
                name: form.getValues('name'),
                type: form.getValues('type'),
                status: form.getValues('status'),
                isRequired: form.getValues('required')==='NOTREQUIRED'?false:true,
                ...form.getValues('type') === 3 ? { answerNumber: 0 } : { answerNumber: form.getValues('answerCodes').length },
                ...form.getValues('type') === 4 ? { answerNumber: 2 } : { answerNumber: form.getValues('answerCodes').length },
                ...form.getValues('type') === 4 ? {answerCodes: [form.getValues('voteLowest'), form.getValues('voteHighest')]}:{answerCodes: form.getValues('answerCodes')},
            }

            const result = await surveysApi.createSurveysQuestion(params)
            if (result.data) {
                openModal('modal_create_success_survey-question')
            }
            else {
                openModal('modal_create_error_survey-question')
            }
        } catch (e) {
            openModal('modal_create_error_survey-question')
        }
    }, [openModal, form.watch(), answerCodes, setAnswerCodes])
    registerModal('modal_confirm_create_survey-question', <ModalConfirm message='Bạn đang thao tác tạo câu hỏi khảo sát' handleConfirm={handleSave} />)

    return (
        <CreateContext.Provider value={{ form, handleConfirm, handleSave, handleAddAnswer, handleRemoveAnswer }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />
        </CreateContext.Provider>
    )
}