import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { surveysApi } from 'src/api-client/survey'
import { useRouter } from 'next/router'
import { Box, CircularProgress } from '@mui/material'
import { checkEditor } from 'src/utils'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'

const defaultValues = {
    name: '',
    type: null,
    status: 'ACTIVE',
    answerContent: '',
    answerCodes: [],
    required: 'NOTREQUIRED',
    voteHighest: '',
    voteLowest: ''
}
export type FormValues = {
    name: string,
    type: number,
    status: string,
    answerContent: string
    answerCodes: any[]
    required: string
    voteHighest: string
    voteLowest: string
}

export interface SurveyQuestionDetailProps {
    id: number,
    name: string,
    type: number,
    isRequired: string,
    answerNumber: number,
    answers: {
        code: string,
        value: any
    }[],
    status: string
}

export interface UpdateProviderProps {
    children: React.ReactNode
}

const schema = yup.object().shape({
    type: yup.number().required('Vui lòng chọn trường này!'),
    name: yup.string().required(obj => checkEditor('editor', obj.value)),
    status: yup.string().required('Vui lòng chọn trường này!')
})

export const UpdateContext = React.createContext<any>({})

export default function UpdateProvider({ children }: UpdateProviderProps) {
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal, closeModal } = useModals();
    const router = useRouter();
    const { slugs } = router.query
    const [loading, setLoading] = React.useState(false);
    const [editing, setEditing] = React.useState(null);
    const [answerCodes, setAnswerCodes] = React.useState<any>([]);
    const [editItems, setEditItems] = React.useState([]);
    const [confirm, setConfirm] = React.useState(false);
    const handleClearForm = () => {
        form.reset(defaultValues)
        closeModal();
    }

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_update_success_survey-question', <ModalSuccess message='Bạn đã chỉnh sửa câu hỏi khảo sát thành công!' handleConfirm={handleClearForm} />)
    registerModal('modal_update_error_survey-question', <ModalError message='Bạn đã chỉnh sửa câu hỏi khảo sát thất bại!' />)

    const handleConfirm = () => {
        openModal('modal_confirm_update_survey-question')
    }

    const handleOnBlur = React.useCallback((index: number) => {
        console.log(editItems)
        if (!editItems.includes(index)) {
            setEditItems([...editItems, index]);
        }
    }, [editItems])

    const handleAddAnswer = React.useCallback(async () => {
        const answer = form.getValues('answerContent');
        if (answer.trim().length > 0) {
            const res = await surveysApi.createSurveysAnswer({
                value: answer,
                questionId: slugs
            }
            )
            if (res && res.data) {
                const newArr = [...form.getValues('answerCodes'), answer.trim()];
                form.setValue('answerCodes', newArr)
            }

        }
    }, [form, slugs])

    const handleRemoveAnswer = React.useCallback(async (id: number) => {
        const filteredAnswers = answerCodes.filter((item, index) => index === id)[0];
        console.log(filteredAnswers)
        const answer = form.getValues('answerCodes').filter((item, index) => index !== id)
        const res = await surveysApi.deleteSurveysAnswer(filteredAnswers.code)

        if (res && res.data) {
            form.setValue('answerCodes', answer)

        }
    }, [answerCodes, form])

    const handleEditAnswer = React.useCallback(async (id: number) => {
        setEditing(id);
        const code = answerCodes.filter((item, index) => index === id)[0];
        const value = form.getValues('answerCodes').filter((item, index) => index === id)[0];

        const res = await surveysApi.updateSurveysAnswer({
            code: code.code,
            value: value,
            questionId: slugs
        })

        if (res && res.data) {
            setEditing(null);
            setEditItems(prev => prev.filter(item => item !== id));
        }
    }, [form.watch('answerCodes')])

    const handleSave = React.useCallback(async () => {
        try {
            openModal('modal_loading')
            const params = {
                id: slugs,
                name: form.getValues('name'),
                type: form.getValues('type'),
                status: form.getValues('status'),
                isRequired: form.getValues('required') === 'NOTREQUIRED' ? false : true,
                ...form.getValues('type') === 3 ? { answerNumber: 0 } : { answerNumber: form.getValues('answerCodes').length },
                ...form.getValues('type') === 4 ? { answerNumber: 2 } : { answerNumber: form.getValues('answerCodes').length },
                ...form.getValues('type') === 4 ? {answerCodes: [form.getValues('voteLowest'), form.getValues('voteHighest')]}:{answerCodes: form.getValues('answerCodes')},
            }

            const result = await surveysApi.updateSurveysQuestion(params)
            if (result.data) {
                openModal('modal_update_success_survey-question')
            }
            else {
                openModal('modal_update_error_survey-question')
            }
        } catch (e) {
            openModal('modal_update_error_survey-question')
        }
    }, [openModal, form.watch()])

    registerModal('modal_confirm_update_survey-question', <ModalConfirm message='Bạn đang thao tác chỉnh sửa câu hỏi khảo sát' handleConfirm={handleSave} />)

    const getInitialValues = React.useCallback(async () => {
        setLoading(true);

        const data = await surveysApi.getSurveysQuestionDetail(slugs)
        if (data) {
            const formValues = {
                name: data.data.name,
                type: data.data.type,
                status: data.data.status,
                answerContent: '',
                answerCodes: data.data.answers.map(item => item.value),
                required: 'NOTREQUIRED',

            }
            form.reset(formValues);
            setAnswerCodes(data.data.answers)
            setLoading(false);
        }
        else {
            router.push('/404');
            setLoading(false);
        }
    }, [form, slugs])

    React.useEffect(() => {
        getInitialValues()
    }, [getInitialValues, slugs])

    return (
        loading ?
            <Box sx={{ margin: '0 auto', width: '100%', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><CircularProgress /></Box> :
            <UpdateContext.Provider value={{ form, editing, editItems, handleConfirm, handleOnBlur, handleEditAnswer, handleSave, handleAddAnswer, handleRemoveAnswer }}>
                {children}
                <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />
            </UpdateContext.Provider>
    )
}
