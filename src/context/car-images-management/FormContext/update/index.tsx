import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useRouter } from 'next/router'
import * as yup from 'yup'
import { format } from 'date-fns'
import { mediaApi } from 'src/api-client'
import { CarImageApi } from 'src/api-client/car-images-management'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'
import { formWithDetails } from 'src/HOCs/car-images-management'

export const defaultValues = {
    modelCode: '',
    gradeCode: '',
    colorCode: '',
    year: new Date(),
    image: [],
    status: ''
}
export type FormValues = {
    modelCode: string,
    gradeCode: string,
    colorCode: string,
    year: Date,
    image: File[],
    status: string
}

export const checkEditor = (field: string, value: string) => {
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(value, 'text/html');
    const textString = htmlDoc.body.textContent;
    console.log(textString);
    if (textString.trim() === '' || textString.trim().length > 0) return `Vui lòng nhập thông tin`;

    return '';
}
export const schema = yup.object().shape({
    modelCode: yup.string().required('Vui lòng nhập thông tin!'),
    gradeCode: yup.string().required('Vui lòng nhập thông tin!'),
    status: yup.string().required('Vui lòng nhập thông tin!'),
    colorCode: yup.string().required('Vui lòng nhập thông tin!'),
    year: yup.string().required('Vui lòng nhập thông tin!'),
    image: yup
        .mixed()
        .test('fileUpload', "Vui lòng chọn một file ảnh!", (value) => { return value[0] })
        .test("fileSize", "Vui lòng tải hình ảnh có kích thước dưới 2MB!", (value) => {
            return value[0] && value[0].size <= 3000000;
        })
        .test("type", "Hệ thống chỉ hỗ trợ: .jpeg, .jpg, .bmp, .pdf and .doc", (value) => {
            return value[0] && (
                value[0].type === "image/jpeg" ||
                value[0].type === "image/bmp" ||
                value[0].type === "image/png" ||
                value[0].type === 'application/pdf' ||
                value[0].type === "application/msword"
            );
        }),
})

export const FormUpdateContext = React.createContext<any>({})

 function FormUpdateProvider({ children, initialValues }: { children: React.ReactNode, initialValues:any }) {
    //hooks 

    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal } = useModals();
    const [loading, setLoading] = React.useState(false)
    const router = useRouter();
    const [confirm, setConfirm] = React.useState(false)
    const { slugs } = router.query

    // register modal

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_update_success_car-image', <ModalSuccess message='Bạn đã cập nhật hình ảnh xe thành công!' />)
    registerModal('modal_update_error_car-image', <ModalError message='Bạn đã cập nhật hình ảnh xe thất bại!' />)

    //handle save 

    const handleSave = async () => {
        try {
            openModal('modal_loading')

            // upload image to server
            const formData = new FormData();
            const imageData = form.getValues('image')[0]
            formData.append('Files', imageData);

            // upload image to server
            const newImg = await mediaApi.uploadImage(formData)
            const params = {
                id: slugs[0],
                modelCode: form.getValues('modelCode'),
                gradeCode: form.getValues('gradeCode'),
                status: form.getValues('status'),
                colorCode: form.getValues('colorCode'),
                year: format(new Date(form.getValues('year')), 'yyyy'),
                image: newImg.data.filePath,
            }
            const result = await CarImageApi.updateVehiclesImage(params)
            if (result.data) {
                getInitialValues()
                openModal('modal_update_success_car-image')
            }
            else {
                openModal('modal_update_error_car-image')
            }
        } catch (e) {
            openModal('modal_update_error_car-image')
        }
    }

    const getInitialValues = React.useCallback(async () => {
        if(initialValues){
            setLoading(true)
            form.reset({
                modelCode: initialValues.modelCode ,
                gradeCode: initialValues.gradeCode ,
                colorCode: initialValues.colorCode,
                year: initialValues.year ,
                image: initialValues.image,
                status: initialValues.status
            })
        setLoading(false)
        }
    }, [initialValues])

    React.useEffect(() => {
        getInitialValues()
    }, [getInitialValues, slugs])

    registerModal('modal_confirm_update_car-image', <ModalConfirm message='Bạn đang thao tác cập nhật hình ảnh xe' handleConfirm={handleSave} />)

    const handleConfirm = () => {
        openModal('modal_confirm_update_car-image')
    }

    return (
        <FormUpdateContext.Provider value={{ form, handleConfirm, handleSave, loading }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />
        </FormUpdateContext.Provider>
    )
}

export default formWithDetails(FormUpdateProvider)