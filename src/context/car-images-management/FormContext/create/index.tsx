import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { mediaApi } from 'src/api-client'
import { CarImageApi } from 'src/api-client/car-images-management'
import { format } from 'date-fns'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'
import { CarImageListContext } from '../../TableContext'
import { formWithDetails } from 'src/HOCs/car-images-management'


export const defaultValues = {
    modelCode: '',
    gradeCode: '',
    colorCode: '',
    year: '',
    image: [],
    status: ''
}

export type FormValues = {
    modelCode: string,
    gradeCode: string,
    colorCode: string,
    year: string,
    image: File[],
    status: string
}

export const checkEditor = (field: string, value: string) => {
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(value, 'text/html');
    const textString = htmlDoc.body.textContent;
    console.log(textString);
    if (textString.trim() === '' || textString.trim().length > 0) return `Vui lòng nhập thông tin`;

    return '';
}
export const schema = yup.object().shape({
    modelCode: yup.string().required('Vui lòng nhập thông tin!'),
    gradeCode: yup.string().required('Vui lòng nhập thông tin!'),
    status: yup.string().required('Vui lòng nhập thông tin!'),
    colorCode: yup.string().required('Vui lòng nhập thông tin!'),
    year: yup.string().required('Vui lòng nhập thông tin!'),
    image: yup
        .mixed()
        .test('fileUpload', "Vui lòng chọn một file ảnh!", (value) => { return value[0] })
        .test("fileSize", "Vui lòng tải hình ảnh có kích thước dưới 2MB!", (value) => {
            return value[0] && value[0].size <= 3000000;
        })
        .test("type", "Hệ thống chỉ hỗ trợ: .jpeg, .jpg, .bmp, .pdf and .doc", (value) => {
            return value[0] && (
                value[0].type === "image/jpeg" ||
                value[0].type === "image/bmp" ||
                value[0].type === "image/png" ||
                value[0].type === 'application/pdf' ||
                value[0].type === "application/msword"
            );
        }),
})


export const FormCreateContext = React.createContext<any>({})

function FormCreateProvider({ children, initialValues }: { children: React.ReactNode, initialValues:any }) {
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal, closeModal } = useModals();
    const [confirm, setConfirm] = React.useState(false);
    const { modelOpt, gradeOpt } = React.useContext(CarImageListContext)
    const [gradeOptsFilter, setGradeOptsFilter]=React.useState([])
    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_create_error_car-image', <ModalError message='Bạn đã tạo hình ảnh xe thất bại!' />)

    console.log(initialValues)

    const handleSave = async () => {

        try {
            openModal('modal_loading')
            const formData = new FormData();
            const imageData = form.getValues('image')[0]
            formData.append('Files', imageData);

            // upload image to server
            const newImg = await mediaApi.uploadImage(formData)
            if (newImg.data) {
                const params = {
                    modelCode: form.getValues('modelCode'),
                    gradeCode: form.getValues('gradeCode'),
                    status: form.getValues('status'),
                    colorCode: form.getValues('colorCode'),
                    year: format(new Date(form.getValues('year')), 'yyyy'),
                    image: newImg.data.filePath,
                }
                const result = await CarImageApi.createVehiclesImage(params)
                if (result.data) {
                    openModal('modal_create_success_car-image')
                }
                else {
                    openModal('modal_create_error_car-image')
                }
            }
        } catch (e) {
            openModal('modal_create_error_car-image')
        }

    }
    registerModal('modal_confirm_create_car-image', <ModalConfirm message='Bạn đang thao tác tạo hình ảnh xe' handleConfirm={handleSave} />)
    const handleConfirm = () => {
        openModal('modal_confirm_create_car-image')
    }

    const handleClearForm = () => {
        form.reset(defaultValues)
        closeModal();
    }
    registerModal('modal_create_success_car-image', <ModalSuccess message='Bạn đã tạo hình ảnh xe thành công!' handleConfirm={handleClearForm} />)


    const onModelCodeChange = React.useCallback(() => {
        const modelObj = modelOpt.filter((item: { value: string }) => item.value === form.watch('modelCode'))[0]
        const newGradeOpts = gradeOpt.filter((item: { label: string }) => modelObj.label?.toLowerCase().includes(item.label.toLowerCase()))
        setGradeOptsFilter(newGradeOpts);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [form.watch('modelCode')])

    React.useEffect(() => {
        onModelCodeChange()
    }, [onModelCodeChange])

    return (
        <FormCreateContext.Provider value={{ form, handleConfirm, handleSave, gradeOptsFilter }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />
        </FormCreateContext.Provider>
    )
}
export default formWithDetails(FormCreateProvider)