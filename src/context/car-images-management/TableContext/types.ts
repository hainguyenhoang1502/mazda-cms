export interface CarImageProps {
    children: React.ReactNode
}
export interface ActiveStatus {
    'ACTIVE': {
        color: 'green',
        title: 'Đang hoạt động'
    },
    'INACTIVE': {
        color: 'red',
        title: 'Không hoạt động'
    }
}
export interface CarImageListRowsType {
    id: number,
    name: string
    thumbnail: string
    description: string
    detailContent: string
    status: string
    createdUser: Date
    createdDate: Date
}
export interface ICarImagePageState {
    total: number
    sort: any
    rows: any[]
    filter: {
        search:string
        modelCode: string
        gradeCode: string
        colorCode: string
    }
    sortColumn: string
    paginationModel: { page: number, pageSize: number }
    loading: boolean

}

export enum ICarImagesActionType {
    SET_TOTAL = 'TOTAL',
    SET_SORT = 'SORT',
    SET_ROWS = 'ROWS',
    SET_SORT_COLUMN = 'SORT_COLUMN',
    SET_PAGINATION_MODEL = 'PAGINATION_MODEL',
    SET_LOADING = 'LOADING',
    SET_FILTER_MODELCODE = "SET_FILTER_MODELCODE",
    SET_FILTER_GRADECODE="SET_FILTER_GRADECODE",
    SET_FILTER_COLORCODE = "SET_FILTER_COLORCODE",
    SET_FILTER_SEARCH = "SET_FILTER_SEARCH",
}
export interface ICarImageAction {
    type: ICarImagesActionType
    payload: any
}
