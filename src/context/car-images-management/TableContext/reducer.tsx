import {
    ICarImagePageState,
    ICarImageAction
} from "./types"
import { ICarImagesActionType as type } from './types'

export const reducer = (state: ICarImagePageState, action: ICarImageAction) => {
    switch (action.type) {
        case type.SET_TOTAL: {
            return {
                ...state,
                total: action.payload
            }
        }
        case type.SET_SORT: {
            return {
                ...state,
                sort: action.payload
            }
        }
        case type.SET_ROWS: {
            return {
                ...state,
                rows: action.payload
            }
        }
        case type.SET_FILTER_SEARCH: {
            return {
                ...state,
                filter: {
                    ...state.filter,
                    search: action.payload
                }
            }
        }
        case type.SET_SORT_COLUMN: {
            return {
                ...state,
                sortColumn: action.payload
            }
        }
        case type.SET_PAGINATION_MODEL: {
            return {
                ...state,
                paginationModel: action.payload
            }
        }
        case type.SET_FILTER_MODELCODE: {
            return {
                ...state,
                filter: {
                    ...state.filter,
                    modelCode: action.payload
                }
            }
        }
        case type.SET_FILTER_COLORCODE: {
            return {
                ...state,
                filter: {
                    ...state.filter,
                    colorCode: action.payload
                }
            }
        }
        case type.SET_FILTER_GRADECODE: {
            return {
                ...state,
                filter: {
                    ...state.filter,
                    gradeCode: action.payload
                }
            }
        }
        case type.SET_LOADING: {
            return {
                ...state,
                loading: action.payload
            }
        }

        default:
            return state
    }
}