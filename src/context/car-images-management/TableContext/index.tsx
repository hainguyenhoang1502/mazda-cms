import React, { useCallback, useState } from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { ICarImagePageState, CarImageProps } from './types'
import { useModals } from 'src/context/ModalContext'
import { ICarImagesActionType as type } from './types'
import { reducer } from './reducer'
import { CarImageApi } from 'src/api-client/car-images-management'

const defaultValues: ICarImagePageState = {
    total: 0,
    sort: 'asc',
    rows: [],
    filter: {
        search: '',
        gradeCode: '',
        modelCode: '',
        colorCode: ''
    },
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    loading: false,
}

export const WarrantyTypeObj = [
    {
        label: 'lịch hẹn dịch vụ xe mới',
        value: 'NEWCAR'
    },
    {
        label: "Phụ tùng thay thế",
        value: 'SPAREPART'
    }
]



export const CarImageListContext = React.createContext<any>({})

export default function CarImageListProvider({ children }: CarImageProps) {
    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const { paginationModel, filter } = state
    const idRef = React.useRef(null);
    const searchValue = React.useRef(null);
    const { registerModal, openModal } = useModals();
    const [modelOpt, setModelOpt] = useState([]);
    const [gradeOpt, setGradeOpt] = useState([]);
    const [colorOpt, setColorOpt] = useState([]);

    //register modal

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_delete_success_car-image', <ModalSuccess message='Bạn đã thao tác xóa hình ảnh xe thành công!' />)
    registerModal('modal_delete_error_car-image', <ModalError message='Bạn đã thao tác xóa hình ảnh xe thất bại!' />)

    //fetch data from api

    const fetchTableData =
        useCallback(async () => {
            dispatch({ type: type.SET_LOADING, payload: true })
            const params = {
                keyword: filter.search,
                pageIndex: paginationModel.page + 1,
                pageSize: paginationModel.pageSize,
                ...filter.modelCode !== 'Tất cả' && filter.modelCode.length > 0 ? { modelCode: filter.modelCode } : {},
                ...filter.gradeCode !== 'Tất cả' && filter.gradeCode.length > 0 ? { gradeCode: filter.gradeCode } : {},
                ...filter.colorCode !== 'Tất cả' && filter.colorCode.length > 0 ? { colorCode: filter.colorCode } : {}
            }
            await CarImageApi.getListVehiclesImage(params).then(res => {
                console.log(res);
                if (res) {
                    dispatch({ type: type.SET_TOTAL, payload: res.data.totalRecords })
                    dispatch({ type: type.SET_ROWS, payload: res.data?.details ? res.data.details : [] })
                    dispatch({ type: type.SET_LOADING, payload: false })
                }
            })
        }, [filter, paginationModel])




    const getOptions = async () => {
        const [listModel, listGrade, listColor] = await Promise.all([
            CarImageApi.getListModelCode(),
            CarImageApi.getListGradeCode(),
            CarImageApi.getListColorCode(),
        ]);
        if (listModel && listModel.data) {
            const options = listModel.data.map(item => {
                return { value: item.modelCode, label: item.modelName }
            })
            setModelOpt([{ label: 'Tất cả', value: 'Tất cả' }, ...options])
        }
        if (listGrade && listGrade.data) {
            const options = listGrade.data.map(item => {
                return { value: item.gradeCode, label: item.gradeName }
            })
            setGradeOpt([{ label: 'Tất cả', value: 'Tất cả' }, ...options])
        }
        if (listColor && listColor.data) {
            const options = listColor.data.map(item => {
                return { label: item.colorNameVi, value: item.colorCode }
            })
            setColorOpt([{ label: 'Tất cả', value: 'Tất cả' }, ...options])
        }
    }

    //debounce when search

    React.useEffect(() => {
        fetchTableData()
    }, [fetchTableData])

    React.useEffect(() => {
        getOptions()
    }, [])

    const handleDelete = async () => {
        openModal('modal_loading')
        try {
            console.log(idRef.current)
            const res = await CarImageApi.deleteVehiclesImage(idRef.current)
            if (res.data) {
                openModal('modal_delete_success_car-image')
                fetchTableData()
            }
            else {
                openModal('modal_delete_error_car-image')
            }
        } catch (e) {
            openModal('modal_delete_error_car-image')
            console.log(e)
        }
    }

    const handleSearch = () => {
        dispatch({ type: type.SET_FILTER_SEARCH, payload: searchValue.current.value });
    };



    const handleClearFilter = () => {
        dispatch({ type: type.SET_FILTER_SEARCH, payload: '' });
        dispatch({ type: type.SET_FILTER_MODELCODE, payload: '' });
        dispatch({ type: type.SET_FILTER_GRADECODE, payload: '' });
        searchValue.current.value = "";
        dispatch({ type: type.SET_FILTER_COLORCODE, payload: '' });
    }

    registerModal('modal_confirm_delete_car-image', <ModalConfirm message='Bạn đang thao tác xóa hình ảnh xe' handleConfirm={handleDelete} />)

    const handleOpenModal = (modalName: string) => {
        openModal(modalName)
    }

    return (
        <CarImageListContext.Provider value={{ ...state, searchValue, setGradeOpt, dispatch, handleClearFilter, handleSearch, handleDelete, handleOpenModal, idRef, modelOpt, gradeOpt, colorOpt }}>{children}</CarImageListContext.Provider>
    )
}