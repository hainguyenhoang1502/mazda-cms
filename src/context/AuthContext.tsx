// ** React Imports
import { createContext, useEffect, useState, ReactNode } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** Config
import authConfig from 'src/configs/auth'
import { ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'

// ** Types
import { AuthValuesType } from './types'

// ** Utils
import { eraseCookie, getCookie, setCookie } from 'src/@core/utils/cookie'

// ** Apis
import { IParamLogin } from 'src/api/authen/type'
import { ApiLogin, ApiLogout } from 'src/api/authen'

// ** Component Imports
import ModalNotify from 'src/views/component/modalNotify'

// ** Store Redux Imports
import { setAccessToken } from 'src/store/utils/accessToken'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetProfile } from 'src/store/apps/profile'
import { setValueBackrop } from 'src/store/utils/loadingBackdrop'
import axiosClient from 'src/api-client/axios-client'


// ** Defaults
const defaultProvider: AuthValuesType = {
  token: getCookie(authConfig.tokenKeyName),
  loading: true,
  setLoading: () => Boolean,
  login: () => Promise.resolve(),
  logout: () => Promise.resolve()
}

const AuthContext = createContext(defaultProvider)

type Props = {
  children: ReactNode
}

const AuthProvider = ({ children }: Props) => {

  // ** States
  const [loading, setLoading] = useState<boolean>(defaultProvider.loading)
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)

  // ** Hooks
  const router = useRouter()

  // ** Store 
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.accessToken)

  useEffect(() => {
    const initAuth = async (): Promise<void> => {
      const storedToken = getCookie(authConfig.tokenKeyName)
      if (storedToken) {
        setLoading(true)
        dispatch(setAccessToken(storedToken))
        await dispatch(fetchDataGetProfile())
        setLoading(false)
      } else {
        setLoading(false)
      }
    }

    initAuth()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  const handleLogin = async (params: IParamLogin, ErrorFallback: () => void) => {

    try {
      dispatch(setValueBackrop(true))
      const res = await ApiLogin(params)
      if (res.code === 200 && res.result && res.data) {
        const token = `Bearer ${res.data.access_token}`;
        setCookie(authConfig.tokenKeyName, token);
        axiosClient.defaults.headers['Authorization'] = token;
        dispatch(setAccessToken(token))
        const returnUrl = router.query.returnUrl
        const redirectURL = returnUrl && returnUrl !== '/' ? returnUrl : '/'
        router.replace(redirectURL as string)
        dispatch(setValueBackrop(false))
        await dispatch(fetchDataGetProfile())
      }
      else {
        dispatch(setValueBackrop(false))
        setModalNotify({
          isOpen: true,
          title: "Đăng nhập thất bại",
          message: res.message,
          isError: true
        })
        if (ErrorFallback) { ErrorFallback() }
      }
    } catch (error) {
      if (ErrorFallback) { ErrorFallback() }
    }
  }

  const handleLogout = async () => {
    try {
      dispatch(setValueBackrop(true))
      const res = await ApiLogout()
      if (res.code === 200 && res.result) {
        dispatch(setAccessToken(null))
        eraseCookie(authConfig.tokenKeyName)
        router.push('/login', "/login")
        dispatch(setValueBackrop(false))
      } else {
        dispatch(setValueBackrop(false))
      }
    } catch (error) {
    }
  }

  const renderModalNotify = () => {
    return (
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    )
  }
  const values = {
    token: store.accessToken,
    loading,
    setLoading,
    login: handleLogin,
    logout: handleLogout
  }

  return <AuthContext.Provider value={values}>{renderModalNotify()}{children}</AuthContext.Provider>
}

export { AuthContext, AuthProvider }
