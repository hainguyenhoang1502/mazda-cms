import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'

export const defaultValues = {
    contactName: '',
    driverPhoneNumber: '',
    customerName: '',
    customerPhoneNumber: '',
    dealerCode: '',
    vinCode: '',
    plateNumber: '',
    describeSituation: '',
    customerRequirements: '',
    itemsToDoSoonCode: '',
    maintenancePackageCode: '',
    appointmentTime: new Date(),
    status: ''
}
export type FormValues = {
    contactName: string,
    driverPhoneNumber: string,
    customerName: string,
    customerPhoneNumber: string,
    dealerCode: string,
    vinCode: string,
    plateNumber: string,
    describeSituation: string,
    customerRequirements: string,
    itemsToDoSoonCode: string,
    maintenancePackageCode: string,
    appointmentTime: Date,
    status: string
}

export const checkEditor = (field: string, value: string) => {
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(value, 'text/html');
    const textString = htmlDoc.body.textContent;
    console.log(textString);
    if (textString.trim() === '' || textString.trim().length > 0) return `Vui lòng nhập thông tin`;

    return '';
}
export const schema = yup.object().shape({
    contactName: yup.string().required('Vui lòng nhập thông tin!'),
    driverPhoneNumber: yup.string().required('Vui lòng nhập thông tin!'),
    status: yup.string().required('Vui lòng nhập thông tin!'),
    vinCode: yup.string().required('Vui lòng nhập thông tin!'),
    describeSituation: yup.string().required('Vui lòng nhập thông tin!'),
    itemsToDoSoonCode: yup.string().required('Vui lòng nhập thông tin!'),
    maintenancePackageCode: yup.string().required('Vui lòng nhập thông tin!'),
    appointmentTime: yup.date().min(new Date(), 'Vui lòng chọn ngày ở tương lai').required('Vui lòng nhập thông tin!'),
    dealerCode: yup.string().required('Vui lòng nhập thông tin!'),
})


export const FormCreateContext = React.createContext<any>({})

export default function FormCreateProvider({ children }: { children: React.ReactNode }) {
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal } = useModals();
    const [confirm, setConfirm] = React.useState(false)

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_create_success_appointment', <ModalSuccess message='Bạn đã tạo lịch đặt hẹn thành công!' />)
    registerModal('modal_create_error_appointment', <ModalError message='Bạn đã tạo lịch đặt hẹn thất bại!' />)

    const handleSave = async () => {
        console.log(form)

        // try {
        //     openModal('modal_loading')
        //     const formData = new FormData();
        //     const imageData = form.getValues('thumbnail')[0]
        //     formData.append('Files', imageData);

        //     // upload image to server
        //     const newImg = await mediaApi.uploadImage(formData)
        //     if (newImg.data) {
        //         const params = {
        //             title: form.getValues('title'),
        //             warrantyPolicyTypeId: form.getValues('warrantyPolicyTypeId'),
        //             status: form.getValues('status'),
        //             content: form.getValues('content'),
        //             thumbnail: newImg.data.filePath,
        //         }
        //         const result = await WarrantyPolicysApi.createWarrantyPolicys(params)
        //         if (result.data) {
        //             openModal('modal_create_success_warranty-policy')
        //         }
        //         else {
        //             openModal('modal_create_error_warranty-policy')
        //         }
        //     }
        // } catch (e) {
        //     openModal('modal_create_error_warranty-policy')
        // }

    }
    registerModal('modal_confirm_create_appointment', <ModalConfirm message='Bạn đang thao tác tạo lịch đặt hẹn' handleConfirm={handleSave} />)
    const handleConfirm = () => {
        openModal('modal_confirm_create_appointment')
    }

    return (
        <FormCreateContext.Provider value={{ form, handleConfirm, handleSave }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />

        </FormCreateContext.Provider>
    )
}