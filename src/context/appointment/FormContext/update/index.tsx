/* eslint-disable */
import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useRouter } from 'next/router'
import * as yup from 'yup'
import { appointmentApi } from 'src/api-client/appointment'
import { convertToCurrentTime } from 'src/utils'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'

export const defaultValues = {
    contactName: '',
    driverPhoneNumber: '',
    customerRequirements: '',
    appointmentTime: new Date(),
    status: ''
}
export type FormValues = {
    contactName: string
    driverPhoneNumber: string
    customerRequirements: string
    appointmentTime: Date
    status: string
}

export const schema = yup.object().shape({
    contactName: yup.string().required('Vui lòng nhập thông tin!'),
    driverPhoneNumber: yup.string().required('Vui lòng nhập thông tin!'),
    customerRequirements: yup.string().required('Vui lòng nhập thông tin!'),
    appointmentTime: yup.date().required('Vui lòng nhập thông tin!').min(new Date(), 'Ngày hẹn phải là một ngày trong tương lai!')
})

export const FormUpdateContext = React.createContext<any>({})

export default function FormUpdateProvider({ children }: { children: React.ReactNode }) {
    //hooks 

    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal } = useModals();
    const [loading, setLoading] = React.useState(false)
    const router = useRouter();
    const [confirm, setConfirm] = React.useState(false)
    const { slugs } = router.query

    // register modal

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_update_success_appointment', <ModalSuccess message='Bạn đã xác nhận lịch đặt hẹn thành công!' />)
    registerModal('modal_update_error_appointment', <ModalError message='Bạn đã xác nhận lịch đặt hẹn thất bại!' />)

    //handle save 

    const handleSave = async () => {
        try {
            openModal('modal_loading')

            // upload image to server
            const params = {
                id: slugs,
                contactName: form.getValues('contactName'),
                driverPhoneNumber: form.getValues('driverPhoneNumber'),
                customerRequirements: form.getValues('customerRequirements'),
                appointmentTime: convertToCurrentTime(form.getValues('appointmentTime')),
            }
            const result = await appointmentApi.updateAppointment(params)
            if (result.data) {
                getInitialValues()
                openModal('modal_update_success_appointment')
            }
            else {
                openModal('modal_update_error_appointment')
            }
        } catch (e) {
            openModal('modal_update_error_appointment')
        }
    }

    const getInitialValues = async () => {
        setLoading(true)
        const res = await appointmentApi.getappointmentDetail(slugs)
        if (res.data) {
            console.log(res.data)
            form.reset({
                contactName: res.data.contactName ? res.data.contactName : '',
                driverPhoneNumber: res.data.driverPhoneNumber ? res.data.driverPhoneNumber : '',
                customerRequirements: res.data.customerRequirements ? res.data.customerRequirements : '',
                appointmentTime: res.data.appointmentTime ? new Date(res.data.appointmentTime) : new Date(),
                status: res.data.status
            })
        }
        else {
            router.push('/404')
        }
        setLoading(false)
    }
    React.useEffect(() => {

        getInitialValues()
    }, [slugs])
    registerModal('modal_confirm_update_appointment', <ModalConfirm message='Bạn đang thao tác xác nhận lịch đặt hẹn' handleConfirm={handleSave} />)
    const handleConfirm = () => {
        openModal('modal_confirm_update_appointment')
    }

    return (
        <FormUpdateContext.Provider value={{ form, handleConfirm, handleSave, loading }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />

        </FormUpdateContext.Provider>
    )
}
/* eslint-enable */