export interface AppointmentProps {
    children: React.ReactNode
}
export interface ActiveStatus {
    'ACTIVE': {
        color: 'green',
        title: 'Đang hoạt động'
    },
    'INACTIVE': {
        color: 'red',
        title: 'Không hoạt động'
    }
}
export interface AppointmentListRowsType {
    id: number,
    name: string
    thumbnail: string
    description: string
    detailContent: string
    status: string
    createdUser: Date
    createdDate: Date
}
export interface IAppointmentPageState {
    total: number
    sort: any
    rows: any[]
    filter: {
        search: string
        status: string
        booking: Date
    }
    sortColumn: string
    paginationModel: { page: number, pageSize: number }
    loading: boolean

}

export enum IAppointmentsActionType {
    SET_TOTAL = 'TOTAL',
    SET_SORT = 'SORT',
    SET_ROWS = 'ROWS',
    SET_SORT_COLUMN = 'SORT_COLUMN',
    SET_PAGINATION_MODEL = 'PAGINATION_MODEL',
    SET_LOADING = 'LOADING',
    SET_FILTER_STATUS = "SET_FILTER_STATUS",
    SET_FILTER_BOOKING="SET_FILTER_BOOKING",
    SET_FILTER_SEARCH = "SET_FILTER_SEARCH",
}
export interface IAppointmentAction {
    type: IAppointmentsActionType
    payload: any
}
