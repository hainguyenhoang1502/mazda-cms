import {
    IAppointmentPageState,
    IAppointmentAction
} from "./types"
import { IAppointmentsActionType as type } from './types'

export const reducer = (state: IAppointmentPageState, action: IAppointmentAction) => {
    switch (action.type) {
        case type.SET_TOTAL: {
            return {
                ...state,
                total: action.payload
            }
        }
        case type.SET_SORT: {
            return {
                ...state,
                sort: action.payload
            }
        }
        case type.SET_ROWS: {
            return {
                ...state,
                rows: action.payload
            }
        }
        case type.SET_FILTER_SEARCH: {
            return {
                ...state,
                filter: {
                    ...state.filter,
                    search: action.payload
                }
            }
        }
        case type.SET_SORT_COLUMN: {
            return {
                ...state,
                sortColumn: action.payload
            }
        }
        case type.SET_PAGINATION_MODEL: {
            return {
                ...state,
                paginationModel: action.payload
            }
        }
        case type.SET_FILTER_STATUS: {
            return {
                ...state,
                filter: {
                    ...state.filter,
                    status: action.payload
                }
            }
        }
        case type.SET_FILTER_BOOKING: {
            return {
                ...state,
                filter: {
                    ...state.filter,
                    booking: action.payload
                }
            }
        }
        case type.SET_LOADING: {
            return {
                ...state,
                loading: action.payload
            }
        }

        default:
            return state
    }
}