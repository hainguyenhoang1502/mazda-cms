import React, { useCallback } from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { IAppointmentPageState, AppointmentProps } from './types'
import { useModals } from 'src/context/ModalContext'
import { IAppointmentsActionType as type } from './types'
import { reducer } from './reducer'
import { appointmentApi } from 'src/api-client/appointment'
import { format } from 'date-fns'

const defaultValues: IAppointmentPageState = {
    total: 0,
    sort: 'asc',
    rows: [],
    filter: {
        status: '',
        search: '',
        booking: null
    },
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    loading: false,

}

export const WarrantyTypeObj = [
    {
        label: 'lịch hẹn dịch vụ xe mới',
        value: 'NEWCAR'
    },
    {
        label: "Phụ tùng thay thế",
        value: 'SPAREPART'
    }
]



export const AppointmentListContext = React.createContext<any>({})

export default function AppointmentListProvider({ children }: AppointmentProps) {
    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const {
        rows,
        paginationModel,
        filter } = state
    const idRef = React.useRef(null);
    const searchValue=React.useRef(null);
    const paramUpdate = React.useRef(null);
    const { registerModal, openModal } = useModals();

    //register modal

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_delete_success_appointment', <ModalSuccess message='Bạn đã thao tác xóa lịch hẹn dịch vụ thành công!' />)
    registerModal('modal_delete_error_appointment', <ModalError message='Bạn đã thao tác xóa lịch hẹn dịch vụ thất bại!' />)
    registerModal('modal_update_success_appointment', <ModalSuccess message='Bạn đã thao tác cập nhật lịch hẹn dịch vụ thành công!' />)
    registerModal('modal_update_error_appointment', <ModalError message='Bạn đã thao tác cập nhật lịch hẹn dịch vụ thất bại!' />)
    registerModal('modal_save_success_appointment', <ModalSuccess message='Bạn đã lưu lịch hẹn dịch vụ thành công!' />)
    registerModal('modal_save_error_appointment', <ModalError message='Bạn đã lưu lịch hẹn dịch vụ thất bại!' />)

    //fetch data from api

    const fetchTableData = 
        useCallback(async () => {
            dispatch({ type: type.SET_LOADING, payload: true })
            const params = {
                keyword: filter.search,
                pageIndex: paginationModel.page + 1,
                pageSize: paginationModel.pageSize,
                ...filter.status!=='Tất cả'&&filter.status ? { status: filter.status } : {},
                ...filter.booking!==null ? { createdDate: filter.booking} : {}
            }
            await appointmentApi.getListAppointment(params).then(res => {
                console.log(res);
                if (res) {
                    dispatch({ type: type.SET_TOTAL, payload: res.data.totalRecords })
                    dispatch({ type: type.SET_ROWS, payload: res.data?.result ? res.data.result : [] })
                    dispatch({ type: type.SET_LOADING, payload: false })
                }
            })
        },[filter, paginationModel])
        // eslint-disable-next-line react-hooks/exhaustive-deps
        // const debouncedFetchTableData = useCallback(
        //     debounce(fetchTableData, 600),
        //     [fetchTableData]
        // )
    const appointmentTimeOptions=['Tất cả',...new Set(rows.map(item=>format(new Date(item.appointmentTime), "dd/MM/yyyy")))].map((item)=>{return {label:item, value:item}})
    
    //debounce when search
    
    React.useEffect(() => {
        fetchTableData()
    }, [fetchTableData])

    const handleDelete = async () => {
        openModal('modal_loading')
        try {
            console.log(idRef.current)
            const res = await appointmentApi.deleteappointment(idRef.current)
            if (res.data) {
                openModal('modal_delete_success_appointment')
                fetchTableData()
            }
            else {
                openModal('modal_delete_error_appointment')
            }
        } catch (e) {
            openModal('modal_delete_error_appointment')
            console.log(e)
        }
    }

    const handleSearch = () => {
        dispatch({ type: type.SET_FILTER_SEARCH, payload: searchValue.current.value });
    };

    const handleUpdateStatus = async () => {
        openModal('modal_loading')
        try {
            console.log(idRef.current)
            const res = await appointmentApi.updateAppointmentStatus(paramUpdate.current)
            if (res.data) {
                openModal('modal_update_success_appointment')
                fetchTableData()
            }
            else {
                openModal('modal_update_error_appointment')
            }
        } catch (e) {
            openModal('modal_update_error_appointment')
            console.log(e)
        }
    }

    const handleClearFilter = () => {
        dispatch({ type: type.SET_FILTER_SEARCH, payload: '' });
        searchValue.current.value="";
        dispatch({ type: type.SET_FILTER_STATUS, payload: '' });
    }

    registerModal('modal_confirm_delete_appointment', <ModalConfirm message='Bạn đang thao tác xóa lịch đặt hẹn' handleConfirm={handleDelete} />)
    registerModal('modal_confirm_update_appointment', <ModalConfirm message='Bạn đang thao tác cập nhật lịch đặt hẹn' handleConfirm={handleUpdateStatus} />)

    const handleOpenModal = (modalName: string) => {
        openModal(modalName)
    }

    return (
        <AppointmentListContext.Provider value={{ ...state,searchValue, paramUpdate, dispatch,appointmentTimeOptions, handleClearFilter, handleSearch, handleDelete, handleOpenModal, idRef }}>{children}</AppointmentListContext.Provider>
    )
}