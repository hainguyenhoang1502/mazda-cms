export interface SignalLightsProps {
    children: React.ReactNode
}
export interface ActiveStatus {
    'ACTIVE':{
        color: 'green',
        title:'Đang hoạt động'
    },
    'INACTIVE':{
        color:'red',
        title:'Không hoạt động'
    }
}
export interface SignalLightsListRowsType {
    id: number,
    name: string
    thumbnail: string
    description: string
    detailContent: string
    status: string
    createdUser: Date
    createdDate: Date
}
export interface ISignalLightsPageState {
    total: number
    sort: any
    rows: SignalLightsListRowsType[]
    search: string
    sortColumn: string
    paginationModel: { page: number, pageSize: number }
    status: string
    loading: boolean
}

export enum ISignalLightsActionType {
    SET_TOTAL = 'TOTAL',
    SET_SORT = 'SORT',
    SET_ROWS = 'ROWS',
    SET_SEARCH_VALUE = 'SEARCH_VALUE',
    SET_SORT_COLUMN = 'SORT_COLUMN',
    SET_PAGINATION_MODEL = 'PAGINATION_MODEL',
    SET_STATUS = 'STATUS',
    SET_LOADING = 'LOADING',
}
export interface ISignalLightsAction {
    type: ISignalLightsActionType
    payload: any
}
