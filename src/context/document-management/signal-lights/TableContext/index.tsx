import React, { useCallback } from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { ISignalLightsPageState, SignalLightsProps } from './types'
import { useModals } from 'src/context/ModalContext'
import {ISignalLightsActionType as type} from './types'
import { reducer } from './reducer'
import { signalLightsApi } from 'src/api-client/documents'


const defaultValues: ISignalLightsPageState = {
    total: 0,
    sort: 'asc',
    rows: [],
    search: '',
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    status: '',
    loading: false,
}
export const statusOptions=[{ label: 'Tất cả', value: '' }, { label: 'Đang hoạt động', value: 'ACTIVE' }, { label: 'Ngưng hoạt động', value: 'INACTIVE' }]

export const SignalLightsContext = React.createContext<any>({})

export default function SignalLightsListProvider({ children }: SignalLightsProps) {
    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const {
        search,
        paginationModel,
        status } = state
    const idRef=React.useRef(null);
    const searchValue= React.useRef(null);
    const { registerModal, openModal } = useModals();

    //register modal
    registerModal('modal_loading', <ModalLoading/>)
    registerModal('modal_delete_success_signal-light', <ModalSuccess message='Bạn đã thao tác xóa đèn cảnh báo thành công!'  />)
    registerModal('modal_delete_error_signal-light', <ModalError message='Bạn đã thao tác xóa đèn cảnh báo thất bại!'  />)
    registerModal('modal_send_success_signal-light', <ModalSuccess message='Bạn đã gửi đèn cảnh báo thành công!'  />)
    registerModal('modal_send_error_signal-light', <ModalError message='Bạn đã gửi đèn cảnh báo thất bại!'  />)

    //fetch data from api
    const fetchTableData = useCallback(
        async () => {
            dispatch({ type: type.SET_LOADING, payload: true })
            await signalLightsApi.getListsignalLights({
                keyword: search,
                pageIndex: paginationModel.page + 1,
                pageSize: paginationModel.pageSize,
                filter:status===null?'':status
            }).then(res => {
                console.log(res);
                if (res) {
                    dispatch({ type: type.SET_TOTAL, payload: res.data.totalRecords })
                    dispatch({ type: type.SET_ROWS, payload: res.data&&res.data?.details?.length>0 ? res.data.details : [] })
                    dispatch({ type: type.SET_LOADING, payload: false })
                }
            })
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [paginationModel, search, status]
    )

    //debounce when search
    

    React.useEffect(() => {
        fetchTableData()
    }, [fetchTableData])


    const handleDeleteSignalLights = async () => {
        openModal('modal_loading')
        try {
            console.log(idRef.current)
            const res = await signalLightsApi.deletesignalLights(idRef.current)
            if (res.data) {
                openModal('modal_delete_success_signal-light')
                fetchTableData()
            }
            else {
                openModal('modal_delete_error_signal-light')
            }
        } catch (e) {
            openModal('modal_delete_error_signal-light')
            console.log(e)
        }
    }

    const handleSearch = () => {
        dispatch({ type: type.SET_SEARCH_VALUE, payload: searchValue.current.value });
    };


    const handleClearFilter = () => {
        dispatch({ type: type.SET_SEARCH_VALUE, payload: '' });
        searchValue.current.value="";
        dispatch({ type: type.SET_STATUS, payload: '' });
    }

    registerModal('modal_confirm_delete_signal-light', <ModalConfirm message='Bạn đang thao tác xóa đèn cảnh báo' handleConfirm={handleDeleteSignalLights}  />)
    
    const handleOpenModal = (modalName: string) => {
        openModal(modalName)
    }
    
return (
        <SignalLightsContext.Provider value={{ ...state,searchValue, dispatch, handleClearFilter, handleSearch,  handleDeleteSignalLights, handleOpenModal,idRef }}>{children}</SignalLightsContext.Provider>
    )
}