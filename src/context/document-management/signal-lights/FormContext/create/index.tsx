import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { signalLightsApi } from 'src/api-client/documents'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { mediaApi } from 'src/api-client'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'

const defaultValues = {
    name: '',
    description: '',
    detailContent: '',
    status: 'ACTIVE',
    thumbnail: []
}
export type FormValues = {
    name: string
    description: string
    status: string
    detailContent: string
    thumbnail: File[]
}

export const status = [
    { value: 'ACTIVE', label: 'Đang hoạt động' },
    { value: 'INACTIVE', label: 'Ngưng hoạt động' }
]

const checkEditor = (field: string, value: string) => {
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(value, 'text/html');
    const textString = htmlDoc.body.textContent;
    console.log(textString);
    if (textString.trim() === '' || textString.trim().length > 0) return `Vui lòng nhập thông tin`;

    return '';
}
const schema = yup.object().shape({
    name: yup.string().required('Vui lòng nhập thông tin!'),
    description: yup.string().required('Vui lòng nhập thông tin!'),
    detailContent: yup.string().required(obj => checkEditor('editor', obj.value)),
    thumbnail: yup
        .mixed()
        .test('fileUpload', "Vui lòng chọn một file ảnh cho đèn cảnh báo!", (value) => { return value[0] })
        .test("fileSize", "Vui lòng tải hình ảnh có kích thước dưới 2mb!", (value) => {
            return value[0] && value[0].size <= 3000000;
        })
        .test("type", "Hệ thống chỉ hỗ trợ: .jpeg, .jpg, .bmp, .pdf and .doc", (value) => {
            return value[0] && (
                value[0].type === "image/jpeg" ||
                value[0].type === "image/bmp" ||
                value[0].type === "image/png" ||
                value[0].type === 'application/pdf' ||
                value[0].type === "application/msword"
            );
        }),

})

export const SignalLightsCreateFormContext = React.createContext<any>({})

export default function SignalLightsCreateProvider({ children }: { children: React.ReactNode }) {
    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal, closeModal } = useModals();
    const [confirm, setConfirm] = React.useState(false)

    const handleClearForm = () => {
        form.reset(defaultValues)
        closeModal();
    }

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_create_success_warranty-policy', <ModalSuccess message='Bạn đã tạo chính sách bảo hành thành công!' handleConfirm={handleClearForm} />)
    registerModal('modal_create_error_warranty-policy', <ModalError message='Bạn đã tạo chính sách bảo hành thất bại!' />)
    const handleSave = async () => {
        try {
            openModal('modal_loading')
            const formData = new FormData();
            const imageData = form.getValues('thumbnail')[0]
            formData.append('Files', imageData);

            // upload image to server
            const newImg = await mediaApi.uploadImage(formData)
            if (newImg.data) {
                const params = {
                    name: form.getValues('name'),
                    description: form.getValues('description'),
                    status: form.getValues('status'),
                    detailContent: form.getValues('detailContent'),
                    thumbnail: newImg.data.filePath,
                }
                const result = await signalLightsApi.createsignalLights(params)
                if (result.data) {
                    openModal('modal_create_success_warranty-policy')
                }
                else {
                    openModal('modal_create_error_warranty-policy')
                }
            }
        } catch (e) {
            openModal('modal_create_error_warranty-policy')
        }
    }
    registerModal('modal_confirm_create_warranty-policy', <ModalConfirm message='Bạn đang thao tác tạo chính sách bảo hành' handleConfirm={handleSave} />)
    const handleConfirm = () => {
        openModal('modal_confirm_create_warranty-policy')
    }

    return (
        <SignalLightsCreateFormContext.Provider value={{ form, handleConfirm, handleSave }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm}/>

        </SignalLightsCreateFormContext.Provider>
    )
}