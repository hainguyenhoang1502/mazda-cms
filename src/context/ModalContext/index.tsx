import React, { createContext } from 'react';

type Props = {
  children: React.ReactNode
}


const ModalsContext = createContext<any>({});

export const useModals = () => React.useContext(ModalsContext);

export default function ModalsProvider({ children }: Props) {
  const [modalName, setModalName] = React.useState(null);
  const [modals, setModals] = React.useState({});

  const registerModal = (modalName, Component) => {
    if (!modals[modalName]) {
      setModals({
        ...modals,
        [modalName]: Component
      });
    }
  };

  const openModal = (modalName) => {
    setModalName(modalName);
  };
  const closeModal = () => {
    setModalName("");
  };
  
return (
    <ModalsContext.Provider value={{ registerModal, closeModal, openModal }}>
      {modalName && modals[modalName]}
      {children}
    </ModalsContext.Provider>
  )

}