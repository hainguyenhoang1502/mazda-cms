/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'
import { useModals } from 'src/context/ModalContext'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { mediaApi } from 'src/api-client'
import { notificationsApi } from 'src/api-client/notification'
import { convertToCurrentTime } from 'src/utils'
import { IFormActionType } from 'src/components/Form/type'
import { ListNotifyUserContext } from '../TableUser'
import { NotificationCtx } from '..'
import ModalLeavePage from 'src/components/Modal/ModalLeavePage'
import useImportExcel from 'src/hooks/useImportExcel'

//#region define form values and dummy data

const defaultValues = {
    notify_cate: '',
    notify_type: '',
    notify_template: '',
    send_date: new Date(),
    notify_title: '',
    gender: [],
    provinces: [],
    carModel: [],
    vinNumber: [],
    editor: '',
    fileUpload: [],
    isShowPopup: false,
    fromDate: null,
    toDate: null, 
    link:""
}
export type FormValues = {
    notify_type: string
    notify_cate: string
    send_date: Date
    notify_template: string
    notify_title: string
    gender: {
        value: string
        label: string
    }[]
    provinces: {
        value: string
        label: string
    }[]
    carModel: {
        value: string
        label: string
    }[]
    vinNumber: {
        value: string
        label: string
    }[]
    editor: string
    fileUpload: File[]
    isShowPopup: boolean
    fromDate: Date
    toDate: Date,
    link:string
}
export const carModelsDUmmy = [
    {
        id: 1,
        attributes: {
            car_id: "1001",
            car_name: "MAZDA-CX5",
            createdAt: "2023-05-08T07:47:34.439Z",
            updatedAt: "2023-05-08T07:47:35.463Z",
            publishedAt: "2023-05-08T07:47:35.459Z"
        }
    },
    {
        id: 2,
        attributes: {
            car_id: "1002",
            car_name: "MAZDA-CX8",
            createdAt: "2023-05-08T07:47:50.533Z",
            updatedAt: "2023-05-08T07:47:53.636Z",
            publishedAt: "2023-05-08T07:47:53.633Z"
        }
    },
    {
        id: 3,
        attributes: {
            car_id: "1003",
            car_name: "MAZDA-CX2",
            createdAt: "2023-05-08T07:48:04.659Z",
            updatedAt: "2023-05-08T07:48:05.864Z",
            publishedAt: "2023-05-08T07:48:05.861Z"
        }
    },
    {
        id: 4,
        attributes: {
            car_id: "1004",
            car_name: "MAZDA-CX10",
            createdAt: "2023-05-08T07:48:20.415Z",
            updatedAt: "2023-05-08T07:48:21.195Z",
            publishedAt: "2023-05-08T07:48:21.192Z"
        }
    },
    {
        id: 5,
        attributes: {
            car_id: "MAZDA-CX8",
            car_name: "MAZDA-CX8",
            createdAt: "2023-05-08T07:47:50.533Z",
            updatedAt: "2023-05-08T07:47:53.636Z",
            publishedAt: "2023-05-08T07:47:53.633Z"
        }
    },
]
export const notifyType = [
    { value: 'SENDNOW', label: 'Gửi ngay' },
    { value: 'SCHEDULE', label: 'Schedule' }
]
export const notifyCate = [
    { value: 'SERVICE', label: 'Dịch vụ' },
    { value: 'PROMOTION', label: 'Khuyến mãi' },
]
export const genderOptions = [

    { value: 'MALE', label: 'Nam' },
    { value: 'FEMALE', label: 'Nữ' },

]
const checkEditor = (field: string, value: string) => {
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(value, 'text/html');
    const textString = htmlDoc.body.textContent;
    console.log(textString);
    if (textString.trim() === '' || textString.trim().length > 0) return `Vui lòng nhập thông tin`;

    return '';
}
const schema = yup.object().shape({
    notify_title: yup.string().required('Vui lòng nhập tiêu đề thông báo'),
    notify_type: yup.string().required('Vui lòng chọn loại thông báo'),
    notify_cate: yup.string().required('Vui lòng chọn danh mục thông báo'),
    send_date: yup.date().when('notify_type', {
        is: 'SCHEDULE',
        then: yup.date().min(new Date(), 'Ngày phải lớn hơn ngày hiện tại').required('This field is required')
    }),
    editor: yup.string().required(obj => checkEditor('editor', obj.value)),
    fileUpload: yup
        .mixed()
        .test('fileUpload', "Hãy chọn một file ảnh cho thông báo!", (value) => { return value[0] })
        .test("fileSize", "The file is too large", (value) => {
            return value[0] && value[0].size <= 3000000;
        })
        .test("type", "Only the following formats are accepted: .jpeg, .jpg, .bmp, .pdf and .doc", (value) => {
            return value[0] && (
                value[0].type === "image/jpeg" ||
                value[0].type === "image/bmp" ||
                value[0].type === "image/png" ||
                value[0].type === 'application/pdf' ||
                value[0].type === "application/msword"
            );
        }),

})

//#endregion

export const FormCreateContext = React.createContext<any>({})

export default function FormCreateProvider({ children }: { children: React.ReactNode }) {

    //#region initial values
    const {
        formAction,
        setFormAction,
        notifyTemplate,
        notifyTemplateContent,
        carModels,
        openTable,
        setOpenTableModal,
        provinces,
    } = React.useContext(NotificationCtx);

    const form = useForm<FormValues>({
        defaultValues,
        mode: 'onChange',
        resolver: yupResolver(schema)
    })
    const { registerModal, openModal, closeModal } = useModals();
    const [confirm, setConfirm] = React.useState(false)
    const [loading, setLoading] = React.useState(false);
    const [vinNumbers, setVinNumbers] = React.useState([]);
    const { data, setFile, setData } = useImportExcel();
    const searchValue = React.useRef(null);
    const createVinNum = React.useRef(null);
    const [search, setSearch] = React.useState('');
    const handleClearForm = () => {
        form.reset(defaultValues)
        closeModal();
    }
    const [deleteArr, setDeleteArr] = React.useState([]);
    const { filterTableUser, setFilterTableUser } = React.useContext(ListNotifyUserContext);

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_create_success_notify', <ModalSuccess message='Bạn đã tạo thông báo thành công!' handleConfirm={handleClearForm} />)
    registerModal('modal_create_error_notify', <ModalError message='Bạn đã tạo thông báo thất bại!' />)
    registerModal('modal_send_success_notify', <ModalSuccess message='Bạn đã gửi thông báo thành công!' />)
    registerModal('modal_send_error_notify', <ModalError message='Bạn đã gửi thông báo thất bại!' />)

    //#endregion
    const handleSearch = () => {
        setSearch(searchValue.current.value)
    }

    //#region handling form actions 

    const handleSave = React.useCallback(async () => {
        try {
            openModal('modal_loading')
            const formData = new FormData();
            const imageData = form.getValues('fileUpload')[0]
            formData.append('Files', imageData);

            // upload image to server
            const newImg = await mediaApi.uploadImage(formData)
            if (newImg.data) {
                const paramssendNow = {
                    title: form.getValues('notify_title'),
                    category: form.getValues('notify_type'),
                    notificationTypeId: form.getValues('notify_cate'),
                    content: form.getValues('editor'),
                    image: newImg.data.filePath,
                    sendTo: JSON.stringify({
                        gender: form.getValues('gender').map(item => item.value).join(','),
                        province: form.getValues('provinces').map(item => item.value).join(','),
                        model: form.getValues('carModel').map(item => item.value).join(',')
                    }),
                    ...(form.getValues('notify_type') === 'SENDNOW' ? {} : { schedule: convertToCurrentTime(form.getValues('send_date')) }),
                    ...form.getValues('notify_template') === '' ? {} : { templateId: form.getValues('notify_template') }

                }
                console.log(paramssendNow)
                const result = await notificationsApi.createNotification(paramssendNow)
                if (result.data) {
                    openModal('modal_create_success_notify')
                }
                else {
                    openModal('modal_create_error_notify')
                }
            }
        } catch (e) {
            openModal('modal_create_error_notify')
        }
    }, [form, openModal])

    const handleOpenModal = React.useCallback((modal: string) => {
        openModal(modal)
    }, [openModal])

    const handleSend = React.useCallback(async () => {
        try {
            openModal('modal_loading')
            const formData = new FormData();
            const imageData = form.getValues('fileUpload')[0]
            formData.append('Files', imageData);
            const newImg = await mediaApi.uploadImage(formData)
            if (newImg.data) {
                const paramssendNow = {
                    title: form.getValues('notify_title'),
                    category: form.getValues('notify_type'),
                    notificationTypeId: form.getValues('notify_cate'),
                    content: form.getValues('editor'),
                    image: newImg.data.filePath,
                    sendTo: JSON.stringify({
                        gender: form.getValues('gender').map(item => item.value).join(','),
                        province: form.getValues('provinces').map(item => item.value).join(','),
                        model: form.getValues('carModel').map(item => item.value).join(',')
                    }),
                    ...(form.getValues('notify_type') === 'SENDNOW' ? {} : { schedule: convertToCurrentTime(form.getValues('send_date')) }),
                    ...form.getValues('notify_template') === '' ? {} : { templateId: form.getValues('notify_template') }

                }
                const result = await notificationsApi.createNotification(paramssendNow)
                if (result.data) {
                    const sendRes = await notificationsApi.sendNotification({
                        notificationId: result.data.notificationId,
                        gender: form.getValues('gender').map(item => item.value).join(','),
                        province: form.getValues('provinces').map(item => item.value).join(','),
                        model: form.getValues('carModel').map(item => item.value).join(','),
                        ...(form.getValues('notify_type') === 'SENDNOW' ? {} : { schedule: convertToCurrentTime(form.getValues('send_date')) }),
                    })

                    if (sendRes.data) {
                        openModal('modal_send_success_notify')
                    }
                }
                else {
                    openModal('modal_send_error_notify')
                }
            }
        } catch (e) {
            openModal('modal_send_error_notify')
        }
    }, [form, openModal])

    const getTemplateContent = React.useCallback(() => {
        setLoading(true);
        setTimeout(() => {
            const template = notifyTemplateContent.filter(item => item.value === form.getValues('notify_template'))
            form.setValue('editor', template[0]?.content)
            setLoading(false);
        }, 500)
    }, [form.watch('notify_template'), notifyTemplateContent])

    registerModal('modal_confirm_send_notify', <ModalConfirm message='Bạn đang thao tác gửi thông báo' handleConfirm={handleSend} />)

    const handleConfirm = React.useCallback(() => {
        openModal('modal_confirm_send_notify')
    }, [openModal])

    const filterSendTo = React.useCallback(() => {
        const newFilter = {
            gender: form.getValues('gender'),
            model: form.getValues('carModel'),
            province: form.getValues('provinces')
        }
        setFilterTableUser(newFilter)
    }, [form, setFilterTableUser])

    const getVinNumberOpts = React.useCallback(() => {
        const vinNumbers = data.map((item) => {
            return {
                label: item.VinNumber,
                value: item.VinNumber
            }
        })
        form.setValue('vinNumber', vinNumbers)
        const dataforTable = data.map((item) => {
            return {
                id: item.VinNumber,
                VinNumber: item.VinNumber
            }
        })
        setVinNumbers(dataforTable)
    }, [data])

    const handleSearchVinNumber = React.useCallback(() => {
        if (search && search.length > 0) {
            console.log(search);
            const filteredData = data.filter(item => item.VinNumber.toLowerCase().includes(search) || item.VinNumber === search);
            const filterVinNumbers = filteredData.map(item => ({
                label: item.VinNumber,
                value: item.VinNumber
            }));
            form.setValue('vinNumber', filterVinNumbers);
            const newArr = filteredData.map((item) => ({
                id: item.VinNumber,
                VinNumber: item.VinNumber
            }));
            setVinNumbers(newArr);
        }
        else {
            getVinNumberOpts()
        }
    }, [search, data, form]);

    const handleDelete = React.useCallback(() => {
        console.log(deleteArr, data)
        const dataforTable = data.filter(item => !deleteArr.includes(item.VinNumber)).map((item) => {
            return {
                id: item.VinNumber,
                VinNumber: item.VinNumber
            }
        })

        setData(dataforTable)
    }, [data, deleteArr, setData])

    const handleCreateVinNum = React.useCallback(() => {
        const newRows = {
            VinNumber: createVinNum.current.value,
            id: createVinNum.current.value
        }
        const findData = data.filter(item => item.VinNumber === newRows.VinNumber)
        if (findData && findData.length === 0 && createVinNum.current.value) {
            setData([newRows, ...data])
            createVinNum.current.value = ""
        }
    }, [data, setData])

    const onFileDataChange = React.useCallback(() => {
        const newVinNumber = Array.from(new Set(data.map((item) => { return { VinNumber: item.VinNumber, id: item.VinNumber } })))
        setVinNumbers(newVinNumber)
        form.setValue('vinNumber', newVinNumber.map(item => { return { label: item.VinNumber, value: item.VinNumber } }));
    }, [data])

    React.useEffect(() => {
        getTemplateContent()
    }, [getTemplateContent])

    React.useEffect(() => {
        filterSendTo()
    }, [filterSendTo])

    React.useEffect(() => {
        getVinNumberOpts()
    }, [getVinNumberOpts])

    React.useEffect(() => {
        handleSearchVinNumber()
    }, [handleSearchVinNumber])

    React.useEffect(() => {
        onFileDataChange()
    }, [onFileDataChange])

    const handleSubmit = formAction === IFormActionType.FORM_SEND ? handleConfirm : handleSave

    //#endregion

    return (
        <FormCreateContext.Provider
            value={{
                form,
                formAction,
                filterTableUser,
                carModels,
                provinces,
                setFormAction,
                handleOpenModal,
                notifyTemplate,
                handleSubmit,
                handleConfirm,
                handleSave,
                openTable,
                setOpenTableModal,
                loading,
                setLoading,
                setFile,
                data,
                vinNumbers,
                handleSearch,
                searchValue,
                deleteArr,
                setDeleteArr,
                handleDelete,
                handleCreateVinNum,
                createVinNum
            }}>
            {children}
            <ModalLeavePage confirm={confirm} setConfirm={setConfirm} />
        </FormCreateContext.Provider>
    )
}