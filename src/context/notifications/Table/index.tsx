import React, { useCallback } from 'react'
import { INotificationState } from './type'
import { INotificationActionType as type } from './type'
import { notificationsApi } from 'src/api-client/notification'
import { reducer } from './reducer'
import { useModals } from '../../ModalContext'
import ModalConfirm from 'src/components/Modal/ModalConfirm'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'

export interface NotificationProps {
    children: React.ReactNode
}
const defaultValues: INotificationState = {
    total: 0,
    sort: 'asc',
    rows: [],
    search: '',
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    startDate: null,
    endDate: null,
    loading: false,
}

export const NotificationsContext = React.createContext<any>({})

export default function NotificationListProvider({ children }: NotificationProps) {
    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const {
        search,
        paginationModel,
        startDate,
        endDate } = state
    const idRef = React.useRef(null);
    const searchValue=React.useRef(null);
    const { registerModal, openModal } = useModals();

    //#region register modal

    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_delete_success_notify', <ModalSuccess message='Bạn đã thao tác xóa thông báo thành công!' />)
    registerModal('modal_delete_error_notify', <ModalError message='Bạn đã thao tác xóa thông báo thất bại!' />)
    registerModal('modal_send_success_notify', <ModalSuccess message='Bạn đã gửi thông báo thành công!' />)
    registerModal('modal_send_error_notify', <ModalError message='Bạn đã gửi thông báo thất bại!' />)

    //#endregion

    //#region handle api and actions
    //fetch data from api
    const fetchTableData = useCallback(
        async () => {
            dispatch({ type: type.SET_LOADING, payload: true })
            await notificationsApi.getListNotifications({
                keyword: search,
                pageIndex: paginationModel.page + 1,
                pageSize: paginationModel.pageSize,
                startDate,
                endDate
            }).then(res => {
                console.log(res);
                if (res) {
                    dispatch({ type: type.SET_TOTAL, payload: res.data.totalRecords })
                    dispatch({ type: type.SET_ROWS, payload: res.data ? res.data.result : [] })
                    dispatch({ type: type.SET_LOADING, payload: false })
                }
            })
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [paginationModel, search, startDate, endDate]
    )

    React.useEffect(() => {
        fetchTableData()
    }, [fetchTableData])



    const handleSendNotify = React.useCallback(async () => {
        openModal('modal_loading')
        try {
            const res = await notificationsApi.sendNotification(idRef.current)
            if (res.data) {
                openModal('modal_send_success_notify')
                fetchTableData()
            }
            else {
                openModal('modal_send_error_notify')
            }

        } catch (e) {
            openModal('modal_send_error_notify')
        }
    }, [fetchTableData, openModal])

    const handleDeleteNotify = React.useCallback(async () => {
        openModal('modal_loading')
        try {
            console.log('clicked delete notification')

            const res = await notificationsApi.deleteNotification(idRef.current?.toString())
            if (res.data) {
                openModal('modal_delete_success_notify')
                fetchTableData()
            }
            else {
                openModal('modal_delete_error_notify')
            }
        } catch (e) {
            openModal('modal_delete_error_notify')
            console.log(e)
        }
    }, [fetchTableData, openModal])

    const handleSearch = () => {
        console.log(searchValue.current.value)
        dispatch({ type: type.SET_SEARCH_VALUE, payload: searchValue.current.value });
    };

    const handleClearFilter = React.useCallback(() => {
        dispatch({ type: type.SET_SEARCH_VALUE, payload: '' });
        searchValue.current.value=""
        dispatch({ type: type.SET_START_DATE, payload: null });
        dispatch({ type: type.SET_END_DATE, payload: null });
    }, [])

    registerModal('modal_confirm_delete_notify', <ModalConfirm message='Bạn đang thao tác xóa thông báo' handleConfirm={handleDeleteNotify} />)
    registerModal('modal_confirm_send_notify', <ModalConfirm message='Bạn đang thao tác gửi thông báo' handleConfirm={handleSendNotify} />)

    const handleOpenModal = React.useCallback((modalName: string) => {
        openModal(modalName)
    }, [openModal])
    
    //#endregion

    return (
        <NotificationsContext.Provider value={{ ...state, dispatch, handleClearFilter,searchValue, handleSearch, handleSendNotify, handleDeleteNotify, handleOpenModal, idRef }}>{children}</NotificationsContext.Provider>
    )
}