
import { INotificationState,INotificationAction } from "./type"
import {INotificationActionType as type} from './type'

export const reducer = (state: INotificationState, action: INotificationAction) => {
    switch (action.type) {
        case type.SET_TOTAL: {
            return {
                ...state,
                total: action.payload
            }
        }
        case type.SET_SORT: {
            return {
                ...state,
                sort: action.payload
            }
        }
        case type.SET_ROWS: {
            return {
                ...state,
                rows: action.payload
            }
        }
        case type.SET_SEARCH_VALUE: {
            return {
                ...state,
                search: action.payload
            }
        }
        case type.SET_SORT_COLUMN: {
            return {
                ...state,
                sortColumn: action.payload
            }
        }
        case type.SET_PAGINATION_MODEL: {
            return {
                ...state,
                paginationModel: action.payload
            }
        }
        case type.SET_START_DATE: {
            return {
               ...state,
                startDate: action.payload
            }
        }
        case type.SET_END_DATE: {
            return {
              ...state,
                endDate: action.payload
            }
        }
        case type.SET_LOADING:{
            return {
                ...state, 
                loading: action.payload
            }
        }
        default:
            return state
    }
}