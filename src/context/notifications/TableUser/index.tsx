import React, { useCallback } from 'react'
import { INotificationState } from './type'
import { INotificationActionType as type } from './type'
import { notificationsApi } from 'src/api-client/notification'
import { reducer } from './reducer'
import { useModals } from '../../ModalContext'
import ModalLoading from 'src/components/Modal/ModalLoading'
import ModalSuccess from 'src/components/Modal/ModalSuccess'
import ModalError from 'src/components/Modal/ModalError'

//#region define values
export interface NotificationProps {
    children: React.ReactNode,
}
const defaultValues: INotificationState = {
    total: 0,
    sort: 'asc',
    rows: [],
    search: '',
    sortColumn: '',
    paginationModel: { page: 0, pageSize: 10 },
    startDate: null,
    endDate: null,
    loading: false,
}

//#endregion

export const ListNotifyUserContext = React.createContext<any>({})

export default function ListNotifyUserProvider({ children }: NotificationProps) {

    //#region Initial values

    const [state, dispatch] = React.useReducer(reducer, defaultValues)
    const {
        search,
        paginationModel,
        startDate,
        endDate } = state
    const idRef = React.useRef(null);
    const searchValue = React.useRef(null);
    const { registerModal, openModal } = useModals();
    const [filterTableUser, setFilterTableUser]= React.useState({
        gender:[],
        province:[],
        model:[]
    })

    //register modal
    registerModal('modal_loading', <ModalLoading />)
    registerModal('modal_delete_success_notify', <ModalSuccess message='Bạn đã thao tác xóa thông báo thành công!' />)
    registerModal('modal_delete_error_notify', <ModalError message='Bạn đã thao tác xóa thông báo thất bại!' />)
    registerModal('modal_send_success_notify', <ModalSuccess message='Bạn đã gửi thông báo thành công!' />)
    registerModal('modal_send_error_notify', <ModalError message='Bạn đã gửi thông báo thất bại!' />)

    //#endregion

    //#region handling tables

    //fetch data from api
    const fetchTableData = useCallback(
        async () => {
            dispatch({ type: type.SET_LOADING, payload: true })
            await notificationsApi.getListNotificationCustomer({
                keyword: search,
                pageIndex: paginationModel.page + 1,
                pageSize: paginationModel.pageSize,
                startDate,
                endDate,
                gender:filterTableUser?.gender?.map(item => item.value).join(','),
                province:filterTableUser?.province?.map(item => item.value).join(','),
                model:filterTableUser?.model?.map(item => item.value).join(',')
            }).then(res => {
                console.log(res);
                if (res) {
                    const data= res.data.result?.map((item, index)=>{
                        return {
                            id: index,
                            ...item
                        }
                    })
                    dispatch({ type: type.SET_TOTAL, payload: res.data.totalRecords })
                    dispatch({ type: type.SET_ROWS, payload: res.data ? data : [] })
                    dispatch({ type: type.SET_LOADING, payload: false })
                }
            })
        },
        [paginationModel, search, startDate, endDate, filterTableUser]
    )

    React.useEffect(() => {
        fetchTableData()
    }, [fetchTableData])


    const handleSearch = React.useCallback(() => {
        dispatch({ type: type.SET_SEARCH_VALUE, payload: searchValue.current.value });
    },[]);


    const handleClearFilter = React.useCallback(() => {
        dispatch({ type: type.SET_SEARCH_VALUE, payload: '' });
        dispatch({ type: type.SET_START_DATE, payload: null });
        searchValue.current.value="";
        dispatch({ type: type.SET_END_DATE, payload: null });
    },[])

    const handleOpenModal = React.useCallback((modalName: string) => {
        openModal(modalName)
    },[openModal])

    //#endregion

    return (
        <ListNotifyUserContext.Provider value={{ ...state, searchValue, dispatch, handleClearFilter, handleSearch, handleOpenModal, idRef, filterTableUser, setFilterTableUser }}>{children}</ListNotifyUserContext.Provider>
    )
}
