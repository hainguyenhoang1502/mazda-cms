import { NotificationRowsType, SortType } from "src/components/notifications/Table/types"

export interface INotificationState{
    total: number
    sort: SortType
    rows: NotificationRowsType[]
    search: string
    sortColumn: string
    paginationModel: { page: number, pageSize: number }
    startDate: Date
    endDate: Date
    loading: boolean
}
export interface INotificationContext extends INotificationState{
    dispatch:React.Dispatch<INotificationAction>
    handleSearch:(value: string)=>void
    handleClearFilter:()=>void
    handleSendNotify:(id:any)=>void
    handleDeleteNotify:()=>void
    handleOpenModal:(modalName:string, id:any)=>void
    idNotifyRef:any
}
export enum INotificationActionType{
    SET_TOTAL='TOTAL',
    SET_SORT='SORT',
    SET_ROWS='ROWS',
    SET_SEARCH_VALUE='SEARCH_VALUE',
    SET_SORT_COLUMN='SORT_COLUMN',
    SET_PAGINATION_MODEL='PAGINATION_MODEL',
    SET_START_DATE='START_DATE',
    SET_END_DATE='END_DATE',
    SET_LOADING='LOADING',
}
export interface INotificationAction{
    type: INotificationActionType
    payload:any
}
