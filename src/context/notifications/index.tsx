import React from 'react'
import useCarmodels from 'src/hooks/useCarModels'
import useProvinces from 'src/hooks/useLocations/useProvinces'
import useNotifyTemplate from 'src/hooks/useNotifyTemplate'

type Props = {
    children: React.ReactNode
}

export const NotificationCtx = React.createContext<any>({})

const NotificationProvider = ({ children }: Props) => {
    const [loading, setLoading] = React.useState(false)
    const { provinces } = useProvinces()
    const { carModels } = useCarmodels()
    const { notifyTemplate, notifyTemplateContent } = useNotifyTemplate()
    const [openTable, setOpenTableModal] = React.useState(false);
    const [formAction, setFormAction] = React.useState('')

    return (

        <NotificationCtx.Provider
            value={{
                loading,
                setLoading,
                provinces,
                carModels,
                notifyTemplate,
                notifyTemplateContent,
                openTable,
                setOpenTableModal,
                formAction,
                setFormAction
            }}>
            {children}
        </NotificationCtx.Provider>
    )
}

export default NotificationProvider