// ** Toolkit imports
import { configureStore } from '@reduxjs/toolkit'

// ** Reducers
// ** Apps
import role from 'src/store/apps/role'
import user from 'src/store/apps/user'
import area from 'src/store/apps/category/area'
import district from 'src/store/apps/category/district'
import brand from 'src/store/apps/category/brand'
import region from 'src/store/apps/category/region'
import dealer from './apps/category/company-dealer/dealer'
import company from './apps/category/company-dealer/company'
import profile from 'src/store/apps/profile'
import customer from 'src/store/apps/customer-information'
import province from 'src/store/apps/category/province'
import permission from 'src/store/apps/permission'
import vehicleCode from 'src/store/apps/vehicle/modelCode'
import listModelCode from 'src/store/apps/vehicle/listModelCode'
import listVehicleVersion from 'src/store/apps/car-management/car-version'
import listVehicleColor from 'src/store/apps/car-management/car-color'
import userManual from 'src/store/apps/user-manual'
import organization from './apps/category/organization'
import warrantyInformation from 'src/store/apps/warranty-information'
import vehicleData from 'src/store/apps/vehicle/model'
import maintenanceHistory from 'src/store/apps/maintenance'

// ** Utils
import accessToken from 'src/store/utils/accessToken'
import loading from 'src/store/utils/loadingBackdrop'

export const store = configureStore({
  reducer: {

    // ** Apps
    area,
    role,
    user,
    brand,
    dealer,
    region,
    district,
    company,
    profile,
    customer,
    vehicleCode,
    listVehicleVersion,
    listModelCode,
    listVehicleColor,
    province,
    userManual,
    permission,
    organization,
    warrantyInformation,
    vehicleData,
    maintenanceHistory,

    // ** Utils
    accessToken,
    loading,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false
    })
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
