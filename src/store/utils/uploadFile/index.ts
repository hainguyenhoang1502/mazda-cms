import { toast } from "react-hot-toast";
import { ApiUploadFile } from "src/api/refData";
import { setValueBackrop } from "../loadingBackdrop";
import { store } from "src/store";

export const handleUploadFile = async (files: Array<File>) => {
    const data = new FormData();

    data.append(`excelFile`, files[0]);

    try {
        store.dispatch(setValueBackrop(true))
        const res = await ApiUploadFile(data)
        store.dispatch(setValueBackrop(false))

        if (res.code === 200 && res.result) {
            return res.data.serverName + res.data.filePath
        } else {
            toast.error("Đăng tải file thất bại. Vui lòng thử lại")
        }
    } catch (error) {
        console.log('%cerror error line:18 ', 'color: red; display: block; width: 100%;', error);
    }

}