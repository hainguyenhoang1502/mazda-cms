import { createAction, createSlice } from '@reduxjs/toolkit'

export const setAccessToken = createAction<string>('')

export const ultisAccessToken = createSlice({
  name: 'counter',
  initialState: {
    accessToken: null
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(setAccessToken, (state, action) => {
        state.accessToken = action.payload
      })
  },
})

export default ultisAccessToken.reducer