import { createSlice } from '@reduxjs/toolkit'

export const ultisLoadingBackdrop = createSlice({
  name: 'LoadingBackdrop',
  initialState: {
    isLoading: false
  },
  reducers: {
    setValueBackrop: (state, action) => {
      state.isLoading = action.payload
    }
  },
})

export const { setValueBackrop } = ultisLoadingBackdrop.actions
export default ultisLoadingBackdrop.reducer