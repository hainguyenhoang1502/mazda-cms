import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { ApiListVehicleModelCode } from "src/api/vehicles";


export const getListModelCodeAction = createAsyncThunk('listModelCode/fetchData', async () => {
    const res = await ApiListVehicleModelCode()
    if (res.code === 200 && res.result && res.data) {
        const arr = res.data.map(item => ({ value: item.modelCode, label: item.modelName }))

        return {
            data: arr,
            isNeedReloadData: true,
        }
    } else {
        return listGradesModelSlice.getInitialState()
    }
})

export const listGradesModelSlice = createSlice({
    name: 'listModelCode',
    initialState: {
        data: [],
        isNeedReloadData: false,
    },
    reducers: {
        handleSetReloadData: (state) => {
            state.isNeedReloadData = true
        }
    },
    extraReducers :builder => {
        builder.addCase(getListModelCodeAction.fulfilled,(state, action) => {
            state.data = action.payload.data
            state.isNeedReloadData = action.payload.isNeedReloadData
        })
    }
})

export default listGradesModelSlice.reducer