// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiListVehicleModelsData } from 'src/api/vehicles';
import { IParamModelList } from 'src/api/vehicles/type';

// ** Fetch Model
export const fetchDataGetVehicleData = createAsyncThunk('appVehicleData/fetchData', async (param: IParamModelList, { dispatch }) => {
  dispatch(startLoading())
  const res = await ApiListVehicleModelsData(param)
  if (res.code == 200 && res.result && res.data) {
    return {
      ...res.data,
      param: param,
      isLoading: false,
      isNeedReloadData: false
    }
  } else {

    return {
      data: [],
      param: param,
      isLoading: false,
      isNeedReloadData: false,
      totalRecords: 10,
      pageIndex: 1,
      pageSize: 10,
      totalPages: 1,
    }
  }
})


export const fetchDataGetFilterVehicleData = createAsyncThunk('appVehicleData/fetchFilterData', async () => {
  const res = await ApiListVehicleModelsData(appVehicleDataSlice.getInitialState().paramFilter)
  if (res.code == 200 && res.result && res.data) {
    const arr = res.data.data.map(item => ({
      label: item.modelName,
      value: item.modelCode
    }))

    return {
      arrFilter: arr,
    }
  }
  
  return {
    arrFilter: [],
  }
})


export const appVehicleDataSlice = createSlice({
  name: 'appVehicleData',
  initialState: {
    data: null,
    param: {
      pageIndex: 1,
      pageSize: 10,
      keyword: '',
    },
    paramFilter: {
      pageIndex: 1,
      pageSize: 9999,
      keyword: '',
    },
    arrFilter: [],
    isLoading: false,
    isNeedReloadData: false,
    totalRecords: 10,
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
  },
  reducers: {
    startLoading: state => {
      state.isLoading = true
    },
    handleSetReloadDataModel: state => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetVehicleData.fulfilled, (state, action) => {
      state.data = action.payload.data,
        state.param = action.payload.param,
        state.totalRecords = action.payload.totalRecords,
        state.pageIndex = action.payload.pageIndex,
        state.totalPages = action.payload.totalPages,
        state.pageSize = action.payload.pageSize,
        state.isLoading = action.payload.isLoading,
        state.isNeedReloadData = action.payload.isNeedReloadData
    }),
      builder.addCase(fetchDataGetFilterVehicleData.fulfilled, (state, action) => {
        state.arrFilter = action.payload.arrFilter
      })
  }
})

export const { startLoading, handleSetReloadDataModel } = appVehicleDataSlice.actions
export default appVehicleDataSlice.reducer
