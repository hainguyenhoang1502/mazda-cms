// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiListVehicleModelsCode } from 'src/api/vehicles';

// ** Fetch Invoices
export const fetchDataGetVehicleCode = createAsyncThunk('appVehicleCode/fetchData', async () => {
  const res = await ApiListVehicleModelsCode()
  if (res.code == 200 && res.result && res.data) {

    const arr = res.data.map(item => ({
        label: item.modelName,
        value: item.modelCode
      }))
  
      return {
        data: res.data,
        arrFilter: arr
      }
  }else {
    return {
      data: [],
      arrFilter: []
    }
  }
})

// interface IParamVehicleCodeRole {
//   payload: {
//     data: Array<string>
//   }
//   type: any,
// }

export const appVehicleCodeSlice = createSlice({
  name: 'appVehicleCode',
  initialState: {
    data: null,
    arrFilter: [],
    dataVehicleCodeRole: []
  },
  reducers: {
    //null
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetVehicleCode.fulfilled, (state, action) => {
      state.data = action.payload.data,
      state.arrFilter = action.payload.arrFilter
    })
  }
})

export default appVehicleCodeSlice.reducer
