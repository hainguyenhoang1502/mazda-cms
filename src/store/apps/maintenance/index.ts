// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiListMaintenanceHistoryAllCms } from 'src/api/warranty';
import { IParamMaintenaceList } from 'src/api/warranty/type';

// ** Fetch Invoices
export const fetchDataGetMaintenanceData = createAsyncThunk('appMaintenanceData/fetchData', async (param: IParamMaintenaceList, { dispatch }) => {
  dispatch(startLoading())
  const res = await ApiListMaintenanceHistoryAllCms(param)
  if (res.code == 200 && res.result && res.data) {

    return {
      ...res.data,
      param: param,
      isLoading: false,
      isNeedReloadData: false
    }
  } else {
    return {
      result: [],
      param: param,
      isLoading: false,
      isNeedReloadData: false,
      totalRecords: 10,
      pageIndex: 1,
      pageSize: 10,
      totalPages: 1,
    }
  }
})

export const appMaintenanceDataSlice = createSlice({
  name: 'appMaintenanceData',
  initialState: {
    data: null,
    param: {
      pageIndex: 1,
      pageSize: 10,
      keyword: '',
      modelCode: null
    },
    isLoading: false,
    isNeedReloadData: false,
    totalRecords: 10,
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
  },
  reducers: {
    startLoading: state => {
      state.isLoading = true
    },
    handleSetReloadDataMaintenance: state => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetMaintenanceData.fulfilled, (state, action) => {
      state.data = action.payload.result,
        state.param = action.payload.param,
        state.totalRecords = action.payload.totalRecords,
        state.pageIndex = action.payload.pageIndex,
        state.totalPages = action.payload.totalPages,
        state.pageSize = action.payload.pageSize,
        state.isLoading = action.payload.isLoading,
        state.isNeedReloadData = action.payload.isNeedReloadData
    })
  }
})

export const { startLoading, handleSetReloadDataMaintenance } = appMaintenanceDataSlice.actions
export default appMaintenanceDataSlice.reducer
