// ApiListVehicleVersion
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { ApiListVehicleVersion } from "src/api/vehicles";
import { IParamVehicleList } from "src/api/vehicles/type";


export const fetchDataGetListVehicleVersion = createAsyncThunk(
    'appCarVersion/fetchData',
    async (param: IParamVehicleList, { dispatch }) => {
        dispatch(startLoading())
        let arr = []
        const res = await ApiListVehicleVersion(param)
        if (res.code == 200 && res.result && res.data) {

            if (res.data.details && res.data.details.length > 0) {
                arr = res.data.details.map(item => ({
                  label: item.gradeName,
                  value: item.gradeCode
                }))
              }

            return {
                param,
                ...res.data,
                isLoading: false,
                isNeedReloadData: false,
                arrFilter: arr
            }
        } else {
            return {
                param,
                arrFilter: arr,
                totalRecords: 10,
                pageIndex: 1,
                pageSize: 10,
                totalPages: 1,
                data: [],
                isLoading: false,
                isNeedReloadData: false
            }
        }
    }
)

export const appVehicleVersionSlice = createSlice({
    name: "appCarVersion",
    initialState: {
        data: [],
        arrFilter: [],
        totalRecords: 10,
        pageIndex: 1,
        pageSize: 10,
        totalPages: 1,
        param: {
            pageIndex: 1,
            pageSize: 10,
            modelCode: null,
            keyword: null,
        },
        isLoading: true,
        isNeedReloadData: false
    },

    reducers: {
        startLoading: state => {
            state.isLoading = true
        },
        handleSetReloadDataVersion: state => {
            state.isNeedReloadData = true
        }
    },
    extraReducers: builder => {
        builder.addCase(fetchDataGetListVehicleVersion.fulfilled, (state, action) => {
            (state.data = action.payload.data),
            (state.totalRecords = action.payload.totalRecords),
            (state.pageIndex = action.payload.pageIndex),
            (state.totalPages = action.payload.totalPages),
            (state.pageSize = action.payload.pageSize),
            (state.param = action.payload.param),
            (state.arrFilter = action.payload.arrFilter),
            (state.isLoading = action.payload.isLoading),
            (state.isNeedReloadData = action.payload.isNeedReloadData)
        })
    }

})

export const { startLoading, handleSetReloadDataVersion } = appVehicleVersionSlice.actions
export default appVehicleVersionSlice.reducer