// ApiListVehicleVersion
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { ApiListVehicleColor } from "src/api/vehicles";
import { IParamVehicleListColor } from "src/api/vehicles/type";


export const fetchDataGetListVehicleColor = createAsyncThunk(
    'appCarColor/fetchData',
    async (param: IParamVehicleListColor, { dispatch }) => {
        dispatch(startLoading())
        let arr = []
        const res = await ApiListVehicleColor(param)
        if (res.code == 200 && res.result && res.data) {

            if (res.data.data && res.data.data.length > 0) {
                arr = res.data.data.map(item => ({
                  label: item.colorNameVi,
                  value: item.colorCode
                }))
              }

            return {
                param,
                ...res.data,
                isLoading: false,
                isNeedReloadData: false,
                arrFilter: arr
            }
        } else {
            return {
                param,
                arrFilter: arr,
                totalRecords: 10,
                pageIndex: 1,
                pageSize: 10,
                totalPages: 1,
                data: [],
                isLoading: false,
                isNeedReloadData: false
            }
        }
    }
)

export const appVehicleColorSlice = createSlice({
    name: "appCarColor",
    initialState: {
        data: [],
        arrFilter: [],
        totalRecords: 10,
        pageIndex: 1,
        pageSize: 10,
        totalPages: 1,
        param: {
            pageIndex: 1,
            pageSize: 10,
            keyword: null,
        },
        isLoading: true,
        isNeedReloadData: false
    },

    reducers: {
        startLoading: state => {
            state.isLoading = true
        },
        handleSetReloadDataColor: state => {
            state.isNeedReloadData = true
        }
    },
    extraReducers: builder => {
        builder.addCase(fetchDataGetListVehicleColor.fulfilled, (state, action) => {
            (state.data = action.payload.data),
            (state.totalRecords = action.payload.totalRecords),
            (state.pageIndex = action.payload.pageIndex),
            (state.totalPages = action.payload.totalPages),
            (state.pageSize = action.payload.pageSize),
            (state.param = action.payload.param),
            (state.arrFilter = action.payload.arrFilter),
            (state.isLoading = action.payload.isLoading),
            (state.isNeedReloadData = action.payload.isNeedReloadData)
        })
    }

})

export const { startLoading, handleSetReloadDataColor } = appVehicleColorSlice.actions
export default appVehicleColorSlice.reducer