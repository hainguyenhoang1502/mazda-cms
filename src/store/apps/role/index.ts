// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiListRoleCms } from 'src/api/authen';
import { IParamFilterListRole } from 'src/api/authen/type';

// ** Fetch Invoices
export const fetchDataGetRole = createAsyncThunk('appRole/fetchData', async (param: IParamFilterListRole) => {
  startLoading()
  const res = await ApiListRoleCms(param)
  if (res.code == 200 && res.result && res.data) {

    return {
      param: param,
      ...res.data,
      isLoading: false,
      isNeedReloadData: false

    }
  } else {

    return {
      param: param,
      totalRecords: 10,
      pageIndex: 1,
      pageSize: 10,
      totalPages: 1,
      roles: [],
      isLoading: false,
      isNeedReloadData: false
    }
  }
})

export const fetchDataFilterGetRole = createAsyncThunk('appRole/fetchFilterData', async (param: IParamFilterListRole) => {
  const res = await ApiListRoleCms(param)
  const arrFilter = []
  if (res.code == 200 && res.result && res.data) {
    res.data.roles.forEach(element => {
      arrFilter.push({
        value: element.id,
        label: element.name
      })
    });
    
    return {
      arrFilter
    }
  } else {
    return {
      arrFilter
    }
  }
})

export const appRoleSlice = createSlice({
  name: 'appRole',
  initialState: {
    data: null,
    totalRecords: 10,
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
    param: {
      keyword: "",
      pageIndex: 1,
      pageSize: 9999,
    },
    arrFilter: [],
    isLoading: true,
    isNeedReloadData: false
  },
  reducers: {
    startLoading: (state) => {
      state.isLoading = true
    },
    handleSetReloadDataRole: (state) => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetRole.fulfilled, (state, action) => {
      state.data = action.payload.roles,
        state.totalRecords = action.payload.totalRecords,
        state.pageIndex = action.payload.pageIndex,
        state.totalPages = action.payload.totalPages,
        state.pageSize = action.payload.pageSize,
        state.param = action.payload.param,
        state.isLoading = action.payload.isLoading,
        state.isNeedReloadData = action.payload.isNeedReloadData

    })
    builder.addCase(fetchDataFilterGetRole.fulfilled, (state, action) => {
      state.arrFilter = action.payload.arrFilter
    })
  }
})

export const { startLoading, handleSetReloadDataRole } = appRoleSlice.actions

export default appRoleSlice.reducer
