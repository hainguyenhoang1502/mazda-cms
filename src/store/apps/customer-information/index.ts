// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiListCustomerCms } from 'src/api/customer';
import { IParamFilterListCustomer } from 'src/api/customer/type';

// ** Fetch Invoices
export const fetchDataGetCustomer = createAsyncThunk('appCustomer/fetchData', async (param: IParamFilterListCustomer, { dispatch }) => {
  dispatch(startLoading())
  const res = await ApiListCustomerCms(param)
  if (res.code == 200 && res.result && res.data) {
    return {
      param: param,
      ...res.data,
      isLoading: false,
      isNeedReloadData: false
    }
  } else {
    return {
      param: param,
      totalRecords: 10,
      pageIndex: 1,
      pageSize: 10,
      totalPages: 1,
      result: [],
      isLoading: false,
      isNeedReloadData: false
    }
  }
})

export const appCustomerSlice = createSlice({
  name: 'appCustomer',
  initialState: {
    data: null,
    totalRecords: 10,
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
    param: {
      keyword: "",
      pageIndex: 1,
      pageSize: 10,
      startDate: null,
      endDate: null
    },
    isLoading: true,
    isNeedReloadData: false
  },
  reducers: {
    startLoading: (state) => {
      state.isLoading = true
    },
    handleSetReloadDataCustomer: (state) => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetCustomer.fulfilled, (state, action) => {
      state.data = action.payload.result,
        state.totalRecords = action.payload.totalRecords,
        state.pageIndex = action.payload.pageIndex,
        state.totalPages = action.payload.totalPages,
        state.pageSize = action.payload.pageSize
      state.param = action.payload.param
      state.isLoading = action.payload.isLoading,
        state.isNeedReloadData = action.payload.isNeedReloadData

    })
  }
})

export const { startLoading, handleSetReloadDataCustomer } = appCustomerSlice.actions

export default appCustomerSlice.reducer
