// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiListWarrantyInformationCms } from 'src/api/warranty';
import { IParamListWarrantyInformation } from 'src/api/warranty/type';

// ** Fetch Invoices
export const fetchDataGetWarrantyInformation = createAsyncThunk('appWarrantyInformation/fetchData', async (param: IParamListWarrantyInformation, { dispatch }) => {
    dispatch(startLoading())
    const res = await ApiListWarrantyInformationCms(param)
    if (res.code == 200 && res.result && res.data) {
      return {
        param: param,
        ...res.data,
        isLoading: false,
        isNeedReloadData: false
      }
    } else {
      return {
        param: param,
        totalRecords: 10,
        pageIndex: 1,
        pageSize: 10,
        totalPages: 1,
        details: [],
        isLoading: false,
        isNeedReloadData: false
      }
    }
  })
  
  export const appWarrantyInformationSlice = createSlice({
    name: 'appWarrantyInformation',
    initialState: {
      data: null,
      totalRecords: 10,
      pageIndex: 1,
      pageSize: 10,
      totalPages: 1,
      param: {
        keyword: '',
        pageIndex: 1,
        pageSize: 10,
        customerId: 0,
        vehicleId: 0,
        startDate: null,
        endDate: null
      },
      isLoading: true,
      isNeedReloadData: false
    },
    reducers: {
      startLoading: (state) => {
        state.isLoading = true
      },
      handleSetReloadData: (state) => {
        state.isNeedReloadData = true
      }
    },
    extraReducers: builder => {
      builder.addCase(fetchDataGetWarrantyInformation.fulfilled, (state, action) => {
        state.data = action.payload.details,
          state.totalRecords = action.payload.totalRecords,
          state.pageIndex = action.payload.pageIndex,
          state.totalPages = action.payload.totalPages,
          state.pageSize = action.payload.pageSize,
          state.param = action.payload.param,
          state.isLoading = action.payload.isLoading,
          state.isNeedReloadData = action.payload.isNeedReloadData
  
      })
    }
  })
  
  export const { startLoading, handleSetReloadData } = appWarrantyInformationSlice.actions
  
  export default appWarrantyInformationSlice.reducer