// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiProfileUserCms } from 'src/api/authen'

// ** Fetch Invoices
export const fetchDataGetProfile = createAsyncThunk('appProfile/fetchData', async () => {
  const res = await ApiProfileUserCms();
  if (res.code === 200 && res.result) {
    return res.data
  }

  return {
    data: null,
    grantedAuthorities: null
  }
})

export const appProfileSlice = createSlice({
  name: 'appProfile',
  initialState: {
    data: null,
    grantedAuthorities: null
  },
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchDataGetProfile.fulfilled, (state, action) => {
      state.data = action.payload
      state.grantedAuthorities = action.payload.grantedAuthorities
    })
  }
})

export default appProfileSlice.reducer
