// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiListPermissionCms } from 'src/api/authen';

// ** Fetch Invoices
export const fetchDataGetPermission = createAsyncThunk('appPermission/fetchData', async () => {
  const res = await ApiListPermissionCms()
  if (res.code == 200 && res.result && res.data) {

    return res.data
  }

  return null
})

interface IParamPermissionRole {
  payload: {
    data: Array<string>
    param: {
      id: string;
      name: string
      description: string
    }
  }
  type: any,
}

export const appPermissionSlice = createSlice({
  name: 'appPermission',
  initialState: {
    data: null,
    dataPermissionRole: [],
    param: {
      id: "",
      name: "",
      description: "",
    }
  },
  reducers: {
    setListPermissionRoleEdit: (state, action: IParamPermissionRole) => {
      state.dataPermissionRole = action.payload.data,
      state.param = action.payload.param
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetPermission.fulfilled, (state, action) => {
      state.data = action.payload
    })
  }
})

export const { setListPermissionRoleEdit } = appPermissionSlice.actions

export default appPermissionSlice.reducer
