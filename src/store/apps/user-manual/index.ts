// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiGetListUserManualCMS } from 'src/api/document';
import { IParamListUserManual } from 'src/api/document/type';


interface PayloadAddLayout {
  payload: {
    id: number,
    imageAddLayout: any
  }
  type: any
}

// ** Fetch Invoices
export const fetchDataGetUserManual = createAsyncThunk('appUserManual/fetchData', async (param: IParamListUserManual, { dispatch }) => {
  dispatch(startLoading())
  const res = await ApiGetListUserManualCMS(param)
  if (res.code == 200 && res.result && res.data) {
    return {
      param: param,
      ...res.data,
      isLoading: false,
      isNeedReloadData: false
    }
  } else {
    return {
      param: param,
      totalRecords: 10,
      pageIndex: 1,
      pageSize: 10,
      totalPages: 1,
      details: [],
      isLoading: false,
      isNeedReloadData: false
    }
  }
})

export const appUserManualSlice = createSlice({
  name: 'appUserManual',
  initialState: {
    data: null,
    totalRecords: 10,
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
    param: {
      keyword: "",
      pageIndex: 1,
      pageSize: 10,
    },
    isLoading: false,
    isNeedReloadData: false,
    id: 0,
    imageAddLayout: null

  },
  reducers: {
    startLoading: (state) => {
      state.isLoading = true
    },
    handleSetReloadDataUserManual: (state) => {
      state.isNeedReloadData = true
    },
    handleSetAddLayout: (state, action: PayloadAddLayout) => {
      state.id = action.payload.id,
      state.imageAddLayout = action.payload.imageAddLayout
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetUserManual.fulfilled, (state, action) => {
      state.data = action.payload.details,
        state.totalRecords = action.payload.totalRecords,
        state.pageIndex = action.payload.pageIndex,
        state.totalPages = action.payload.totalPages,
        state.pageSize = action.payload.pageSize
      state.param = action.payload.param,
        state.isLoading = action.payload.isLoading,
        state.isNeedReloadData = action.payload.isNeedReloadData

    })
  }
})

export const { startLoading, handleSetReloadDataUserManual, handleSetAddLayout } = appUserManualSlice.actions

export default appUserManualSlice.reducer
