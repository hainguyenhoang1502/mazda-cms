// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiListBussinessTypeCMS, ApiListOrgTreeCMS } from 'src/api/organization'
import { IResponeOrgTree } from 'src/api/organization/type'

// ** Fetch BussinessType
export const fetchDataGetListBussinessType = createAsyncThunk('appOrganization/fetchBussinessTypeData', async () => {
  const res = await ApiListBussinessTypeCMS()
  if (res.code === 200 && res.result && res.data) {
    return {
      dataBussinessType: res.data,
    }
  } else {
    return {
      dataBussinessType: [],
    }
  }
})



// ** Fetch OrganizationTree
export const fetchDataGetOrganizationTree = createAsyncThunk('appOrganization/fetchOrganizationTreeData', async () => {
  const res = await ApiListOrgTreeCMS()
  let transformedData = []
  if (res.code === 200 && res.result && res.data) {
    if (res.data && res.data.length > 0) {
      transformedData = TransformedDataOrg(res.data)
    }

    return {
      dataOrgTree: transformedData.length > 0 ? transformedData : res.data,
    }
  } else {

    return {
      dataOrgTree: [],
    }
  }
})


const TransformedDataOrg = (data: IResponeOrgTree["data"]) => {
  const transformedData = data.map(region => {
    let allCodeRegionArea = []
    let allComapnyDealer = []
    const allChild = region.areas.map(area => area.areaCode);


    const areas = area => {
      const allChild = area.companies.map(company => company.companyCode);
      const companies = company => {
        const allChild = company.dealers.map(dealer => dealer.dealerCode);

        // ** Set all DealderCode
        allComapnyDealer = [...allComapnyDealer, ...allChild]

        return {
          ...company,
          allChild,
          dealers: company.dealers.map(dealer => ({
            ...dealer,
          }))
        };

      };

      // ** Set all CompanyCode
      allComapnyDealer = [...allComapnyDealer, ...allChild]

      return {
        ...area,
        allChild,
        companies: area.companies.map(company => companies(company))
      };
    };


    // ** Set all AreaCode of Region
    allCodeRegionArea = [...allCodeRegionArea, ...allChild]
    allCodeRegionArea.push(region.regionCode)

    return {
      ...region,
      allChild,
      areas: region.areas.map(area => areas(area)),
      allCodeRegionArea,
      allComapnyDealer
    };
  });

  return transformedData;
}

export const appOrganizationSlice = createSlice({
  name: 'appOrganization',
  initialState: {
    dataBussinessType: [],
    dataOrgTree: [],
  },
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchDataGetListBussinessType.fulfilled, (state, action) => {
      state.dataBussinessType = action.payload.dataBussinessType
    })
    builder.addCase(fetchDataGetOrganizationTree.fulfilled, (state, action) => {
      state.dataOrgTree = action.payload.dataOrgTree
    })
  }
})

export default appOrganizationSlice.reducer
