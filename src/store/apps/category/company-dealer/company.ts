import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { ApiGetListCompanyCms } from "src/api/organization";
import { IParamFilter } from "src/api/type";

export const fetchDataGetListCompany = createAsyncThunk('appCompany/fetchData', async (param: IParamFilter, { dispatch }) => {
  dispatch(startLoading())
  let arr = []
  const res = await ApiGetListCompanyCms(param)
  if (res.code == 200 && res.result && res.data) {
    if (res.data.data && res.data.data.length > 0) {
      arr = res.data.data.map(item => ({
        label: item.legalEntityName,
        value: item.code
      }))

    }
    
    return {
      param: param,
      ...res.data,
      arrFilter: arr,
      isLoading: false,
      isNeedReloadData: false
    }
  } else {
    return {
      param: param,
      totalRecords: 10,
      pageIndex: 1,
      pageSize: 10,
      totalPages: 1,
      data: [],
      arrFilter: [],
      isLoading: false,
      isNeedReloadData: false
    }
  }
}
)

export const appCompanySlice = createSlice({
  name: "appCompany",
  initialState: {
    data: [],
    arrFilter: [],
    totalRecords: 10,
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
    param: {
      pageIndex: 1,
      pageSize: 10,
      keyword: "",
      status: "",
    },
    isLoading: true,
    isNeedReloadData: false
  },

  reducers: {
    startLoading: state => {
      state.isLoading = true
    },
    handleSetReloadDataCompany: state => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetListCompany.fulfilled, (state, action) => {
      state.data = action.payload.data,
        state.arrFilter = action.payload.arrFilter,
        state.totalRecords = action.payload.totalRecords,
        state.pageIndex = action.payload.pageIndex,
        state.totalPages = action.payload.totalPages,
        state.pageSize = action.payload.pageSize,
        state.param = action.payload.param,
        state.isLoading = action.payload.isLoading,
        state.isNeedReloadData = action.payload.isNeedReloadData
    })
  }

})

export const { startLoading, handleSetReloadDataCompany } = appCompanySlice.actions
export default appCompanySlice.reducer