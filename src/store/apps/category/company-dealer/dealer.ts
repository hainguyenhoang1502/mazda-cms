import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { ApiGetListDealerCms } from "src/api/organization";
import { IParamFilter } from "src/api/type";

export const fetchDataGetListDealers = createAsyncThunk('appDealer/fetchData', async (param: IParamFilter, { dispatch }) => {
  dispatch(startLoading())
  const res = await ApiGetListDealerCms(param)
  if (res.code == 200 && res.result && res.data) {

    return {
      param: param,
      ...res.data,
      isLoading: false,
      isNeedReloadData: false,
    }
  } else {
    return {
      param: param,
      totalRecords: 10,
      pageIndex: 1,
      pageSize: 10,
      totalPages: 1,
      data: [],
      isLoading: false,
      isNeedReloadData: false,
    }
  }
}
)

export const fetchDataGetListFilterDealers = createAsyncThunk('appDealer/fetchDataFilter', async () => {
  const res = await ApiGetListDealerCms(appDealerSlice.getInitialState().paramFilter)
  if (res.code == 200 && res.result && res.data && res.data.data && res.data.data.length > 0) {
    const arr = res.data.data.map(item => ({
      label: item.name,
      value: item.code
    }))
    
    return {
      arrFilter: arr,
    }
  } else {

    return {
      arrFilter: [],

    }
  }
}
)

export const appDealerSlice = createSlice({
  name: "appDealer",
  initialState: {
    data: [],
    arrFilter: [],
    totalRecords: 10,
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
    param: {
      pageIndex: 1,
      pageSize: 10,
      keyword: "",
      status: "",
    },
    paramFilter: {
      pageIndex: 1,
      pageSize: 9999,
      keyword: "",
      status: "ACTIVE",
    },
    isLoading: true,
    isNeedReloadData: false
  },

  reducers: {
    startLoading: state => {
      state.isLoading = true
    },
    handleSetReloadDataBrand: state => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetListDealers.fulfilled, (state, action) => {
      state.data = action.payload.data,
        state.totalRecords = action.payload.totalRecords,
        state.pageIndex = action.payload.pageIndex,
        state.totalPages = action.payload.totalPages,
        state.pageSize = action.payload.pageSize,
        state.param = action.payload.param,
        state.isLoading = action.payload.isLoading,
        state.isNeedReloadData = action.payload.isNeedReloadData
    }),
      builder.addCase(fetchDataGetListFilterDealers.fulfilled, (state, action) => {
        state.arrFilter = action.payload.arrFilter
      })
  }
})

export const { startLoading, handleSetReloadDataBrand } = appDealerSlice.actions
export default appDealerSlice.reducer