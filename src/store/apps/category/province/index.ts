import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { ApiPostListProvince } from 'src/api/refData'
import { IParamFilter } from 'src/api/type'

export const fetchDataGetListProvince = createAsyncThunk(
  'appProvince/fetchData',
  async (param: IParamFilter, { dispatch }) => {
    dispatch(startLoading())
    const res = await ApiPostListProvince(param)
    if (res.code == 200 && res.result && res.data) {

      const arr = res.data.result.map(item => ({
        label: item.locationName,
        value: item.code
      }))

      return {
        param,
        arrFilter: arr,
        ...res.data,
        isLoading: false,
        isNeedReloadData: false
      }
    } else {
      return {
        param: param,
        totalRecords: 10,
        pageIndex: 1,
        pageSize: 10,
        totalPages: 1,
        arrFilter: [],
        result: [],
        isLoading: false,
        isNeedReloadData: false
      }
    }
  }
)


export const fetchDataGetListFilterAllProvince = createAsyncThunk('appProvince/fetchFilterData', async () => {
  const res = await ApiPostListProvince(appProvinceSlice.getInitialState().paramFilter)
  if (res.code == 200 && res.result && res.data && res.data.result) {
    const arr = res.data.result.map(item => ({
      label: item.locationName,
      value: item.code
    }))

    return {
      arrFilter: arr,
    }
  } else {
    return {
      arrFilter: [],
    }
  }
}
)

export const appProvinceSlice = createSlice({
  name: 'appProvince',
  initialState: {
    data: null,
    totalRecords: 10,
    arrFilter: [],
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
    param: {
      keyword: '',
      pageIndex: 1,
      pageSize: 10,
      status: null
    },
    paramFilter: {
      keyword: '',
      pageIndex: 1,
      pageSize: 9999,
      status: "ACTIVE"
    },
    isLoading: true,
    isNeedReloadData: false
  },
  reducers: {
    startLoading: state => {
      state.isLoading = true
    },
    handleSetReloadDataProvince: state => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetListProvince.fulfilled, (state, action) => {
      ; (state.data = action.payload.result),
        (state.totalRecords = action.payload.totalRecords),
        (state.pageIndex = action.payload.pageIndex),
        (state.totalPages = action.payload.totalPages),
        (state.pageSize = action.payload.pageSize),
        (state.param = action.payload.param),
        (state.isLoading = action.payload.isLoading),
        (state.isNeedReloadData = action.payload.isNeedReloadData)
    }),
      builder.addCase(fetchDataGetListFilterAllProvince.fulfilled, (state, action) => {
        state.arrFilter = action.payload.arrFilter
      })
  }
})
export default appProvinceSlice.reducer
export const { startLoading, handleSetReloadDataProvince } = appProvinceSlice.actions
