import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { ApiGetListArea } from 'src/api/refData'
import { IParamFilter } from 'src/api/type'

export const fetchDataGetListArea = createAsyncThunk(
  'appArea/fetchData',
  async (param: IParamFilter, { dispatch }) => {
    dispatch(startLoading())
    const res = await ApiGetListArea(param)
    if (res.code == 200 && res.result && res.data) {

      const arr = res.data.result.map(item => ({
        label: item.areaName,
        value: item.code
      }))

      return {
        param: param,
        arrFilter: arr,
        ...res.data,
        isLoading: false,
        isNeedReloadData: false
      }
    } else {
      return {
        param: param,
        totalRecords: 10,
        pageIndex: 1,
        pageSize: 10,
        totalPages: 1,
        arrFilter: [],
        result: [],
        isLoading: false,
        isNeedReloadData: false
      }
    }
  }
)

export const appAreaSlice = createSlice({
  name: 'appArea',
  initialState: {
    data: null,
    totalRecords: 10,
    arrFilter: [],
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
    param: {
      keyword: '',
      pageIndex: 1,
      pageSize: 10,
      status: null
    },
    isLoading: true,
    isNeedReloadData: false
  },
  reducers: {
    startLoading: state => {
      state.isLoading = true
    },
    handleSetReloadData: state => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetListArea.fulfilled, (state, action) => {
      ;(state.data = action.payload.result),
        (state.totalRecords = action.payload.totalRecords),
        (state.pageIndex = action.payload.pageIndex),
        (state.totalPages = action.payload.totalPages),
        (state.pageSize = action.payload.pageSize),
        (state.param = action.payload.param),
        (state.isLoading = action.payload.isLoading),
        (state.arrFilter = action.payload.arrFilter),
        (state.isNeedReloadData = action.payload.isNeedReloadData)
    })
  }
})
export default appAreaSlice.reducer
export const { startLoading, handleSetReloadData } = appAreaSlice.actions
