// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { ApiGetListRegion } from 'src/api/refData'
import { IParamFilter } from 'src/api/type'

// ** Fetch Invoices
export const fetchDataGetListRegion = createAsyncThunk(
  'appRegion/fetchData',
  async (param: IParamFilter, { dispatch }) => {
    dispatch(startLoading())
    const res = await ApiGetListRegion(param)
    if (res.code == 200 && res.result && res.data) {

      const arr = res.data.result.map(item => ({
        label: item.regionName,
        value: item.code
      }))

      return {
        param: param,
        ...res.data,
        isLoading: false,
        isNeedReloadData: false,
        arrFilter: arr
      }
    } else {
      return {
        param: param,
        totalRecords: 10,
        data:null,
        pageIndex: 1,
        pageSize: 10,
        totalPages: 1,
        result: [],
        arrFilter: [],
        isLoading: false,
        isNeedReloadData: false
      }
    }
  }
)

export const appRegionSlice = createSlice({
  name: 'appRegion',
  initialState: {
    data: null,
    arrFilter: [],
    totalRecords: 10,
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
    param: {
      keyword: '',
      pageIndex: 1,
      pageSize: 10,
      status: null
    },
    isLoading: true,
    isNeedReloadData: false
  },
  reducers: {
    startLoading: state => {
      state.isLoading = true
    },
    handleSetReloadDataRegion: state => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetListRegion.fulfilled, (state, action) => {
        (state.data = action.payload.result),
        (state.totalRecords = action.payload.totalRecords),
        (state.pageIndex = action.payload.pageIndex),
        (state.totalPages = action.payload.totalPages),
        (state.pageSize = action.payload.pageSize),
        (state.param = action.payload.param),
        (state.isLoading = action.payload.isLoading),
        (state.arrFilter = action.payload.arrFilter),
        (state.isNeedReloadData = action.payload.isNeedReloadData)
    })
  }
})

export const { startLoading, handleSetReloadDataRegion } = appRegionSlice.actions

export default appRegionSlice.reducer
