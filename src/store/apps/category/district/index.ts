import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { ApiPostListDistrict } from 'src/api/refData'
import { IParamFilter } from 'src/api/type'

export const fetchDataGetListDistrict = createAsyncThunk(
  'appDistrict/fetchData',
  async (param: IParamFilter, { dispatch }) => {
    dispatch(startLoading())
    const res = await ApiPostListDistrict(param)
    if (res.code == 200 && res.result && res.data) {

      const arr = res.data.result.map(item => ({
        label: item.districtFullName,
        value: item.code
      }))

      return {
        param,
        arrFilter: arr,
        ...res.data,
        isLoading: false,
        isNeedReloadData: false
      }
    } else {
      return {
        param,
        totalRecords: 10,
        pageIndex: 1,
        pageSize: 10,
        totalPages: 1,
        arrFilter: [],
        result: [],
        isLoading: false,
        isNeedReloadData: false
      }
    }
  }
)

export const appDistrictSlice = createSlice({
  name: 'appDistrict',
  initialState: {
    data: null,
    totalRecords: 10,
    arrFilter: [],
    pageIndex: 1,
    pageSize: 10,
    totalPages: 1,
    param: {
      keyword: '',
      pageIndex: 1,
      pageSize: 10,
      status: null
    },
    isLoading: true,
    isNeedReloadData: false
  },
  reducers: {
    startLoading: state => {
      state.isLoading = true
    },
    handleSetReloadDataDistrict: state => {
      state.isNeedReloadData = true
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchDataGetListDistrict.fulfilled, (state, action) => {
      ;(state.data = action.payload.result),
        (state.totalRecords = action.payload.totalRecords),
        (state.pageIndex = action.payload.pageIndex),
        (state.totalPages = action.payload.totalPages),
        (state.pageSize = action.payload.pageSize),
        (state.param = action.payload.param),
        (state.isLoading = action.payload.isLoading),
        (state.arrFilter = action.payload.arrFilter),
        (state.isNeedReloadData = action.payload.isNeedReloadData)
    })
  }
})
export default appDistrictSlice.reducer
export const { startLoading, handleSetReloadDataDistrict } = appDistrictSlice.actions
