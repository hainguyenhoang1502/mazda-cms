import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { ApiOrganizationBrandList } from "src/api/organization";
import { IParamBrandList } from "src/api/organization/type";


export const fetchDataGetListBrand = createAsyncThunk(
    'appBrand/fetchData',
    async (param: IParamBrandList, { dispatch }) => {
        dispatch(startLoading())
        let arr = []
        const res = await ApiOrganizationBrandList(param)
        if (res.code == 200 && res.result && res.data) {

            if (res.data.data && res.data.data.length > 0) {
                arr = res.data.data.map(item => ({
                  label: item.name,
                  value: item.code
                }))
          
              }

            return {
                param,
                ...res.data,
                isLoading: false,
                isNeedReloadData: false,
                arrFilter: arr
            }
        } else {
            return {
                param: param,
                arrFilter: arr,
                totalRecords: 10,
                pageIndex: 1,
                pageSize: 10,
                totalPages: 1,
                data: [],
                isLoading: false,
                isNeedReloadData: false
            }
        }
    }
)

export const appBrandSlice = createSlice({
    name: "appBrand",
    initialState: {
        data: [],
        arrFilter: [],
        totalRecords: 10,
        pageIndex: 1,
        pageSize: 10,
        totalPages: 1,
        param: {
            pageIndex: 1,
            pageSize: 10,
            code: "",
            status: "",
        },
        isLoading: true,
        isNeedReloadData: false
    },

    reducers: {
        startLoading: state => {
            state.isLoading = true
        },
        handleSetReloadDataBrand: state => {
            state.isNeedReloadData = true
        }
    },
    extraReducers: builder => {
        builder.addCase(fetchDataGetListBrand.fulfilled, (state, action) => {
            (state.data = action.payload.data),
            (state.totalRecords = action.payload.totalRecords),
            (state.pageIndex = action.payload.pageIndex),
            (state.totalPages = action.payload.totalPages),
            (state.pageSize = action.payload.pageSize),
            (state.param = action.payload.param),
            (state.arrFilter = action.payload.arrFilter),
            (state.isLoading = action.payload.isLoading),
            (state.isNeedReloadData = action.payload.isNeedReloadData)
        })
    }

})

export const { startLoading, handleSetReloadDataBrand } = appBrandSlice.actions
export default appBrandSlice.reducer