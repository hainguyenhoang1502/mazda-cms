/* eslint-disable */
import axiosClient from "../axios-client"
import {
    SLUG_VEHICLE
} from 'src/configs/envConfig'

export interface ParamsList{
    keyword:string
    pageIndex:number
    pageSize:number
    modelCode?:string
    gradeCode?:string
    colorCode?:string
}

export const CarImageApi = {
    createVehiclesImage(params: any) {

        return axiosClient.post(`/${SLUG_VEHICLE}/api/cms/create-vehicle-image`, params)
    },

    getListVehiclesImage(paramsList:ParamsList){

        return axiosClient.post(`/${SLUG_VEHICLE}/api/cms/list-vehicle-images`, paramsList)
    },

    getListModelCode(){

        return axiosClient.get(`/${SLUG_VEHICLE}/api/cms/list-vehicle-models-code`)
    },

    getListGradeCode(){

        return axiosClient.get(`/${SLUG_VEHICLE}/api/cms/list-vehicle-grades-code`)
    },

    getListColorCode(){

        return axiosClient.get(`/${SLUG_VEHICLE}/api/cms/list-vehicle-colors-code`)
    },

    deleteVehiclesImage(id: string) {

        return axiosClient.delete(`/${SLUG_VEHICLE}/api/cms/delete-vehicle-image/${id}`)
    },

    getVehiclesImageDetail(id: any) {

        return axiosClient.get(`/${SLUG_VEHICLE}/api/cms/vehicle-images-detail-by-id?id=${id}`)
    },

    updateVehiclesImage(params: any) {

        return axiosClient.put(`/${SLUG_VEHICLE}/api/cms/update-vehicle-image`, params)
    }
}
/* eslint-enable */