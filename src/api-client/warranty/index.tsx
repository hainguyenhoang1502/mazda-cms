/* eslint-disable */
import axiosClient from "../axios-client"
import {
    SLUG_WARRANTY
} from 'src/configs/envConfig'
import { IPostFile } from "./type"

export interface ParamsList {
    keyword: string
    pageIndex: number
    pageSize: number
    status?: string
}

export const WarrantyPolicysApi = {

    createWarrantyPolicys(params: any) {

        return axiosClient.post(`/${SLUG_WARRANTY}/api/cms/create-warranty-policy`, params)
    },

    getListWarrantyPolicys(paramsList: ParamsList) {

        return axiosClient.post(`/${SLUG_WARRANTY}/api/cms/list-warranty-policy`, paramsList)
    },

    deleteWarrantyPolicys(id: string) {

        return axiosClient.delete(`/${SLUG_WARRANTY}/api/cms/delete-warranty-policy?id=${id}`)
    },

    getWarrantyPolicysDetail(id: any) {

        return axiosClient.get(`/${SLUG_WARRANTY}/api/cms/warranty-policy-detail-by-id?id=${id}`)
    },

    updateWarrantyPolicys(params: any) {

        return axiosClient.put(`/${SLUG_WARRANTY}/api/cms/update-warranty-policy`, params)
    },

    postFileWarranty(params: IPostFile) {
        
        return axiosClient.post(`/${SLUG_WARRANTY}/api/cms/import-warranty-information`, params)
    },

    postFileMaintainence(params: IPostFile) {
        
        return axiosClient.post(`/${SLUG_WARRANTY}/api/cms/import-maintenance-history`, params)
    },
}
/* eslint-enable */