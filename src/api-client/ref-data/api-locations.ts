import axiosClient from "../axios-client"
import {
    SLUG_REF_DATA
} from 'src/configs/envConfig'


export const locationsApi={
    getListProvinces(){
        return axiosClient.get(`/${SLUG_REF_DATA}/api/locations/get-provinces`)
    },
    getDistrictByProvinceId(provinceId:string){
        return axiosClient.get(`/${SLUG_REF_DATA}/api/locations/get-district-by-province-id?provinceId=${provinceId}`)
    },
    getWardByDistrictId(districtId:string){
        return axiosClient.get(`/${SLUG_REF_DATA}/api/locations/get-ward-by-district-id?districtId=${districtId}`)
 
    }
}