import axiosClient from "../axios-client"
import {
    SLUG_MEDIA_URL
} from 'src/configs/envConfig'


export const mediaApi={
    getImage(path:string): Promise<Blob>{
        return axiosClient.get(`/${SLUG_MEDIA_URL}/${path}`, {
            responseType: 'blob'
        })
    },
    getImageWithFullPath(paths:string):Promise<Blob>{
        return axiosClient.get(`${paths}`, {
            responseType: 'blob'
        })
    },
    uploadImage(formData:FormData){
        return axiosClient.post(`/${SLUG_MEDIA_URL}/api/files/upload-image`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
    },
}