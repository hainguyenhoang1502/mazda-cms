/* eslint-disable */
import axiosClient from "../axios-client"
import {
    SLUG_APPOINTMENT
} from 'src/configs/envConfig'

export interface ParamsList{
    keyword:string
    pageIndex:number
    pageSize:number
    createdDate?:Date
    status?:string
}

export const appointmentApi = {
    createappointment(params: any) {

        return axiosClient.post(`/${SLUG_APPOINTMENT}/api/appointment/create`, params)
    },
    getListAppointment(paramsList:ParamsList){

        return axiosClient.post(`/${SLUG_APPOINTMENT}/api/cms/list-appointment`, paramsList)
    },
    updateAppointmentStatus(param:{id:string, status:string}){

        return axiosClient.put(`/${SLUG_APPOINTMENT}/api/cms/update-appointment-status`, param)
    },
    deleteappointment(id: string) {

        return axiosClient.delete(`/${SLUG_APPOINTMENT}/api/cms/delete/${id}`)
    },

    getappointmentDetail(id: any) {

        return axiosClient.get(`/${SLUG_APPOINTMENT}/api/cms/appointment-detail-by-id?id=${id}`)
    },

    updateAppointment(params: any) {

        return axiosClient.put(`/${SLUG_APPOINTMENT}/api/cms/update-appointment`, params)
    }
}
/* eslint-enable */