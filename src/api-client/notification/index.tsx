
import axiosClient from "../axios-client"
import {
    SLUG_NOTIFICATION
} from 'src/configs/envConfig'


export const notificationsApi = {
    createNotification(paramssendNow: any) {
        return axiosClient.post(`/${SLUG_NOTIFICATION}/api/cms/create-notification`, paramssendNow)
    },

    sendNotification(notificationId: any) {

        return axiosClient.post(`/${SLUG_NOTIFICATION}/api/cms/send-notification`, notificationId)
    },

    getListNotifications(paramsList:any){
        return axiosClient.post(`/${SLUG_NOTIFICATION}/api/cms/list-notification`,paramsList)
    },

    getListNotificationCustomer(paramsList:any){
        return axiosClient.post(`/${SLUG_NOTIFICATION}/api/cms/list-customer-by-send-to-notification`,paramsList)
    },

    deleteNotification(notifyId: string) {
        let listId = []
        listId = [...listId, notifyId]

        return axiosClient.post(`/${SLUG_NOTIFICATION}/api/cms/delete-notification`, { listId: listId })
    },

    getNotificationDetail(notifyId: any) {
        return axiosClient.post(`/${SLUG_NOTIFICATION}/api/cms/notification-detail-by-id`, { id: notifyId })
    },

    updateNotification(params: any) {

        return axiosClient.put(`/${SLUG_NOTIFICATION}/api/cms/notification-update`, params)
    }
}