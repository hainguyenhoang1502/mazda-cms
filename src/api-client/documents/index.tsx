/* eslint-disable */
import axiosClient from "../axios-client"
import {
    SLUG_DOCUMENTS
} from 'src/configs/envConfig'

export interface ParamsList{
    keyword:string
    pageIndex:number
    pageSize:number
    filter:string
}

export const signalLightsApi = {
    createsignalLights(params: any) {

        return axiosClient.post(`/${SLUG_DOCUMENTS}/api/signallights/create`, params)
    },
    getListsignalLights(paramsList:ParamsList){

        return axiosClient.get(`/${SLUG_DOCUMENTS}/api/signallights/list/${paramsList.pageIndex}/${paramsList.pageSize}?keyword=${paramsList.keyword}&status=${paramsList.filter}`)
    },
    deletesignalLights(id: string) {

        return axiosClient.delete(`/${SLUG_DOCUMENTS}/api/signallights/delete/${id}`)
    },

    getsignalLightsDetail(id: any) {

        return axiosClient.get(`/${SLUG_DOCUMENTS}/api/signallights/deltail/${id}`)
    },

    updatesignalLights(params: any) {

        return axiosClient.put(`/${SLUG_DOCUMENTS}/api/signallights/update`, params)
    }
}
/* eslint-enable */