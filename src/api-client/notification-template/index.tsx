/* eslint-disable */
import axiosClient from "../axios-client"
import {
    SLUG_NOTIFICATION
} from 'src/configs/envConfig'

export interface ParamsList{
    keyword:string
    pageIndex:number
    pageSize:number
    status?:string
}

export interface ParamCreate{
    name:string
    content:string
    status:string
}
export interface ParamUpdate{
    name:string
    content:string
    status:string
}

export const notificationTemplateApi = {
    createTemplate(params: ParamCreate) {

        return axiosClient.post(`/${SLUG_NOTIFICATION}/api/cms/create-notification-template`, params)
    },

    getListTemplate(paramsList:ParamsList){

        return axiosClient.post(`/${SLUG_NOTIFICATION}/api/cms/list-notification-template`, paramsList)
    },

    deleteTemplate(id: string) {

        return axiosClient.delete(`/${SLUG_NOTIFICATION}/api/cms/delete-notification-template?id=${id}`)
    },

    getDetailTemplate(id: any) {

        return axiosClient.get(`/${SLUG_NOTIFICATION}/api/cms/notification-template-detail-by-id?id=${id}`)
    },

    updateNotificationTemplate(params: ParamUpdate) {

        return axiosClient.put(`/${SLUG_NOTIFICATION}/api/cms/update-notification-template`, params)
    }
}
/* eslint-enable */