
import axiosClient from "../axios-client"
import {
    SLUG_DOCUMENTS
} from 'src/configs/envConfig'

export interface ParamsList{
    keyword:string
    pageIndex:number
    pageSize:number
    status?:string
}

export const surveysApi = {

    //#region Survey
    createSurveys(params: any) {

        return axiosClient.post(`/${SLUG_DOCUMENTS}/api/surveys/create`, params)
    },
    getListSurveys(paramsList:any){

        return axiosClient.get(`/${SLUG_DOCUMENTS}/api/surveys/list/${paramsList.pageIndex}/${paramsList.pageSize}?keyword=${paramsList.keyword}&status=${paramsList.status}${paramsList.fromDate ? `&fromDate=${paramsList.fromDate}` : ''}${paramsList.toDate ? `&toDate=${paramsList.toDate}` : ''}`);

    },
    deleteSurveys(id: string) {

        return axiosClient.delete(`/${SLUG_DOCUMENTS}/api/surveys/delete/${id}`)
    },

    getSurveysDetail(id: any) {

        return axiosClient.get(`/${SLUG_DOCUMENTS}/api/surveys/deltail/${id}`)
    },

    updateSurveys(params: any) {

        return axiosClient.put(`/${SLUG_DOCUMENTS}/api/surveys/update`, params)
    },
    addSurveyQuestion(params: any) {

        return axiosClient.post(`/${SLUG_DOCUMENTS}/api/surveys/add-question`, params)
    },

    //#endregion

    //#region Question

    createSurveysQuestion(params: any) {

        return axiosClient.post(`/${SLUG_DOCUMENTS}/api/surveys/create-question`, params)
    },
    getListSurveysQuestion(params:any){

        return axiosClient.get(`/${SLUG_DOCUMENTS}/api/surveys/question-list/${params.pageIndex}/${params.pageSize}/?keyword=${params.keyword}&status=${params.status}`)
    },
    deleteSurveysQuestion(id: string) {

        return axiosClient.delete(`/${SLUG_DOCUMENTS}/api/surveys/delete-question/${id}`)
    },
    removeSurveysQuestion(params: any) {

        return axiosClient.delete(`/${SLUG_DOCUMENTS}/api/surveys/remove-question?SurveyId=${params.surveyId}&QuestionId=${params.questionId}`)
    },

    getSurveysQuestionDetail(id: any) {

        return axiosClient.get(`/${SLUG_DOCUMENTS}/api/surveys/question-deltail/${id}`)
    },

    updateSurveysQuestion(params: any) {

        return axiosClient.put(`/${SLUG_DOCUMENTS}/api/surveys/update-question`, params)
    },
    createSurveysAnswer(params: any) {

        return axiosClient.post(`/${SLUG_DOCUMENTS}/api/surveys/add-answer`, params)
    },
    updateSurveysAnswer(params: any) {

        return axiosClient.put(`/${SLUG_DOCUMENTS}/api/surveys/update-answer`, params)
    },
    deleteSurveysAnswer(id: string) {

        return axiosClient.delete(`/${SLUG_DOCUMENTS}/api/surveys/delete-answer/${id}`)
    },

    //#endregion
}
