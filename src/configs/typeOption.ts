
interface IOptionsFilter {
  value: number | string
  label: number | string
}
interface IPagination {
  pageIndex: number
  pageSize: number
  totalPages: number
  totalRecords: number
}
interface PickerProps {
  label?: string
  error?: boolean
  registername?: string
  texticon?: string
}

interface ModalConfirmState {
  isOpen: boolean,
  title: string
  action: Function
}
interface ModalNotifyState {
  isOpen: boolean;
  title: string;
  actionReturn?: Function
  message?: string
  isError?:boolean
  textBtn?:string
}
export type { IOptionsFilter, IPagination, PickerProps, ModalConfirmState ,ModalNotifyState}
