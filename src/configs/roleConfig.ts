
export const permissonConfig = {

  // ** Customers
  CUSTOMER_ADD: "customer.create",
  CUSTOMER_EDIT: "customer.edit",
  CUSTOMER_EXPORT: "customer.export",
  CUSTOMER_DELETE: "customer.delete",
  CUSTOMER_LIST_VIEW: "customer.list.view",

  // ** Roles
  ROLE_VIEW: "role.view",
  ROLE_EDIT: "role.edit",
  ROLE_CREATE: "role.create",
  ROLE_ASSIGN: "role.assign",
  ROLE_DELETE: "role.delete",

  // ** Users
  USER_VIEW: "user.view",
  USER_EDIT: "user.edit",
  USER_CREATE: "user.create",
  USER_LIST_VIEW: "user.list.view",

  // ** Appointment
  APPOINTMENT_EDIT: "appointment.edit",
  APPOINTMENT_EXPORT: "appointment.export",
  APPOINTMENT_CREATE: "appointment.create",
  APPOINTMENT_DELETE: "appointment.delete",
  APPOINTMENT_LIST_VIEW: "appointment.list.view",

  // ** Brand
  BRAND_EDIT: "brand.edit",
  BRAND_DELETE: "brand.delete",
  BRAND_CREATE: "brand.create",
  BRAND_LIST_VIEW: "brand.list.view",

  // ** Organization
  ORG_EDIT: "org.edit",
  ORG_CREATE: "org.create",
  ORG_DELETE: "org.delete",
  ORG_LIST_VIEW: "org.list.view",

  // ** Location
  LOCATION_EDIT: "location.edit",
  LOCATION_CREATE: "location.create",
  LOCATION_DELETE: "location.delete",
  LOCATION_LIST_VIEW: "location.list.view",

  // ** Survey
  SURVEY_QUESTION_EDIT: "survey.question.edit",
  SURVEY_QUESTION_CREATE: "survey.question.create",
  SURVEY_QUESTION_DELETE: "survey.question.delete",
  SURVEY_QUESTION_LIST_VIEW: "survey.question.list.view",

  // ** Warranty Info
  WARRANTY_INFO_EDIT: "warranty.info.edit",
  WARRANTY_INFO_CREATE: "warranty.info.create",
  WARRANTY_INFO_DELETE: "warranty.info.delete",
  WARRANTY_INFO_LIST_VIEW: "warranty.info.list.view",

  // ** Warranty Policy
  WARRANTY_POLICY_EDIT: "warranty.policy.edit",
  WARRANTY_POLICY_CREATE: "warranty.policy.create",
  WARRANTY_POLICY_DELETE: "warranty.policy.delete",
  WARRANTY_POLICY_LIST_VIEW: "warranty.policy.list.view",

  // ** Notify
  NOTIFY_SEND: "notify.send",
  NOTIFY_EDIT: "notify.edit",
  NOTIFY_CREATE: "notify.create",
  NOTIFY_DELETE: "notify.delete",
  NOTIFY_LIST_VIEW: "notify.list.view",

  // ** Document Signallight
  DOC_SIGNALLIGHT_EDIT: "doc.signallight.edit",
  DOC_SIGNALLIGHT_CREATE: "doc.signallight.create",
  DOC_SIGNALLIGHT_DELETE: "doc.signallight.delete",
  DOC_SIGNALLIGHT_LIST_VIEW: "doc.signallight.list.view",

  // ** Document Usermanual
  DOC_USERMANUAL_EDIT: "doc.usermanual.edit",
  DOC_USERMANUAL_CREATE: "doc.usermanual.create",
  DOC_USERMANUAL_DELETE: "doc.usermanual.delete",
  DOC_USERMANUAL_LIST_VIEW: "doc.usermanual.list.view",

  // ** Maintainence
  MAINTENANCE_EDIT: "maintenance.edit",
  MAINTENANCE_CREATE: "maintenance.create",
  MAINTENANCE_DELETE: "maintenance.delete",
  MAINTENANCE_LIST_VIEW: "maintenance.list.view",

  // ** Car Imgae
  CAR_IMAGE_EDIT: "car.image.edit",
  CAR_IMAGE_CREATE: "car.image.create",
  CAR_IMAGE_DELETE: "car.image.delete",
  CAR_IMAGE_LIST_VIEW: "car.image.list.view",

  //** Car Version/
  CAR_VERSION_EDIT: "car.version.edit",
  CAR_VERSION_CREATE: "car.version.create",
  CAR_VERSION_DELETE: "car.version.delete",
  CAR_VERSION_LIST_VIEW: "car.version.list.view",
}
