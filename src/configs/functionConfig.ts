
import { WWW_SERVICE_IMAGE } from './envConfig'
import {
  ENTERPRISE,
  PERSONAL,
} from './initValueConfig'
import { IOptionsFilter } from './typeOption'

/* Date Time   */

const renderDate = date => {
  if (date) {
    const dt = new Date(date)
    const day = dt.getDate() > 9 ? dt.getDate() : `0` + dt.getDate()
    const month = dt.getMonth() > 8 ? dt.getMonth() + 1 : `0` + (dt.getMonth() + 1)
    const year = dt.getFullYear()

    return day.toString() + '/' + month.toString() + '/' + year
  } else {
    return ''
  }
}
const renderMonth = date => {
  if (date) {
    const dt = new Date(date)
    const month = dt.getMonth() > 8 ? dt.getMonth() + 1 : `0` + (dt.getMonth() + 1)
    const year = dt.getFullYear()

    return month + '/' + year
  } else {
    return ''
  }
}

/* Option Filter  */

const OPTIONS_GENDER: Array<IOptionsFilter> = [
  {
    value: 'MALE',
    label: 'Nam'
  },
  {
    value: 'FEMALE',
    label: 'Nữ'
  }
]

const renderNameGender = (value: string) => {
  const item = OPTIONS_GENDER.find(elm => elm.value === value)

  return item ? item.label : ''
}

const OPTIONS_TYPE_CUSTOMER: Array<IOptionsFilter> = [
  {
    value: PERSONAL,
    label: 'Khách hàng cá nhân'
  },
  {
    value: ENTERPRISE,
    label: 'Khách hàng doanh nghiệp'
  }
]
const renderNameTypeCustomer = (value: string) => {
  const item = OPTIONS_TYPE_CUSTOMER.find(elm => elm.value === value)

  return item ? item.label : ''
}
const OPTIONS_STATUS: Array<IOptionsFilter> = [
  {
    value: 'ACTIVE',
    label: 'Đang hoạt động'
  },
  {
    value: 'INACTIVE',
    label: 'Ngưng hoạt động'
  }
]

const OPTIONS_DOCUMENT: Array<IOptionsFilter> = [
  {
    value: 'VANBAN',
    label: 'Văn bản'
  },
  {
    value: 'MULCHOICE',
    label: 'Chọn nhiều đáp án'
  },
  {
    value: 'ONECHOICE',
    label: 'Chọn 1 đáp án'
  },
]

const OPTIONS_STATUS_FILTER = [
  {
    value: null,
    label: 'Tất cả'
  },
  ...OPTIONS_STATUS,
]

const renderNameStatus = (value: string | number) => {
  const item = OPTIONS_STATUS.find(elm => elm.value === value)

  return item ? item.label : ''
}

// ** Render Text Title
const renderTextTitle = (text: string) => {
  if (text) {
    return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase()
  }
}

const renderUrlImageUpload = (img: string) => {
  if (!img) return
  
  return img.replace(WWW_SERVICE_IMAGE, "")
}

export {
  renderDate,
  renderMonth,
  renderNameGender,
  renderNameTypeCustomer,
  renderNameStatus,
  renderTextTitle,
  renderUrlImageUpload,
  OPTIONS_GENDER,
  OPTIONS_TYPE_CUSTOMER,
  OPTIONS_STATUS,
  OPTIONS_DOCUMENT,
  OPTIONS_STATUS_FILTER
}
