import { IPagination, ModalConfirmState, ModalNotifyState } from './typeOption'


export const INIT_VALUE_PAGINATION: IPagination = {
  pageIndex: 1,
  pageSize: 10,
  totalPages: 1,
  totalRecords: 10
}
export const INIT_STATE_MODAL_CONFIRM: ModalConfirmState = {
  isOpen: false,
  title: "",
  action: () => { return }
}
export const INIT_STATE_MODAL_NOTIFY: ModalNotifyState = {
  isOpen: false,
  title: "",
  message: ""
}
export const regexPhone = /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/
export const regexEmail = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
export const regexWebsite = /^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(:(\d+))?(\/[-a-z\d%_.~+]*)*(\?[;&a-z\d%_.~+=-]*)?(#\S*)?$/i
export const regexPassword = /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=[\]{}|;:,.<>?])[^ ]{8,}$/
export const regexUserName = /^[A-Za-z0-9.\-_]{4,30}$/;
export const ERROR_NULL_VALUE_INPUT = 'Vui lòng nhập thông tin'
export const ERROR_PHONE_VALUE_INPUT = 'Số điện thoại không hợp lệ'
export const ERROR_EMAIL_VALUE_INPUT = 'Email không hợp lệ '
export const ERROR_WEBSITE_VALUE_INPUT = 'Website không hợp lệ'
export const ERROR_PASSSWORD_VALUE_INPUT = "Mật khẩu không đủ mạnh! Ít nhất phải có 6 ký tự, trong đó có  1 ký tự số, 1 ký tự viết hoa và 1 ký tự đặc biệt"
export const ERROR_INVALID_PASSWORD = "Mật khẩu không hợp lệ"
export const ERROR_USERNAME_VALUE_INPUT = "Tên đăng nhập không hợp lệ"
export const ENTERPRISE = "ENTERPRISE"
export const PERSONAL = "PERSONAL"
export const UNKNOW_ERROR = "Lỗi không xác định"
export const ACTION_ACTIVE_TEXT = "Kích hoạt"
export const ACTION_INACTIVE_TEXT = "Vô hiệu hóa"
export const NODATA_TEXT = "Không có dữ liệu"
export const MAX_SIZE_IMAGE = 5242880
export const ERROR_VALUE_INPUT_IMAGE = "Định dạng hình ảnh không hợp lệ hoặc có kích thước > 5MB"