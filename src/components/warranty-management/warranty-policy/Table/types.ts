export type NotificationRowsType = {
    id: number
    title: string
    content: string
    createDate: string
    sendDate: string
    type: NotificationType
    category: number
    status: string
}

export type NotificationType= 'SENDNOW'|'SCHEDULE'

export type SortType = 'asc' | 'desc' | undefined | null

export type TableNotificationsProps = {
    total: number
    sort: SortType
    rows: NotificationRowsType[]
    searchValue: string
    sortColumn: string
    paginationModel: { page: number, pageSize: number }
    startDate: Date
    endDate: Date
    setTotal:(total:number)=>void
    setRows:(rows:NotificationRowsType[])=>void
    setSearchValue:(value:string)=>void
    setStartDate:(startDate:Date)=>void
    setEndDate:(endDate:Date)=>void
    setPaginationModel:(paginationModel:{ page: number, pageSize: number })=>void
    handleSearch:(value:string)=>void
    handleSendNotify:(id:string|string[])=>void
    handleClearFilter:(value:string)=>void
    fetchTableData:(sort:SortType, searchValue:string, sortColumn:string, startDate:Date, endDate:Date)=>void
}