
// ** React Imports
import { ChangeEvent } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridPaginationModel, GridRenderCellParams, GridSortModel } from '@mui/x-data-grid'

// ** ThirdParty Components

// ** Custom Components
import CustomChip from 'src/@core/components/mui/chip'

// ** Types Imports
import { ThemeColor } from 'src/@core/layouts/types'
import { Button, SelectChangeEvent, Tooltip } from '@mui/material'
import Icon from 'src/@core/components/icon'
import React from 'react'
import Link from 'next/link'
import { WarrantyPolicyListContext, WarrantyTypeObj } from 'src/context/warranty-management/waranty-policy'
import { ISignalLightsActionType } from 'src/context/document-management/signal-lights/TableContext/types'
import { statusOptions } from 'src/context/document-management/signal-lights'
import WarrantyPolicyToolbar from '../Toolbar'

interface StatusObj {
    [key: string]: {
        title: string
        color: ThemeColor
    }
}



const statusObj: StatusObj = {
    'ACTIVE': { title: 'Đang hoạt động', color: 'success' },
    'INACTIVE': { title: 'Ngưng hoạt động', color: 'error' },
}

export interface WarrantyPolicyListRowsType {
    id: number,
    name: string
    thumbnail: string
    description: string
    detailContent: string
    status: string
    createdUser: Date
    createdDate: Date
}
const TableWarrantyPolicy = () => {
    const {
        total,
        rows,
        searchValue,
        paginationModel,
        status,
        loading,
        dispatch,
        handleClearFilter,
        handleSearch,
        handleOpenModal,
        idRef } = React.useContext(WarrantyPolicyListContext)

    const columns: GridColDef[] = [
        {
            flex: 0.25,
            minWidth: 150,
            field: 'actions',
            sortable: false,
            headerName: 'Hành động',
            renderCell: ({ row }) => {

                return (
                    <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center' }}>
                        {
                            <Tooltip title="Xem chi tiết chính sách">
                                <Link
                                    style={{ display: 'flex', alignContent: 'center' }}
                                    href={`/warranty-management/warranty-policy/${row.id}`}
                                >
                                    <Icon icon='solar:pen-new-round-linear' fontSize={20} color="#32475CDE" />
                                </Link >
                            </Tooltip>
                        }

                        {
                            <Tooltip title="Xóa chính sách">
                                <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                                    onClick={() => {
                                        idRef.current = row.id
                                        handleOpenModal('modal_confirm_delete_warranty-policy', row.id)
                                    }}>
                                    <Icon icon='mdi:trash-can-circle-outline' fontSize={20} color="#32475CDE" />
                                </Button>
                            </Tooltip>
                        }
                    </Box>
                )
            }
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Tiêu đề',
            field: 'notify_title',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.title}
                </Typography>
            )
        },

        {
            flex: 0.5,
            type: 'text',
            minWidth: 200,
            headerName: 'Loại',
            field: 'send_date',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {WarrantyTypeObj.filter(item => item.value === params.row.warrantyPolicyTypeId)[0].label}
                </Typography>
            )
        },
        {
            flex: 0.175,
            minWidth: 200,
            field: 'status',
            headerName: 'Trạng thái',
            renderCell: (params: GridRenderCellParams) => {
                if (params.row.status) {
                    const status = statusObj[params.row.status]

                    return <CustomChip rounded size='small' skin='light' color={status.color} label={status.title} />
                }

                return <CustomChip rounded size='small' skin='light' color='primary' label='Chưa xử lý' />
            }
        },
    ]

    const handleSortChange = (sort: GridSortModel) => {
        dispatch({ type: ISignalLightsActionType.SET_SORT, payload: sort })
    }
    const handleChangePagination = (pagination: GridPaginationModel) => {
        dispatch({ type: ISignalLightsActionType.SET_PAGINATION_MODEL, payload: pagination })
    }

    return (
        <Card>
            <DataGrid
                sx={{ overflow: 'unset' }}
                autoHeight
                pagination
                rows={rows}
                loading={loading}
                rowCount={total}
                columns={columns}
                sortingMode='server'
                paginationMode='server'
                pageSizeOptions={[10, 25, 50]}
                paginationModel={paginationModel}
                onSortModelChange={handleSortChange}
                slots={{ toolbar: WarrantyPolicyToolbar }}
                onPaginationModelChange={handleChangePagination}
                slotProps={{
                    toolbar: {
                        search: searchValue,
                        handleSearch:handleSearch,
                        onChange: (event: ChangeEvent<HTMLInputElement>) => handleSearch(event.target.value),
                        status: status,
                        options: statusOptions,
                        handleChangeStatus: (event: SelectChangeEvent) => dispatch({ type: ISignalLightsActionType.SET_STATUS, payload: event.target.value as string }),
                        handleClearFilter: handleClearFilter
                    },
                    baseButton: {
                        variant: 'outlined'
                    }
                }}
            />

        </Card>
    )
}

export default TableWarrantyPolicy