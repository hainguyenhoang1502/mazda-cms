import { UseFormReturn } from 'react-hook-form'
import { FormValues } from 'src/context/notifications/Form/create'

export interface FormCreateNotifyProps {
    handleSubmit: () => void
    form: UseFormReturn<FormValues, any>
}
export * from './CreateFormWarrantyPolicy'
export * from './UpdateFormWarrantyPolicy'