import { Box } from '@mui/material'
import * as React from 'react'
import Icon from 'src/@core/components/icon'


export default function DropdownIcon() {
  return (
    <Box sx={{position:'absolute', right:'3%', top:'60%', transform:'translateY(-50%)'}}>
      <Icon icon='material-symbols:keyboard-arrow-down' />
    </Box>
  )
}
