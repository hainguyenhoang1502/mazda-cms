/* eslint-disable */
// ** React Imports
import { ChangeEvent, Ref } from 'react'
import React from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import IconButton from '@mui/material/IconButton'
import { GridToolbarExport, GridToolbarFilterButton } from '@mui/x-data-grid'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import { SingleDatePicker } from 'src/components/Common/DatePicker'
import { ReactDatePickerProps } from 'react-datepicker'
import { useTheme } from '@mui/material/styles'
import Button from '@mui/material/Button'
import { Grid, SelectChangeEvent } from '@mui/material'
import { DropdownDefault } from 'src/components/Common/Dropdown'
import router from 'next/router'

interface DefaultToolBarProps {
    search: Ref<any>
    handleSearch: () => void
    onChange: (e: React.ChangeEvent) => void
    modelCode: string
    gradeCode: string
    colorCode: string
    modelOpts: any[]
    colorOpts: any[]
    gradeOpts: any[]
    handleChangeColor: (event: SelectChangeEvent) => void
    handleChangeModel: (event: SelectChangeEvent) => void
    handleChangeGrade: (event: SelectChangeEvent) => void
    handleClearFilter: () => void
}

export default function Toolbar(props: DefaultToolBarProps) {

    return (
        <Grid container spacing={2} sx={{ paddingX: 5 }}>
            <Box
                sx={{
                    gap: 2,
                    display: 'flex',
                    flexWrap: 'wrap',
                    alignItems: 'center',
                    p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                }}
            >
                <TextField
                    size='small'
                    placeholder='Tìm kiếm...'
                    inputRef={props.search}
                    onKeyDown={(e) => {
                        if (e.keyCode == 13) {
                            props.handleSearch()
                        }
                    }}
                    InputProps={{
                        startAdornment: (
                            <Box sx={{ mr: 2, display: 'flex' }}>
                                <Icon icon='bx:search' fontSize={20} />
                            </Box>
                        )
                    }}
                    sx={{
                        width: {
                            xs: 1,
                            sm: 'auto'
                        },
                        '& .MuiInputBase-root > svg': {
                            mr: 2
                        },
                        '& .MuiInput-underline:before': {
                            borderBottom: 1,
                            borderColor: 'divider'
                        }
                    }}
                />
                <DropdownDefault
                    options={props.modelOpts}
                    value={props.modelCode}
                    label='Dòng xe'
                    handleChange={props.handleChangeModel} />
                <DropdownDefault
                    options={props.gradeOpts}
                    value={props.gradeCode}
                    label='Phiên bản'
                    handleChange={props.handleChangeGrade} />
                <DropdownDefault
                    options={props.colorOpts}
                    value={props.colorCode}
                    label='Màu xe'
                    handleChange={props.handleChangeColor} />
                <Button variant='outlined' size='medium' onClick={props.handleSearch}>
                    Lọc
                </Button>
                <Button variant='outlined' size='medium' color="error" onClick={props.handleClearFilter}>
                    Xóa bộ lọc
                </Button>
                <Button variant='contained' startIcon={<Icon icon='bx:plus' />} onClick={() => router.push('/car-images-management/create')}>
                    Thêm mới
                </Button>
            </Box>

        </Grid>
    )
}
/* eslint-enable */