/* eslint-disable */

// ** React Imports
import { ChangeEvent } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridPaginationModel, GridRenderCellParams, GridSortModel } from '@mui/x-data-grid'

// ** ThirdParty Components

// ** Custom Components
import CustomChip from 'src/@core/components/mui/chip'

// ** Types Imports
import { ThemeColor } from 'src/@core/layouts/types'
import CustomAvatar from 'src/@core/components/mui/avatar'
// ** Utils Import
import NotificationToolbar from 'src/components/Toolbar/NotificationToolbar'
import { Button, SelectChangeEvent, Tooltip } from '@mui/material'
import Icon from 'src/@core/components/icon'
import { NotificationsContext } from 'src/context/notifications/Table'
import React from 'react'
import { format } from 'date-fns'
import Link from 'next/link'
import { SignalLightsContext, statusOptions } from 'src/context/document-management/signal-lights'
import { ISignalLightsActionType } from 'src/context/document-management/signal-lights/TableContext/types'
import SignalLightToolbar from '../Toolbar'
import { CarImageListContext } from 'src/context/car-images-management/TableContext'
import Toolbar from '../Toolbar'
import { ICarImagesActionType } from 'src/context/car-images-management/TableContext/types'

interface StatusObj {
    [key: string]: {
        title: string
        color: ThemeColor
    }
}



const statusObj: StatusObj = {
    'ACTIVE': { title: 'Đang hoạt động', color: 'success' },
    'INACTIVE': { title: 'Ngưng hoạt động', color: 'error' },
}

export interface SignalLightsListRowsType {
    id: number,
    name: string
    thumbnail: string
    description: string
    detailContent: string
    status: string
    createdUser: Date
    createdDate: Date
}
const TableCarImage = () => {
    const {
        total,
        rows,
        searchValue,
        paginationModel,
        loading,
        filter,
        dispatch,
        handleClearFilter,
        handleSearch,
        modelOpt,
        gradeOpt,
        colorOpt,
        handleOpenModal,
        idRef } = React.useContext(CarImageListContext)

    const columns: GridColDef[] = [
        {
            flex: 0.25,
            minWidth: 150,
            field: 'actions',
            sortable: false,
            headerName: 'Hành động',
            renderCell: ({ row }) => {

                return (
                    <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center' }}>
                        {
                            <Tooltip title="Xem chi tiết">
                                <Link
                                    style={{ display: 'flex', alignContent: 'center' }}
                                    href={`/car-images-management/${row.id}`}
                                >
                                    <Icon icon='solar:pen-new-round-linear' fontSize={20} color="#32475CDE" />
                                </Link >
                            </Tooltip>
                        }
                        {
                            <Tooltip title="Xóa">
                                <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                                    onClick={() => {
                                        idRef.current = row.id
                                        handleOpenModal('modal_confirm_delete_car-image', row.id)
                                    }}>
                                    <Icon icon='mdi:trash-can-circle-outline' fontSize={20} color="#32475CDE" />
                                </Button>
                            </Tooltip>
                        }
                    </Box>
                )
            }
        },
        {
            flex: 0.125,
            type: 'text',
            minWidth: 150,
            headerName: 'Dòng xe',
            field: 'modelName',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.modelName}
                </Typography>
            )
        },
        {
            flex: 0.125,
            type: 'text',
            minWidth: 300,
            headerName: 'Phiên bản xe',
            field: 'gradeName',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.gradeName}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 100,
            headerName: 'màu sắc ',
            field: 'colorNameVi',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.colorNameVi}
                </Typography>
            )
        },

        {
            flex: 0.5,
            type: 'text',
            minWidth: 150,
            headerName: 'Năm sản xuất',
            field: 'year',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.year}
                </Typography>
            )
        },
        {
            flex: 0.5,
            type: 'text',
            minWidth: 100,
            headerName: 'Hình ảnh',
            field: 'image',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    <CustomAvatar
                        src={params.row.image}
                        variant='rounded'
                        alt={params.row.modelName}
                        sx={{  objectFit: "cover", height:'auto', margin:'5px' }}
                    />
                </Typography>
            )
        },
        {
            flex: 0.175,
            minWidth: 200,
            field: 'status',
            headerName: 'Trạng thái',
            renderCell: (params: GridRenderCellParams) => {
                if (params.row.status) {
                    const status = statusObj[params.row.status]

                    return <CustomChip rounded size='small' skin='light' color={status.color} label={status.title} />
                }

                return <CustomChip rounded size='small' skin='light' color='primary' label='Chưa xử lý' />
            }
        },
    ]

    const handleSortChange = (sort: GridSortModel) => {
        dispatch({ type: ICarImagesActionType.SET_SORT, payload: sort })
    }
    const handleChangePagination = (pagination: GridPaginationModel) => {
        dispatch({ type: ICarImagesActionType.SET_PAGINATION_MODEL, payload: pagination })
    }

    return (
        <Card>
            <DataGrid
                sx={{ overflow: 'unset' }}
                autoHeight
                pagination
                rows={rows}
                loading={loading}
                rowCount={total}
                columns={columns}
                sortingMode='server'
                paginationMode='server'
                pageSizeOptions={[10, 25, 50]}
                paginationModel={paginationModel}
                onSortModelChange={handleSortChange}
                slots={{ toolbar: Toolbar }}
                onPaginationModelChange={handleChangePagination}
                slotProps={{
                    toolbar: {
                        search: searchValue,
                        handleSearch: handleSearch,
                        onChange: (event: ChangeEvent<HTMLInputElement>) => handleSearch(event.target.value),
                        modelCode: filter.modelCode,
                        gradeCode: filter.gradeCode,
                        colorCode: filter.colorCode,
                        modelOpts: modelOpt,
                        colorOpts: colorOpt,
                        gradeOpts: gradeOpt,
                        handleChangeModel: (event: SelectChangeEvent) => dispatch({ type: ICarImagesActionType.SET_FILTER_MODELCODE, payload: event.target.value as string }),
                        handleChangeGrade: (event: SelectChangeEvent) => dispatch({ type: ICarImagesActionType.SET_FILTER_GRADECODE, payload: event.target.value as string }),
                        handleChangeColor: (event: SelectChangeEvent) => dispatch({ type: ICarImagesActionType.SET_FILTER_COLORCODE, payload: event.target.value as string }),
                        handleClearFilter: handleClearFilter
                    },
                    baseButton: {
                        variant: 'outlined'
                    }
                }}
            />

        </Card>
    )
}

export default TableCarImage
/* eslint-enable */