
// ** React Imports

import React from 'react'

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import { SelectField } from 'src/components/Common/Input'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import FileUpLoadField from 'src/components/Common/Input/FileUpLoadField'
import router from 'next/router'
import { Box, CircularProgress } from '@mui/material'
import { statusOptions } from 'src/context/document-management/signal-lights'
import { FormUpdateContext } from 'src/context/car-images-management/FormContext/update'
import DatePickerField from 'src/components/Common/Input/DatePickerField'
import { CarImageListContext } from 'src/context/car-images-management/TableContext'


// Component form
export default function FormUpdate() {

    const { loading, handleConfirm, form } = React.useContext(FormUpdateContext)
    const { modelOpt, gradeOpt, colorOpt } = React.useContext(CarImageListContext)
    console.log(form.watch())

    return (
        loading ? <Box sx={{ margin: '0 auto', width: '100%', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><CircularProgress /></Box> :
            <form onSubmit={form.handleSubmit(handleConfirm)}>
                <h3></h3>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                            <CardHeader title='Thông tin hình ảnh xe' />
                            <CardContent>
                                <Grid container spacing={10} justifyContent='space-between'>
                                    <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                        <FormControl fullWidth>
                                            <SelectField  required control={form.control} name='modelCode' label='Dòng xe' options={modelOpt.filter(item => item.value !== 'Tất cả')} />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                        <FormControl fullWidth>
                                            <SelectField  required control={form.control} name='gradeCode' label='Phiên bản' options={gradeOpt.filter(item => item.value !== 'Tất cả')} />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                        <FormControl fullWidth>
                                            <SelectField  required control={form.control} name='colorCode' label='Màu sắc' options={colorOpt.filter(item => item.value !== 'Tất cả')} />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                        <FormControl fullWidth>
                                            <DatePickerField required  control={form.control} name='year' label='Năm sản xuất' showYearPicker dateFormat="yyyy" variant='outlined' />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                        <FormControl fullWidth>
                                            <SelectField required  control={form.control} name='status' label='Trạng thái' options={statusOptions.filter(item => item.value !== '')} />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={10}>
                                        <Grid item xs={6}>
                                            <FormControl fullWidth>
                                                <FileUpLoadField required  name='image' label='Chọn hình ảnh xe' control={form.control} />
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} sx={{ textAlign: 'center' }}>
                                        <Button sx={{ marginX: '10px' }} size='large' type='submit' variant='contained'  >
                                            Lưu
                                        </Button>
                                        <Button size='large' onClick={() => router.back()} variant='outlined'>
                                            Hủy
                                        </Button>
                                    </Grid>
                                </Grid>

                            </CardContent>
                        </Card>

                    </Grid>

                </Grid>
            </form >
    )
}
