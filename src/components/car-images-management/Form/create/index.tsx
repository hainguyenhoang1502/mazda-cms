import React from 'react'

import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import { SelectField } from 'src/components/Common/Input'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import FileUpLoadField from 'src/components/Common/Input/FileUpLoadField'
import router from 'next/router'
import { statusOptions } from 'src/context/document-management/signal-lights'
import DatePickerField from 'src/components/Common/Input/DatePickerField'
import { FormCreateContext } from 'src/context/car-images-management/FormContext/create'
import { CarImageListContext } from 'src/context/car-images-management/TableContext'


export default function FormCreate() {
    const { form, handleConfirm, gradeOptsFilter } = React.useContext(FormCreateContext)
    const { modelOpt, colorOpt } = React.useContext(CarImageListContext)

    return (
        <form onSubmit={form.handleSubmit(handleConfirm)}>
            
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                        <CardHeader title='Thông tin hình ảnh xe' />
                        <CardContent>
                            <Grid container spacing={10} justifyContent='center'>
                                <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                    <FormControl fullWidth>
                                        <SelectField control={form.control} name='modelCode' label='Dòng xe' options={modelOpt.filter(item => item.value !== 'Tất cả')} />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                    <FormControl fullWidth>
                                        <SelectField control={form.control} name='gradeCode' label='Phiên bản' options={gradeOptsFilter.filter(item => item.value !== 'Tất cả')} />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                    <FormControl fullWidth>
                                        <SelectField control={form.control} name='colorCode' label='Màu sắc' options={colorOpt.filter(item => item.value !== 'Tất cả')} />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                    <FormControl fullWidth>
                                        <DatePickerField control={form.control} name='year' label='Năm sản xuất' showYearPicker dateFormat="yyyy" variant='outlined' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='right'>
                                    <FormControl fullWidth>
                                        <SelectField control={form.control} name='status' label='Trạng thái' options={statusOptions.filter(item => item.value !== '')} />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={10}>
                                    <Grid item xs={6}>
                                        <FormControl fullWidth>
                                            <FileUpLoadField name='image' label='Chọn hình ảnh xe' control={form.control} />
                                        </FormControl>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                                    <Button sx={{ marginX: '10px' }} size='large' type='submit' variant='contained'  >
                                        Lưu
                                    </Button>
                                    <Button size='large' color='error' onClick={() => router.back()} variant='outlined'>
                                        Hủy
                                    </Button>
                                </Grid>
                            </Grid>

                        </CardContent>
                    </Card>

                </Grid>

            </Grid>
        </form >
    )
}
