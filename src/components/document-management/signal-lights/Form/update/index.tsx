/* eslint-disable no-undef */

// ** React Imports

import React from 'react'

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import { SelectField, TextInputField } from 'src/components/Common/Input'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import ReactDraftWysiwygField from 'src/components/Common/Input/ReactDraftWysiwygField'
import FileUpLoadField from 'src/components/Common/Input/FileUpLoadField'
import router from 'next/router'
import TextAreaField from 'src/components/Common/Input/TextEditorField'
import { Box, CircularProgress } from '@mui/material'
import { statusOptions } from 'src/context/document-management/signal-lights'
import { SignalLightUpdateContext } from 'src/context/document-management/signal-lights/FormContext/update'


// Component form
export default function FormUpdateSignalLight() {

    const { loading, handleConfirm, form } = React.useContext(SignalLightUpdateContext)
    
    return (
        loading ? <Box sx={{ margin: '0 auto', width: '100%', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><CircularProgress /></Box> :
        <form onSubmit={form.handleSubmit(handleConfirm)}>
            <h3></h3>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                        <CardHeader title='Thông tin Đèn cảnh báo' />
                        <CardContent>
                            <Grid container spacing={10} justifyContent='space-between'>
                                <Grid item xs={12}>
                                    <FormControl fullWidth>
                                        <TextInputField required control={form.control} name='name' label='Tên đèn cảnh báo *' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth>
                                        <TextAreaField required control={form.control} name='description' label='Mô tả *' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth>
                                        <ReactDraftWysiwygField required name='detailContent' label='Nội dung chi tiết' height="300" control={form.control} />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={4} sx={{marginTop:6}} textAlign='right'>
                                    <FormControl fullWidth>
                                        <SelectField required control={form.control} name='status' label='Trạng thái *' options={statusOptions.filter(item=>item.value!=='')} />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <FileUpLoadField required name='thumbnail' label='Chọn hình ảnh Thumbnail' control={form.control} />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                                    <Button sx={{ marginX: '10px' }} size='large' type='submit' variant='contained'  >
                                        Lưu
                                    </Button>
                                    <Button size='large' color='error' onClick={() => router.back()} variant='outlined'>
                                        Hủy
                                    </Button>
                                </Grid>
                            </Grid>

                        </CardContent>
                    </Card>

                </Grid>

            </Grid>
        </form >
    )
}
