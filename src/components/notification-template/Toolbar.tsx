/* eslint-disable */
// ** React Imports
import { ChangeEvent } from 'react'
import React from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import IconButton from '@mui/material/IconButton'
import { GridToolbarExport, GridToolbarFilterButton } from '@mui/x-data-grid'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import { SingleDatePicker } from 'src/components/Common/DatePicker'
import { ReactDatePickerProps } from 'react-datepicker'
import { useTheme } from '@mui/material/styles'
import Button from '@mui/material/Button'
import { Grid, SelectChangeEvent } from '@mui/material'
import { DropdownDefault } from 'src/components/Common/Dropdown'
import router from 'next/router'

interface DefaultToolBarProps {
    search: string
    clearSearch: () => void
    onChange: (e: React.ChangeEvent) => void
    status: string
    options: any[]
    handleSearch: () => void
    handleChangeStatus: (event: SelectChangeEvent) => void
    handleClearFilter: () => void
}

export default function Toolbar(props: DefaultToolBarProps) {

    return (
        <Grid container spacing={2} justifyContent={'space-between'} sx={{ paddingX: 5 }}>
            <Box
                sx={{
                    gap: 2,
                    display: 'flex',
                    flexWrap: 'no-wrap',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                }}
            >
                <TextField
                    size='small'
                    value={props.search}
                    onChange={props.onChange}
                    placeholder='Tìm theo tên hoặc mô tả...'
                    InputProps={{
                        startAdornment: (
                            <Box sx={{ mr: 2, display: 'flex' }}>
                                <Icon icon='bx:search' fontSize={20} />
                            </Box>
                        )
                    }}
                    sx={{
                        width: {
                            xs: 1,
                            sm: 'auto'
                        },
                        '& .MuiInputBase-root > svg': {
                            mr: 2
                        },
                        '& .MuiInput-underline:before': {
                            borderBottom: 1,
                            borderColor: 'divider'
                        }
                    }}
                />
                <DropdownDefault
                    options={props.options}
                    value={props.status}
                    label='Trạng thái'
                    handleChange={props.handleChangeStatus} />
                <Button variant='outlined' size='medium' onClick={props.handleSearch}>
                    Lọc
                </Button>
                <Button variant='outlined' size='medium' color='error' onClick={props.handleClearFilter}>
                    Xóa bộ Lọc
                </Button>
            </Box>

            <Box
                sx={{
                    gap: 2,
                    display: 'flex',
                    flexWrap: 'no-wrap',
                    alignItems: 'center',
                    justifyContent: 'end',
                    p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                }}
            >

                <Button variant='contained' startIcon={<Icon icon='bx:plus' />} onClick={() => router.push('/notification-template/create')}>
                    Thêm mới
                </Button>
            </Box>
        </Grid>
    )
}
/* eslint-enable */