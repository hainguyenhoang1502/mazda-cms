/* eslint-disable */

// ** React Imports
import { ChangeEvent } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridPaginationModel, GridRenderCellParams, GridSortModel } from '@mui/x-data-grid'

// ** ThirdParty Components

// ** Custom Components
import CustomChip from 'src/@core/components/mui/chip'

// ** Types Imports
import { ThemeColor } from 'src/@core/layouts/types'
// ** Utils Import
import { Button, SelectChangeEvent, Tooltip } from '@mui/material'
import Icon from 'src/@core/components/icon'
import React from 'react'
import Link from 'next/link'
import Toolbar from './Toolbar'
import { NotificationTemplateListContext, statusOptions } from 'src/context/notification-template/Table'
import { INotificationTemplatesActionType } from 'src/context/notification-template/Table/types'

interface StatusObj {
    [key: string]: {
        title: string
        color: ThemeColor
    }
}



const statusObj: StatusObj = {
    'ACTIVE': { title: 'Đang hoạt động', color: 'success' },
    'INACTIVE': { title: 'Ngưng hoạt động', color: 'error' },
}


const Table = () => {
    const {
        total,
        rows,
        paginationModel,
        loading,
        filter,
        dispatch,
        handleClearFilter,
        handleSearch,
        handleOpenModal,
        idRef } = React.useContext(NotificationTemplateListContext)

    const columns: GridColDef[] = [
        {
            flex: 0.25,
            maxWidth: 150,
            field: 'actions',
            sortable: false,
            headerName: 'Hành động',
            renderCell: ({ row }) => {

                return (
                    <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center' }}>
                        {
                            <Tooltip title="Xem chi tiết">
                                <Link
                                    style={{ display: 'flex', alignContent: 'center' }}
                                    href={`/notification-template/${row.id}`}
                                >
                                    <Icon icon='solar:pen-new-round-linear' fontSize={20} color="#32475CDE" />
                                </Link >
                            </Tooltip>
                        }
                        {
                            <Tooltip title="Xóa">
                                <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                                    onClick={() => {
                                        idRef.current = row.id
                                        handleOpenModal('modal_confirm_delete_notification-template', row.id)
                                    }}>
                                    <Icon icon='mdi:trash-can-circle-outline' fontSize={20} color="#32475CDE" />
                                </Button>
                            </Tooltip>
                        }
                    </Box>
                )
            }
        },
        {
            flex: 0.125,
            type: 'text',
            minWidth: 150,
            headerName: 'Tên template',
            field: 'modelName',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.name}
                </Typography>
            )
        },
        {
            flex: 0.175,
            minWidth: 200,
            field: 'status',
            headerName: 'Trạng thái',
            renderCell: (params: GridRenderCellParams) => {
                if (params.row.status) {
                    const status = statusObj[params.row.status]

                    return <CustomChip rounded size='small' skin='light' color={status.color} label={status.title} />
                }

                return <CustomChip rounded size='small' skin='light' color='primary' label='Chưa xử lý' />
            }
        },
    ]

    const handleSortChange = (sort: GridSortModel) => {
        dispatch({ type: INotificationTemplatesActionType.SET_SORT, payload: sort })
    }
    const handleChangePagination = (pagination: GridPaginationModel) => {
        dispatch({ type: INotificationTemplatesActionType.SET_PAGINATION_MODEL, payload: pagination })
    }

    return (
        <Card>
            <DataGrid
                sx={{ overflow: 'unset' }}
                autoHeight
                pagination
                rows={rows}
                loading={loading}
                rowCount={total}
                columns={columns}
                sortingMode='server'
                paginationMode='server'
                pageSizeOptions={[10, 25, 50]}
                paginationModel={paginationModel}
                onSortModelChange={handleSortChange}
                slots={{ toolbar: Toolbar }}
                onPaginationModelChange={handleChangePagination}
                slotProps={{
                    toolbar: {
                        search: filter.keyword,
                        clearSearch: () => handleClearFilter(''),
                        handleSearch:()=>handleSearch(),
                        onChange: (event: ChangeEvent<HTMLInputElement>) => dispatch({ type: INotificationTemplatesActionType.SET_FILTER_KEYWORD, payload: event.target.value as string }),
                        status:filter.status,
                        options:statusOptions,
                        handleChangeStatus: (event: SelectChangeEvent) => dispatch({ type: INotificationTemplatesActionType.SET_FILTER_STATUS, payload: event.target.value as string }),
                        handleClearFilter: handleClearFilter
                    },
                    baseButton: {
                        variant: 'outlined'
                    }
                }}
            />

        </Card>
    )
}

export default Table
/* eslint-enable */