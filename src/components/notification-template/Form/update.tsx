/* eslint-disable no-undef */

// ** React Imports

import React from 'react'

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import { UseFormReturn } from 'react-hook-form'
import { SelectField, TextInputField } from 'src/components/Common/Input'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import ReactDraftWysiwygField from 'src/components/Common/Input/ReactDraftWysiwygField'
import router from 'next/router'
import { FormValues } from 'src/context/document-management/signal-lights/FormContext/create'
import { statusOptions } from 'src/context/notification-template/Table'
import { FormUpdateContext } from 'src/context/notification-template/Form/update'
import CheckboxField from 'src/components/Common/Input/CheckboxField'
import DatePickerField from 'src/components/Common/Input/DatePickerField'
import FileUpLoadField from 'src/components/Common/Input/FileUpLoadField'
import { notificationsTypeOptions } from 'src/hooks/useNotificationsType'



export interface FormCreateNotifyProps {
    handleSubmit: () => void
    form: UseFormReturn<FormValues, any>
}

// Component form
export default function FormUpdate() {
    const { form, handleConfirm } = React.useContext(FormUpdateContext)

    return (
        <form onSubmit={form.handleSubmit(handleConfirm)}>
            <h3></h3>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                        <CardHeader title='Thông tin template' />
                        <CardContent>
                            <Grid container spacing={10} justifyContent='space-between'>
                                <Grid item xs={12}>
                                    <FormControl fullWidth>
                                        <TextInputField required control={form.control} name='name' label='Tên template *' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth>
                                        <ReactDraftWysiwygField required height="300" name='content' label='Nội dung chi tiết' control={form.control} />
                                    </FormControl>
                                </Grid>
                                <Grid item container xs={12} justifyContent={'space-between'} textAlign='right'>
                                    <Grid container flexDirection={'column'} item xs={6} spacing={10} sx={{ marginTop: 6 }} textAlign='right'>
                                        <Grid item>
                                            <FormControl fullWidth>
                                                <SelectField required control={form.control} name='status' label='Trạng thái' options={statusOptions.filter(item => item.value !== '')} />
                                            </FormControl>
                                        </Grid>
                                        <Grid item>
                                            <FormControl fullWidth>
                                                <SelectField required control={form.control} name='type' label='Loại' options={notificationsTypeOptions.filter(item => item.value !== '')} />
                                            </FormControl>
                                        </Grid>
                                        <Grid justifySelf={'left'} sx={{ width: 'fit-content' }} item>
                                            <FormControl fullWidth>
                                                <CheckboxField required control={form.control} name='isShowPopup' label='Hiển thị Popup' />
                                            </FormControl>
                                        </Grid>
                                        {
                                            form.watch('isShowPopup') &&
                                            <>


                                                <Grid container spacing={6} item>
                                                    <Grid xs={6} item>
                                                        <FormControl fullWidth>
                                                            <DatePickerField required control={form.control} name='fromDate' label='Từ ngày' timeFormat='HH:mm' timeIntervals={5} showTimeSelect />
                                                        </FormControl>
                                                    </Grid>
                                                    <Grid xs={6} item>
                                                        <FormControl fullWidth>
                                                            <DatePickerField required control={form.control} name='endDate' label='Đến ngày' timeFormat='HH:mm' timeIntervals={5} showTimeSelect />
                                                        </FormControl>
                                                    </Grid>
                                                </Grid>
                                                <Grid container spacing={6} item>
                                                    <Grid xs={12} item>
                                                        <FormControl fullWidth>
                                                            <TextInputField required control={form.control} name='link' label='Link liên kết' />
                                                        </FormControl>
                                                    </Grid>
                                                </Grid>
                                            </>
                                        }

                                    </Grid>
                                    <Grid item xs={4} textAlign='left'>
                                        <FormControl fullWidth>
                                            <FileUpLoadField name='image' label='Chọn hình ảnh' control={form.control} />
                                        </FormControl>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                                    <Button sx={{ marginX: '10px' }} size='large' type='submit' variant='contained'  >
                                        Lưu
                                    </Button>
                                    <Button size='large' color='error' onClick={() => router.back()} variant='outlined'>
                                        Hủy
                                    </Button>
                                </Grid>
                            </Grid>

                        </CardContent>
                    </Card>

                </Grid>

            </Grid>
        </form >
    )
}
