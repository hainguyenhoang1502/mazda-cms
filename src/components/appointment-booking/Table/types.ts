export type AppointmentsRowsType = {
    id: number
    title: string
    content: string
    createDate: string
    sendDate: string
    type: AppointmentsType
    category: number
    status: string
}

export type AppointmentsType= 'SENDNOW'|'SCHEDULE'

export type SortType = 'asc' | 'desc' | undefined | null

export type TableAppointmentsProps = {
    total: number
    sort: SortType
    rows: AppointmentsRowsType[]
    searchValue: string
    sortColumn: string
    paginationModel: { page: number, pageSize: number }
    startDate: Date
    endDate: Date
    setTotal:(total:number)=>void
    setRows:(rows:AppointmentsRowsType[])=>void
    setSearchValue:(value:string)=>void
    setStartDate:(startDate:Date)=>void
    setEndDate:(endDate:Date)=>void
    setPaginationModel:(paginationModel:{ page: number, pageSize: number })=>void
    handleSearch:(value:string)=>void
    handleSendNotify:(id:string|string[])=>void
    handleClearFilter:(value:string)=>void
    fetchTableData:(sort:SortType, searchValue:string, sortColumn:string, startDate:Date, endDate:Date)=>void
}