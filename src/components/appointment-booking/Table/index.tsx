/* eslint-disable */
// ** React Imports
import { ChangeEvent } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridPaginationModel, GridRenderCellParams, GridSortModel } from '@mui/x-data-grid'

// ** ThirdParty Components

// ** Custom Components

// ** Types Imports
import { ThemeColor } from 'src/@core/layouts/types'

// ** Utils Import
import { Button, SelectChangeEvent, Tooltip } from '@mui/material'
import Icon from 'src/@core/components/icon'
import React from 'react'
import Link from 'next/link'
import Toolbar from '../Toolbar'
import { AppointmentListContext } from 'src/context/appointment/TableContext'
import { IAppointmentsActionType } from 'src/context/appointment/TableContext/types'
import ChipsDropdown from 'src/components/Common/Dropdown/ChipsDropdown'
import { differenceInHours, differenceInMinutes, differenceInSeconds, format, formatDistance } from 'date-fns'

interface StatusObj {
    [key: string]: {
        title: string
        color: ThemeColor
    }
}



const statusObj: StatusObj = {
    'CONFIRM': { title: 'Đã xác nhận', color: 'success' },
    'NEW': { title: 'Đang chờ xử lý', color: 'warning' },
    'PROCESSING': { title: 'Đang xử lý', color: 'info' },
    'CANCEL': { title: 'Hủy lịch hẹn', color: 'error' },
}
const statusOptions = [{
    label: 'Đã xác nhận',
    value: 'CONFIRM'
},
{
    label: 'Đang chờ xử lý',
    value: 'NEW'
},
{
    label: 'Đang xử lý',
    value: 'PROCESSING'
},
{
    label: 'Hủy lịch hẹn',
    value: 'CANCEL'
},
]

const handleRenderOptions = (option) => {
    switch (option) {
        case 'NEW': {
            return statusOptions;
        }
        case 'PROCESSING': {
            return statusOptions.filter(item => item.value !== 'NEW')
        }
        case 'CONFIRM': {
            return statusOptions.filter(item => item.value === 'CONFIRM')
        }
        case 'CANCEL': {
            return statusOptions.filter(item => item.value === 'CANCEL')
        }
        default:
            return []
    }
}

const handleRenderTime = (timeStart, timeEnd) => {
    if (timeStart && timeEnd) {
        const start = new Date(timeStart);
        const end = new Date(timeEnd);

        return formatDistance(start, end);
    }
    return ''
}

const TableAppointments = () => {
    const { total,
        rows,
        filter,
        searchValue,
        paginationModel,
        dispatch,
        loading,
        handleClearFilter,
        handleOpenModal,
        idRef,
        paramUpdate,
        appointmentTimeOptions,
        handleSearch } = React.useContext(AppointmentListContext)

    const columns: GridColDef[] = [
        {
            flex: 0.25,
            minWidth: 150,
            field: 'actions',
            sortable: false,
            headerName: 'Hành động',
            renderCell: (params) => {

                return (
                    <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center' }}>
                        {
                            <Tooltip title="Xem chi tiết">
                                <Link
                                    style={{ display: 'flex', alignContent: 'center' }}
                                    href={`/appointment-booking/${params.row.id}`}
                                >
                                    <Icon icon='solar:pen-new-round-linear' fontSize={20} color="#32475CDE" />
                                </Link >
                            </Tooltip>
                        }

                        {
                            <Tooltip title="Xóa">
                                <Button disabled={params.row.status !== "NEW"} sx={{ p: 0, minWidth: "30px" }}
                                    onClick={() => {
                                        idRef.current = params.row.id
                                        handleOpenModal('modal_confirm_delete_appointment', params.row.id)
                                    }}>
                                    <Icon icon='mdi:trash-can-circle-outline' fontSize={20} color={params.row.status==='NEW'?'#32475CDE':''} />
                                </Button>
                            </Tooltip>
                        }
                    </Box>
                )
            }
        },
        {
            flex: 0.175,
            minWidth: 200,
            field: 'status',
            headerName: 'Trạng thái',
            renderCell: (params: GridRenderCellParams) => {
                if (params.row.status) {
                    const status = statusObj[params.row.status]
                    return <ChipsDropdown value={statusOptions.filter(item => item.value === params.row.status)[0]} options={handleRenderOptions(params.row.status)} onChange={(event) => {
                        event.preventDefault()
                        handleOpenModal('modal_confirm_update_appointment')
                        const updateObj = {
                            id: params.row.id,
                            status: event.target.value
                        }
                        paramUpdate.current = updateObj
                    }} />
                }

                return <ChipsDropdown options={statusOptions} />
            }
        },
        {
            flex: 0.125,
            type: 'text',
            minWidth: 150,
            headerName: 'Mã lịch hẹn ',
            field: 'code',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.appointmentCode}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Showroom',
            field: 'showroom',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.showroom}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Ngày tạo',
            field: 'createdDate',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.createdDate && format(new Date(params.row.createdDate), 'dd/MM/yyyy')}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Giờ hẹn',
            field: 'appointmentTime',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.appointmentTime && format(new Date(params.row.appointmentTime), 'dd/MM/yyyy HH:mm')}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Giờ xử lý',
            field: 'handleTime',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.timeStartProcess && format(new Date(params.row.timeStartProcess), 'dd/MM/yyyy HH:mm')}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Thời gian xử lý',
            field: 'proccessTime',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {handleRenderTime(params.row.timeStartProcess, params.row.timeEndProcess)}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'số vin',
            field: 'vinNumber',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.vinNumber}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Biển số',
            field: 'plateNumber',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.plateNumber}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Chủ xe',
            field: 'customerName',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.customerName}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'SĐT chủ xe',
            field: 'phoneNumber',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.phoneNumber}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 200,
            headerName: 'Yêu cầu khách hàng',
            field: 'customerReq',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.customerRequirements}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Tên liên hệ',
            field: 'contactName',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.contactName}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Gói bảo dưỡng',
            field: 'maintenancePackage',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.maintenancePackage}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'mô tả hiện trạng',
            field: 'describeSituation',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.describeSituation}
                </Typography>
            )
        },
        {
            flex: 0.5,
            type: 'text',
            minWidth: 250,
            headerName: 'các hạng mục cần làm mới',
            field: 'itemsToDoSoon',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.itemsToDoSoon}
                </Typography>
            )
        },

    ]

    const handleSortChange = (sort: GridSortModel) => {
        dispatch({ type: IAppointmentsActionType.SET_SORT, payload: sort })
    }
    const handleChangePagination = (pagination: GridPaginationModel) => {
        dispatch({ type: IAppointmentsActionType.SET_PAGINATION_MODEL, payload: pagination })
    }

    return (
        <Card>
            <DataGrid
                sx={{ overflow: 'unset' }}
                autoHeight
                pagination
                rows={rows}
                loading={loading}
                rowCount={total}
                columns={columns}
                sortingMode='server'
                paginationMode='server'
                pageSizeOptions={[10, 25, 50]}
                paginationModel={paginationModel}
                onSortModelChange={handleSortChange}
                slots={{ toolbar: Toolbar }}
                onPaginationModelChange={handleChangePagination}
                slotProps={{
                    toolbar: {
                        search: searchValue,
                        handleSearch: handleSearch,
                        onChange: (event: ChangeEvent<HTMLInputElement>) => handleSearch(event.target.value),
                        optionsStatus: [{ label: 'Tất cả', value: 'Tất cả' }, ...statusOptions],
                        appointmentDate: filter.booking,
                        handleChangeStatus: (event: SelectChangeEvent) => dispatch({ type: IAppointmentsActionType.SET_FILTER_STATUS, payload: event.target.value as string }),
                        handleChangeAppointmentDate: (date: Date) => dispatch({ type: IAppointmentsActionType.SET_FILTER_BOOKING, payload: date }),
                        handleClearFilter: handleClearFilter
                    },
                    baseButton: {
                        variant: 'outlined'
                    }
                }}
            />
        </Card>
    )
}

export default TableAppointments
/* eslint-enable */