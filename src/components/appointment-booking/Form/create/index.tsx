/* eslint-disable no-undef */

// ** React Imports

import React from 'react'

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import { UseFormReturn } from 'react-hook-form'
import { SelectField, TextInputField } from 'src/components/Common/Input'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import router from 'next/router'
import { FormCreateContext, FormValues } from 'src/context/appointment/FormContext/create'
import TextAreaField from 'src/components/Common/Input/TextEditorField'
import DatePickerField from 'src/components/Common/Input/DatePickerField'



export interface FormCreateNotifyProps {
    handleSubmit: () => void
    form: UseFormReturn<FormValues, any>
}

// Component form
export default function FormCreate() {
    const { form, handleConfirm } = React.useContext(FormCreateContext)

    return (
        <form onSubmit={form.handleSubmit(handleConfirm)}>
            <h3></h3>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                        <CardHeader title='Thông tin khách hàng' />
                        <CardContent>
                            <Grid container spacing={10} justifyContent='space-between'>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <TextInputField required control={form.control} name='customerName' label='Chủ xe *' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <TextInputField required control={form.control} name='customerPhoneNumber' label='Số điện thoại chủ xe *' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <TextInputField required control={form.control} name='contactName' label='Tên liên hệ *' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <TextInputField required control={form.control} name='driverPhoneNumber' label='Số điện thoại tài xế *' />
                                    </FormControl>
                                </Grid>
                                

                            </Grid>

                        </CardContent>
                    </Card>
                    <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                        <CardHeader title='Thông tin xe' />
                        <CardContent>
                            <Grid container spacing={10} justifyContent='space-between'>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <TextInputField required control={form.control} name='plateNumber' label='Biển số' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <TextInputField required control={form.control} name='vinCode' label='Số vin' />
                                    </FormControl>
                                </Grid>
                            </Grid>

                        </CardContent>
                    </Card>
                    <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                        <CardHeader title='Thông tin đặt hẹn' />
                        <CardContent>
                            <Grid container spacing={10} justifyContent='space-between'>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <SelectField required control={form.control} name='dealerCode' label='Showroom' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <TextInputField required control={form.control} name='describeSituation' label='Mô tả hiện trạng' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <TextAreaField required control={form.control} name='customerRequirements' label='Yêu cầu khách hàng' />
                                    </FormControl>
                                </Grid>
                                <Grid item container alignContent={'space-between'} xs={6}>
                                    <FormControl fullWidth>
                                        <SelectField required control={form.control} name='itemsToDoSoonCode' label='Các hạng mục cần làm mới ' />
                                    </FormControl>
                                    <FormControl fullWidth>
                                        <SelectField required control={form.control} name='maintenancePackageCode' label='Gói bảo dưỡng ' />
                                    </FormControl>
                                </Grid>
                                <Grid item container alignContent={'space-between'} xs={6}>
                                    <FormControl fullWidth>
                                        <DatePickerField required control={form.control} name='appointmentTime' label='Giờ hẹn' timeFormat='HH:mm' timeIntervals={5} showTimeSelect/>
                                    </FormControl>
                                </Grid>
                                <Grid item container alignContent={'space-between'} xs={6}>
                                    <FormControl fullWidth>
                                        <SelectField required control={form.control} name='status' label='Trạng thái ' />
                                    </FormControl>
                                </Grid>
                            </Grid>

                        </CardContent>
                    </Card>
                    <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                        <CardContent>
                            <Grid container spacing={10} justifyContent='space-between'>
                                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                                    <Button sx={{ marginX: '10px' }} size='large' type='submit' variant='contained'  >
                                        Lưu
                                    </Button>
                                    <Button size='large' color='error' onClick={() => router.back()} variant='outlined'>
                                        Hủy
                                    </Button>
                                </Grid>


                            </Grid>

                        </CardContent>
                    </Card>

                </Grid>

            </Grid>
        </form >
    )
}
