/* eslint-disable no-undef */

// ** React Imports

import React from 'react'

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import { TextInputField } from 'src/components/Common/Input'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import router from 'next/router'
import TextAreaField from 'src/components/Common/Input/TextEditorField'
import { Box, CircularProgress } from '@mui/material'
import { FormUpdateContext } from 'src/context/appointment/FormContext/update'
import DatePickerField from 'src/components/Common/Input/DatePickerField'


// Component form
export default function FormUpdate() {

    const { loading, handleConfirm, form } = React.useContext(FormUpdateContext)
    
    return (
        loading ? <Box sx={{ margin: '0 auto', width: '100%', height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><CircularProgress /></Box> :
        <form onSubmit={form.handleSubmit(handleConfirm)}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                        <CardHeader title='Thông tin lịch hẹn' />
                        <CardContent>
                            <Grid container spacing={10} justifyContent='center'>
                                <Grid item xs={10}>
                                    <FormControl fullWidth>
                                        <TextInputField required disabled={form.getValues('status')!=='PROCESSING'} control={form.control} name='contactName' label='Tên liên hệ' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={10}>
                                    <FormControl fullWidth>
                                        <TextInputField required disabled={form.getValues('status')!=='PROCESSING'} control={form.control} name='driverPhoneNumber' label='Số điện thoại tài xế' />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={10}>
                                    <FormControl fullWidth>
                                        <TextAreaField required disabled={form.getValues('status')!=='PROCESSING'} control={form.control} name='customerRequirements' label='Yêu cầu khách hàng' />
                                    </FormControl>
                                </Grid>
                               <Grid item xs={10} textAlign='right'>
                                    <FormControl fullWidth>
                                        <DatePickerField required disabled={form.getValues('status')!=='PROCESSING'} name='appointmentTime' label='Giờ hẹn ' control={form.control} variant='outlined' timeFormat='HH:mm' timeIntervals={5} showTimeSelect/>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={10} sx={{ textAlign: 'center' }}>
                                    <Button disabled={form.getValues('status')!=='PROCESSING'} sx={{ marginX: '10px' }} size='large' type='submit' variant='contained'  >
                                        Lưu
                                    </Button>
                                    <Button size='large' color='error' onClick={() => router.back()} variant='outlined'>
                                        Hủy
                                    </Button>
                                </Grid>
                            </Grid>

                        </CardContent>
                    </Card>

                </Grid>

            </Grid>
        </form >
    )
}
