/* eslint-disable */
// ** React Imports
import React, { Ref } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import { GridToolbarExport } from '@mui/x-data-grid'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import { useTheme } from '@mui/material/styles'
import Button from '@mui/material/Button'
import { Grid, SelectChangeEvent } from '@mui/material'
import { DropdownDefault } from 'src/components/Common/Dropdown'
import router from 'next/router'
import { SingleDatePicker } from 'src/components/Common/DatePicker'
import { ReactDatePickerProps } from 'react-datepicker'

interface DefaultToolBarProps {
    search: Ref<any>
    handleSearch: () => void
    clearSearch: () => void
    onChange: (e: React.ChangeEvent) => void
    optionsStatus: any[]
    appointmentDate: Date
    handleChangeStatus: (event: SelectChangeEvent) => void
    handleChangeAppointmentDate: (date: Date) => void
    handleClearFilter: () => void
}

export default function Toolbar(props: DefaultToolBarProps) {
    const theme = useTheme()
    const { direction } = theme
    const popperPlacement: ReactDatePickerProps['popperPlacement'] = direction === 'ltr' ? 'bottom-start' : 'bottom-end'
    return (
        <Grid container spacing={2} sx={{ paddingX: 5 }}>
            <Box
                sx={{
                    gap: 2,
                    display: 'flex',
                    flexWrap: 'wrap',
                    alignItems: 'center',
                    p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                }}
            >
                <TextField
                    size='small'
                    placeholder='Tìm kiếm...'
                    inputRef={props.search}
                    onKeyDown={(e) => {
                        if (e.keyCode == 13) {
                            props.handleSearch()
                        }
                    }}
                    InputProps={{
                        startAdornment: (
                            <Box sx={{ mr: 2, display: 'flex' }}>
                                <Icon icon='bx:search' fontSize={20} />
                            </Box>
                        )
                    }}
                    sx={{
                        width: {
                            xs: 1,
                            sm: 'auto'
                        },
                        '& .MuiInputBase-root > svg': {
                            mr: 2
                        },
                        '& .MuiInput-underline:before': {
                            borderBottom: 1,
                            borderColor: 'divider'
                        }
                    }}
                />
                <SingleDatePicker
                    popperPlacement={popperPlacement}
                    date={props.appointmentDate}
                    handleChange={props.handleChangeAppointmentDate}
                    label='Ngày đặt hẹn'
                    onChange={props.handleChangeAppointmentDate}
                    maxDate={new Date()}
                    dateFormat='dd-MM-yyyy'
                />
                <DropdownDefault
                    options={props.optionsStatus}
                    label='Trạng thái'
                    handleChange={props.handleChangeStatus} />

                <Button variant='outlined' size='medium' onClick={props.handleSearch}>
                    Lọc
                </Button>
                <Button variant='outlined' size='medium' color={'error'} onClick={props.handleClearFilter}>
                    Xóa bộ Lọc
                </Button>
                <Button variant='contained' startIcon={<Icon icon='bx:plus' />} onClick={() => router.push('/appointment-booking/create')}>
                    Thêm lịch hẹn
                </Button>
                <Button variant='contained' color='error'>
                    Xóa
                </Button>
                <GridToolbarExport printOptions={{ disableToolbarButton: true }} />
            </Box>
        </Grid>
    )
}
/* eslint-enable */