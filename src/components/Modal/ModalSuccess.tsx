// ** React Imports
import { Fragment } from 'react'

// ** MUI Imports
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import Success from '../Common/Success'
import { useModals } from 'src/context/ModalContext'
import { Box, Button } from '@mui/material'
import router from 'next/router'

export interface ModalLoadingProps {
  title?: string;
  message?: string;
  handleConfirm?: () => void;
  handleClose?: () => void;
}


const ModalSuccess = ({ message }: ModalLoadingProps) => {
  const { closeModal } = useModals();

  return (
    <Fragment>
      <Dialog
        open={true}
        disableEscapeKeyDown
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
        onClose={closeModal}
        fullWidth
        sx={{ minHeight: '500px' }}
      >
        <DialogContent>
          <DialogContentText id='alert-dialog-description'>
            <Success content={message} />
            <Box sx={{ mt: 5, display: "flex", justifyContent: "center", gap: "50px" }}>
              <Button sx={{ minWidth: "120px", backgroundColor:'#71DD37' }} variant="contained" color='success' onClick={()=>router.back()}>Quay lại</Button>
              <Button sx={{ minWidth: "120px" }} variant="contained" onClick={ ()=>{router.reload()}}>Tiếp tục</Button>
            </Box>
          </DialogContentText>
        </DialogContent>

      </Dialog>
    </Fragment>
  )
}

export default ModalSuccess
