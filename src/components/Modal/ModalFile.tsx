import { Modal } from '@mui/material';
import React, { ReactNode, createContext, useContext, useState } from 'react'

const Context = createContext(null);

interface IModalFileProps {
    open: boolean;
    handleClose: () => void;
    children: React.ReactElement
}



const ModalFile = ({ open, handleClose, children }: IModalFileProps) => {
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            {children}
        </Modal>
    )
}

ModalFile.Group = ({ children }: { children: ReactNode }) => {

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [open, setOpen] = useState<boolean>(false)
    const handleClose = () => setOpen(false)
    const handleOpen = () => setOpen(true)

    return <Context.Provider value={{ open, handleClose, handleOpen }}>
        {children}
    </Context.Provider>
}

export const useModalFile = (): { open: boolean, handleClose: () => void, handleOpen: () => void } => {
    const context = useContext(Context)
    if (typeof context === 'undefined') throw new Error("Component must be consumped within ModalFile.Group")

    return context
}

export default ModalFile