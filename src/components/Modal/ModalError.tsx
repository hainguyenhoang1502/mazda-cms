// ** React Imports
import { Fragment } from 'react'

// ** MUI Imports
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import Error from '../Common/Error'
import { useModals } from 'src/context/ModalContext'
import { Box, Button } from '@mui/material'
import router from 'next/router'

export interface ModalLoadingProps{
     open?: boolean;
     title?: string;
     message?: string;
     handleConfirm?: () => void;
     handleClose?: () => void;
}


const ModalError = ({ message, handleConfirm }:ModalLoadingProps) => {
const {closeModal}=useModals();
  
return (
    <Fragment>
      <Dialog
        open={true}
        disableEscapeKeyDown
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
        onClose={closeModal}
        fullWidth
        sx={{minHeight:'500px'}}
      >
        <DialogContent>
          <DialogContentText id='alert-dialog-description'>
            <Error message={message}/>
            <Box sx={{ mt: 5, display: "flex", justifyContent: "center", gap: "50px" }}>
              <Button sx={{ minWidth: "120px" }} variant="contained" onClick={ handleConfirm ||closeModal}>Xác nhận</Button>
              <Button sx={{ minWidth: "120px" }} variant="contained" color='error' onClick={()=>router.back()}>Quay lại</Button>
            </Box>
          </DialogContentText>
        </DialogContent>
        
      </Dialog>
    </Fragment>
  )
}

export default ModalError
