// ** React Imports

// ** MUI Imports
import Button from '@mui/material/Button'
import { useModals } from 'src/context/ModalContext'
import { Box, Modal, Typography } from '@mui/material'

export interface ModalConfirmProps{
     title?: string;
     message?: string;
     handleConfirm?: (id?:any) => void;
     handleClose?: () => void;
}


const ModalConfirm = ({message, handleConfirm}:ModalConfirmProps) => {
const {closeModal}=useModals();
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};
  
  return (
    <Box>
    <Modal
      open={true}
      onClose={closeModal}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      sx={{ zIndex: 9999 }}
    >
      <Box sx={style}>
        <Box sx={{ mt: 2, maxWidth: "100%", display: "flex", justifyContent: "center" }} >
          <img src="/images/pages/warning.png" width="auto" height={"100%"} alt="" />
        </Box>
        <Typography sx={{ margin: "15px 0", fontSize: '28px', fontWeight: 600, color: '#32475CDE', textAlign: "center" }}>
          Bạn có chắc chắn thực hiện điều này?
        </Typography>
        <Typography sx={{ margin: "10px 0", textAlign: "center", fontSize: "16px" }}>
          {message}
        </Typography>
        <Box sx={{ mt: 5, display: "flex", justifyContent: "center", gap: "10px" }}>
          <Button sx={{ minWidth: "120px" }} variant="contained" onClick={handleConfirm}>Đồng ý</Button>
          <Button sx={{ minWidth: "120px" }} variant="outlined" color='error' onClick={closeModal}>Hủy</Button>
        </Box>
      </Box>
    </Modal>
  </Box>
  )
}

export default ModalConfirm
