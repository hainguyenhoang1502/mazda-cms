import { Box, Modal, Typography, Button } from '@mui/material';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { useModals } from 'src/context/ModalContext';

export interface ModalLeavePageProps {
    confirm: boolean,
    setConfirm: (value: boolean) => void
}

const ModalLeavePage = ({ confirm, setConfirm }: ModalLeavePageProps) => {
    const router = useRouter();
    const { registerModal, openModal, closeModal } = useModals();

    registerModal(
        'leave_page',
        <LeavePageModal
            message="Những thay đổi của bạn có thể không được lưu!"
            handleConfirm={() => {
                setConfirm(true);
                closeModal('leave_page');
                router.back(); // Replace '/' with the desired URL where you want to navigate after confirming
            }}
            handleClose={() => {
                setConfirm(false);
                closeModal('leave_page');
            }}
        />
    );


    const handleRouteChange = () => {
        // Perform any necessary cleanup or additional actions here
    };

    const handleBeforePopState = React.useCallback(({ url }) => {
        // Prompt the user with a confirmation dialog or perform any other necessary actions
        if (!confirm) {
            openModal('leave_page');
            window.history.pushState(null, '', url);

            return false; // Prevents the router from changing the URL

        }

        return true;
    }, [confirm, openModal]);

    useEffect(() => {

        router.events.on('routeChangeStart', handleRouteChange);
        router.beforePopState(handleBeforePopState);

        return () => {
            router.events.off('routeChangeStart', handleRouteChange);
            router.beforePopState(undefined);
        };
    }, [confirm, handleBeforePopState, handleRouteChange, openModal, router]);

    return null; // Return null since this component doesn't render anything
};

export interface ModalConfirmProps {
    title?: string;
    message?: string;
    handleConfirm?: () => void;
    handleClose?: () => void;
}

const LeavePageModal = ({
    message,
    handleConfirm,
    handleClose,
}: ModalConfirmProps) => {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 500,
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
    };

    return (
        <Box>
            <Modal
                open={true}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                sx={{ zIndex: 9999 }}
            >
                <Box sx={style}>
                    <Box
                        sx={{ mt: 2, maxWidth: '100%', display: 'flex', justifyContent: 'center' }}
                    >
                        <img src="/images/pages/warning.png" width="auto" height={'100%'} alt="" />
                    </Box>
                    <Typography
                        sx={{ margin: '15px 0', fontSize: '28px', fontWeight: 600, color: '#32475CDE', textAlign: 'center' }}
                    >
                        Bạn có chắc chắn thực hiện điều này?
                    </Typography>
                    <Typography sx={{ margin: '10px 0', textAlign: 'center', fontSize: '16px' }}>
                        {message}
                    </Typography>
                    <Box sx={{ mt: 5, display: 'flex', justifyContent: 'center', gap: '10px' }}>
                        <Button sx={{ minWidth: '120px' }} variant="contained" onClick={handleConfirm}>
                            Xác nhận
                        </Button>
                        <Button sx={{ minWidth: '120px' }} variant="contained" color="error" onClick={handleClose}>
                            Hủy
                        </Button>
                    </Box>
                </Box>
            </Modal>
        </Box>
    );
};

export default ModalLeavePage;
