// ** React Imports
import { Fragment } from 'react'

// ** MUI Imports
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import { CircularProgress } from '@mui/material'
import { useModals } from 'src/context/ModalContext'


const ModalLoading = () => {
  const {closeModal}=useModals();
  
  return (
    <Fragment>
      <Dialog
        open={true}
        disableEscapeKeyDown
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
        onClose={closeModal}
        fullWidth
      >
        <DialogContent>
          <DialogContentText id='alert-dialog-description' sx={{minHeight:'300px', textAlign:'center', display:'flex', alignItems:'center', justifyContent:'center'}}>
            <CircularProgress/>
          </DialogContentText>
        </DialogContent>
        
      </Dialog>
    </Fragment>
  )
}

export default ModalLoading
