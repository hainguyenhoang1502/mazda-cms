// ** React Imports

// ** MUI Imports
import { Box, Modal } from '@mui/material'
import TableNotifyUser from '../notifications/Table/TableUser';
import { useContext } from 'react';
import { FormCreateContext } from 'src/context/notifications/Form/create';

export interface ModalConfirmProps {
    title?: string;
    message?: string;
    open?:boolean;
    handleConfirm?: (id?: any) => void;
    handleClose?: () => void;
}


const ModalTableUser = ({  }: ModalConfirmProps) => {
    
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '90%',
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
    };

    const {openTable, setOpenTableModal}= useContext(FormCreateContext);

    return (
        <Box>
            <Modal
                open={openTable}
                onClose={setOpenTableModal(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                sx={{ zIndex: 9999 }}
            >
                <Box sx={style}>

                    <TableNotifyUser />

                </Box>
            </Modal>
        </Box>
    )
}

export default ModalTableUser
