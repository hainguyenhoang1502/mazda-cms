/* eslint-disable */

import Link from 'next/link'

import Box from '@mui/material/Box'
import { styled, useTheme } from '@mui/material/styles'
import Typography, { TypographyProps } from '@mui/material/Typography'
import { useDropzone } from 'react-dropzone'
import { Button } from '@mui/material'
import Icon from 'src/@core/components/icon'

interface FileProp {
  name: string
  type: string
  size: number
}



const HeadingTypography = styled(Typography)<TypographyProps>(({ theme }) => ({
  marginBottom: theme.spacing(5),
  [theme.breakpoints.down('sm')]: {
    marginBottom: theme.spacing(4)
  }
}))
export interface FileUploaderSingleProp {
  files: File[]
  setFiles: (file: File[]) => void
}

const FileUploaderSingle = ({ files, setFiles }: FileUploaderSingleProp) => {

  const { getRootProps, getInputProps } = useDropzone({
    multiple: false,
    accept: {
      'image/*': ['.png', '.jpg', '.jpeg', '.gif']
    },
    onDrop: (acceptedFiles: File[]) => {
      setFiles(acceptedFiles.map((file: File) => Object.assign(file)))
    }
  })

  const img = () => {
    return files.map((file: FileProp) => (
      typeof file !== 'object' ?
        <img alt={file} className='single-file-image' src={file} style={{ maxWidth: '400px', maxHeight: "200px", objectFit: "cover" }} />
        :
        <img key={file.name} alt={file.name} className='single-file-image' src={URL.createObjectURL(file as any)} style={{ maxWidth: '400px', maxHeight: "200px", objectFit: "cover" }} />

    ))
  }

  return (
    <Box {...getRootProps({ className: 'dropzone' })} sx={{ height: '100%' }}>
      <input {...getInputProps()} />
      {files.length ? (
        img()
      ) : (
        <Box sx={{ border: '1px dashed #9A9AB0', borderRadius: '5px', display: 'flex', flexDirection: 'column', alignItems: 'center', textAlign: 'center', justifyContent: 'center', padding: '10px' }}>
          <Icon icon='tabler:cloud-upload' fontSize={70} />
          <Box sx={{ display: 'flex', flexDirection: 'column', textAlign: ['center', 'center', 'inherit'] }}>
            <HeadingTypography variant='h5'>Drag&Drop files here</HeadingTypography>
            <Typography color='textSecondary' sx={{ marginBottom: '20px' }}>
              or
            </Typography>
            <Typography color='textSecondary' sx={{ '& a': { color: 'primary.main', textDecoration: 'none' } }}>
              <Link href='/' onClick={e => e.preventDefault()}>
                <Button variant='outlined' sx={{ color: '#15B3C9', borderColor: '#15B3C9' }}>browser here</Button>
              </Link>
            </Typography>
          </Box>
        </Box>
      )}
    </Box>
  )
}

export default FileUploaderSingle
/* eslint-enable */