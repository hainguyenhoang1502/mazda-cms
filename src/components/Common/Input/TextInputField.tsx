/* eslint-disable */

import * as React from 'react'
import TextField, { TextFieldProps } from '@mui/material/TextField'
import { Control, Controller, useController } from 'react-hook-form'
import { Box, FormHelperText, MenuItem, Typography } from '@mui/material'
import Icon from 'src/@core/components/icon'

export type IInputProps = TextFieldProps & {
  name: string
  control: Control<any>
  label?: string
  options?: {
    value: any
    label: string
  }[]
  required?: boolean
}

export default function TextInputField({
  name,
  control,
  label,
  options,
  onChange: externalOnChange,
  onBlur: externalOnBlur,
  ref: externalRef,
  value: externalValue,
  disabled,
  required,
  ...rest
}: IInputProps) {
  const {
    field: { onBlur, ref },
  } = useController({
    name,
    control
  })

  return (
    <Box>
      <Controller
        name={name}
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange }, fieldState }) => (
          <>
            <TextField
              name={name}
              disabled={disabled}
              value={value}
              label={
                <Typography variant="subtitle1">
                  {label} {required && <span style={{ color: 'red' }}>*</span>}
                </Typography>
              }
              onChange={onChange}
              onBlur={onBlur}
              inputRef={ref}
              defaultValue={value}
              fullWidth
              size='small'
              {...rest}
            >
              {options?.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
            {fieldState.error ? (
              <FormHelperText style={{ color: 'red', display:'flex', alignItems:'center', gap:'5px' }}>
                <Icon icon={'material-symbols:error'} fontSize={16} />
                <Typography fontSize={16} color={'red'}>
                  {fieldState.error?.message}
                </Typography>
              </FormHelperText>
            ) : (
              ''
            )}
          </>
        )} /*  */
      />
    </Box>
  )
}
/* eslint-enable */