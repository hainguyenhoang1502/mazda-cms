/* eslint-disable */
import * as React from 'react';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import { Control, Controller, useController } from 'react-hook-form';
import { Autocomplete, Box, Chip, FormControl, FormHelperText, InputLabel, Typography } from '@mui/material';
import Icon from 'src/@core/components/icon';

type Option = {
  value: string;
  label: string;
};

export type IInputProps = TextFieldProps & {
  name: string;
  control: Control<any>;
  label?: string;
  options?: Option[];
  placeholder?: string;
  required?: boolean;
};

export default function AutocompleteField({
  name,
  control,
  label,
  options = [],
  placeholder,
  disabled,
  required
}: IInputProps) {
  const {
    field: { value },
    fieldState: { },
  } = useController({
    name,
    control,
  });


  return (
    <Box>
      <Controller
        name={name}
        control={control}
        rules={{ required: true }}
        defaultValue={value}
        render={({ field: { value, onChange }, fieldState }) => {

          return (
            <FormControl fullWidth>

              <Autocomplete
                defaultValue={value}
                multiple
                fullWidth
                disabled={disabled}
                options={options}
                value={value}
                onChange={(e, newValue: Option[] | null) => onChange(newValue)}
                renderInput={(params) => (
                  <TextField {...params}
                    label={
                      <Typography variant="subtitle1">
                        {label} {required && <span style={{ color: 'red' }}>*</span>}
                      </Typography>
                    }
                    variant="outlined"
                    sx={{ border: 'none', maxHeight:'300px'}}
                    placeholder={placeholder}
                    fullWidth 
                    
                    />
                )}
                renderTags={(value: Option[], getTagProps) =>
                  value.map((option: Option, index: number) => (
                    <Chip
                      variant="filled"
                      size="medium"
                      sx={{ display: 'flex', borderRadius: '10px', minWidth: '150px', justifyContent: 'space-between', backgroundColor: 'rgba(189, 189, 189, 0.47)' }}
                      label={option.label}
                      {...(getTagProps({ index }) as {})}
                      key={option.value}
                    />
                  ))
                }
                isOptionEqualToValue={(option, value) => option.value === value?.value}
              />
              {fieldState.error ? (
                <FormHelperText style={{ color: 'red', display: 'flex', alignItems: 'center', gap: '5px' }}>
                  <Icon icon={'material-symbols:error'} fontSize={16} />
                  <Typography fontSize={16} color={'red'}>
                    {fieldState.error?.message}
                  </Typography>
                </FormHelperText>
              ) : (
                ''
              )}
            </FormControl>
          );
        }}
      />
    </Box>
  );
}
/* eslint-enable */