/* eslint-disable */

import * as React from 'react'
import { TextFieldProps } from '@mui/material/TextField'
import { Control, Controller } from 'react-hook-form'
import { FormHelperText, Typography } from '@mui/material'
import { TextField, InputAdornment } from '@mui/material'
import Icon from 'src/@core/components/icon'
import DatePicker, { ReactDatePickerProps } from 'react-datepicker'
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'

export type IInputProps = TextFieldProps & {
  name: string
  control: Control<any>
  label?: string
  showYearPicker?: boolean
  dateFormat?: string
  yearItemNumber?: number
  showTimeSelect?: boolean
  timeFormat?: string
  maxDate?: Date
  timeIntervals?: number
  required?: boolean
}

export default function DatePickerField({
  name,
  control,
  label,
  showTimeSelect,
  helperText,
  dateFormat,
  showYearPicker,
  timeFormat,
  timeIntervals,
  yearItemNumber,
  maxDate,
  onChange: externalOnChange,
  onBlur: externalOnBlur,
  ref: externalRef,
  value: externalValue,
  disabled,
  required,
  ...rest
}: IInputProps) {

  return (
    <DatePickerWrapper>
      <Controller
        name={name}
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange }, fieldState }) => (
          <>
            <DatePicker
              {...showTimeSelect ? { showTimeSelect } : {}}
              {...timeFormat ? { timeFormat: timeFormat } : {}}
              {...timeIntervals ? { timeIntervals: timeIntervals } : {}}
              selected={value}
              disabled={disabled}
              id='date-time-picker'
              popperPlacement='bottom-start'
              {...maxDate ? { maxDate: new Date() } : {}}
              dateFormat={dateFormat ? dateFormat : 'dd/MM/yyyy h:mm aa'}
              {...showYearPicker ? { showYearPicker } : {}}
              onChange={date => {
                onChange(date ? new Date(date.valueOf()) : null)
              }}
              customInput={
                <TextField
                  id='outlined-basic'
                  label={
                    <Typography variant="subtitle1">
                      {label} {required && <span style={{ color: 'red' }}>*</span>}
                    </Typography>
                  }
                  size='small'
                  disabled={disabled}
                  fullWidth
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position='start'>
                        <Icon icon='bx:calendar' />
                      </InputAdornment>
                    )
                  }}
                  {...rest}

                ></TextField>
              }
            />
            {fieldState.error ? (
              <FormHelperText style={{ color: 'red', display: 'flex', alignItems: 'center', gap: '5px' }}>
                <Icon icon={'material-symbols:error'} fontSize={16} />
                <Typography fontSize={16} color={'red'}>
                  {fieldState.error?.message}
                </Typography>
              </FormHelperText>
            ) : (
              ''
            )}
          </>
        )} /*  */
      />
    </DatePickerWrapper>
  )
}
/* eslint-enable */