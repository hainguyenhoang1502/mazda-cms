/* eslint-disable */

import * as React from 'react'
import TextField, { TextFieldProps } from '@mui/material/TextField'
import { Control, Controller, useController } from 'react-hook-form'
import { Box, Checkbox, FormControlLabel, FormHelperText, MenuItem, Typography } from '@mui/material'
import Icon from 'src/@core/components/icon'

export type IInputProps = TextFieldProps & {
    name: string
    control: Control<any>
    label?: string
    required?: boolean
}

export default function CheckboxField({
    name,
    control,
    label,
    onChange: externalOnChange,
    onBlur: externalOnBlur,
    ref: externalRef,
    value: externalValue,
    disabled,
    required,
    ...rest
}: IInputProps) {
    const {
        field: { onBlur, ref },
    } = useController({
        name,
        control
    })

    return (
        <Box>
            <Controller
                name={name}
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange }, fieldState }) => (
                    <>
                        <FormControlLabel label={label} control={
                            <Checkbox
                                checked={value}
                                onChange={onChange}
                                inputProps={{ 'aria-label': 'controlled' }}
                            />
                        } />
                        {fieldState.error ? (
                            <FormHelperText style={{ color: 'red', display: 'flex', alignItems: 'center', gap: '5px' }}>
                                <Icon icon={'material-symbols:error'} fontSize={16} />
                                <Typography fontSize={16} color={'red'}>
                                    {fieldState.error?.message}
                                </Typography>
                            </FormHelperText>
                        ) : (
                            ''
                        )}
                    </>
                )} /*  */
            />
        </Box>
    )
}
/* eslint-enable */