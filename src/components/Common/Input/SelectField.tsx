/* eslint-disable */

import * as React from 'react'
import { TextFieldProps } from '@mui/material/TextField'
import { Control, Controller } from 'react-hook-form'
import { FormControl, FormHelperText, InputLabel, MenuItem, Select, Typography } from '@mui/material'
import Icon from 'src/@core/components/icon'
import { DropdownIcon } from 'src/components/Icon'

export type IInputProps = TextFieldProps & {
  name: string
  control: Control<any>
  label?: string
  options?: {
    value: any
    label: string
  }[]
  shrink?: boolean
  required?: boolean
}

export default function SelectField({
  name,
  control,
  shrink,
  label,
  options,
  disabled,
  required
}: IInputProps) {

  return (
    <Controller
      name={name}
      control={control}
      rules={{ required: true }}
      render={({ field: { value, onChange }, fieldState }) => (
        <FormControl>
          <InputLabel shrink={shrink} size='small' htmlFor='demo-simple-select-helper-label'>
            {label} {required && <span style={{ color: 'red' }}>*</span>}
          </InputLabel>
          <Select
            disabled={disabled}
            label={label}
            size='small'
            value={value ? value : null}
            id='demo-simple-select-helper'
            labelId='demo-simple-select-helper-label'
            IconComponent={DropdownIcon}
            onChange={onChange}
            sx={{ textAlign: 'left' }}
          >
            {options?.map((item, index) => {
              return (
                <MenuItem key={index} value={item.value}>
                  {item.label}
                </MenuItem>
              )
            })}
          </Select>
          {fieldState.error ? (
            <FormHelperText style={{ color: 'red', display: 'flex', alignItems: 'center', gap: '5px' }}>
              <Icon icon={'material-symbols:error'} fontSize={16} />
              <Typography fontSize={16} color={'red'}>
                {fieldState.error?.message}
              </Typography>
            </FormHelperText>
          ) : (
            ''
          )}
        </FormControl>
      )} /*  */
    />
  )
}
/* eslint-enable */