/* eslint-disable */

import * as React from 'react'
import { TextFieldProps } from '@mui/material/TextField'
import { Control, Controller } from 'react-hook-form'
import { Box, FormControl, FormHelperText, InputLabel, Typography } from '@mui/material'
import FileUploaderSingle from 'src/components/FileUpload/FileUploaderSingle'
import Icon from 'src/@core/components/icon'

export type IInputProps = TextFieldProps & {
    name: string
    control: Control<any>
    label?: string
    options?: any
    placeholder?: string
    required?: boolean
}

export default function FileUpLoadField({
    name,
    label,
    control,
    onChange: externalOnChange,
    onBlur: externalOnBlur,
    ref: externalRef,
    value: externalValue,
    disabled,
    required
}: IInputProps) {

    return (
        <Box>
            <Controller
                name={name}
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange }, fieldState }) => {
                    return (
                        <FormControl fullWidth>
                            {label && <Typography sx={{ marginY: '20px' }}>
                                {label} {required && <span style={{ color: 'red' }}>*</span>}
                            </Typography>}
                            <FileUploaderSingle files={value} setFiles={onChange} />
                            {fieldState.error ? (
                                <FormHelperText style={{ color: 'red', display: 'flex', alignItems: 'center', gap: '5px' }}>
                                    <Icon icon={'material-symbols:error'} fontSize={16} />
                                    <Typography fontSize={16} color={'red'}>
                                        {fieldState.error?.message}
                                    </Typography>
                                </FormHelperText>
                            ) : (
                                ''
                            )}
                        </FormControl>
                    )
                }} />
        </Box>
    )
}
/* eslint-enable */