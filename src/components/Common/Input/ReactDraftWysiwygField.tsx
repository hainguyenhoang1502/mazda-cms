/*eslint-disable*/

import React, { useMemo, useCallback, useEffect } from 'react';
import { TextFieldProps } from '@mui/material/TextField';
import { Control, Controller } from 'react-hook-form';
import { Box, FormControl, FormHelperText, Typography } from '@mui/material';
import ReactDraftWysiwyg from 'src/@core/components/react-draft-wysiwyg';
import { ContentState, EditorState, convertToRaw } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import draftToHtml from 'draftjs-to-html';
import { mediaApi } from 'src/api-client';
import Icon from 'src/@core/components/icon';

export type IInputProps = TextFieldProps & {
  name: string;
  control: Control<any>;
  label?: string;
  options?: any;
  placeholder?: string;
  height?: string;
  handleBlur?: () => void;
  required?: boolean;
};

export default function ReactDraftWysiwygField({
  name,
  control,
  label,
  disabled,
  height,
  required,
  handleBlur
}: IInputProps) {


  return (

    <Box>
      <Controller
        name={name}
        control={control}
        rules={{ required: true }}
        render={({ field, fieldState }) => {
          const { value, onChange } = field;

          const prepareDraft = useCallback((value: string) => {
            const contentBlock = htmlToDraft(value);
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);

            return editorState;
          }, []);

          const [editorState, setEditorState] = React.useState(EditorState.createEmpty());

          console.log(value)
          useEffect(() => {
            if (value.length > 0) {
              setEditorState(prev => prev = prepareDraft(value));
            }
          }, [])
          return <Editor required={required} field={field} fieldState={fieldState} height={height}
            editorState={editorState} setEditorState={setEditorState} onChange={onChange}
            label={label} disabled={disabled} handleBlur={handleBlur} />
        }}
      />
    </Box>
  );
}

const Editor = ({ field, fieldState, label, disabled, required, height, onChange, setEditorState, editorState, handleBlur }) => {


  const handleEditorStateChange = useCallback(
    (newEditorState: EditorState) => {
      const forFormik = draftToHtml(convertToRaw(newEditorState.getCurrentContent()));
      onChange(forFormik);
      setEditorState(newEditorState);
    },
    [onChange]
  );

  const uploadCallback = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new window.FileReader();
      reader.onloadend = async () => {
        const form_data = new FormData();
        form_data.append("file", file);
        const res = await mediaApi.uploadImage(form_data);

        if (res.data) {
          const imageUrl = `${res.data.serverName}/${res.data.filePath}`;
          const imageObject = { data: { link: imageUrl } };
          resolve(imageObject);
        } else {
          reject(new Error("Error uploading image"));
        }
      };
      reader.readAsDataURL(file);
    });
  };


  return (
    <FormControl fullWidth>
      {label && (
        <Typography sx={{ marginY: '20px' }}>
          {label} {required && <span style={{ color: 'red' }}>*</span>}
        </Typography>
      )}
      <ReactDraftWysiwyg
        readOnly={disabled}
        toolbar={{
          inline: { inDropdown: true },
          list: { inDropdown: true },
          textAlign: { inDropdown: true },
          link: { inDropdown: true },
          options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'link', 'embedded', 'image', 'history'],
          image: { uploadCallback: uploadCallback },
          history: {
            inDropdown: false,
            className: undefined,
            component: undefined,
            dropdownClassName: undefined,
            options: ['undo', 'redo'],
          },
        }}
        editorStyle={{ minHeight: `${height}px`, padding: '0 10px', border: '1px solid rgb(207 207 207 / 87%)' }}
        editorState={editorState}
        onEditorStateChange={handleEditorStateChange}
        onFocus={handleBlur}
      />
      {fieldState.error && (
        <FormHelperText style={{ color: 'red', display: 'flex', alignItems: 'center', gap: '5px' }}>
          <Icon icon={'material-symbols:error'} fontSize={16} />
          <Typography fontSize={16} color={'red'}>
            {fieldState.error?.message}
          </Typography>
        </FormHelperText>
      )}
    </FormControl>
  );
}
/*eslint-enable*/
