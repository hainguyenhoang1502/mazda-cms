

import DatePicker, { ReactDatePickerProps } from 'react-datepicker'


import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'

import { TextField, InputAdornment } from '@mui/material'

import Icon from 'src/@core/components/icon'

type SingleDatePickerProps = ReactDatePickerProps & {
  date: Date
  handleChange: (date: Date) => void
  popperPlacement: ReactDatePickerProps['popperPlacement']
  label: string
}

const SingleDatePicker = ({ popperPlacement, date, handleChange, label, ...rest }: SingleDatePickerProps) => {
  return (
    <DatePickerWrapper>
      <DatePicker
        selected={date}
        id='picker-open-date'
        popperPlacement={popperPlacement}
        openToDate={new Date()}
        onChange={(date: Date) => handleChange(date)}
        customInput={
          <TextField
            id='outlined-basic'
            label={label || ''}
            size='small'
            InputProps={{
              endAdornment: (
                <InputAdornment position='start'>
                  <Icon icon='bx:calendar' />
                </InputAdornment>
              )
            }}
            
          />
        }
        {...rest}
      />
    </DatePickerWrapper>
  )
}

export default SingleDatePicker
