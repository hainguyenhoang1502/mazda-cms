import { Box, CircularProgress } from "@mui/material";
import * as React from "react";


export default function Loading() {
  return (
    <>
      
      <Box sx={{margin:'0 auto', width:'100%', minHeight:'300px'}}>
        <CircularProgress/>
      </Box>
      
    </>
  );
}
