import { FormControl, Select, SelectChangeEvent } from '@mui/material'
import MenuItem from '@mui/material/MenuItem'
import InputLabel from '@mui/material/InputLabel'
import * as React from 'react'
import { DropdownIcon } from 'src/components/Icon'

export interface IDropdownDefaultProps {
  options: any[]
  label: string
  handleChange: (event: SelectChangeEvent) => void
  value?: any
}

export default function DropdownDefault({label, options, handleChange}: IDropdownDefaultProps) {
  return (
    <FormControl>
      <InputLabel  size='small' htmlFor='demo-simple-select-helper-label'>
            {label}
          </InputLabel>
          <Select
            label={label}
            size='small'
            id='demo-simple-select-helper'
            labelId='demo-simple-select-helper-label'
            IconComponent={DropdownIcon}
            onChange={handleChange}
            sx={{textAlign:'left'}}
          >
            {options?.map((item, index) => {
              return (
                <MenuItem  key={index} value={item.value}>
                  {item.label}
                </MenuItem>
              )
            })}
          </Select>
    </FormControl>
  )
}
