import { FormControl, Select, SelectProps } from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import * as React from 'react';
import CustomChip from 'src/@core/components/mui/chip';

export type IDropdownDefaultProps = SelectProps&{
  options: {
    label:string,
    value:any
  }[];
  label?: string;
  value?: {
    label:string,
    value:any
  };
}

const statusIcon={
  "NEW":"#ffd402",
  "PROCESSING":"#08efef",
  "CONFIRM":"#5aff00",
  "CANCEL":"red"
}
const statusChipColor={
  "NEW":"warning",
  "PROCESSING":"info",
  "CONFIRM":"success",
  "CANCEL":"error"
}

export default function ChipsDropdown({ label, options, value, ...rest }: IDropdownDefaultProps) {
  return (
    <FormControl fullWidth>
      <InputLabel size="small" htmlFor="demo-simple-select-helper-label">
        {label}
      </InputLabel>
      <Select
        label={label}
        size="small"
        value={value ? value.value : ''} // Update the value prop here
        id="demo-simple-select-helper"
        labelId="demo-simple-select-helper-label"
        sx={{ "& .MuiSvgIcon-root": { color: value ? statusIcon[value?.value] : "#333" } }}
        SelectDisplayProps={{ style: { padding: 0, width: '100%', border: 'none' } }}
        {...rest}
      >
        {options?.map((item, index) => {
          return (
            <MenuItem key={index} value={item.value}>
              <CustomChip
                key={index}
                rounded
                size="small"
                skin="light"
                color={item.value ? statusChipColor[item.value] : 'primary'}
                label={item.label}
                sx={{ width: '100%', paddingRight: '30px' }}
              />
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
}

