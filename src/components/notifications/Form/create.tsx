/* eslint-disable no-undef */

// ** React Imports

import React from 'react'

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'


// ** Icon Imports
import { SelectField, TextInputField } from 'src/components/Common/Input'
import DatePickerField from 'src/components/Common/Input/DatePickerField'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import AutocompleteField from 'src/components/Common/Input/AutoCompleteField'
import ReactDraftWysiwygField from 'src/components/Common/Input/ReactDraftWysiwygField'
import FileUpLoadField from 'src/components/Common/Input/FileUpLoadField'
import router from 'next/router'

import { FormCreateContext, genderOptions, notifyCate, notifyType } from 'src/context/notifications/Form/create'
import { IFormActionType } from 'src/components/Form/type'
import { Box, Modal, Skeleton, TextField } from '@mui/material'
import TableNotifyUser from '../Table/TableUser'
import Icon from 'src/@core/components/icon'
import TableVinNumber from '../Table/TableVinNumber'
import CheckboxField from 'src/components/Common/Input/CheckboxField'

// Component form
export default function CreateForm() {
    const { provinces,
        carModels,
        notifyTemplate,
        handleSubmit,
        openTable,
        form,
        loading,
        setOpenTableModal,
        setFile,
        vinNumbers,
        setFormAction,
        searchValue,
        handleSearch,
        handleDelete,
        deleteArr,
        setDeleteArr,
        createVinNum,
        handleCreateVinNum
    } = React.useContext(FormCreateContext)
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '90%',
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
    };


    return (
        <>
            <form onSubmit={form.handleSubmit(handleSubmit)} onKeyDown={(e) => { if (e.keyCode == 13) e.preventDefault() }}>
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                            <CardHeader title='Thông tin thông báo' />
                            <CardContent>
                                <Grid container spacing={10} justifyContent='space-between'>
                                    <Grid item xs={12}>
                                        <FormControl fullWidth>
                                            <TextInputField required control={form.control} name='notify_title' label='Tiêu đề thông báo' />
                                        </FormControl>
                                    </Grid>

                                    <Grid item xs={6} textAlign='right'>
                                        <FormControl fullWidth>
                                            <SelectField required control={form.control} name='notify_cate' label='Danh mục thông báo' options={notifyCate} />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={6} textAlign='right'>
                                        <FormControl fullWidth>
                                            <SelectField required control={form.control} name='notify_type' label='Loại thông báo' options={notifyType} defaultValue='SENDNOW' />
                                        </FormControl>
                                    </Grid>
                                    {form.watch('notify_type') === 'SCHEDULE' && <Grid item xs={12} textAlign='right'>
                                        <FormControl fullWidth>
                                            <DatePickerField required name='send_date' label='Ngày gửi' control={form.control} variant='outlined' timeFormat='HH:mm' timeIntervals={5} showTimeSelect />
                                        </FormControl>
                                    </Grid>}
                                </Grid>

                            </CardContent>
                        </Card>
                        <h3>Gửi đến</h3>
                        <Card sx={{ marginY: '20px' }}>
                            <CardHeader title='Giới tính' />
                            <CardContent>
                                <Grid container spacing={3} justifyContent='space-between'>
                                    <Grid item xs={12}>
                                        <FormControl fullWidth>
                                            <AutocompleteField name="gender" control={form.control} label='Giới tính' placeholder='Giới tính' options={[{ value: '', label: 'Tất cả' }, ...genderOptions]} />
                                        </FormControl>
                                    </Grid>
                                </Grid>

                            </CardContent>
                        </Card>
                        <Card sx={{ marginY: '20px' }}>
                            <CardHeader title='Điểm bán hàng' />
                            <CardContent>
                                <Grid container spacing={3} justifyContent='space-between'>
                                    <Grid item xs={12}>
                                        <FormControl fullWidth>
                                            <AutocompleteField name="provinces" control={form.control} label='Tỉnh thành' placeholder='Điểm bán hàng' options={[{ value: '', label: 'Tất cả' }, ...provinces]} />
                                        </FormControl>
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                        <Card sx={{ marginY: '20px' }}>
                            <CardHeader title='Sản phẩm' />
                            <CardContent>
                                <Grid container spacing={3} justifyContent='space-between'>
                                    <Grid item xs={12}>
                                        <FormControl fullWidth>
                                            <AutocompleteField name="carModel" control={form.control} label='Sản phẩm' placeholder='Sản phẩm' options={[{ value: '', label: 'Tất cả' }, ...carModels]} />
                                        </FormControl>
                                    </Grid>
                                </Grid>

                            </CardContent>
                        </Card>
                        <Card>
                            <Grid container justifyContent={'space-between'} alignItems={'center'}>
                                <Grid item>
                                    <CardHeader title='Số VIN' />
                                </Grid>

                            </Grid>
                            <CardContent>
                                <Grid container spacing={3} justifyContent='space-between'>
                                    <Grid item xs={12}>
                                        <FormControl fullWidth>
                                            {/* <AutocompleteField name="carModel" control={form.control} label='Sản phẩm' placeholder='Sản phẩm' options={[{value:'', label:'Tất cả'},...carModels]} /> */}
                                            <Box sx={{ display: 'flex', gap: '10px', marginY: '10px' }}>
                                                <TextField
                                                    size='small'
                                                    placeholder='Thêm số Vin'
                                                    inputRef={createVinNum}
                                                    onKeyDown={(e) => {
                                                        if (e.keyCode == 13) {
                                                            handleCreateVinNum()
                                                        }
                                                    }}
                                                    sx={{
                                                        flex: 1,
                                                        width: {
                                                            xs: 1,
                                                            sm: 'auto'
                                                        },
                                                        '& .MuiInputBase-root > svg': {
                                                            mr: 2
                                                        },
                                                        '& .MuiInput-underline:before': {
                                                            borderBottom: 1,
                                                            borderColor: 'divider'
                                                        }
                                                    }}
                                                />
                                                <Button size='large' onClick={() => handleCreateVinNum()} type='button' variant='contained'>
                                                    Thêm
                                                </Button>
                                                <Grid item>
                                                    <input type='file' style={{ display: 'none' }} id="import-excel-button" onChange={(e) => setFile(e.target.files[0])} />
                                                    <label htmlFor="import-excel-button">
                                                        <Button component="span" variant='outlined' sx={{ border: '1px dashed', marginRight: 5 }}>
                                                            <Icon icon={'mdi:paperclip'} /> Tải tệp
                                                        </Button>
                                                    </label>
                                                </Grid>
                                            </Box>
                                            <TableVinNumber
                                                rows={vinNumbers}
                                                searchValue={searchValue}
                                                loading={undefined}
                                                handleSearch={handleSearch}
                                                handleDelete={handleDelete}
                                                deleteArr={deleteArr}
                                                setDeleteArr={setDeleteArr}
                                            />
                                            {/* <AutocompleteField name="vinNumber" control={form.control} label='Số Vin' placeholder='' options={[{ value: '', label: 'Tất cả' }, ...vinNumbers]} /> */}
                                        </FormControl>
                                    </Grid>
                                </Grid>

                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={6}>

                        <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                            <CardHeader title='Nội dung chi tiết' />
                            <CardContent>
                                <Grid container spacing={6} className='match-height'>
                                    <Grid item xs={12} textAlign='right'>
                                        <FormControl fullWidth>
                                            <SelectField control={form.control} name='notify_template' label='Template thông báo' options={notifyTemplate} />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} >
                                        {loading ? (
                                            <>
                                                <Skeleton
                                                    animation="wave"
                                                    height={50}
                                                    style={{ margin: 0 }}
                                                    width="100%"
                                                />
                                                <Skeleton
                                                    animation="wave"
                                                    height={300}
                                                    width="100%"
                                                />
                                            </>

                                        ) : (<ReactDraftWysiwygField required height="300" name='editor' control={form.control} />)}

                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                        <Card sx={{ marginY: '20px' }}>
                            <CardHeader title='Chọn hình ảnh thông báo' />
                            <CardContent>
                                <Grid container spacing={3} justifyContent='space-between'>
                                    <Grid item xs={12}>
                                        <FormControl fullWidth>
                                            <FileUpLoadField required name='fileUpload' control={form.control} />
                                        </FormControl>
                                    </Grid>
                                </Grid>

                            </CardContent>
                        </Card>
                        <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                            <CardContent sx={{ overflow: 'unset' }}>
                                <Grid justifySelf={'left'} sx={{ width: 'fit-content' }} item>
                                    <FormControl fullWidth>
                                        <CheckboxField required control={form.control} name='isShowPopup' label='Hiển thị Popup' />
                                    </FormControl>
                                </Grid>
                                {
                                    form.watch('isShowPopup') &&
                                    <>
                                        <Grid container spacing={6} sx={{ marginBottom: '20px' }} item>
                                            <Grid xs={6} item>
                                                <FormControl fullWidth>
                                                    <DatePickerField required control={form.control} name='fromDate' label='Từ ngày' timeFormat='HH:mm' timeIntervals={5} showTimeSelect />
                                                </FormControl>
                                            </Grid>
                                            <Grid xs={6} item>
                                                <FormControl fullWidth>
                                                    <DatePickerField required control={form.control} name='endDate' label='Đến ngày' timeFormat='HH:mm' timeIntervals={5} showTimeSelect />
                                                </FormControl>
                                            </Grid>
                                        </Grid>
                                        <Grid container spacing={6} item>
                                            <Grid xs={12} item>
                                                <FormControl fullWidth>
                                                    <TextInputField required control={form.control} name='name' label='Link liên kết' />
                                                </FormControl>
                                            </Grid>
                                        </Grid>
                                    </>
                                }

                            </CardContent>
                        </Card>

                    </Grid>
                    <Grid item container gap={3} xs={12} sx={{ textAlign: 'center', justifyContent: 'center' }}>
                        <Button size='large' onClick={() => setOpenTableModal(true)} type='button' variant='contained'>
                            Xem danh sách khách hàng
                        </Button>
                        <Button size='large' onClick={() => setFormAction(IFormActionType.FORM_SEND)} type='submit' variant='contained'>
                            Gửi
                        </Button>
                        <Button onClick={() => setFormAction(IFormActionType.FORM_SAVE)} size='large' type='submit' variant='contained'  >
                            Lưu
                        </Button>
                        <Button size='large' onClick={() => router.back()} variant='outlined'>
                            Hủy
                        </Button>
                    </Grid>
                </Grid>
            </form >
            <Box>
                <Modal
                    open={openTable}
                    onClose={() => setOpenTableModal(false)}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                    sx={{ zIndex: 9999 }}
                >
                    <Box sx={style}>

                        <TableNotifyUser />

                    </Box>
                </Modal>
            </Box>
        </>
    )
}
