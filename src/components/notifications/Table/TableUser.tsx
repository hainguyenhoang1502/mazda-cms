// ** MUI Imports
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridPaginationModel, GridRenderCellParams, GridSortModel } from '@mui/x-data-grid'

// ** ThirdParty Components

// ** Custom Components

// ** Types Imports

// ** Utils Import
import React from 'react'
import { INotificationActionType } from 'src/context/notifications/Table/type'
import { ListNotifyUserContext } from 'src/context/notifications/TableUser'
import Toolbar from '../Toolbar'
import { genderOptions } from 'src/context/notifications/Form/create'


const TableNotifyUser = () => {
    const { total,
        rows,
        searchValue,
        paginationModel,
        startDate,
        endDate,
        dispatch,
        loading,
        handleClearFilter,
        handleSearch } = React.useContext(ListNotifyUserContext)

    const columns: GridColDef[] = [ 
        {
            flex: 0.125,
            type: 'text',
            minWidth: 150,
            headerName: 'Tên khách hàng ',
            field: 'fullName',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.fullName}
                </Typography>
            )
        },
        {
            flex: 1.2,
            type: 'text',
            minWidth: 150,
            headerName: 'Giới tính ',
            field: 'gender',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', textOverflow: 'ellipsis' }}>
                    {genderOptions.filter(item=>item.value===params.row.gender)[0].label}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Ngày tạo',
            field: 'phoneNumber',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.phoneNumber}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Email',
            field: 'email',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.email }
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Địa chỉ',
            field: 'address',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.address}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 200,
            headerName: 'Phường/Xã',
            field: 'ward',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.ward}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 200,
            headerName: 'Quận/Huyện',
            field: 'district',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.district}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 200,
            headerName: 'Tỉnh/Thành phố',
            field: 'province',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.province}
                </Typography>
            )
        }
    ]

    const handleSortChange = (sort: GridSortModel) => {
        dispatch({ type: INotificationActionType.SET_SORT, payload: sort })
    }
    const handleChangePagination = (pagination: GridPaginationModel) => {
        dispatch({ type: INotificationActionType.SET_PAGINATION_MODEL, payload: pagination })
    }
    
    return (
        <Card>
            <DataGrid
                sx={{ overflow: 'unset' }}
                autoHeight
                pagination
                rows={rows}
                loading={loading}
                rowCount={total}
                columns={columns}
                sortingMode='server'
                paginationMode='server'
                pageSizeOptions={[10, 25, 50]}
                paginationModel={paginationModel}
                onSortModelChange={handleSortChange}
                slots={{ toolbar: Toolbar }}
                onPaginationModelChange={handleChangePagination}
                slotProps={{
                    toolbar: {
                        search: searchValue,
                        handleSearch:handleSearch,
                        startDate: startDate,
                        endDate: endDate,
                        handleChangeStartDate: (date: Date) => dispatch({ type: INotificationActionType.SET_START_DATE, payload: date }),
                        handleChangeEndDate: (date: Date) => dispatch({ type: INotificationActionType.SET_END_DATE, payload: date }),
                        handleClearFilter: handleClearFilter
                    },
                    baseButton: {
                        variant: 'outlined'
                    }
                }}
            />

        </Card>
    )
}

export default TableNotifyUser
