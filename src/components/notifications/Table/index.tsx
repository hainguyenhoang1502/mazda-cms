// ** React Imports
// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridPaginationModel, GridRenderCellParams, GridSortModel } from '@mui/x-data-grid'

// ** ThirdParty Components

// ** Custom Components
import CustomChip from 'src/@core/components/mui/chip'

// ** Types Imports
import { ThemeColor } from 'src/@core/layouts/types'

// ** Utils Import
import NotificationToolbar from 'src/components/Toolbar/NotificationToolbar'
import { Button, Tooltip } from '@mui/material'
import Icon from 'src/@core/components/icon'
import { NotificationsContext } from 'src/context/notifications/Table'
import React from 'react'
import { format } from 'date-fns'
import Link from 'next/link'
import { INotificationActionType } from 'src/context/notifications/Table/type'

interface StatusObj {
    [key: string]: {
        title: string
        color: ThemeColor
    }
}



const statusObj: StatusObj = {
    'SENT': { title: 'Đã gửi', color: 'success' },
    'UNSENT': { title: 'Chưa gửi', color: 'error' },
}

const notifyCate = {
    'SERVICE': 'Dịch vụ',
    'PROMOTION': 'Khuyến mãi'
}
const notifyType = {
    'SENDNOW': 'Gửi ngay',
    'SCHEDULE': 'Schedule'
}


const TableNotifications = () => {
    const { total,
        rows,
        searchValue,
        paginationModel,
        startDate,
        endDate,
        dispatch,
        loading,
        handleClearFilter,
        handleOpenModal,
        idRef,
        handleSearch } = React.useContext(NotificationsContext)

    const columns: GridColDef[] = [ 
        {
            flex: 0.25,
            minWidth: 150,
            field: 'actions',
            sortable: false,
            headerName: 'Hành động',
            renderCell: ({ row }) => {

                return (
                    <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center' }}>
                        {
                            <Tooltip title="Gửi thông báo">
                                <Button disabled={row.status === 'SENT'} sx={{ p: 0, minWidth: "30px" }} onClick={() => {
                                    idRef.current = row.id
                                    handleOpenModal('modal_confirm_send_notify', row.id)
                                }}>
                                    <Icon icon='ion:paper-plane-outline' fontSize={20} color={row.status === 'SENT' ? '' : "#32475CDE"} />
                                </Button >
                            </Tooltip>
                        }
                        {
                            <Tooltip title="Xem chi tiết thông báo">
                                <Link
                                    style={{ display: 'flex', alignContent: 'center' }}
                                    href={`/notifications/${row.id}`}
                                >
                                    <Icon icon='solar:pen-new-round-linear' fontSize={20} color="#32475CDE" />
                                </Link >
                            </Tooltip>
                        }

                        {
                            <Tooltip title="Xóa thông báo">
                                <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                                    onClick={() => {
                                        idRef.current = row.id
                                        handleOpenModal('modal_confirm_delete_notify', row.id)
                                    }}>
                                    <Icon icon='mdi:trash-can-circle-outline' fontSize={20} color="#32475CDE" />
                                </Button>
                            </Tooltip>
                        }
                    </Box>
                )
            }
        },
        {
            flex: 1.2,
            type: 'text',
            minWidth: 250,
            headerName: 'Tiêu đề ',
            field: 'notify_title',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', textOverflow: 'ellipsis' }}>
                    {params.row.title}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Ngày tạo',
            field: 'create_date',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {format(new Date(params.row.createdDate), 'dd/MM/yyyy')}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Ngày gửi',
            field: 'send_date',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {(params.row.timePush || params.row.schedule) && format(new Date(params.row.timePush || params.row.schedule), 'dd/MM/yyyy')}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Loại thông báo',
            field: 'notify_type',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {notifyType[params.row.category]}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 200,
            headerName: 'Danh mục thông báo',
            field: 'notify_cate',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {notifyCate[params.row.notificationTypeId]}
                </Typography>
            )
        },
        {
            flex: 0.175,
            minWidth: 140,
            field: 'status',
            headerName: 'Trạng thái',
            renderCell: (params: GridRenderCellParams) => {
                if (params.row.status) {
                    const status = statusObj[params.row.status]

                    return <CustomChip rounded size='small' skin='light' color={status.color} label={status.title} />
                }

                return <CustomChip rounded size='small' skin='light' color='primary' label='Chưa xử lý' />
            }
        },


    ]

    const handleSortChange = (sort: GridSortModel) => {
        dispatch({ type: INotificationActionType.SET_SORT, payload: sort })
    }
    const handleChangePagination = (pagination: GridPaginationModel) => {
        dispatch({ type: INotificationActionType.SET_PAGINATION_MODEL, payload: pagination })
    }

    return (
        <Card>
            <DataGrid
                sx={{ overflow: 'unset' }}
                autoHeight
                pagination
                rows={rows}
                loading={loading}
                rowCount={total}
                columns={columns}
                sortingMode='server'
                paginationMode='server'
                pageSizeOptions={[10, 25, 50]}
                paginationModel={paginationModel}
                onSortModelChange={handleSortChange}
                slots={{ toolbar: NotificationToolbar }}
                onPaginationModelChange={handleChangePagination}
                slotProps={{
                    toolbar: {
                        search: searchValue,
                        clearSearch: () => handleSearch(''),
                        handleSearch:handleSearch,

                        // onChange: (event: ChangeEvent<HTMLInputElement>) => handleSearch(event.target.value),
                        startDate: startDate,
                        endDate: endDate,
                        handleChangeStartDate: (date: Date) => dispatch({ type: INotificationActionType.SET_START_DATE, payload: date }),
                        handleChangeEndDate: (date: Date) => dispatch({ type: INotificationActionType.SET_END_DATE, payload: date }),
                        handleClearFilter: handleClearFilter
                    },
                    baseButton: {
                        variant: 'outlined'
                    }
                }}
            />

        </Card>
    )
}

export default TableNotifications
