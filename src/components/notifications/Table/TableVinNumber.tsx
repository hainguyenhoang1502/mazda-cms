import { Grid, Box, TextField, Button } from '@mui/material'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridRenderCellParams } from '@mui/x-data-grid'
import React, { Ref } from 'react'
import Icon from 'src/@core/components/icon'

interface DefaultToolBarProps {
    search: Ref<any>
    handleSearch: () => void
    onChange: (e: React.ChangeEvent) => void
    handleDelete: () => void
    deleteArr: any[]
    setDeleteArr: (newvalue) => void
}

const TableVinNumber = ({
    rows,
    searchValue,
    loading,
    handleSearch,
    handleDelete,
    deleteArr,
    setDeleteArr
}) => {
    console.log(rows)
    const columns: GridColDef[] = [
        {
            flex: 0.125,
            type: 'text',
            minWidth: 150,
            headerName: 'VinNumber',
            field: 'vinNumber',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {params.row.VinNumber}
                </Typography>
            )
        }
    ]

    return (
        <Card>
            <DataGrid
                sx={{ overflow: 'unset', height: '500px', overflowY: 'auto' }}
                rows={rows}
                loading={loading}
                columns={columns}
                checkboxSelection
                slots={{ toolbar: Toolbar }}
                onRowSelectionModelChange={(newRowSelectionModel) => {
                    setDeleteArr(newRowSelectionModel);
                }}
                rowSelectionModel={deleteArr}
                slotProps={{
                    toolbar: {
                        search: searchValue,
                        handleSearch: handleSearch,
                        handleDelete: handleDelete,
                    },
                    baseButton: {
                        variant: 'outlined'
                    }
                }}
            />

        </Card>
    )
}



function Toolbar(props: DefaultToolBarProps) {

    return (

        <Grid container spacing={2} sx={{ paddingX: 5 }} justifyContent={'center'}>
            <Grid item lg={12}>
                <Box
                    sx={{
                        gap: 2,
                        display: 'flex',
                        flexWrap: 'no-wrap',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                    }}
                >
                    <Grid container spacing={2}>
                        <Grid item >
                            <TextField
                                size='small'

                                // value={props.search}
                                // onChange={props.onChange}
                                placeholder='Tìm kiếm...'
                                inputRef={props.search}
                                onKeyDown={(e) => {
                                    if (e.keyCode == 13) {
                                        props.handleSearch()
                                    }
                                }}
                                InputProps={{
                                    startAdornment: (
                                        <Box sx={{ mr: 2, display: 'flex' }}>
                                            <Icon icon='bx:search' fontSize={20} />
                                        </Box>
                                    )
                                }}
                                sx={{
                                    width: {
                                        xs: 1,
                                        sm: 'auto'
                                    },
                                    '& .MuiInputBase-root > svg': {
                                        mr: 2
                                    },
                                    '& .MuiInput-underline:before': {
                                        borderBottom: 1,
                                        borderColor: 'divider'
                                    }
                                }}
                            />
                        </Grid>
                        <Grid item>
                            <Button variant='outlined' color='primary' size='medium' onClick={props.handleSearch}>
                                Lọc
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button variant='contained' color='error' size='medium' onClick={props.handleDelete}>
                                Xóa
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            </Grid>

        </Grid>
    )
}

export default TableVinNumber
