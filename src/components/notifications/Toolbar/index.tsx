import React, { Ref } from 'react'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import Icon from 'src/@core/components/icon'
import { SingleDatePicker } from 'src/components/Common/DatePicker'
import { ReactDatePickerProps } from 'react-datepicker'
import { useTheme } from '@mui/material/styles'
import Button from '@mui/material/Button'
import { Grid } from '@mui/material'
import { ListNotifyUserContext } from 'src/context/notifications/TableUser'

interface DefaultToolBarProps {
    search: Ref<any>
    handleSearch: () => void
    onChange: (e: React.ChangeEvent) => void
    startDate: Date
    endDate: Date
    handleChangeStartDate: (date: Date) => void
    handleChangeEndDate: (date: Date) => void
    handleClearFilter: () => void
}

export default function Toolbar(props: DefaultToolBarProps) {
    const theme = useTheme()
    const { direction } = theme
    const popperPlacement: ReactDatePickerProps['popperPlacement'] = direction === 'ltr' ? 'bottom-start' : 'bottom-end'
    const { total } = React.useContext(ListNotifyUserContext)

    return (

        <Grid container spacing={2} sx={{ paddingX: 10 }} justifyContent={'center'}>
            <Grid item lg={12}>
                <Box
                    sx={{
                        gap: 2,
                        display: 'flex',
                        flexWrap: 'no-wrap',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                    }}
                >
                    <Grid container spacing={2}>
                        <Grid item xs={2}>
                            <TextField
                                size='small'

                                // value={props.search}
                                // onChange={props.onChange}
                                placeholder='Tìm kiếm...'
                                inputRef={props.search}
                                onKeyDown={(e) => {
                                    if (e.keyCode == 13) {
                                        props.handleSearch()
                                    }
                                }}
                                InputProps={{
                                    startAdornment: (
                                        <Box sx={{ mr: 2, display: 'flex' }}>
                                            <Icon icon='bx:search' fontSize={20} />
                                        </Box>
                                    )
                                }}
                                sx={{
                                    width: {
                                        xs: 1,
                                        sm: 'auto'
                                    },
                                    '& .MuiInputBase-root > svg': {
                                        mr: 2
                                    },
                                    '& .MuiInput-underline:before': {
                                        borderBottom: 1,
                                        borderColor: 'divider'
                                    }
                                }}
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <SingleDatePicker
                                popperPlacement={popperPlacement}
                                date={props.startDate}
                                handleChange={props.handleChangeStartDate}
                                label='Ngày bắt đầu'
                                onChange={props.handleChangeStartDate}
                                maxDate={new Date()}
                                dateFormat='dd-MM-yyyy'
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <SingleDatePicker
                                popperPlacement={popperPlacement}
                                date={props.endDate}
                                handleChange={props.handleChangeEndDate}
                                label='Ngày kết thúc'
                                onChange={props.handleChangeEndDate}
                                minDate={new Date(props.startDate)}
                                maxDate={new Date()}
                                dateFormat='dd-MM-yyyy'
                            />
                        </Grid>
                        <Grid item>
                            <Button variant='outlined' color='primary' size='medium' onClick={props.handleSearch}>
                                Lọc
                            </Button>
                        </Grid>
                        <Grid item>

                            <Button variant='outlined' color='error' size='medium' onClick={props.handleClearFilter}>
                                Xóa bộ lọc
                            </Button>
                            <Button sx={{ margin: '0 10px' }} variant='outlined'>
                                Tổng số lượng: {total? total:0}
                            </Button>
                        </Grid>

                    </Grid>
                </Box>
            </Grid>

        </Grid>
    )
}
