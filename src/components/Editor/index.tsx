
import { EditorState } from 'draft-js'
import toast from 'react-hot-toast'

import ReactDraftWysiwyg from 'src/@core/components/react-draft-wysiwyg'
import { ApiUploadFileImage } from 'src/api/refData'
import { ERROR_VALUE_INPUT_IMAGE, MAX_SIZE_IMAGE } from 'src/configs/initValueConfig'

const style = { minHeight: '200px', padding: '0 10px', border: '1px solid rgb(207 207 207 / 87%)' }

const EditorControlled = (props: { value: EditorState, setValue: (state: EditorState) => void, placeholder?: string }) => {

  const uploadCallback = (file) => {

    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onloadend = async () => {
        const data = new FormData();
        data.append("file", file);

        if (file) {
          if (file.size > MAX_SIZE_IMAGE) {
            toast.error(ERROR_VALUE_INPUT_IMAGE)
          } else {
            const res = await ApiUploadFileImage(data)
            if (res.code === 200 && res.result) {
              resolve({ data: { link: res.data.serverName + "/" + res.data.filePath } });
            } else {
              toast.error("Đăng tải ảnh thất bại. Vui lòng thử lại")
            }
          }
        }
      };
      reader.readAsDataURL(file);
      reader.onerror = (error) => {
        reject(error);
      };
    });
  }

  const config = {
    image: { uploadCallback: uploadCallback },
  };

  return <ReactDraftWysiwyg toolbar={config} editorState={props.value} onEditorStateChange={data => props.setValue(data)} editorStyle={style} placeholder={props.placeholder} />
}

export default EditorControlled

