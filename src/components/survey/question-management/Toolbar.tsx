/* eslint-disable */
// ** React Imports
import { ChangeEvent, Ref } from 'react'
import React from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import IconButton from '@mui/material/IconButton'
import { GridToolbarExport, GridToolbarFilterButton } from '@mui/x-data-grid'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import { SingleDatePicker } from 'src/components/Common/DatePicker'
import { ReactDatePickerProps } from 'react-datepicker'
import { useTheme } from '@mui/material/styles'
import Button from '@mui/material/Button'
import { Grid, SelectChangeEvent } from '@mui/material'
import { DropdownDefault } from 'src/components/Common/Dropdown'
import router from 'next/router'

interface DefaultToolBarProps {
    search: Ref<any>;
    onChange: (e: React.ChangeEvent) => void
    statusOpt:any[]
    status:string
    handleChangeStatus: (event: SelectChangeEvent) => void
    handleSearch:()=>void
    handleClear: () => void
}

export default function Toolbar(props: DefaultToolBarProps) {

    return (
        <Grid container spacing={2} sx={{ paddingX: 5 }} justifyContent={'space-between'}>
            <Grid item lg={8} container spacing={1}>
                <Box
                    sx={{
                        gap: 1,
                        display: 'flex',
                        flexWrap: 'no-wrap',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                    }}
                >
                    <TextField
                        size='small'
                        // value={props.search}
                        // onChange={props.onChange}
                        placeholder='Tìm kiếm...'
                        inputRef={props.search}
                        onKeyDown={(e)=>{
                            if(e.keyCode == 13){
                                props.handleSearch()
                             }
                        }}
                        InputProps={{
                            startAdornment: (
                                <Box sx={{ mr: 2, display: 'flex' }}>
                                    <Icon icon='bx:search' fontSize={20} />
                                </Box>
                            )
                        }}
                        sx={{
                            width: {
                                xs: 1,
                                sm: 'auto'
                            },
                            '& .MuiInputBase-root > svg': {
                                mr: 2
                            },
                            '& .MuiInput-underline:before': {
                                borderBottom: 1,
                                borderColor: 'divider'
                            }
                        }}
                    />
                    <DropdownDefault 
                        options={props.statusOpt}
                        value={props.status}
                        label='Trạng thái'
                        handleChange={props.handleChangeStatus} />
                    
                    <Button variant='outlined' size='medium' onClick={props.handleSearch}>
                        Lọc
                    </Button>
                    <Button variant='outlined' size='medium' color="error" onClick={props.handleClear}>
                        Xóa lọc
                    </Button>
                </Box>
            </Grid>
            <Grid item lg={3}>
                <Box
                    sx={{
                        gap: 2,
                        display: 'flex',
                        flexWrap: 'no-wrap',
                        alignItems: 'center',
                        justifyContent: 'end',
                        p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                    }}
                >
                    <Button variant='contained' startIcon={<Icon icon='bx:plus' />} onClick={()=>router.push('/survey/question-management/create')}>
                        Thêm mới
                    </Button>
                </Box>
            </Grid>
        </Grid>
    )
}
/* eslint-enable */