/* eslint-disable no-undef */

// ** React Imports

import React from 'react'

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import { SelectField, TextInputField } from 'src/components/Common/Input'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import router from 'next/router'
import { CreateContext } from 'src/context/survey/question-management/create'
import { questionType } from 'src/context/survey/question-management'
import ReactDraftWysiwygField from 'src/components/Common/Input/ReactDraftWysiwygField'
import { Box, IconButton, Typography } from '@mui/material'
import Icon from 'src/@core/components/icon'
import { statusOptions } from 'src/utils'
import RadioField from 'src/components/Common/Input/RadioField'
import { questionsTypeOptions } from 'src/hooks/useQuestionType'


// Component form
export default function FormCreate() {
    const { form, handleConfirm, handleAddAnswer, handleRemoveAnswer } = React.useContext(CreateContext)

    const handleRenderQuestion = (type: number) => {
        switch (type) {
            case 1:
            case 2:
                return (<Box>
                    {form.getValues('answerCodes').map((item, index) => {
                        return (
                            <Grid key={index} container xs={12} alignItems={'center'} justifyContent={'space-between'} sx={{ marginTop: '20px' }}>
                                <Grid item xs={11}>
                                    <ReactDraftWysiwygField height='50' control={form.control} name={`answerCodes[${index}]`} label='' />
                                </Grid>
                                <Grid item xs={1}>
                                    <IconButton onClick={() => handleRemoveAnswer(index)}>
                                        <Icon icon='fluent-mdl2:cancel' fontSize={30} />
                                    </IconButton >
                                </Grid>
                            </Grid>)
                    })}
                    <FormControl fullWidth>
                        <Grid container xs={12} alignItems={'center'}>
                            <Grid item xs={12}>
                                <ReactDraftWysiwygField required height='50' control={form.control} name='answerContent' label='' />
                            </Grid>
                        </Grid>
                    </FormControl>

                    <FormControl fullWidth>
                        <Button sx={{ marginY: '10px' }} size='large' color='primary' onClick={handleAddAnswer} variant='outlined'  >
                            +  Thêm câu trả lời
                        </Button>
                    </FormControl>
                </Box>)
            case 4:
                return (<Box>
                    <Grid container xs={12}>
                        <Grid container flexDirection={'column'} spacing={6} item xs={5}>
                            <Grid item>
                                <FormControl fullWidth>
                                    <TextInputField required control={form.control} name='voteLowest' label='Giá trị thấp nhất' />
                                </FormControl>
                            </Grid>
                            <Grid item>
                                <FormControl fullWidth>
                                    <TextInputField required control={form.control} name='voteHighest' label='Giá trị cao nhất' />
                                </FormControl>
                            </Grid>
                        </Grid>
                        <Grid sx={{ border: '1px dashed #696CFF', marginLeft: '20px', paddingRight: '10px' }} item container flexDirection={'column'} alignItems={'center'} xs={6} spacing={3}>
                            <Grid item container justifyContent={'space-between'}>
                                <Grid item>
                                    <Typography>{form.getValues('voteLowest')}</Typography>
                                </Grid>
                                <Grid item>
                                    <Typography>{form.getValues('voteHighest')}</Typography>
                                </Grid>
                            </Grid>
                            <Grid item container spacing={3} justifyContent={'space-evenly'}>
                                {
                                    Array.from({ length: 5 }, (_, index) => {
                                        return (
                                            <Grid key={index} item>
                                                <Icon fontSize={60} icon="iwwa:star-o" />
                                            </Grid>
                                        )
                                    })
                                }
                            </Grid>

                        </Grid>
                    </Grid>
                </Box>)
        }
    }

    return (
        <form onSubmit={form.handleSubmit(handleConfirm)}>
            <h3></h3>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                        <CardContent>
                            <Grid container spacing={10} justifyContent='center'>
                                <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='left'>
                                    <FormControl fullWidth>
                                        {/* <ReactDraftWysiwygField required height='50' control={form.control} name='name' label='Tên câu hỏi' /> */}
                                        <TextInputField required control={form.control} name='name' label='Câu hỏi' />
                                    </FormControl>
                                </Grid>
                                <Grid item container spacing={6} xs={10}>
                                    <Grid container spacing={6} flexDirection={'column'} item xs={6}>
                                        <Grid item>
                                            <FormControl fullWidth>
                                                <SelectField required control={form.control} name='type' label='Loại câu hỏi' options={questionType} />
                                            </FormControl>
                                        </Grid>
                                        <Grid item>
                                            <FormControl fullWidth>
                                                <SelectField required control={form.control} name='status' label='Trạng thái *' options={statusOptions.filter(item => item.value !== '')} />
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={6} flexDirection={'column'} item xs={6}>
                                        <Grid item>
                                            <FormControl fullWidth>
                                                <Grid item container>
                                                    {questionsTypeOptions.map((item, index) => {
                                                        return (
                                                            <Grid key={index} item>
                                                                <RadioField control={form.control} name='required' radioValue={item.value} label={item.label} />
                                                            </Grid>
                                                        )
                                                    })}
                                                </Grid>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                {form.getValues('type') !== null && form.getValues('type') !== 3 &&
                                    <Grid item xs={10} sx={{ marginTop: 6 }} textAlign='left'>
                                        <Typography sx={{ marginY: '10px' }}>Danh sách câu trả lời</Typography>
                                        {handleRenderQuestion(form.getValues('type'))}

                                    </Grid>
                                }

                                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                                    <Button sx={{ marginX: '10px' }} size='large' type='submit' variant='contained'  >
                                        Lưu
                                    </Button>
                                    <Button size='large' color='error' onClick={() => router.back()} variant='outlined'>
                                        Hủy
                                    </Button>
                                </Grid>
                            </Grid>

                        </CardContent>
                    </Card>

                </Grid>

            </Grid>
        </form >
    )
}
