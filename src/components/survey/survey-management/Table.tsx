
// ** React Imports
// import { ChangeEvent } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridPaginationModel, GridRenderCellParams, GridSortModel } from '@mui/x-data-grid'

// ** ThirdParty Components

// ** Custom Components
import CustomChip from 'src/@core/components/mui/chip'

// ** Types Imports
import { ThemeColor } from 'src/@core/layouts/types'
import { Button, SelectChangeEvent, Tooltip } from '@mui/material'
import Icon from 'src/@core/components/icon'
import React from 'react'
import Link from 'next/link'
import Toolbar from './Toolbar'
import { IActionType } from 'src/context/survey/survey-management/types'
import { statusOptions } from 'src/utils'
import { Context } from 'src/context/survey/survey-management'
import { format } from 'date-fns'

interface StatusObj {
    [key: string]: {
        title: string
        color: ThemeColor
    }
}



const statusObj: StatusObj = {
    'ACTIVE': { title: 'Đang hoạt động', color: 'success' },
    'INACTIVE': { title: 'Ngưng hoạt động', color: 'error' },
}


const Table = () => {
    const {
        total,
        rows,
        searchValue,
        paginationModel,
        loading,
        filter,
        dispatch,
        handleClearFilter,
        handleSearch,
        handleOpenModal,
        idRef } = React.useContext(Context)

    const columns: GridColDef[] = [
        {
            flex: 0.25,
            minWidth: 150,
            field: 'actions',
            sortable: false,
            headerName: 'Hành động',
            renderCell: ({ row }) => {

                return (
                    <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center' }}>
                        {
                            <Tooltip title="Xem chi tiết">
                                <Link
                                    style={{ display: 'flex', alignContent: 'center' }}
                                    href={`/survey/survey-management/${row.id}`}
                                >
                                    <Icon icon='solar:pen-new-round-linear' fontSize={20} color="#32475CDE" />
                                </Link >
                            </Tooltip>
                        }
                        {
                            <Tooltip title="Xóa">
                                <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                                    onClick={() => {
                                        idRef.current = row.id
                                        handleOpenModal('modal_confirm_delete_survey-management', row.id)
                                    }}>
                                    <Icon icon='mdi:trash-can-circle-outline' fontSize={20} color="#32475CDE" />
                                </Button>
                            </Tooltip>
                        }
                    </Box>
                )
            }
        },
        {
            flex: 0.125,
            type: 'text',
            minWidth: 300,
            headerName: 'Tiêu đề',
            field: 'title',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    <div dangerouslySetInnerHTML={{__html:params.row.title}}></div>
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Ngày bắt đầu',
            field: 'startDate',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {format(new Date(params.row.startDate), 'dd/MM/yyyy')}
                </Typography>
            )
        },
        {
            flex: 0.75,
            type: 'text',
            minWidth: 150,
            headerName: 'Ngày kết thúc',
            field: 'endDate',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary' }}>
                    {format(new Date(params.row.endDate), 'dd/MM/yyyy')}
                </Typography>
            )
        },
        {
            flex: 0.125,
            type: 'text',
            minWidth: 150,
            headerName: 'Số lượng câu hỏi',
            field: 'type',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.questionNumber}
                </Typography>
            )
        },

        {
            flex: 0.175,
            minWidth: 200,
            field: 'status',
            headerName: 'Trạng thái',
            renderCell: (params: GridRenderCellParams) => {
                if (params.row.status) {
                    const status = statusObj[params.row.status]

                    return <CustomChip rounded size='small' skin='light' color={status.color} label={status.title} />
                }

                return <CustomChip rounded size='small' skin='light' color='primary' label='Chưa xử lý' />
            }
        },
    ]

    const handleSortChange = (sort: GridSortModel) => {
        dispatch({ type: IActionType.SET_SORT, payload: sort })
    }
    const handleChangePagination = (pagination: GridPaginationModel) => {
        dispatch({ type: IActionType.SET_PAGINATION_MODEL, payload: pagination })
    }

    return (
        <Card>
            <DataGrid
                sx={{ overflow: 'unset' }}
                autoHeight
                pagination
                rows={rows}
                loading={loading}
                rowCount={total}
                columns={columns}
                sortingMode='server'
                paginationMode='server'
                pageSizeOptions={[10, 25, 50]}
                paginationModel={paginationModel}
                onSortModelChange={handleSortChange}
                slots={{ toolbar: Toolbar }}
                onPaginationModelChange={handleChangePagination}
                slotProps={{
                    toolbar: {
                        search: searchValue,
                        clearSearch: () => handleSearch(''),

                        // onChange: (event: ChangeEvent<HTMLInputElement>) => handleSearch(event.target.value),
                        status:filter.status,
                        handleSearch:handleSearch,
                        statusOpt:statusOptions,
                        handleChangeStatus: (event: SelectChangeEvent) => dispatch({ type: IActionType.SET_FILTER_STATUS, payload: event.target.value as string }),
                        handleClearFilter: handleClearFilter,
                        startDate: filter.startDate,
                        endDate: filter.endDate,
                        handleChangeStartDate: (date: Date) => dispatch({ type: IActionType.SET_FILTER_START_DATE, payload: date }),
                        handleChangeEndDate: (date: Date) => dispatch({ type: IActionType.SET_FILTER_END_DATE, payload: date }),
                    },
                    baseButton: {
                        variant: 'outlined'
                    }
                }}
            />

        </Card>
    )
}

export default Table
/* eslint-enable */