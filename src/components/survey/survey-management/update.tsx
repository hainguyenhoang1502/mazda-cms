import React from 'react'

//** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import { SelectField, TextInputField } from 'src/components/Common/Input'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import router from 'next/router'
import { CardHeader } from '@mui/material'
import { statusOptions } from 'src/utils'
import DatePickerField from 'src/components/Common/Input/DatePickerField'
import { UpdateContext } from 'src/context/survey/survey-management/update'
import Table from './TableQuestion'
import ModalTable from './TableQuestionModal'
import { FormContext } from 'src/context/survey/survey-management/formCtx'


// Component form
export default function UpdateCreate() {
    const { form, handleConfirm } = React.useContext(UpdateContext)
    const { setOpenModalTable } = React.useContext(FormContext)

    return (
        <form onSubmit={form.handleSubmit(handleConfirm)}>
            <h3></h3>
            <Grid container spacing={3}>
                <Grid container spacing={3} item xs={12}>
                    <Grid item xs={6}>
                        <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                            <CardHeader title='Thông tin khảo sát' />
                            <CardContent>
                                <Grid container spacing={12} justifyContent='center'>
                                    <Grid item xs={12} sx={{ marginTop: 6 }} textAlign='left'>
                                        <FormControl fullWidth>
                                            <TextInputField required control={form.control} name='title' label='Tiêu đề khảo sát' />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={6} sx={{ marginTop: 6 }} textAlign='right'>
                                        <FormControl fullWidth>
                                            <DatePickerField required dateFormat="dd-MM-yyyy" name='startDate' label='Thời gian bắt đầu' control={form.control} variant='outlined' />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={6} sx={{ marginTop: 6 }} textAlign='right'>
                                        <FormControl fullWidth>
                                            <DatePickerField required dateFormat="dd-MM-yyyy" name='endDate' label='Thời gian kết thúc' control={form.control} variant='outlined' />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} sx={{ marginTop: 6 }} textAlign='right'>
                                        <FormControl fullWidth>
                                            <SelectField required control={form.control} name='status' label='Trạng thái' options={statusOptions.filter(item => item.value !== '')} />
                                        </FormControl>
                                    </Grid>

                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={6}>
                        <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                            <CardContent>
                                <Button sx={{ marginX: '10px' }} size='large' variant='contained' onClick={() => setOpenModalTable(true)} >
                                    Thêm câu hỏi
                                </Button>

                            </CardContent>
                        </Card>
                        <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                            {/* <CardHeader title='Thông tin khảo sát' /> */}
                            <CardContent>
                                <FormControl fullWidth>
                                    <Table />
                                </FormControl>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid >
                <Grid container spacing={3} >
                    <Grid item xs={12}>
                        <Card sx={{ marginY: '20px', overflow: 'unset' }}>
                            <CardContent>
                                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                                    <Button sx={{ marginX: '10px' }} size='large' type='submit' variant='contained'  >
                                        Lưu
                                    </Button>
                                    <Button size='large' color='error' onClick={() => router.back()} variant='outlined'>
                                        Hủy
                                    </Button>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <ModalTable />
            </Grid>
        </form >
    )
}
