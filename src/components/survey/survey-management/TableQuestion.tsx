// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridPaginationModel, GridRenderCellParams, GridSortModel } from '@mui/x-data-grid'

// ** Utils Import
import { Button, Tooltip } from '@mui/material'
import Icon from 'src/@core/components/icon'
import React from 'react'
import { questionType } from 'src/context/survey/question-management'
import { IActionType } from 'src/context/survey/question-management/types'
import { TableQuestionCtx } from 'src/context/survey/survey-management/tableQuestion'






const Table = () => {
    const {
        idRef,
        paginationModel,
        loading,
        dispatch,
        handleDelete,
        tableQuestion } = React.useContext(TableQuestionCtx)



    const columns: GridColDef[] = [
        {
            flex: 0.25,
            minWidth: 150,
            field: 'actions',
            sortable: false,
            headerName: 'Hành động',
            renderCell: ({ row }) => {

                return (
                    <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center' }}>
                        {
                            <Tooltip title="Xóa">
                                <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                                    onClick={() => {
                                        idRef.current=row.id
                                        handleDelete(row.id)
                                    }}>
                                    <Icon icon='mdi:trash-can-circle-outline' fontSize={20} color="#32475CDE" />
                                </Button>
                            </Tooltip>
                        }
                    </Box>
                )
            }
        },
        {
            flex: 0.125,
            type: 'text',
            minWidth: 300,
            headerName: 'Câu hỏi',
            field: 'name',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    <div dangerouslySetInnerHTML={{ __html: params.row.name }}></div>
                </Typography>
            )
        },
        {
            flex: 0.125,
            type: 'text',
            minWidth: 150,
            headerName: 'Loại câu hỏi',
            field: 'type',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {questionType.filter(item => item.value === params.row.type)[0].label}
                </Typography>
            )
        },

        {
            flex: 0.5,
            type: 'text',
            minWidth: 150,
            headerName: 'Danh sách câu trả lời',
            field: 'answerNumber',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.answerNumber}
                </Typography>
            )
        }
    ]

    const handleSortChange = (sort: GridSortModel) => {
        dispatch({ type: IActionType.SET_SORT, payload: sort })
    }
    const handleChangePagination = (pagination: GridPaginationModel) => {
        dispatch({ type: IActionType.SET_PAGINATION_MODEL, payload: pagination })
    }

    return (
        <Card>
            <DataGrid
                sx={{ overflow: 'unset', height:500 }}
                rows={tableQuestion}
                loading={loading}
                autoPageSize
                columns={columns}
                sortingMode='server'
                paginationModel={paginationModel}
                onSortModelChange={handleSortChange}
                onPaginationModelChange={handleChangePagination}
                
            />

        </Card>
    )
}

export default Table