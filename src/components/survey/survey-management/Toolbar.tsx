// ** React Imports
import React, { Ref } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import { SingleDatePicker } from 'src/components/Common/DatePicker'
import { ReactDatePickerProps } from 'react-datepicker'
import { useTheme } from '@mui/material/styles'
import Button from '@mui/material/Button'
import { Grid, SelectChangeEvent } from '@mui/material'
import { DropdownDefault } from 'src/components/Common/Dropdown'
import router from 'next/router'

interface DefaultToolBarProps {
    search: Ref<any>;
    clearSearch: () => void
    onChange: (e: React.ChangeEvent) => void
    statusOpt: any[]
    status: string
    startDate: Date
    endDate: Date
    handleSearch: () => void
    handleChangeStatus: (event: SelectChangeEvent) => void
    handleChangeStartDate: (date: Date) => void
    handleChangeEndDate: (date: Date) => void
    handleClearFilter: () => void
}

export default function Toolbar(props: DefaultToolBarProps) {
    const theme = useTheme()
    const { direction } = theme
    const popperPlacement: ReactDatePickerProps['popperPlacement'] = direction === 'ltr' ? 'bottom-start' : 'bottom-end'

    return (
        <Grid container spacing={2} sx={{ paddingX: 5 }} justifyContent={'space-between'}>
            <Grid item lg={12}>
                <Box
                    sx={{
                        gap: 2,
                        display: 'flex',
                        flexWrap: 'wrap',
                        alignItems: 'center',
                        p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                    }}
                >
                    <TextField
                        size='small'
                        placeholder='Tìm kiếm...'
                        inputRef={props.search}
                        onKeyDown={(e) => {
                            if (e.keyCode == 13) {
                                props.handleSearch()
                            }
                        }}
                        InputProps={{
                            startAdornment: (
                                <Box sx={{ mr: 2, display: 'flex' }}>
                                    <Icon icon='bx:search' fontSize={20} />
                                </Box>
                            )
                        }}
                        sx={{
                            width: {
                                xs: 1,
                                sm: 'auto'
                            },
                            '& .MuiInputBase-root > svg': {
                                mr: 2
                            },
                            '& .MuiInput-underline:before': {
                                borderBottom: 1,
                                borderColor: 'divider'
                            }
                        }}
                    />
                    <SingleDatePicker
                        popperPlacement={popperPlacement}
                        date={props.startDate}
                        handleChange={props.handleChangeStartDate}
                        label='Ngày bắt đầu'
                        onChange={props.handleChangeStartDate}
                        dateFormat='dd-MM-yyyy'
                    />
                    <SingleDatePicker
                        popperPlacement={popperPlacement}
                        date={props.endDate}
                        handleChange={props.handleChangeEndDate}
                        label='Ngày kết thúc'
                        onChange={props.handleChangeEndDate}
                        minDate={new Date(props.startDate)}
                        dateFormat='dd-MM-yyyy'
                    />
                    <DropdownDefault
                        options={props.statusOpt}
                        value={props.status}
                        label='Trạng thái'
                        handleChange={props.handleChangeStatus} />

                    <Button variant='outlined' size='medium' onClick={props.handleSearch}>
                        Lọc
                    </Button>
                    <Button variant='outlined' size='medium' color='error' onClick={props.handleClearFilter}>
                        Xóa bộ lọc
                    </Button>
                    <Button variant='contained' startIcon={<Icon icon='bx:plus' />} onClick={() => router.push('/survey/survey-management/create')}>
                        Tạo khảo sát
                    </Button>
                </Box>
                <Box
                    sx={{
                        gap: 2,
                        display: 'flex',
                        flexWrap: 'no-wrap',
                        alignItems: 'center',
                        justifyContent: 'end',
                        p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                    }}
                >

                </Box>
            </Grid>
        </Grid>
    )
}
/* eslint-enable */