import { ChangeEvent } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { DataGrid, GridColDef, GridPaginationModel, GridRenderCellParams, GridSortModel } from '@mui/x-data-grid'

// ** Utils Import
import { Button, Grid, Modal, SelectChangeEvent } from '@mui/material'
import React from 'react'
import { questionType } from 'src/context/survey/question-management'
import { statusOptions } from 'src/hooks/useStatusOpt'
import { IActionType } from 'src/context/survey/question-management/types'
import { TableQuestionModalCtx } from 'src/context/survey/survey-management/tableQuestionModal'
import Toolbar from './TableQuestionToolbar'

const ModalTable = () => {
    const {
        total,
        rows,
        searchValue,
        paginationModel,
        loading,
        filter,
        dispatch,
        handleClearFilter,
        handleSearch,
        questionsSelected,
        setQuestionSelected,
        handleAddQuestion,
        setOpenModalTable,
        openModalTable } = React.useContext(TableQuestionModalCtx)


    const columns: GridColDef[] = [
        {
            flex: 0.125,
            type: 'text',
            minWidth: 300,
            headerName: 'Câu hỏi',
            field: 'name',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    <div dangerouslySetInnerHTML={{ __html: params.row.name }}></div>
                </Typography>
            )
        },
        {
            flex: 0.125,
            type: 'text',
            minWidth: 150,
            headerName: 'Loại câu hỏi',
            field: 'type',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {questionType.filter(item => item.value === params.row.type)[0].label}
                </Typography>
            )
        },

        {
            flex: 0.5,
            type: 'text',
            minWidth: 150,
            headerName: 'Danh sách câu trả lời',
            field: 'answerNumber',
            valueGetter: params => new Date(params.value),
            renderCell: (params: GridRenderCellParams) => (
                <Typography variant='body2' sx={{ color: 'text.primary', overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'no-wrap' }}>
                    {params.row.answerNumber}
                </Typography>
            )
        }
    ]

    const handleSortChange = (sort: GridSortModel) => {
        dispatch({ type: IActionType.SET_SORT, payload: sort })
    }
    const handleChangePagination = (pagination: GridPaginationModel) => {
        dispatch({ type: IActionType.SET_PAGINATION_MODEL, payload: pagination })
    }
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '90%',
        height:'fit-content',
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
    };

    return (

        <Modal
            open={openModalTable}
            onClose={()=>setOpenModalTable(false)}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <div style={{ height: 500, width: '100%' }}>
                    <DataGrid
                        sx={{ overflow: 'unset' }}
                        pagination
                        rows={rows}
                        loading={loading}
                        checkboxSelection
                        onRowSelectionModelChange={(newRowSelectionModel) => {
                            setQuestionSelected(Array.from(new Set([...questionsSelected, ...newRowSelectionModel])));
                        }}
                        rowSelectionModel={Array.from(new Set(questionsSelected))}
                        rowCount={total}
                        columns={columns}
                        sortingMode='server'
                        paginationMode='server'
                        pageSizeOptions={[10, 25, 50]}
                        paginationModel={paginationModel}
                        onSortModelChange={handleSortChange}
                        slots={{ toolbar: Toolbar }}
                        onPaginationModelChange={handleChangePagination}
                        slotProps={{
                            toolbar: {
                                search: searchValue,
                                handleSearch: handleSearch,
                                onChange: (event: ChangeEvent<HTMLInputElement>) => handleSearch(event.target.value),
                                status: filter.status,
                                statusOpt: statusOptions,
                                handleChangeStatus: (event: SelectChangeEvent) => dispatch({ type: IActionType.SET_FILTER_STATUS, payload: event.target.value as string }),
                                handleClearFilter: handleClearFilter
                            },
                            baseButton: {
                                variant: 'outlined'
                            }
                        }}
                    />
                </div>

                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                    <Button sx={{ marginX: '10px' }} size='large' onClick={()=>handleAddQuestion()} variant='contained'  >
                        Lưu
                    </Button>
                    <Button size='large' onClick={()=>setOpenModalTable(false)} variant='outlined'>
                        Hủy
                    </Button>
                </Grid>
            </Box>

        </Modal>
    )

}

export default ModalTable