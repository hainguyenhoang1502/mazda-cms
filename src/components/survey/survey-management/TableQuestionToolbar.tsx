// ** React Imports
import React, { Ref } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import { Button, Grid, SelectChangeEvent } from '@mui/material'
import { DropdownDefault } from 'src/components/Common/Dropdown'

interface DefaultToolBarProps {
    search: Ref<any>
    handleSearch: () => void
    onChange: (e: React.ChangeEvent) => void
    statusOpt: any[]
    status: string
    startDate: Date
    endDate: Date
    handleChangeStatus: (event: SelectChangeEvent) => void
    handleChangeStartDate: (date: Date) => void
    handleChangeEndDate: (date: Date) => void
    handleClearFilter: () => void
}

export default function Toolbar(props: DefaultToolBarProps) {

    return (
        <Grid container spacing={2} sx={{ paddingX: 2 }} justifyContent={'flex-start'}>
            <Grid item lg={12}>
                <Box
                    sx={{
                        gap: 2,
                        display: 'flex',
                        flexWrap: 'no-wrap',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                        p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
                    }}
                >
                    <TextField
                        size='small'
                        placeholder='Tìm kiếm...'
                        inputRef={props.search}
                        onKeyDown={(e) => {
                            if (e.keyCode == 13) {
                                props.handleSearch()
                            }
                        }}
                        InputProps={{
                            startAdornment: (
                                <Box sx={{ mr: 2, display: 'flex' }}>
                                    <Icon icon='bx:search' fontSize={20} />
                                </Box>
                            )
                        }}
                        sx={{
                            width: {
                                xs: 1,
                                sm: 'auto'
                            },
                            '& .MuiInputBase-root > svg': {
                                mr: 2
                            },
                            '& .MuiInput-underline:before': {
                                borderBottom: 1,
                                borderColor: 'divider'
                            }
                        }}
                    />
                    <Grid item>
                        <DropdownDefault
                            options={props.statusOpt}
                            value={props.status}
                            label='Trạng thái'
                            handleChange={props.handleChangeStatus} />
                    </Grid>
                    <Button variant='outlined' size='medium' onClick={props.handleSearch}>
                        Lọc
                    </Button>
                    <Button variant='outlined' size='medium' color='error' onClick={props.handleClearFilter}>
                        Xóa bộ lọc
                    </Button>
                </Box>
            </Grid>
        </Grid>
    )
}
/* eslint-enable */