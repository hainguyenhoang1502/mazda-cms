/* eslint-disable */

// ** React Imports

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'

// ** Third Party Imports
import * as yup from 'yup'
import toast from 'react-hot-toast'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

// ** Icon Imports
import { SelectField, TextInputField } from 'src/components/Common/Input'
import DatePickerField from 'src/components/Common/Input/DatePickerField'
import AutocompleteField from 'src/components/Common/Input/AutoCompleteField'

// định nghĩa các trường trong form
type FormValues = {
  firstName: string
  lastName: string
  password: string
  currencies: string
  email: string
  dateOfBirth: Date
}

//định nghĩa các giá trị khởi tạo
const defaultValues = {
  firstName: '',
  lastName: '',
  password: '',
  currencies: '',
  email: '',
  dateOfBirth: new Date()
}

// Show errors
const showErrors = (field: string, valueLen: number, min?: number) => {
  if (valueLen === 0) {
    return `${field} field is required`
  } else if (valueLen > 0 && valueLen < min) {
    return `${field} must be at least ${min} characters`
  } else {
    return ''
  }
}

// Định nghĩa validate
const schema = yup.object().shape({
  email: yup.string().email().required(),
  lastName: yup
    .string()
    .min(3, obj => showErrors('lastName', obj.value.length, obj.min))
    .required(),
  password: yup
    .string()
    .min(8, obj => showErrors('password', obj.value.length, obj.min))
    .required(),
  firstName: yup
    .string()
    .min(3, obj => showErrors('firstName', obj.value.length, obj.min))
    .required(),
  currencies: yup.string().required(obj => showErrors('currencies', obj.value.length)),
  dateOfBirth: yup.date().max(new Date(), 'Date must be in the pass').required('This field is required'),
  provinces: yup
    .array()
    .min(1, 'At least one province is required')
    .of(
      yup.object().shape({
        value: yup.string().required(),
        label: yup.string().required(),
      })
    )
})

// Dummy data option cho select
const currencies = [
  {
    value: 'USD',
    label: '$'
  },
  {
    value: 'EUR',
    label: '€'
  },
  {
    value: 'BTC',
    label: '฿'
  },
  {
    value: 'JPY',
    label: '¥'
  }
]

// Component form
export default function FormDefault() {
  // ** Hook
  const form = useForm<FormValues>({
    defaultValues,
    mode: 'onChange',
    resolver: yupResolver(schema)
  })

  const onSubmit = () => {
    console.log(form.formState.dirtyFields)
    toast.success('Form Submitted')
  }
  console.log(form.watch())
  return (
    <Grid container spacing={3}>
      <Grid item xs={6}>
        <Card>
          <CardHeader title='Validation Schema With OnChange' />
          <CardContent>
            <form onSubmit={form.handleSubmit(onSubmit)}>
              <Grid container spacing={3} justifyContent='space-between'>
                <Grid item xs={6} textAlign='right'>
                  <FormControl fullWidth>
                    <DatePickerField name='dateOfBirth' label='Date of birth' control={form.control} variant='outlined' />
                  </FormControl>
                </Grid>
                <Grid item xs={6}>
                  <FormControl fullWidth>
                    <TextInputField control={form.control} name='firstName' label='First Name' />
                  </FormControl>
                </Grid>
                <Grid item xs={6} textAlign='right'>
                  <FormControl fullWidth>
                    <TextInputField control={form.control} name='lastName' label='Last Name' />
                  </FormControl>
                </Grid>
                <Grid item xs={6} textAlign='right'>
                  <FormControl fullWidth>
                    <TextInputField control={form.control} name='email' label='Email' />
                  </FormControl>
                </Grid>
                <Grid item xs={6} textAlign='right'>
                  <FormControl fullWidth>
                    <SelectField control={form.control} name='currencies' label='Currencies' options={currencies} />
                  </FormControl>
                </Grid>

                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                  <Button size='large' type='submit' variant='contained'>
                    Submit
                  </Button>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card>
          <CardHeader title='Validation Schema With OnChange' />
          <CardContent>
            <form onSubmit={form.handleSubmit(onSubmit)}>
              <Grid container spacing={3} justifyContent='space-between'>
                <Grid item xs={6} textAlign='right'>
                  <FormControl fullWidth>
                    <DatePickerField name='dateOfBirth' label='Date of birth' control={form.control} variant='outlined' />
                  </FormControl>
                </Grid>
                <Grid item xs={6}>
                  <FormControl fullWidth>
                    <TextInputField control={form.control} name='firstName' label='First Name' />
                  </FormControl>
                </Grid>
                <Grid item xs={6} textAlign='right'>
                  <FormControl fullWidth>
                    <TextInputField control={form.control} name='lastName' label='Last Name' />
                  </FormControl>
                </Grid>
                <Grid item xs={12}>
                  <FormControl fullWidth>
                    <AutocompleteField name="carModel" control={form.control} label='Sản phẩm' placeholder='Sản phẩm'  />
                  </FormControl>
                </Grid>
                <Grid item xs={6} textAlign='right'>
                  <FormControl fullWidth>
                    <SelectField control={form.control} name='currencies' label='Currencies' options={currencies} />
                  </FormControl>
                </Grid>

                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                  <Button size='large' type='submit' variant='contained'>
                    Submit
                  </Button>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}
/* eslint-enable */