/* eslint-disable */
// ** React Imports
import { ChangeEvent } from 'react'
import React from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import IconButton from '@mui/material/IconButton'
import { GridToolbarExport, GridToolbarFilterButton } from '@mui/x-data-grid'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import { SingleDatePicker } from 'src/components/Common/DatePicker'
import { ReactDatePickerProps } from 'react-datepicker'
import { useTheme } from '@mui/material/styles'
import Button from '@mui/material/Button'
import { Grid } from '@mui/material'

interface DefaultToolBarProps {
  search: string
  clearSearch: () => void
  onChange: (e: React.ChangeEvent) => void
  startDate: Date
  endDate: Date
  handleChangeStartDate: (date: Date) => void
  handleChangeEndDate: (date: Date) => void
  handleClearFilter: () => void
}

export default function DefaultToolbar(props: DefaultToolBarProps) {
  const theme = useTheme()
  const { direction } = theme
  const popperPlacement: ReactDatePickerProps['popperPlacement'] = direction === 'ltr' ? 'bottom-start' : 'bottom-end'
  
return (
    <Grid container spacing={2} sx={{ paddingX: 2 }}>
      <Grid item lg={7}>
        <Box
          sx={{
            gap: 2,
            display: 'flex',
            flexWrap: 'no-wrap',
            alignItems: 'center',
            justifyContent: 'space-between',
            p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
          }}
        >
          <TextField
            size='small'
            value={props.search}
            onChange={props.onChange}
            placeholder='Search…'
            InputProps={{
              startAdornment: (
                <Box sx={{ mr: 2, display: 'flex' }}>
                  <Icon icon='bx:search' fontSize={20} />
                </Box>
              )
            }}
            sx={{
              width: {
                xs: 1,
                sm: 'auto'
              },
              '& .MuiInputBase-root > svg': {
                mr: 2
              },
              '& .MuiInput-underline:before': {
                borderBottom: 1,
                borderColor: 'divider'
              }
            }}
          />
          <SingleDatePicker
            popperPlacement={popperPlacement}
            date={props.startDate}
            handleChange={props.handleChangeStartDate}
            label='Ngày bắt đầu'
            onChange={props.handleChangeStartDate}
          />
          <SingleDatePicker
            popperPlacement={popperPlacement}
            date={props.endDate}
            handleChange={props.handleChangeEndDate}
            label='Ngày kết thúc'
            onChange={props.handleChangeEndDate}
          />
          <Button variant='outlined' size='medium' onClick={props.handleClearFilter}>
            Lọc
          </Button>
        </Box>
      </Grid>
      <Grid item lg={5}>
        <Box
          sx={{
            gap: 2,
            display: 'flex',
            flexWrap: 'no-wrap',
            alignItems: 'center',
            justifyContent: 'end',
            p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
          }}
        >
          <Button variant='contained' startIcon={<Icon icon='bx:plus' />}>
            Thêm khách hàng
          </Button>
          <Button variant='contained' color='error'>
            Xóa
          </Button>
          <GridToolbarExport printOptions={{ disableToolbarButton: true }} />
        </Box>
      </Grid>
    </Grid>
  )
}
/* eslint-enable */