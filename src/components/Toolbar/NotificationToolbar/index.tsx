import React, { Ref } from 'react'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import Icon from 'src/@core/components/icon'
import { SingleDatePicker } from 'src/components/Common/DatePicker'
import { ReactDatePickerProps } from 'react-datepicker'
import { useTheme } from '@mui/material/styles'
import Button from '@mui/material/Button'
import { Grid } from '@mui/material'
import router from 'next/router'

interface DefaultToolBarProps {
  search: Ref<any>
  clearSearch: () => void
  onChange: (e: React.ChangeEvent) => void
  startDate: Date
  endDate: Date
  handleSearch: () => void
  handleChangeStartDate: (date: Date) => void
  handleChangeEndDate: (date: Date) => void
  handleClearFilter: () => void
}

export default function NotificationToolbar(props: DefaultToolBarProps) {
  const theme = useTheme()
  const { direction } = theme
  const popperPlacement: ReactDatePickerProps['popperPlacement'] = direction === 'ltr' ? 'bottom-start' : 'bottom-end'

  return (
    <Grid container spacing={2} sx={{ paddingX: 5 }}>
      <Box
        sx={{
          gap: 2,
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          p: theme => `${theme.spacing(6, 0, 4, 0)} !important`
        }}
      >
        <TextField
          size='small'

          // value={props.search}
          // onChange={props.onChange}
          placeholder='Tìm kiếm...'
          inputRef={props.search}
          onKeyDown={(e) => {
            if (e.keyCode == 13) {
              props.handleSearch()
            }
          }}
          InputProps={{
            startAdornment: (
              <Box sx={{ mr: 2, display: 'flex' }}>
                <Icon icon='bx:search' fontSize={20} />
              </Box>
            )
          }}
          sx={{
            width: {
              xs: 1,
              sm: 'auto'
            },
            '& .MuiInputBase-root > svg': {
              mr: 2
            },
            '& .MuiInput-underline:before': {
              borderBottom: 1,
              borderColor: 'divider'
            }
          }}
        />
        <SingleDatePicker
          popperPlacement={popperPlacement}
          date={props.startDate}
          handleChange={props.handleChangeStartDate}
          label='Ngày bắt đầu'
          onChange={props.handleChangeStartDate}
          maxDate={new Date()}
          dateFormat='dd-MM-yyyy'
        />
        <SingleDatePicker
          popperPlacement={popperPlacement}
          date={props.endDate}
          handleChange={props.handleChangeEndDate}
          label='Ngày kết thúc'
          onChange={props.handleChangeEndDate}
          minDate={new Date(props.startDate)}
          maxDate={new Date()}
          dateFormat='dd-MM-yyyy'
        />
        <Button variant='outlined' color='primary' size='medium' onClick={props.handleSearch}>
          Lọc
        </Button>
        <Button variant='outlined' color='error' size='medium' onClick={props.handleClearFilter}>
          Xóa bộ lọc
        </Button>
        <Button variant='contained' startIcon={<Icon icon='bx:plus' />} onClick={() => router.push('/notifications/create')}>
          Thêm thông báo
        </Button>
      </Box>
    </Grid>
  )
}
