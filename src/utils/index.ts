export const debounce = (func, delay) => {
  let timer;

  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func(...args);
    }, delay);
  };
};

export const checkIfArrayValid = (array: any[]) => {
  if (array && array.length > 0) {
    return true;
  }

  return false;
}
export const convertToCurrentTime = (date: Date) => {
  const currentTime = new Date(date); // Current time in the local time zone
  const gmtPlus7Time = new Date(currentTime.getTime() + (7 * 60 * 60 * 1000));

  return gmtPlus7Time
}

export const checkEditor = (field: string, value: string) => {
  const parser = new DOMParser();
  const htmlDoc = parser.parseFromString(value, 'text/html');
  const textString = htmlDoc.body.textContent;
  if (textString.trim() === '' || textString.trim().length > 0) return `Vui lòng nhập thông tin`;

  return '';
}

export const statusOptions=[{ label: 'Tất cả', value: '' }, { label: 'Đang hoạt động', value: 'ACTIVE' }, { label: 'Ngưng hoạt động', value: 'INACTIVE' }]

export const status = [
  { value: 'ACTIVE', label: 'Đang hoạt động' },
  { value: 'INACTIVE', label: 'Ngưng hoạt động' }
]

