// ** Type import
import { VerticalNavItemsType } from 'src/@core/layouts/types'
import { permissonConfig } from 'src/configs/roleConfig'

const navigation = (): VerticalNavItemsType => {
  return [
    
    // {
    //   title: 'Dashboard',
    //   path: '/home',
    //   icon: 'bx:home-circle'
    // },
    {
      title: 'Quản lý Danh mục',
      icon: 'bx:food-menu',
      children: [
        {
          title: 'Thương hiệu',
          path: '/category/brand',
          permisson: permissonConfig.BRAND_LIST_VIEW
        },

        // {
        //   title: 'Dòng xe',
        //   path: '/category/model'
        // },
        {
          title: 'Công ty - Đại lý',
          path: '/category/company',
          permisson: permissonConfig.ORG_LIST_VIEW
        },
        {
          title: 'Vùng miền',
          path: '/category/region',
          permisson: permissonConfig.LOCATION_LIST_VIEW
        },
        {
          title: 'Khu vực',
          path: '/category/area',
          permisson: permissonConfig.LOCATION_LIST_VIEW
        },
        {
          title: 'Tỉnh / Thành',
          path: '/category/province',
          permisson: permissonConfig.LOCATION_LIST_VIEW
        },
        {
          title: 'Quận / Huyện',
          path: '/category/district',
          permisson: permissonConfig.LOCATION_LIST_VIEW
        }
      ]
    },
    {
      title: 'Thông tin khách hàng',
      path: '/customer-information',
      icon: 'bx:user',
      permisson: permissonConfig.CUSTOMER_LIST_VIEW
    },
    {
      title: 'Quản lý Đặt hẹn',
      path: '/appointment-booking',
      icon: 'bx:calendar',
      permisson: permissonConfig.APPOINTMENT_LIST_VIEW
    },

    {
      title: 'Quản lý Khảo sát',
      icon: 'mdi:file-question-outline',
      children: [
        {
          title: 'Danh sách câu hỏi',
          path: '/survey/question-management'
        },
        {
          title: 'Danh sách khảo sát',
          path: '/survey/survey-management'
        },
      ]
    },
    {
      title: 'Quản lý Bảo hành',
      icon: 'icon-park-outline:protect',
      children: [
        {
          title: 'Thông tin bảo hành xe',
          path: '/warranty-management/warranty-information',
          permisson: permissonConfig.WARRANTY_INFO_LIST_VIEW
        },
        {
          title: 'Chính sách bảo hảnh',
          path: '/warranty-management/warranty-policy',
          permisson: permissonConfig.WARRANTY_POLICY_LIST_VIEW
        },

      ]
    },
    {
      title: 'Quản lý Thông báo',
      icon: 'fe:notice-active',
      children: [
        {
          title: 'Danh sách thông báo',
          path: '/notifications',
          permisson: permissonConfig.NOTIFY_LIST_VIEW
        },
        {
          title: 'Danh sách template',
          path: '/notification-template',
          permisson: permissonConfig.NOTIFY_LIST_VIEW
        }

      ]
    },
    {
      title: 'Quản lý Tài liệu',
      icon: 'mdi:file-document-edit-outline',
      children: [
        {
          title: 'Đèn cảnh báo và chỉ báo',
          path: '/documents-management/signal-lights',
          permisson: permissonConfig.DOC_SIGNALLIGHT_LIST_VIEW
        },
        {
          title: 'Hướng dẫn sử dụng ',
          path: '/documents-management/user-manual',
          permisson: permissonConfig.DOC_USERMANUAL_LIST_VIEW
        }

      ]
    },
    {
      title: 'Lịch sử bảo dưỡng',
      icon: 'mdi:briefcase-clock-outline',
      path: '/maintenance',
    },
    {
      title: 'Quản lý xe',
      icon: 'ri:car-fill',
      children: [
        {
          title: 'Dòng xe',
          path: '/car-management/model',
        },
        {
          title: 'Phiên bản xe',
          path: '/car-management/car-version',
        },
        {
          title: 'Màu xe',
          path: '/car-management/car-color',
        },
        {
          title: 'Hình ảnh xe',
          path: '/car-management/car-images-management',
          permisson: permissonConfig.CAR_IMAGE_LIST_VIEW
        },


      ]
    },

    // {
    //   title: 'Quản lý hình ảnh xe',
    //   icon: 'ri:car-fill',
    //   path: '/car-images-management',
    //   permisson: permissonConfig.CAR_IMAGE_LIST_VIEW
    // },
    {
      title: 'Hệ thống',
      icon: 'icon-park-outline:system',
      children: [
        {
          title: 'Tạo tài khoản',
          path: '/system/user/create',
          permisson: permissonConfig.USER_CREATE
        },
        {
          title: 'Danh sách người dùng',
          path: '/system/user',
          permisson: permissonConfig.USER_LIST_VIEW
        },
        {
          title: 'Phân quyền',
          path: '/system/role',
          permisson: permissonConfig.ROLE_VIEW
        },
        {
          title: 'Lệnh sửa chữa',
          path: '/repair-list',
        },
        {
          title: 'Cài đặt',
          path: '/system/setting/mazda',
        },
      ]
    },
  ]
}

export default navigation
