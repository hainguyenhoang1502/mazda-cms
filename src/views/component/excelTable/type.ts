export interface IExcelTableProps {
    data: Array<{ [key: string]: any }>;
    columns: Array<{ [key: string]: any }>;
    success: boolean;
    text: string;
    loading: boolean
}