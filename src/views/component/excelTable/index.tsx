import React, { useEffect } from 'react'
import MaterialTable from 'material-table'
import { tableIcons } from 'src/constants/icons'
import * as XLSX from 'xlsx'
import { Box, Button, CircularProgress, Typography } from '@mui/material';
import IconifyIcon from 'src/@core/components/icon';
import { IExcelTableProps } from './type';
import { useModalFile } from 'src/components/Modal/ModalFile';


const ExcelTable = ({ data, columns, success, text, loading }: IExcelTableProps) => {
    const { handleClose } = useModalFile()
    const downloadExcel = () => {
        if (data?.length > 0) {
            const newData = data.map((row: any) => {
                delete row.tableData //delete tableData column in excel

                return row
            })
            const workSheet = XLSX.utils.json_to_sheet(newData)
            const workBook = XLSX.utils.book_new()
            XLSX.utils.book_append_sheet(workBook, workSheet, 'data')
            XLSX.write(workBook, { bookType: 'xlsx', type: 'binary' })

            //download
            XLSX.writeFile(workBook, 'data.xlsx')
        }
    }


    function excelDateToJSDate(excelDate: number) {
        const daysSinceEpoch = excelDate - 25569; // Đổi từ ngày Excel cố định sang ngày Unix cố định (1/1/1970)
        const millisecondsPerDay = 24 * 60 * 60 * 1000; // Số mili giây trong một ngày
        const milliseconds = daysSinceEpoch * millisecondsPerDay; // Số mili giây tính từ ngày Unix cố định

        return new Date(milliseconds).toLocaleDateString('en-GB')
    }

    useEffect(() => {
        for (let i = 0; i < data?.length; i++) {
            for (const key in data[i]) {
                if (key.toLocaleLowerCase().includes('date')) {
                    data[i][key] = excelDateToJSDate(+data[i][key])
                }
            }
        }
    }, [data])

    if (success) {
        setTimeout(() => {
            handleClose();
        }, 1500)

        return (
            <Box sx={{ height: '300px', width: '50%', border: '1px dashed #333', margin: '50px auto', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', borderRadius: '4px', padding: '8px', gap: '20px' }}>
                <Typography sx={{ fontSize: '30px' }}>Không có lỗi</Typography>
                <IconifyIcon icon='gg:check-o' color="green" fontSize='50px' />
            </Box>
        )
    }

    return (


        <Box sx={{ minHeight: '350px', display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
            {loading
                ? <CircularProgress sx={{ color: '#6B93B0' }} />
                : <MaterialTable icons={tableIcons} title={text} data={data} columns={columns} style={{ width: '100%' }} actions={[{
                    icon: () => <Button variant="outlined" sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 2 }}>
                        <IconifyIcon fontSize={16} icon='material-symbols:download' />
                        Download Table
                    </Button>,
                    tooltip: "Download Excel Table",
                    onClick: () => downloadExcel(),
                    isFreeAction: true,
                }]} />}
        </Box>
    )
}

export default ExcelTable