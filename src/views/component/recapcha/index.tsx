import React, { useEffect, useRef } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';

import { getApiRecaptcha } from "src/api/google"
import { RECAPTCHA_SITE_KEY } from 'src/configs/envConfig';

export default function Recapcha(props) {
  const recapcheRef = useRef<any>();

  useEffect(() => {
    resetReCaptcha()
  }, [])
  useEffect(() => {
    if (props.isReset) {
      resetReCaptcha()
      props.onChange("")    
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.isReset])

  const resetReCaptcha = async () => {
    await recapcheRef.current.reset();
  }
  const handleChange = async (value: string) => {
    const res = await getApiRecaptcha(value);
    if (res && res.success) {
      if (window.location.origin.includes(res.hostname))
        props.onChange(value);
    }
  }
  
  return (
    <ReCAPTCHA
      ref={recapcheRef}
      sitekey={RECAPTCHA_SITE_KEY}
      onChange={handleChange}
    />
  )
}

