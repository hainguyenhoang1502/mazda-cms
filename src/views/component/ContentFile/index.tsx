import { Box, Button, SxProps, Typography } from '@mui/material'
import LoadingButton from '@mui/lab/LoadingButton';
import React, { useState } from 'react'
import { useDropzone } from 'react-dropzone'
import IconifyIcon from 'src/@core/components/icon'
import { handleUploadFile } from 'src/store/utils/uploadFile'
import ExcelTable from '../excelTable';
import { IContentFile, IColumnsRows } from './type';
import { toast } from 'react-hot-toast';
import { useModalFile } from 'src/components/Modal/ModalFile';
import * as XLSX from 'xlsx'

const BoxStyle: SxProps = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    height: '65%',
    overflowY: 'auto',
    transform: 'translate(-50%, -50%)',
    width: '80%',
    bgcolor: 'background.paper',
    borderRadius: '4px',
    boxShadow: 24,
    '&::-webkit-scrollbar': {
        width: 10,
        borderRadius: '4px'
    },
    '&::-webkit-scrollbar-track': {
        background: '#ebebeb',
        borderRadius: '4px'

    },
    '&::-webkit-scrollbar-thumb': {
        background: '#b4b4b4',
        borderRadius: '4px'
    }

}

const EXTENSIONS = ['xlsx', 'xls', 'csv']

const ContentFile = ({ functionGetFile, title, linkDownload }: IContentFile) => {
    const [upload, setUpload] = useState<boolean>(false)
    const [filePath, setFilePath] = useState<string>('')
    const [colums, setColums] = useState<IColumnsRows>()
    const [rows, setRows] = useState<IColumnsRows>()
    const [loading, setLoading] = useState<boolean>(false)
    const [success, setSuccess] = useState<boolean>(false)
    const [disableImport, setDisableImport] = useState<boolean>(true)
    const [text, setText] = useState<string>('Preview')

    const { handleClose } = useModalFile()

    const convertToJson = (headers: Array<any>, data: Array<any>) => {
        const rows: Array<object> = []
        data.forEach((row) => {
            const rowData: { [key: string]: any } = {}

            row.forEach((element: any, index: number) => {
                rowData[headers[index]] = element
            })
            rows.push(rowData);
        })

        return rows
    }

    const handleExcelDrop = async (acceptedFiles: Array<File>) => {
        if (acceptedFiles.length > 0) {
            const name = acceptedFiles[0].name.split('.')
            if (EXTENSIONS.includes(name[name.length - 1])) {
                try {
                    const link = await handleUploadFile(acceptedFiles)
                    setFilePath(link) // filePath để import
                    setUpload(true) // hiện trạng thái đã upload
                    setDisableImport(false) // cho phép ấn nút import
                } catch (error) {
                    console.log('%cerror index.tsx line:78 ', 'color: red; display: block; width: 100%;', error);
                }

            } else {
                handleClose()
                toast.error('Vui lòng upload file excel')
            }


            // preview
            const reader = new FileReader()

            reader.onload = (event: ProgressEvent<FileReader>) => {
                if (event.target?.readyState === FileReader.DONE) {
                    //parse data
                    const bstr = event.target.result
                    const workBook = XLSX.read(bstr, { type: 'binary' })

                    //GET first sheet
                    const workSheetName = workBook.SheetNames[0] //sheet1
                    const workSheet = workBook.Sheets[workSheetName]

                    //convert to array
                    const fileData = XLSX.utils.sheet_to_json(workSheet, { header: 1 }) //array[0] là header

                    const headers = fileData[0] as Array<any>
                    const heads = headers.map((head) => ({ title: head, field: head }))
                    setColums(heads)
                    fileData.splice(0, 1) //bỏ đi array[0]
                    setRows(convertToJson(headers, fileData)) //content
                }
            }
            reader.readAsBinaryString(acceptedFiles[0])
        }

    };

    const { getRootProps, getInputProps } = useDropzone({
        onDrop: handleExcelDrop,
        multiple: false, // Giới hạn chỉ chọn một tệp tin
        maxFiles: 1,
    })

    const handleImport = async () => {
        if (filePath) {
            try {
                setLoading(true)
                const res = await functionGetFile({ filePath })
                if (res.data.length > 0 && res.code === 200 && res.result) {
                    const data = res.data[0]
                    const colums = Object.keys(data).map((item) => ({
                        title: item, field: item, headerStyle: {
                            backgroundColor: '#039be5',
                            color: '#fff',
                            border: '1px solid #fff',
                        }
                    }))
                    setColums(colums)
                    setRows(res.data)
                    setText('Danh sách lỗi')
                } else if (res.code === -1 && !res.result) {
                    handleClose()
                    toast.error(res.message)
                }
                else {
                    setSuccess(true)
                }

            } catch (error) {
                console.log('%error ContentFile.tsx line:45 ', 'color: red; display: block; width: 100%;', error);
            } finally {
                setLoading(false)
                setDisableImport(true)
                setUpload(false)
            }
        }
    }

    return (
        <Box sx={BoxStyle}>
            <Box sx={{ borderBottom: '1px dashed #333', padding: 5, display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Typography component='span'>
                    {title}
                </Typography>

                <Box sx={{ cursor: 'pointer' }} onClick={handleClose}>
                    <IconifyIcon icon='material-symbols:close' />
                </Box>
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: 5 }}>
                <Box sx={{ display: 'flex', gap: 2 }}>
                    <Button sx={{ border: `1px solid ${upload ? 'green' : '#6B93B0'}`, color: `${upload ? 'green' : '#6B93B0'}` }} {...getRootProps()}>
                        <input
                            type='file'
                            id='upload'
                            hidden
                            accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                            {...getInputProps()}
                        />
                        <IconifyIcon icon='material-symbols:upload' color={`${upload ? 'green' : '#6B93B0'}`} />
                        Tải file lên
                    </Button>
                    <LoadingButton loading={loading} sx={{ border: '1px solid #6B93B0', background: '#6B93B0', color: '#fff', '&:hover': { background: '#3d9ce0' } }} variant='contained' onClick={handleImport} disabled={disableImport}>
                        <IconifyIcon icon='clarity:import-line' color='#fff' />
                        Import File
                    </LoadingButton>
                </Box>
                <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', gap: 1 }}>
                    <Typography component='span'>
                        Nếu chưa có file mẫu download
                    </Typography>
                    <Typography component='a' href={linkDownload} download sx={{
                        textDecoration: 'none', color: '#76b4ee', fontWeight: 600, ':hover': {
                            color: '#1f8cf3'
                        }
                    }}>tại đây
                    </Typography>
                </Box>

            </Box>
            <Box>
                <ExcelTable data={rows} columns={colums} success={success} text={text} loading={loading} />
            </Box>
            <Box sx={{ borderTop: '1px solid #c1c1c1', padding: 2, display: 'flex', justifyContent: 'flex-end' }} onClick={handleClose}>
                <Button variant='outlined' sx={{ fontSize: '10px', padding: 2, width: 'max-content', border: `1px solid #6B93B0`, color: '#6B93B0' }}>Thoát
                </Button>
            </Box>
        </Box>
    )
}

export default ContentFile