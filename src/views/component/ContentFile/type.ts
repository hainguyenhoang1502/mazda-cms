export type IColumnsRows = Array<{ [key: string]: any }>
export interface IContentFile {
    functionGetFile: Function,
    title: string,
    linkDownload: string,
};