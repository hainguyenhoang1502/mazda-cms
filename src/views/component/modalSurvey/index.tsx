
import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { Card, Grid } from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  overflow: "auto",
  maxHeight: 550,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};

export interface IModalAlertProps {
  isOpen: boolean;
  toggle: Function;
  actionReturn: Function
  message?: string,
  rows: Array<any>,
  columns: GridColDef[],
}



// const columns: GridColDef[] = [
//   {
//       flex: 0.25,
//       minWidth: 400,
//       field: 'question',
//       sortable: false,
//       headerName: 'Câu hỏi',
//       renderCell: ({ row }) => {
//           return (
//               <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px', flexWrap: "wrap", whiteSpace: 'pre-wrap' }}>
//                   <TypographyDataGrid wrap='wrap'>
//                       {row.question}
//                   </TypographyDataGrid>
//               </Box>
//           )
//       }
//   },
//   {
//       flex: 0.25,
//       minWidth: 150,
//       field: 'questionType',
//       headerName: 'Loại câu hỏi',
//       sortable: false,
//       renderCell: ({ row }) => {
//           return (
//               <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
//                   <TypographyDataGrid noWrap>
//                       {row.questionType}
//                   </TypographyDataGrid>
//               </Box>
//           )
//       }
//   },
//   {
//       flex: 0.5,
//       minWidth: 200,
//       field: 'questionList',
//       sortable: false,
//       headerName: 'danh sách câu trả lời',
//       align: 'left',
//       renderCell: ({ row }: any) => {
//           return (
//               <Box sx={{ display: 'flex', alignItems: 'center' }}>
//                   <TypographyDataGrid
//                       noWrap
//                   >
//                       {row.questionList}
//                   </TypographyDataGrid>
//               </Box>
//           )
//       }
//   },
//   {
//       flex: 0.25,
//       minWidth: 200,
//       field: 'action',
//       headerName: 'hành động',
//       sortable: false,
//       renderCell: () => {
//           return (
//               <Box sx={{ display: 'flex', alignItems: 'center' }}>
//                   {
//                       canDelete &&
//                       <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
//                           onClick={() => handleDelete()}>
//                           <Icon icon='bx:trash' fontSize={20} color="#32475CDE" />
//                       </Button>
//                   }
//               </Box>
//           )
//       }
//   },
// ]

// const rows = [
//   { id: 1, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống ?', questionType: 'Văn bản', questionList: 'Văn bản' },
//   { id: 2, question: 'Anh/ Chị có dự định tham khảo dòng xe nào từ Mazda không?', questionType: 'Văn bản', questionList: 'Văn bản' },
//   { id: 3, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
//   { id: 4, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
//   { id: 5, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
//   { id: 6, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
//   { id: 7, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
//   { id: 8, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
//   { id: 9, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
// ];



export default function ModalSurvey(props: IModalAlertProps) {
  return (
    <Box>
      <Modal
        open={props.isOpen}
        onClose={() => props.toggle()}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        sx={{ zIndex: 9999 }}
      >
        <Box sx={style}>
          <Grid item xs={12}>
            <Card>
              <DataGrid
                autoHeight
                rows={(props.rows || [])}
                disableColumnFilter={true}
                columns={props.columns}

                // loading={store.isLoading}
                checkboxSelection
                rowCount={10}
                hideFooterPagination
                sx={{
                  '& .MuiDataGrid-virtualScrollerContent': {
                    marginBottom: '10px'
                  },
                  '& .MuiDataGrid-footerContainer': {
                    display: 'none'
                  }
                }}
              />
            </Card>
          </Grid>
        </Box>
      </Modal>
    </Box>
  );
}