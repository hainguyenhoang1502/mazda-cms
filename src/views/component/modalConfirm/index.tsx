
import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import { INIT_STATE_MODAL_CONFIRM } from 'src/configs/initValueConfig';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};
export interface IModalAlertProps {
  isOpen: boolean;
  toggle: Function;
  action: Function
  title?: string
}
export default function ModalConfirm(props: IModalAlertProps) {
  return (
    <Box>
      <Modal
        open={props.isOpen}
        onClose={() => props.toggle(INIT_STATE_MODAL_CONFIRM)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        sx={{ zIndex: 9999 }}
      >
        <Box sx={style}>
          <Box
            sx={{
              mt: 2,
              maxWidth: "100%",
              display: "flex",
              justifyContent: "center",
              '& svg': { mb: 0, color: 'warning.main' }
            }}>
            <Icon icon='bx:error-circle' fontSize='10rem' />
          </Box>
          <Typography sx={{ margin: "15px 0", fontSize: '28px', fontWeight: 600, color: '#32475CDE', textAlign: "center" }}>
            Bạn có chắc chắn thực hiện điều này?
          </Typography>
          <Typography sx={{ margin: "10px 0", textAlign: "center", fontSize: "16px" }}>
            {props.title}
          </Typography>
          <Box sx={{ mt: 5, display: "flex", justifyContent: "center", gap: "10px" }}>
            <Button sx={{ minWidth: "120px" }} variant="contained" onClick={() => props.action()}>Xác nhận</Button>
            <Button sx={{ minWidth: "120px" }} variant="contained" color='error' onClick={() => props.toggle(INIT_STATE_MODAL_CONFIRM)}>Hủy</Button>
          </Box>
        </Box>
      </Modal>
    </Box>
  );
}