import styled from "@emotion/styled";
import Typography from "@mui/material/Typography";
import Box from "@mui/system/Box";
import IconifyIcon from "src/@core/components/icon";

const CustomMultiValueBox = styled(Box)({
  width: 'max-content',
  padding: "7px 10px",
  background: "#e5e2e2",
  borderRadius: '15px',
  display: "flex",
  justifyContent: "space-between",
  gap: "8px",
  alignItems: "center"
})

const CustomBoxContainer = styled(Box)({
  display: "flex",
  gap: "8px",
  alignItems: "center",
  width: "100%",
  flexWrap: "wrap"
})

type CustomMultiBoxProps = {
  list: Array<any>,
  handelDelte: (index: number) => void
}
export const CustomMultiBox = (props: CustomMultiBoxProps) => {

  return (
    <CustomBoxContainer>
      {
        props.list && props.list.length > 0 && props.list.map((item, index) => (
          <CustomMultiValueBox key={index}>
            <Typography noWrap sx={{ fontWeight: 400, fontSize: "14px", color: "#757575" }}>
              {item}
            </Typography>
            <IconifyIcon icon="iconamoon:close-duotone" color="#757575" fontSize="14px" onClick={() => props.handelDelte(index)} />
          </CustomMultiValueBox>
        ))
      }
    </CustomBoxContainer>
  )
}