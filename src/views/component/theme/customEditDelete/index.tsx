import Button from "@mui/material/Button"
import Tooltip from "@mui/material/Tooltip"
import Link from "next/link"
import router from "next/router"
import Icon from "src/@core/components/icon"
import { checkUserPermission } from "src/@core/utils/permission"

export const DeleteDataGrid = ({ handleDelete, permission, }) => {
  const canDelete = checkUserPermission(permission)

  return (
    canDelete && (
      <Tooltip title="Xóa" placement='top'>
        <Button color='primary' sx={{ p: 0, minWidth: '30px' }}>
          <Icon icon='mdi:trash-can-circle-outline' fontSize={20} color='#32475CDE' onClick={handleDelete} />
        </Button>
      </Tooltip>
    )
  )
}

export const EditDataGrid = ({ permission, slug }) => {
  const canEdit = checkUserPermission(permission)

  return (
    canEdit && (
      <Tooltip title="Cập nhật" placement='top'>
        <Button sx={{ p: 0, minWidth: '30px' }} onClick={() => router.push(slug)}>
          <Icon icon='solar:pen-new-round-linear' fontSize={20} color='#32475CDE' />
        </Button>
      </Tooltip>
    )
  )
}

export const CreateButtonDataGrid = ({ permission, slug }) => {
  const canCreate = checkUserPermission(permission)
  
  return (
    canCreate && (
      <Link href={slug} as={slug}>
        <Button variant="contained">
          + thêm
        </Button>
      </Link>
    )
  )
}