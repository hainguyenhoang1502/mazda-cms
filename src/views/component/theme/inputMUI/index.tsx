
import OutlinedInput from '@mui/material/OutlinedInput'
import Typography from '@mui/material/Typography'
import { MuiFormControlTextField, MuiInputLabel } from '../customTheme'
import FormHelperText from '@mui/material/FormHelperText'
import Box from '@mui/system/Box'

const InputRequiredMUI = ({ field, label, ...props }) => {

  return (
    <MuiFormControlTextField fullWidth>
      <MuiInputLabel htmlFor={props.id} error={props.texterror ? true : false}>
        {label} <Typography sx={{ color: "red" }}>{props.texticon || ""}</Typography>
      </MuiInputLabel>
      <OutlinedInput
        fullWidth
        id={props.id}
        label={props.texticon ? label + " " + props.texticon : label}
        {...field}
        {...props}
        error={props.texterror ? true : false}
      />
      {
        props.texterror &&
        <Box sx={{ display: "flex", color: "red", alignItems: "center" }}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2a1 1 0 0 0 0-2z" /></svg>
          <FormHelperText sx={{ color: "#f4071e", marginLeft: "3px", fontSize: '0.8rem' }} id="component-error-text">{props.texterror}</FormHelperText>
        </Box >
      }
    </MuiFormControlTextField>
  )
}
export default InputRequiredMUI