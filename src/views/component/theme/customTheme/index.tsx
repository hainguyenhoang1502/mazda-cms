import styled from '@emotion/styled'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'

const MuiInputLabel = styled(InputLabel)({
    display: 'flex',
    gap: '5px'
})
const MuiFormControlTextField = styled(FormControl)({
   
})

export{
    MuiInputLabel,
    MuiFormControlTextField
}