import MenuItem from "@mui/material/MenuItem";
import { GridToolbarContainer, GridToolbarExportContainer, gridFilteredSortedRowIdsSelector, gridVisibleColumnDefinitionsSelector, gridVisibleColumnFieldsSelector, useGridApiContext } from "@mui/x-data-grid";
import * as XLSX from 'xlsx';

function CustomToolbar(props) {

  return (
    <GridToolbarContainer {...props}>
      <ExportButton />
    </GridToolbarContainer>
  );
}
export function ExportButton(props) {

  return (
    <GridToolbarExportContainer {...props}>
      <ExportMenuItem />
    </GridToolbarExportContainer>
  );
}

export function ExportMenuItem(props) {
  const apiRef = useGridApiContext();
  const { hideMenu } = props;

  return (
    <MenuItem
      onClick={() => {
        handleExport(apiRef);
        hideMenu?.();
      }}
    >
      Download Excel
    </MenuItem>
  );
}

function getExcelData(apiRef) {

  // Select rows and columns
  const filteredSortedRowIds = gridFilteredSortedRowIdsSelector(apiRef);
  const visibleColumnsField = gridVisibleColumnFieldsSelector(apiRef);

  // Format the data. Here we only keep the value
  const data = filteredSortedRowIds.map((id) => {
    const row = {};
    visibleColumnsField.forEach((field) => {
      row[field] = apiRef.current.getCellParams(id, field).value;
    });
    
    return row;
  });

  return data;
}

function getExcelDataKeys(apiRef) {

  // Select rows and columns

  const visibleColumnDefinitions = gridVisibleColumnDefinitionsSelector(apiRef);

  // Format the data. Here we only keep the value

  const row = []
  visibleColumnDefinitions.forEach((item) => {
    row.push(item.headerName);
  });

  return row;


}


function handleExport(apiRef) {
  const data = getExcelData(apiRef);
  const keys = Object.keys(data && data.length && data[0])
  const dataKeys = getExcelDataKeys(apiRef)

  const rows = data.map((row) => {
    const mRow = {};
    for (const key of keys) {
      mRow[key] = row[key];
    }
    
    return mRow;
  });

  const worksheet = XLSX.utils.json_to_sheet(rows);
  XLSX.utils.sheet_add_aoa(worksheet, [[...dataKeys ]], { origin: 'A1', });

  const workbook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(workbook, worksheet, "Sheet 1");
  
  XLSX.writeFile(workbook, "table.xlsx", { compression: true });
}

export {
  CustomToolbar
}