import Typography from "@mui/material/Typography"

export default function TypographyDataGrid({ children, ...props }) {
    return (
        <Typography {...props} variant={props.variant || "body1"} fontSize={props.fontSize || "0.875rem"}>{children}</Typography>
    )
}