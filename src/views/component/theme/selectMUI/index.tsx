import Box from "@mui/system/Box"
import Select from "@mui/material/Select"
import Typography from "@mui/material/Typography"
import { FormHelperText, MenuItem } from "@mui/material"
import { MuiFormControlTextField, MuiInputLabel } from "../customTheme"


const SelectMUI = ({ label, options, ...props }) => {

  return (
    <MuiFormControlTextField fullWidth size={props.size}>
      <MuiInputLabel shrink={props.shrink} htmlFor={props.id} error={props.texterror ? true : false}>
        {label} <Typography sx={{ color: "red" }}>{props.texticon || ""}</Typography>
      </MuiInputLabel>
      {
        props.value || props.onChange ?
          <Select
            id={props.id}
            label={props.texticon ? label + " " + props.texticon : label}
            error={props.texterror ? true : false}
            value={props.value}
            
            onChange={props.onChange}
            {...props}
          >
            {options && options.length > 0 &&
              options.map((item, index) => (
                <MenuItem key={index} value={item.value}>{item.label}</MenuItem>
              ))}
          </Select> :
          <Select
            id={props.id}
            label={props.texticon ? label + " " + props.texticon : label}
            error={props.texterror ? true : false}
            {...props}
          >
            {options && options.length > 0 &&
              options.map((item, index) => (
                <MenuItem value={item.value} key={index}>{item.label}</MenuItem>
              ))}
          </Select>
      }
      {
        props.texterror &&
        <Box sx={{ display: "flex", color: "red", alignItems: "center" }}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2a1 1 0 0 0 0-2z" /></svg>
          <FormHelperText sx={{ color: "#f4071e", marginLeft: "3px", fontSize: '0.8rem' }} id="component-error-text">{props.texterror}</FormHelperText>
        </Box >
      }
    </MuiFormControlTextField>
  )
}
export default SelectMUI