import Box from "@mui/system/Box"
import Radio from "@mui/material/Radio"
import RadioGroup from "@mui/material/RadioGroup"
import Typography from "@mui/material/Typography"
import FormHelperText from "@mui/material/FormHelperText"
import FormControlLabel from "@mui/material/FormControlLabel"
import { MuiFormControlTextField, MuiInputLabel } from "../customTheme"

const RadioMUI = ({ label, options, ...props }) => {

  return (
    <MuiFormControlTextField fullWidth size={props.size} sx={{ height: "100%" }}>
      <MuiInputLabel htmlFor={props.id} error={props.texterror ? true : false}>
        {label} <Typography sx={{ color: "red" }}>{props.texticon || ""}</Typography>
      </MuiInputLabel>
      <RadioGroup
        row
        aria-labelledby="demo-row-radio-buttons-group-label"
        name="row-radio-buttons-group"
        sx={{ height: "100%" }}
        {...props}
      >
        {
          options && options.length > 0 && options.map((item, index) => (
            <FormControlLabel key={index} value={item.value} disabled={item.disabled} control={<Radio />} label={item.label} />
          ))
        }
      </RadioGroup>
      {
        props.texterror &&
        <Box sx={{ display: "flex", color: "red", alignItems: "center" }}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2a1 1 0 0 0 0-2z" /></svg>
          <FormHelperText sx={{ color: "#f4071e", marginLeft: "3px", fontSize: '0.8rem' }} id="component-error-text">{props.texterror}</FormHelperText>
        </Box >
      }
    </MuiFormControlTextField>
  )
}
export default RadioMUI