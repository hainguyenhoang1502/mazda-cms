import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import DatePicker from 'react-datepicker'
import { PickersComponentForm } from '../pickersComponent'


const DatePickerMUI = ({ label, ...props }) => {

  return (
    <DatePickerWrapper>
      <DatePicker
        selectsStart
        id={props.id}
        selected={props.value && new Date(props.value)}
        dateFormat="dd/MM/yyyy"
        customInput={<PickersComponentForm label={label} texticon={props.texticon} registername={props.name} error={props.texterror} />}
        onChange={(e: Date) => props.onChange(e && new Date(e))}
        disabled={props.disabled}
        minDate={props.minDate}
        maxDate={props.maxDate}
      />
    </DatePickerWrapper>
  )
}
export default DatePickerMUI