import TextField from '@mui/material/TextField'
import InputAdornment from "@mui/material/InputAdornment"
import Typography from '@mui/material/Typography'
import OutlinedInput from '@mui/material/OutlinedInput'
import FormControl from '@mui/material/FormControl'

import { forwardRef } from "react"

import Icon from "src/@core/components/icon"
import { MuiInputLabel } from '../customTheme'

import { PickerProps } from "src/configs/typeOption"
import Box from '@mui/system/Box'
import FormHelperText from '@mui/material/FormHelperText'


export const PickersComponentFilter = forwardRef(({ ...props }: PickerProps, ref) => {

  return (
    <TextField
      size='small'
      inputRef={ref}
      fullWidth
      {...props}
      label={props.label || ''}
      error={props.error}
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <Icon icon='bx:calendar' fontSize={20} />
          </InputAdornment>
        ),
      }}
    />
  )
})

export const PickersComponentForm = forwardRef(({ ...props }: PickerProps, ref) => {

  return (
    <FormControl fullWidth>
      <MuiInputLabel htmlFor="my-date" error={props.error ? true : false}>
        {props.label}<Typography sx={{ color: "red" }}>{props.texticon || ""}</Typography>
      </MuiInputLabel>
      <OutlinedInput
        inputRef={ref}
        id="my-date"
        fullWidth
        {...props}
        label={props.label || ''}
        error={props.error}
        endAdornment={
          <InputAdornment position="end">
            <Icon icon='bx:calendar' fontSize={20} />
          </InputAdornment>
        }
      />
      {
        props.error &&
        <Box sx={{ display: "flex", color: "red", alignItems: "center" }}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2a1 1 0 0 0 0-2z" /></svg>
          <FormHelperText sx={{ color: "#f4071e", marginLeft: "3px", fontSize: '0.8rem' }} id="component-error-text">{props.error}</FormHelperText>
        </Box >
      }
    </FormControl>
  )
})