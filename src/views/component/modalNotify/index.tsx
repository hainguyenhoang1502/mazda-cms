import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import Icon from 'src/@core/components/icon';
import { INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};
export interface IModalAlertProps {
  isOpen: boolean;
  toggle: Function;
  actionReturn?: Function
  title: string;
  message?: string
  isError?: boolean
  textBtn?: string
}
export default function ModalNotify(props: IModalAlertProps) {
  return (
    <Box>

      <Modal
        open={props.isOpen}
        onClose={() => props.toggle(INIT_STATE_MODAL_NOTIFY)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        sx={{ zIndex: 9999 }}
      >
        <Box sx={style}>
          <Box
            sx={{
              mt: 2,
              maxWidth: "100%",
              height: "150px",
              display: "flex",
              justifyContent: "center",
              '& svg': {
                mb: 0,
                color: !props.isError ? 'success.main' : 'error.main'
              }
            }} >
            <Icon fontSize='10rem' icon={!props.isError ? 'bx:check-circle' : 'bx:x-circle'} />
          </Box>
          <Typography sx={{ my: 5, fontSize: '28px', textAlign: "center" }}>
            {props.title}
          </Typography>
          <Typography sx={{ margin: "10px 0", textAlign: "center" }}>
            {props.message}
          </Typography>
          <Box sx={{ mt: 5, display: "flex", justifyContent: "center", gap: "10px" }}>
            {
              props.actionReturn &&
              <Button onClick={() => props.actionReturn()} variant="contained" >{props.textBtn}</Button>
            }
            <Button
              onClick={() => props.toggle(INIT_STATE_MODAL_NOTIFY)}
              variant="contained"
              color={!props.isError ? "success" : "error"}>
              {!props.isError ? 'Tiếp tục' : "Thử lại"}
            </Button>
          </Box>
        </Box>
      </Modal>
    </Box>
  );
}