
// ** MUI Imports
import Typography from '@mui/material/Typography';
import Box from '@mui/system/Box';
import { Button, TablePagination } from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';

// ** React Imports
import { useContext, useState } from 'react';

// ** Components Imports
import Icon from 'src/@core/components/icon';
import ModalConfirm from 'src/views/component/modalConfirm';
import ModalNotify from 'src/views/component/modalNotify';

// ** Config Imports
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption';
import { renderDate } from 'src/configs/functionConfig';
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig';

// ** Api Imports
import { IParamListVehicleByCustomer, IResultDataItemVehicleByCustomer } from 'src/api/vehicles/type';
import { ApiDeleteVehicleCustomerCms } from 'src/api/vehicles';

// ** Context Imports
import { DetailCustomerContext } from 'src/pages/customer-information/[slug]';

// ** Interface Imports
import { CustomerVehicleListProps } from './interface';

const CustomerVehicleList = (props: CustomerVehicleListProps) => {

  /* Context */
  const context = useContext(DetailCustomerContext);
  const { listVehicle, isLoading, paginationItem, paramFilter, getListVehicleByCustomer } = context

  /* Props */
  const { setItemClickVehicle } = props

  /* Hooks */
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)



  const handleDelete = (item: IResultDataItemVehicleByCustomer) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xóa Xe ' + item.modelName,
      action: () => { getDeleteVehicleCustomer(item.id.toString()) }
    })
  }

  const getDeleteVehicleCustomer = async (id: string) => {
    const param = {
      vehicleId: id
    }
    const res = await ApiDeleteVehicleCustomerCms(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Xe Thành Công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      getListVehicleByCustomer(paramFilter)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Xóa Xe Thất bại",
        message: res.message || UNKNOW_ERROR,
        isError:true

      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const columnsDataVehicleList: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      headerName: 'Hành động',
      renderCell: ({ row }) => {

        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <Button sx={{ p: 0, minWidth: "30px" }}>
              <Icon icon='bx:pencil' fontSize={20} color="#32475CDE" />
            </Button >
            <Button color='error' sx={{ p: 0, minWidth: "30px" }} onClick={() => setItemClickVehicle(row)}>
              <Icon icon='ic:baseline-history' fontSize={20} color="#32475CDE" />
            </Button >
            <Button color='primary' sx={{ p: 0, minWidth: "30px" }}>
              <Icon icon='bx:trash' fontSize={20} color="#32475CDE" onClick={() => handleDelete(row)} />
            </Button>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'modelName',
      headerName: 'Tên xe',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.modelName}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'licensePlate',
      headerName: 'Biển số',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.licensePlate}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'vin',
      headerName: 'vin',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.vin}
            </Typography>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 200,
      field: 'warrantyRegistrationDate ',
      headerName: 'Ngày đăng ký bảo hành',
      align: 'center',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', }}>
            <Typography noWrap >
              {renderDate(row.warrantyRegistrationDate)}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'lastMaintenanceDate',
      headerName: 'NGÀY BẢO DƯỠNG GẦN NHẤT',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {renderDate(row.lastMaintenanceDate)}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'engineNumber',
      headerName: 'Số máy',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.engineNumber}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'frameNumber',
      headerName: 'số khung',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.frameNumber}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'numberKmCurrent',
      headerName: 'số km hiện tại',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.numberKmCurrent}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 100,
      field: 'modelColor',
      headerName: 'màu ',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.modelColor}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'yearOfManufacture',
      headerName: 'năm sản xuất',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.yearOfManufacture}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'engine',
      headerName: 'động cơ',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.engine}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'dealerName',
      headerName: 'Đại lý',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.dealerName}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'batteryType',
      headerName: 'loại ắc quy',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.batteryType}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'tireType',
      headerName: 'Loại lốp',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.tireType}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'spareTireType',
      headerName: 'Loại lốp dự phòng',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.spareTireType}
            </Typography>
          </Box>
        )
      }
    },

  ]

  const handleChangePage = (e, page) => {
    const param: IParamListVehicleByCustomer = {
      ...paramFilter,
      pageIndex: page + 1,
    }
    getListVehicleByCustomer(param)
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamListVehicleByCustomer = {
      ...paramFilter,
      pageSize: Number(event.target.value) + 1,
    }
    getListVehicleByCustomer(param)
  }

  return (
    <>
      <DataGrid
        autoHeight
        rows={listVehicle}
        columns={columnsDataVehicleList}
        loading={isLoading}
        rowCount={paginationItem && paginationItem.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          }
        }}
      />
      <TablePagination
        component="div"
        count={paginationItem && paginationItem.totalRecords}
        page={paginationItem && paginationItem.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={paginationItem && paginationItem.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />

    </>
  )


}

export default CustomerVehicleList