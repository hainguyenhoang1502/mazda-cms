
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** Components Imports
import CustomerDetailLeft from './CustomerDetailLeft'
import CustomerDetailRight from './CustomerDetailRight'

const CustomerInformationDetail = () => {

  return (
    <>
      <Grid item xs={12} md={5} lg={4}>
        <CustomerDetailLeft />
      </Grid>
      <Grid item xs={12} md={7} lg={8}>

        <CustomerDetailRight/>
      </Grid>
    </>
  )
}
export default CustomerInformationDetail
