
// ** MUI Imports
import { ChipPropsColorOverrides } from '@mui/material/Chip'
import { OverridableStringUnion } from '@mui/types'

// ** Api Types Imports
import { IResultDataDetailCustomer } from 'src/api/customer/type'
import { IParamListVehicleByCustomer, IResultDataItemVehicleByCustomer } from 'src/api/vehicles/type'

// ** Config Imports
import { IPagination } from 'src/configs/typeOption'

interface CustomerInformationDetailProps {
  dataDetail: IResultDataDetailCustomer
  listVehicle: Array<IResultDataItemVehicleByCustomer>
  isLoading: boolean
  paginationItem: IPagination
  paramFilter: IParamListVehicleByCustomer
  getListVehicleByCustomer: Function
}
interface CustomerDetailLeftProps {
  dataDetail: IResultDataDetailCustomer
}
interface IDataTableProps {
  title: string
  data: Array<IDataItemTableProps>
}
interface IDataItemTableProps {
  title: string
  value: string | number
  color?: OverridableStringUnion<
    'default' | 'primary' | 'secondary' | 'error' | 'info' | 'success' | 'warning',
    ChipPropsColorOverrides
  >
}
interface CustomerVehicleListProps {
  setItemClickVehicle: Function
}
interface CustomerDetailHistoryMaintenanceProps {
  itemClickVehicle: IResultDataItemVehicleByCustomer
}

export type {
  CustomerInformationDetailProps,
  CustomerDetailLeftProps,
  IDataTableProps,
  CustomerVehicleListProps,
  CustomerDetailHistoryMaintenanceProps
}
