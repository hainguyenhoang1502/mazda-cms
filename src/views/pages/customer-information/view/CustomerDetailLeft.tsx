
// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import TableCell from "@mui/material/TableCell";
import styled from '@emotion/styled'
import Button from '@mui/material/Button'

// ** React Imports
import { useContext, useState } from 'react'

// ** Components Imports
import CustomChip from 'src/@core/components/mui/chip'
import CustomAvatar from 'src/@core/components/mui/avatar'
import ModalConfirm from 'src/views/component/modalConfirm'
import ModalNotify from 'src/views/component/modalNotify'

// ** Utlis Imports
import { getInitials } from 'src/@core/utils/get-initials'

// ** Interface Services Imports 
import { dataTable } from './service'

// ** Config Imports
import { renderNameTypeCustomer } from 'src/configs/functionConfig'
import { ACTION_ACTIVE_TEXT, ACTION_INACTIVE_TEXT, INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'

// ** Api Imports
import { ApiCustomerUpdateStatusCms } from 'src/api/customer'

// ** Context Imports
import { DetailCustomerContext } from 'src/pages/customer-information/[slug]'
import Link from 'next/link';
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption';

// **Custom MUI 

const MuiTableCell = styled(TableCell)`
  :last-of-type {
    width: 145px;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size:16px;
  }
`;
const MuiTableCellContent = styled(TableCell)`
  :last-of-type {
    max-width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size:16px;
    border:0;
    padding:0
  }
`;

const CustomerDetailLeft = () => {

  const props = useContext(DetailCustomerContext);
  const { dataDetail } = props

  const status = dataDetail.status === "ACTIVE" ? ACTION_INACTIVE_TEXT : ACTION_ACTIVE_TEXT

  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  const handleChangeStatus = async () => {
    const statusName = dataDetail.status === "ACTIVE" ? "INACTIVE" : "ACTIVE"
    const param = {
      customerId: dataDetail.id,
      status: statusName,
    }
    const res = await ApiCustomerUpdateStatusCms(param)
    if (res && res.code == 200 && res.result) {
      window.location.reload()
    } else {
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      setModalNotify({
        isOpen: true,
        title: `${status} trạng thái khách hàng Thất bại`,
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
    }
  }
  const handleConfirm = () => {
    setModalConfirm({
      isOpen: true,
      title: `Bạn đang thao tác ${status} trạng thái khách hàng`,
      action: () => handleChangeStatus()
    })
  }

  return (
    dataDetail &&
    <Card>

      <CardContent sx={{ pt: 12, display: 'flex', padding: "24px 18px", alignItems: 'center', flexDirection: 'column' }}>
        {dataDetail.avatar ? (
          <CustomAvatar
            src={dataDetail.avatar}
            variant='rounded'
            alt={dataDetail.fullName}
            sx={{ width: 250, height: 250, my: 6 }}
          />
        ) : (
          <CustomAvatar
            skin='light'
            variant='rounded'
            sx={{ width: 250, height: 250, fontWeight: 600, mb: 6, fontSize: '3rem' }}
          >
            {dataDetail.fullName && getInitials(dataDetail.fullName)}
          </CustomAvatar>
        )}
        <Typography variant='h4' sx={{ mb: 2.5, fontSize: '1.375rem !important' }}>
          {dataDetail.fullName}
        </Typography>
        <Typography sx={{ mb: 2.5, fontWeight: 400, color: 'rgba(50, 71, 92, 0.6)' }}>
          {renderNameTypeCustomer(dataDetail.customerTypeId)}
        </Typography>
      </CardContent>
      {
        dataTable(dataDetail).map((item, indexItem) => (
          item.data.length > 0 &&
          <CardContent sx={{ pt: 12, padding: "0 18px", flexDirection: 'column' }} key={item.data.length}>
            <Typography variant='h5' sx={{ mb: 2.5, fontSize: '1.375rem !important' }}>
              {item.title}
            </Typography>
            <TableContainer component={Paper}>
              <Table aria-label="customized table">
                <TableBody>
                  {item.data && item.data.length > 0 && item.data.map((row, index) => (
                    <TableRow key={index - indexItem}>
                      <MuiTableCell component="th" sx={{ border: "0" }} style={{ padding: "14px 0" }} >
                        {row.title}
                      </MuiTableCell>
                      {
                        row.title.includes("Trạng thái") ?
                          <MuiTableCellContent style={{ padding: "14px 0" }}>
                            <CustomChip
                              rounded skin='light'
                              size='small'
                              label={row.value}
                              color={row.color} />
                          </MuiTableCellContent>
                          :
                          <MuiTableCellContent style={{ padding: "14px 0" }}>{row.value}</MuiTableCellContent>
                      }
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </CardContent>

        ))
      }
      <CardContent sx={{ textAlign: "center", marginTop: "10px", gap: "10px", display: "flex", justifyContent: "center" }}>
        <Link href={`/customer-information/edit/${dataDetail.id}`} as={`/customer-information/edit/${dataDetail.id}`}>
          <Button variant="contained">
            Chỉnh sửa
          </Button>
        </Link>
        <Button
          variant="contained"
          color={dataDetail.status === "ACTIVE" ? "error" : "success"}
          onClick={() => handleConfirm()}
        >
          {status}
        </Button>
      </CardContent>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={(setModalNotify)} />
    </Card>
  )
}
export default CustomerDetailLeft