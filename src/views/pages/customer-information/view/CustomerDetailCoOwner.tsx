
// ** MUI Imports
import CardContent from "@mui/material/CardContent"
import Button from "@mui/material/Button"
import Grid from "@mui/material/Grid"
import { Typography } from "@mui/material"

// ** React Imports
import { useContext, useEffect, useState } from "react"

// ** Components Imports
import { InputRequiredMUI, SelectMUI } from "src/views/component/theme"
import DatePickerMUI from "src/views/component/theme/dateMUI"

// ** Third Party Import
import { Field, Form, Formik } from "formik"

// ** Config Imports
import { OPTIONS_GENDER, OPTIONS_STATUS } from "src/configs/functionConfig"

// ** Interface Services Imports 
import { INIT_VALUE_CO_OWNER } from "./service"
import { ApiCreateCustomerCo, ApiCustomerByPhone, ApiCustomerUpdateCms, ApiDeleteCustomerCo, ApiDetailCustomerCms, ApiGetCustomerCo } from "src/api/customer"
import { ERROR_EMAIL_VALUE_INPUT, ERROR_NULL_VALUE_INPUT, ERROR_PHONE_VALUE_INPUT, INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR, regexPhone } from "src/configs/initValueConfig"
import ModalConfirm from "src/views/component/modalConfirm"
import { IOptionsFilter, ModalConfirmState, ModalNotifyState } from "src/configs/typeOption"
import ModalNotify from "src/views/component/modalNotify"
import { setValueBackrop } from "src/store/utils/loadingBackdrop";
import { useDispatch } from "react-redux"
import { DetailCustomerContext } from "src/pages/customer-information/[slug]"
import { useSelector } from "react-redux"
import { AppDispatch, RootState } from "src/store"
import { fetchDataGetListFilterAllProvince } from "src/store/apps/category/province";
import { ApiGetLocationDistrictByProvinceId, ApiGetLocationWardByDistrictId } from "src/api/refData"
import * as Yup from 'yup'
import Link from "next/link"


const CustomerDetailCoOwner = (props) => {

  /* Props */
  const { itemClickVehicle } = props

  // state
  const [isNoData, setIsNoData] = useState<boolean>(true)
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)
  const [customerCo, setCustomerCo] = useState(null)
  const [customerCoExist, setCustomerCoExist] = useState(null)
  const [listDistrict, setListDistrict] = useState<Array<IOptionsFilter>>([])
  const [listWard, setListWard] = useState<Array<IOptionsFilter>>([])
  const [customer, setCustomer] = useState(null)

  // Redux
  const dispatch = useDispatch<AppDispatch>()
  const storeProvince = useSelector((state: RootState) => state.province)

  /* Context */
  const context = useContext(DetailCustomerContext);
  const { listVehicle, customerId } = context
  const itemVehicle = listVehicle && listVehicle[0]

  // yup

  const CustomerCoSchema = Yup.object().shape({
    fullName: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
    birthday: Yup.date().required(ERROR_NULL_VALUE_INPUT).nullable(),
    phoneNumber: Yup.string().required(ERROR_NULL_VALUE_INPUT).matches(regexPhone, ERROR_PHONE_VALUE_INPUT).nullable(),
    email: Yup.string().required(ERROR_NULL_VALUE_INPUT).email(ERROR_EMAIL_VALUE_INPUT).nullable(),
    gender: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
    relationship: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
    provinceId: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
    districtId: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
    wardId: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
    address: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
    status: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),

  })

  useEffect(() => {
    if (itemVehicle && itemVehicle.id && customerId) {
      const initParam = {
        pageIndex: 1,
        pageSize: 1,
        vehicleId: listVehicle[0].id
      }
      getCustomerCo(initParam)
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemVehicle, customerId])

  useEffect(() => {
    if (itemClickVehicle) {
      const initParam = {
        pageIndex: 1,
        pageSize: 1,
        vehicleId: itemClickVehicle.id
      }
      getCustomerCo(initParam)
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemClickVehicle])

  useEffect(() => {

    if (storeProvince && (!storeProvince.arrFilter || storeProvince.arrFilter.length === 0)) {
      dispatch(fetchDataGetListFilterAllProvince())
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (customerCo) {
      getListLocationDistricts(customerCo.provinceId)
      getListLocationWards(customerCo.districtId)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [customerCo])

  // format null --> ''
  const formatValue = (data) => {
    const newData = { ...data };
    for (const key in newData) {
      if (newData[key] === null) {
        newData[key] = '';
      }
    }

    return newData;
  }

  // ditrict and ward

  const onChangeSelectProvince = (event) => {
    getListLocationDistricts(event)
  }

  const getListLocationDistricts = async (id: number | string) => {
    const res = await ApiGetLocationDistrictByProvinceId(id)
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      const arr = res.data.map(item => ({
        label: item.locationName,
        value: item.code
      }));
      setListDistrict(arr)
    } else {
      setListDistrict([])
    }
  }

  const getListLocationWards = async (id: number | string) => {
    const res = await ApiGetLocationWardByDistrictId(id)
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      const arr = res.data.map(item => ({
        label: item.locationName,
        value: item.code
      }));
      setListWard(arr)
    } else {
      setListWard([])

    }
  }

  const getCustomerCo = async (param) => {

    const res = await ApiGetCustomerCo(param)
    if (res.code === 200 && res.result) {

      const dataFormated = formatValue(res.data.result[0])

      setIsNoData(false)
      setCustomerCo(dataFormated)
    } else {
      setCustomerCo(null)
      setIsNoData(true)
    }
  }

  const handleSubmitForm = (values) => {

    setModalConfirm({ //on popup
      isOpen: true,
      title: "Bạn đang thao tác Thêm người đồng sở hữu",
      action: () => {
        return handleCreate(values)
      }
    })

  }

  const getDetailCustomer = async (id) => {
    const customer = await ApiDetailCustomerCms({id: id})
      if (customer.code === 200 && customer.result) {
        setCustomer(customer.data)
      }
  }

  const updateDetailCustomer = async (data) => {

    const param = {...customer, ...data}
    
    await ApiCustomerUpdateCms(param)
  }

  const handleBlurPhoneNumber = async (e) => {

    const phoneNumber = e.currentTarget.value

    setCustomerCoExist(null)
    dispatch(setValueBackrop(true)) // on loading

    const res = await ApiCustomerByPhone(phoneNumber)

    if (res.code === 200 && res.result) {
      setCustomerCoExist(res.data)
      if (res.data) {
        getListLocationDistricts(res.data.provinceId)
        getListLocationWards(res.data.districtId)
        getDetailCustomer(res.data.id)
      }
    }

    dispatch(setValueBackrop(false)) // off loading

  }

  function getChangedFields(object1, object2) {
    const changedFields = {};
  
    for (const key in object1) {
      if (object1[key] !== object2[key]) {
        changedFields[key] = object2[key];
      }
    }
  
    return changedFields;
  }

  const handleCreate = async (values) => {

    setModalConfirm(INIT_STATE_MODAL_CONFIRM) // off popup
    dispatch(setValueBackrop(true)) // on loading

    //call api create customer co
    const param = { ...values, vehicleId: itemClickVehicle.id || listVehicle[0].id }

    const res = await ApiCreateCustomerCo(param)

    if (res.code === 200 && res.result) {

      setModalNotify({
        isOpen: true,
        title: 'Thêm Thành Công'
      })

      const initParam = {
        pageIndex: 1,
        pageSize: 1,
        vehicleId: itemClickVehicle.id || listVehicle[0].id
      }

      getCustomerCo(initParam)

      const changeField = getChangedFields(customerCoExist, values)

      if ( customer && Object.keys(changeField).length >= 1 ) {
        updateDetailCustomer(changeField)
      }

    } else {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thất Bại',
        message: res.message || UNKNOW_ERROR
      })
    }

    dispatch(setValueBackrop(false)) // off loading
  }

  const handleDelete = (id: string) => {

    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác xoá Người đồng sở hữu',
      action: () => { getDelete(id) }
    })
  }

  const getDelete = async (id: string) => {

    const param = {
      customerId: id,
      vehicleId: itemClickVehicle.id || listVehicle[0].id
    }

    dispatch(setValueBackrop(true)) // on loading

    const res = await ApiDeleteCustomerCo(param)

    dispatch(setValueBackrop(false)) // off loading

    if (res.code == 200 && res.result) {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Người đồng sở hữu Thành Công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      setIsNoData(true)
      setCustomerCo(null)
      setCustomerCoExist(null)
    } else {
      setModalNotify({
        ...modalNotify,
        isOpen: true,
        title: "Xóa Khách Hàng Thất bại",
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }

  }

  return (
    <>
      {
        isNoData ?
          <CardContent sx={{ textAlign: "center" }}>
            <Typography sx={{ paddingBottom: "20px" }} color="error">
              Không có người đồng sỡ hữu
            </Typography>
            {
              itemClickVehicle?.id || listVehicle[0]?.id ? 
              <Button variant="contained" onClick={() => {setIsNoData(false); setCustomerCo(null); setCustomerCoExist(null) }}>Thêm</Button>
              : ''
            }
          </CardContent>
          :
          <CardContent>
            <Formik
              initialValues={customerCoExist || customerCo || INIT_VALUE_CO_OWNER}
              enableReinitialize
              onSubmit={values => handleSubmitForm(values)}
              validationSchema={CustomerCoSchema}
              validateOnBlur
            >
              {(props) => {

                return (
                  <Form>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-fullName"
                          label="Họ và Tên"
                          name="fullName"
                          component={InputRequiredMUI}
                          texterror={props.touched.fullName && props.errors.fullName}
                          texticon="*"
                          disabled={customerCo ? true : false}
                        />
                      </Grid>
                      <Grid item lg={6} xs={12} >

                        <Field
                          id="my-birthday"
                          label="Ngày sinh"
                          name="birthday"
                          component={DatePickerMUI}
                          dateFormat="dd/MM/yyyy"
                          texterror={props.touched.birthday && props.errors.birthday}
                          texticon="*"
                          value={props.values.birthday}
                          disabled={customerCo ? true : false}
                          onChange={(event) => {
                            props.setFieldValue("birthday", event);
                          }}
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-phoneNumber"
                          label="Số điện thoại"
                          name="phoneNumber"
                          component={InputRequiredMUI}
                          texterror={props.touched.phoneNumber && props.errors.phoneNumber}
                          texticon="*"
                          disabled={customerCo ? true : false}
                          onBlur={e => {
                            // call the built-in handleBur
                            handleBlurPhoneNumber(e)
                          }}
                        />

                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-email"
                          label="Email"
                          name="email"
                          component={InputRequiredMUI}
                          texterror={props.touched.email && props.errors.email}
                          texticon="*"
                          disabled={customerCo ? true : false}
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="gender"
                          label="Giới tính"
                          as="select"
                          component={SelectMUI}
                          texticon="*"
                          name="gender"
                          texterror={props.touched.gender && props.errors.gender}
                          value={props.values.gender}
                          disabled={customerCo ? true : false}
                          onChange={(event) => {
                            props.setFieldValue("gender", event.target.value);
                          }}
                          options={OPTIONS_GENDER}
                        />

                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-relationship"
                          label="Mối quan hệ"
                          name="relationship"
                          component={InputRequiredMUI}
                          texterror={props.touched.relationship && props.errors.relationship}
                          texticon="*"
                          disabled={customerCo ? true : false}
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="provinceId"
                          label="Tỉnh/Thành phố"
                          component={SelectMUI}
                          texticon="*"
                          name="provinceId"
                          texterror={props.touched.provinceId && props.errors.provinceId}
                          options={storeProvince.arrFilter}
                          value={props.values.provinceId}
                          disabled={customerCo ? true : false}
                          onChange={(event) => {
                            props.setFieldValue("provinceId", event.target.value);
                            props.setFieldValue("districtId", "");
                            props.setFieldValue("wardId", "");
                            onChangeSelectProvince(event.target.value)
                          }}
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="districtId"
                          label="Quận/huyện"
                          component={SelectMUI}
                          texticon="*"
                          name="districtId"
                          texterror={props.touched.districtId && props.errors.districtId}
                          options={listDistrict}
                          value={props.values.districtId}
                          disabled={customerCo ? true : false}
                          onChange={(event) => {
                            props.setFieldValue("districtId", event.target.value);
                            props.setFieldValue("wardId", "");
                            getListLocationWards(event.target.value)
                          }}
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="wardId"
                          label="Phường/xã"
                          component={SelectMUI}
                          texticon="*"
                          name="wardId"
                          texterror={props.touched.wardId && props.errors.wardId}
                          options={listWard}
                          value={props.values.wardId}
                          disabled={customerCo ? true : false}
                          onChange={(event) => {
                            props.setFieldValue("wardId", event.target.value);
                          }}
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-address"
                          label="Địa chỉ"
                          name="address"
                          component={InputRequiredMUI}
                          texterror={props.touched.address && props.errors.address}
                          texticon="*"
                          disabled={customerCo ? true : false}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="status"
                          label="Trạng thái"
                          component={SelectMUI}
                          texticon="*"
                          name="status"
                          texterror={props.touched.status && props.errors.status}
                          options={OPTIONS_STATUS}
                          value={props.values.status}
                          disabled={customerCo ? true : false}
                          onChange={(event) => {
                            props.setFieldValue("status", event.target.value);
                          }}
                        />
                      </Grid>
                    </Grid>
                    <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
                      {customerCo
                        ?
                        <>
                          <Button variant='contained' color='error' onClick={() => { handleDelete(props.values.id) }}>Xóa</Button>
                          <Link href={`/customer-information/edit/${props.values.id}/`}>
                            <Button variant='contained'>Chỉnh sửa</Button>
                          </Link>
                        </>
                        :
                        <Button variant="contained" type="submit">Lưu</Button>
                      }
                    </CardContent>
                  </Form>
                )
              }
              }
            </Formik>
          </CardContent>
      }
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CustomerDetailCoOwner