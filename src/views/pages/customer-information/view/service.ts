
// ** Config Imports
import { renderDate, renderNameGender, renderNameStatus } from 'src/configs/functionConfig'

// ** Interface Types Imports 
import { IResultDataDetailCustomer } from 'src/api/customer/type'
import { IDataTableProps } from './interface'

const renderDataCompany = (data: IResultDataDetailCustomer) => {
  if (data.customerTypeId === 'PERSONAL') {
    return []
  } else {
    return [
      {
        title: 'Tên công ty:',
        value: data.companyName
      },
      {
        title: 'Số điện thoại:',
        value: data.companyPhoneNumber
      },
      {
        title: 'Mã số thuế: ',
        value: data.companyTaxCode
      },
      {
        title: 'Website:',
        value: data.companyWebsite
      },
      {
        title: 'Địa chỉ: ',
        value: data.companyAddress
      },
      {
        title: 'Phường/Xã: ',
        value: data.companyWard
      },
      {
        title: 'Quận/Huyện:',
        value: data.companyDistrict
      },
      {
        title: 'Tỉnh/Thành phố:',
        value: data.companyProvince
      }
    ]
  }
}
const dataTable = (data: IResultDataDetailCustomer) => {
  const arr: Array<IDataTableProps> = [
    {
      title: 'Thông tin',
      data: [
        {
          title: 'Họ tên:',
          value: data.fullName
        },
        {
          title: 'Ngày sinh:',
          value: renderDate(data.birthday)
        },
        {
          title: 'Giới tính:',
          value: renderNameGender(data.gender)
        },
        {
          title: 'Số điện thoại:',
          value: data.phoneNumber
        },
        {
          title: 'Email:',
          value: data.email
        },
        {
          title: 'Mã số thuế:',
          value: data.personalTaxCode
        }
      ]
    },
    {
      title: 'Địa chỉ',
      data: [
        {
          title: 'Địa chỉ:',
          value: data.address
        },
        {
          title: 'Phường/Xã:',
          value: data.ward
        },
        {
          title: 'Quận/Huyện:',
          value: data.district
        },
        {
          title: 'Tỉnh/ thành phố:',
          value: data.province
        }
      ]
    },
    {
      title: 'Công ty',
      data: renderDataCompany(data)
    },
    {
      title: 'Khác',
      data: [
        {
          title: 'Ngày tạo:',
          value: renderDate(data.createdDate)
        },
        {
          title: 'Trạng thái:',
          value: renderNameStatus(data.status),
          color: data.status === 'ACTIVE' ? 'success' : 'error'
        }
      ]
    }
  ]

  return arr
}


const INIT_VALUE_CO_OWNER={
  fullName:"",
  birthday:"",
  gender:"",
  phoneNumber:"",
  email:"",
  provinceId:"",
  districtId:"",
  wardId:"",
  address:"",
  relationship:"",
  status:""
} 
export { dataTable,INIT_VALUE_CO_OWNER }
