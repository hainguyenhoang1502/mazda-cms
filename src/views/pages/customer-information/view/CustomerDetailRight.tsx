
// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography';

// ** Components Imports
import CustomerDetailHistoryMaintenance from './CustomerDetailHistoryMaintenance';
import CustomerDetailCoOwner from './CustomerDetailCoOwner';
import CustomerVehicleList from './CustomerDetailVehicleList';
import { useContext, useEffect, useState } from 'react';
import { DetailCustomerContext } from 'src/pages/customer-information/[slug]';
import { IResultDataItemVehicleByCustomer } from 'src/api/vehicles/type';

const CustomerDetailRight = () => {

  const context = useContext(DetailCustomerContext);
  const { listVehicle } = context
  const itemVehicle = listVehicle && listVehicle[0]

  const [itemClickVehicle, setItemClickVehicle] = useState<IResultDataItemVehicleByCustomer>(itemVehicle)

  useEffect(() => {
    if (itemVehicle) {
      setItemClickVehicle(itemVehicle)
    }
  }, [itemVehicle])
  
  return (
    <>
      <Card>
        <CardContent sx={{ pt: 12, flexDirection: 'column' }}>
          <Typography noWrap sx={{ fontWeight: 500, textTransform: 'capitalize', padding: "0 0 20px 20px", fontSize: "24px" }}>
            Xe Của Bạn
          </Typography>
          <CustomerVehicleList setItemClickVehicle={setItemClickVehicle} />
        </CardContent>
      </Card>
      {
        <Card sx={{ marginTop: "24px" }}>
          <CardContent sx={{ pt: 12, flexDirection: 'column' }}>
            <Typography noWrap sx={{ fontWeight: 500, textTransform: 'capitalize', padding: "0 0 20px 20px", fontSize: "24px" }}>
              Lịch Sử Bảo Dưỡng {itemClickVehicle && `- ${itemClickVehicle.modelName} - ${itemClickVehicle.licensePlate}`}
            </Typography>
            <CustomerDetailHistoryMaintenance itemClickVehicle={itemClickVehicle} />
          </CardContent>
        </Card>
      }
      {
        <Card sx={{ marginTop: "24px" }}>
          <CardContent sx={{ pt: 12, flexDirection: 'column' }}>
            <Typography noWrap sx={{ fontWeight: 500, textTransform: 'capitalize', padding: "0 0 20px 20px", fontSize: "24px" }}>
              Thông tin người đồng sở hữu {itemClickVehicle && `- ${itemClickVehicle.modelName} - ${itemClickVehicle.licensePlate}`}
            </Typography>
            <CustomerDetailCoOwner itemClickVehicle={itemClickVehicle} />
          </CardContent>
        </Card>
      }
    </>
  )
}
export default CustomerDetailRight

