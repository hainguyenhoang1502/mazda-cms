
// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment, TablePagination } from '@mui/material'
import { DataGrid, GridColDef } from '@mui/x-data-grid';

// ** React Imports
import { useContext, useEffect, useState } from 'react'

// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Third Party Import
import DatePicker from 'react-datepicker'

// ** Config Imports
import { renderDate } from 'src/configs/functionConfig'
import { INIT_VALUE_PAGINATION } from 'src/configs/initValueConfig';

// ** Api Imports
import { ApiListMaintenanceHistoryByVehicleCms } from 'src/api/warranty';
import { IParamListMaintenaceHistory, IResponseListMantenanceHistoryByVehicle } from 'src/api/warranty/type';

// ** Context Imports
import { DetailCustomerContext } from 'src/pages/customer-information/[slug]'

// ** Styles
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import { IPagination } from 'src/configs/typeOption';
import { PickersComponentFilter, TypographyDataGrid } from 'src/views/component/theme';
import { CustomerDetailHistoryMaintenanceProps } from './interface';


const CustomerDetailHistoryMaintenance = (props: CustomerDetailHistoryMaintenanceProps) => {

  /* Context */
  const context = useContext(DetailCustomerContext);
  const { listVehicle, customerId } = context
  const itemVehicle = listVehicle && listVehicle[0]

  /* Props */
  const { itemClickVehicle } = props

  /* Hook */
  const [listMaintenance, setListMaintenance] = useState<IResponseListMantenanceHistoryByVehicle["data"]["result"]>([])
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const [paginationItem, setPaginationItem] = useState<IPagination>(INIT_VALUE_PAGINATION)
  const [paramFilter, setParamFilter] = useState<IParamListMaintenaceHistory>({
    keyword: '',
    pageIndex: 1,
    pageSize: 10,
    vehicleId: null,
    startDate: null,
    endDate: null
  })

  useEffect(() => {
    if (itemVehicle && itemVehicle.id && customerId) {
      const initParam = {
        keyword: '',
        pageIndex: 1,
        pageSize: 10,
        customerId: customerId,
        vehicleId: listVehicle[0].id,
        startDate: null,
        endDate: null
      }
      getLisMaintenanceByCustomer(initParam)
    } else {
      setIsLoading(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemVehicle, customerId])

  useEffect(() => {
    if (itemClickVehicle) {
      const initParam = {
        ...paramFilter,
        vehicleId: itemClickVehicle.id,
      }
      getLisMaintenanceByCustomer(initParam)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itemClickVehicle])

  /* Function */
  const getLisMaintenanceByCustomer = async (param) => {
    setIsLoading(true)
    setParamFilter(param)
    const res = await ApiListMaintenanceHistoryByVehicleCms(param)
    if (res && res.code === 200 && res.result && res.data) {
      setListMaintenance(res.data.result || [])
      setPaginationItem({
        pageIndex: res.data.pageIndex,
        pageSize: res.data.pageSize,
        totalPages: res.data.totalPages,
        totalRecords: res.data.totalRecords
      })
    } else {
      setListMaintenance([])
    }
    setIsLoading(false)
  }



  const handleFilterDate = () => {
    getLisMaintenanceByCustomer(paramFilter)
  }
  const handleClearFilter = () => {
    const param = {
      ...paramFilter,
      keyword: '',
      pageIndex: 1,
      pageSize: 10,
      startDate: null,
      endDate: null
    }
    getLisMaintenanceByCustomer(param)

  }
  const handleChangeInput = (value, field) => {
    const body = { ...paramFilter }
    body[field] = value
    setParamFilter(body)
  }
  const handleChangePage = (e, page) => {
    const param: IParamListMaintenaceHistory = {
      ...paramFilter,
      pageIndex: page + 1,
    }
    getLisMaintenanceByCustomer(param)
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamListMaintenaceHistory = {
      ...paramFilter,
      pageSize: Number(event.target.value),
    }
    getLisMaintenanceByCustomer(param)
  }
  const columnsDataVehicleList: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'roNo',
      sortable: false,
      headerName: 'Mã RO',
      renderCell: ({ row }) => {

        return (
          <TypographyDataGrid noWrap >
            {row.roNo}
          </TypographyDataGrid>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'roDate',
      sortable: false,
      headerName: 'rodate',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {renderDate(row.roDate)}
            </TypographyDataGrid>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 150,
      field: 'modelName',
      sortable: false,
      headerName: 'dòng xe',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.modelName}
            </TypographyDataGrid>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 150,
      field: 'licensePlate',
      sortable: false,
      headerName: 'Biển số',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.licensePlate}
            </TypographyDataGrid>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 150,
      field: 'numberKm',
      sortable: false,
      headerName: 'số km',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.numberKm}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 250,
      field: 'dealerName',
      sortable: false,
      headerName: 'đại lý',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.dealerName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 250,
      field: 'maintenanceType ',
      sortable: false,
      headerName: 'loại bảo dưỡng',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex' }}>
            <TypographyDataGrid noWrap >
              {row.maintenanceType}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 180,
      field: 'receiveDate',
      sortable: false,
      headerName: 'ngày vào xưởng',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {renderDate(row.receiveDate)}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 180,
      field: 'deliveryDate',
      sortable: false,
      headerName: 'ngày giao xe',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {renderDate(row.deliveryDate)}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    
  ]

  return (
    <Box>
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', mb: 4 }}>
        <TextField
          size='small'
          value={paramFilter.keyword}
          sx={{ mr: 4, width: "20%" }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            ),
          }}
          placeholder='Tìm kiếm…'
          onChange={e => handleChangeInput(e.target.value, "keyword")}

        />
        <DatePickerWrapper sx={{ display: 'flex', gap: "1rem", maxWidth: "320px", mr: 4 }}>
          <DatePicker
            selectsStart
            id='event-start-date'
            selected={paramFilter.startDate ? new Date(paramFilter.startDate) : null}
            dateFormat="dd/MM/yyyy"
            maxDate={paramFilter.endDate && new Date(paramFilter.endDate)}
            customInput={<PickersComponentFilter label='Từ ngày' registername='startDate' />}
            onChange={(date: Date) => handleChangeInput(new Date(date), "startDate")}
          />
          <DatePicker
            selectsStart
            id='event-start-date'
            selected={paramFilter.endDate ? new Date(paramFilter.endDate) : null}
            dateFormat="dd/MM/yyyy"
            minDate={paramFilter.startDate && new Date(paramFilter.startDate)}
            customInput={<PickersComponentFilter label='Đến ngày' registername='endDate' />}
            onChange={(date: Date) => handleChangeInput(new Date(date), "endDate")}
          />
        </DatePickerWrapper>
        <Button variant="outlined" onClick={handleFilterDate} sx={{ mr: 4 }} >
          Lọc
        </Button>
        <Button variant="outlined" onClick={handleClearFilter} color='error'>
          Xóa bộ lọc
        </Button>
      </Box>
      <Box>
        <DataGrid
          autoHeight
          rows={listMaintenance}
          loading={isLoading}
          columns={columnsDataVehicleList}
          hideFooterPagination
          sx={{
            '& .MuiDataGrid-virtualScrollerContent': {
              marginBottom: "10px"
            },
            '& .MuiDataGrid-footerContainer': {
              display: "none"
            }
          }}
        />
        <TablePagination
          component="div"
          count={paginationItem.totalRecords}
          page={paginationItem.pageIndex - 1}
          onPageChange={handleChangePage}
          rowsPerPage={paginationItem.pageSize}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Box>
    </Box>
  )
}
export default CustomerDetailHistoryMaintenance