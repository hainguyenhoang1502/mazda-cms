
// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment } from '@mui/material'

// ** React Imports
import { useState } from 'react'

// ** Third Party Import
import DatePicker from 'react-datepicker'
import toast from 'react-hot-toast';

// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Styles
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'

// ** Interface Services Imports 
import { IParamValuesFilter, TableHeaderProps } from './interface'

// ** Utils
import { checkUserPermission } from 'src/@core/utils/permission'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetCustomer } from 'src/store/apps/customer-information'
import { CreateButtonDataGrid } from 'src/views/component/theme/customEditDelete'
import { PickersComponentFilter } from 'src/views/component/theme'


const TableHeader = (props: TableHeaderProps) => {

  /* Props */
  const { arrRowId, setModalConfirm, getDeleteCustomer } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.customer)


  const [valuesFilter, setValuesFilter] = useState<IParamValuesFilter>({
    keyword: "",
    startDate: null,
    endDate: null
  })

  const handleChangeInput = (value, field) => {
    const body = { ...valuesFilter }
    body[field] = value
    setValuesFilter(body)
  }

  const handleFilterDate = () => {
    const param = {
      ...store.param,
      ...valuesFilter,
      pageIndex: 1,
    }
    dispatch(fetchDataGetCustomer(param))
  }

  const handleClearFilter = () => {
    const param = {
      ...store.param,
      keyword: "",
      startDate: null,
      endDate: null
    }
    setValuesFilter(param)
    dispatch(fetchDataGetCustomer(param))

  }

  const handleDelete = () => {
    if (arrRowId.length <= 0) {
      toast.error("Bạn chưa chọn khách hàng cần xóa")
    } else {
      setModalConfirm({
        isOpen: true,
        title: `Bạn đang thao tác Xóa ${arrRowId.length} khách hàng `,
        action: () => { getDeleteCustomer(arrRowId.toString()) }
      })
    }
  }

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: "25px"
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ mr: 4, width: "30%" }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            ),
          }}
          placeholder='Tìm kiếm…'
          onChange={(e) => handleChangeInput(e.target.value, "keyword")}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              handleFilterDate()
            }
          }}
        />

        <DatePickerWrapper sx={{ display: 'flex', gap: "1rem", width: "60%", mr: 4, maxWidth: "320px" }}>
          <DatePicker
            selectsStart
            id='event-start-date'
            selected={valuesFilter.startDate ? new Date(valuesFilter.startDate) : null}
            dateFormat="dd/MM/yyyy"
            maxDate={valuesFilter.endDate && new Date(valuesFilter.endDate)}
            customInput={<PickersComponentFilter label='Từ ngày' registername='startDate' />}
            onChange={(date: Date) => handleChangeInput(new Date(date), "startDate")}
          />
          <DatePicker
            selectsStart
            id='event-start-date'
            selected={valuesFilter.endDate ? new Date(valuesFilter.endDate) : null}
            dateFormat="dd/MM/yyyy"
            minDate={valuesFilter.startDate && new Date(valuesFilter.startDate)}
            customInput={<PickersComponentFilter label='Đến ngày' registername='endDate' />}
            onChange={(date: Date) => handleChangeInput(new Date(date), "endDate")}
          />
        </DatePickerWrapper>

        <Button variant="outlined" onClick={handleFilterDate} sx={{ mr: 4 }} >
          Lọc
        </Button>
        <Button variant="outlined" onClick={handleClearFilter} color='error'>
          Xóa bộ lọc
        </Button>
      </Box>
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', gap: "1rem", justifyContent: "end", maxWidth: "100%" }}>
        <CreateButtonDataGrid permission={permissonConfig.CUSTOMER_ADD} slug={"/customer-information/create"} />
        {
          checkUserPermission(permissonConfig.CUSTOMER_DELETE) &&
          <Button variant="contained" color='error' onClick={handleDelete}>
            Xóa
          </Button>
        }
      </Box>
    </Box>
  )
}

export default TableHeader
