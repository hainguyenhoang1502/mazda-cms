// ** MUI Imports
import Box from '@mui/system/Box'
import Button from '@mui/material/Button'
import { DataGrid, GridColDef } from "@mui/x-data-grid"
import TablePagination from '@mui/material/TablePagination'

// ** Next Imports
import router from "next/router"

// ** Component Imports
import Icon from 'src/@core/components/icon'
import CustomChip from 'src/@core/components/mui/chip'
import CustomAvatar from 'src/@core/components/mui/avatar'
import { CustomToolbar, TypographyDataGrid } from "src/views/component/theme"

// ** Config Imports
import { permissonConfig } from "src/configs/roleConfig"
import { renderNameGender, renderNameStatus } from "src/configs/functionConfig"

// ** Type Interface Imports
import { ListDataTableProps } from "./interface"
import { IParamFilterListCustomer, IResultDataDetailCustomer } from "src/api/customer/type"

// ** Utils Import
import { checkUserPermission } from "src/@core/utils/permission"

// ** Store Redux Imports
import { AppDispatch, RootState } from "src/store"
import { useDispatch, useSelector } from "react-redux"
import { fetchDataGetCustomer } from "src/store/apps/customer-information"
import { DeleteDataGrid, EditDataGrid } from 'src/views/component/theme/customEditDelete'

const ListDataTable = (props: ListDataTableProps) => {

  const { setModalConfirm, getDeleteCustomer, setArrRowId } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.customer)

  const handleChangePage = (e, page) => {
    const param: IParamFilterListCustomer = {
      ...store.param,
      pageIndex: page + 1,
    }
    dispatch(fetchDataGetCustomer(param))
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamFilterListCustomer = {
      ...store.param,
      pageSize: Number(event.target.value),
    }
    dispatch(fetchDataGetCustomer(param))
  }

  const handleDelete = (item: IResultDataDetailCustomer) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xóa khách hàng ' + `${item.fullName ? item.fullName : item.phoneNumber}`,
      action: () => { getDeleteCustomer(item.id.toString()) }
    })
  }

  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }) => {

        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission={permissonConfig.CUSTOMER_EDIT} slug={`/customer-information/edit/${row.id}`} />
            <Button color='error' sx={{ p: 0, minWidth: "30px" }} onClick={() => router.push(`/customer-information/${row.id}`)}>
              <Icon icon='mdi:eye-outline' fontSize={20} color="#32475CDE" />
            </Button >
            <DeleteDataGrid permission={permissonConfig.CUSTOMER_DELETE} handleDelete={() => handleDelete(row)} />
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'fullName',
      headerName: 'Tên khách hàng',
      sortable: false,
      renderCell: ({ row }) => {

        return (
          <Box sx={{ display: "flex", alignItems: "center", gap: "10px" }}>
            {row.avatar ? (
              <CustomAvatar
                src={row.avatar}
                variant='rounded'
                alt={row.fullName}
                sx={{ width: 30, height: 30, objectFit: "cover" }}
              />
            ) : (
              <CustomAvatar
                skin='light'
                variant='rounded'
                sx={{ width: 30, height: 30, fontWeight: 600, fontSize: '3rem' }}
              >
              </CustomAvatar>
            )}
            <TypographyDataGrid noWrap >
              {row.fullName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 100,
      field: 'numberOfCarsOwned ',
      sortable: false,
      headerName: 'Số lượng xe sở hữu',
      align: 'center',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.numberOfCarsOwned || 0}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 100,
      field: 'gender',
      headerName: 'Giới tính',
      sortable: false,
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {renderNameGender(row.gender)}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'phoneNumber',
      sortable: false,
      headerName: 'Số điện thoại',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.phoneNumber}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'email',
      sortable: false,
      headerName: 'Email',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >{row.email}</TypographyDataGrid>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 200,
      field: 'address',
      sortable: false,
      headerName: 'Địa chỉ',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.address}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'ward',
      sortable: false,
      headerName: 'Phường/xã',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.ward}
            </TypographyDataGrid>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district',
      sortable: false,
      headerName: 'Quận/huyện',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.district}
            </TypographyDataGrid>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 200,
      field: 'province',
      sortable: false,
      headerName: 'Tỉnh/Thành phố',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.province}
            </TypographyDataGrid>
          </Box>
        )
      }
    }
    , {
      flex: 0.25,
      minWidth: 200,
      field: 'status',
      sortable: false,
      headerName: 'Trạng thái',
      renderCell: ({ row }: any) => {

        return <CustomChip rounded skin='light' size='small' label={renderNameStatus(row.status)} color={row.status === "ACTIVE" ? "success" : "error"} />
      }
    }

  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={store.data || []}
        disableColumnFilter={true}
        columns={columns}
        checkboxSelection
        loading={store.isLoading}
        rowCount={store.totalRecords}
        onRowSelectionModelChange={item => setArrRowId(item)}
        hideFooterPagination
        components={{
          Toolbar: checkUserPermission(permissonConfig.CUSTOMER_EXPORT) && CustomToolbar,
        }}
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          },
          '& .MuiDataGrid-footerContainer': {
            display: "none"
          }
        }}
      />
      <TablePagination
        component="div"
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>


  )
}
export default ListDataTable