
// ** MUI Imports

import Grid from '@mui/material/Grid'

// ** React  

import { useState } from 'react'
import router from 'next/router'

// ** Components 

import CreateCustomerLeft from './CreateCustomerLeft'
import CreateCustomerRight from './CreateCustomerRight'
import ModalNotify from 'src/views/component/modalNotify'

// ** Api  

import { ApiCreateCustomerCms, ApiCustomerUpdateCms } from 'src/api/customer'

// ** Config 

import { ENTERPRISE, INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Interface

import { IBodyDataCreate } from './interface'
import toast from 'react-hot-toast'
import { INIT_VALUE_DATA_BODY_CREATE } from './service'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetCustomer } from 'src/store/apps/customer-information'
import { renderUrlImageUpload } from 'src/configs/functionConfig'

const CreateCustomerMain = () => {
  const dispatch = useDispatch<AppDispatch>();
  const store = useSelector((state: RootState) => state.customer)
  const [valueImg, setValueImg] = useState<string>("")
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)

  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  const handleCreateCustomer = async (data: IBodyDataCreate) => {
    if (!valueImg) {
      toast.error("Bạn chưa thêm ảnh đại diện")
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      let param: IBodyDataCreate = INIT_VALUE_DATA_BODY_CREATE
      if (data.customerTypeId === ENTERPRISE) {
        param = {
          ...data,
          avatar: valueImg
        }
      } else {
        param = {
          ...data,
          avatar: valueImg,
          companyName: "",
          companyPhoneNumber: "",
          companyTaxCode: "",
          companyWebsite: "",
          companyAddress: "",
          companyWardId: "",
          companyDistrictId: "",
          companyProvinceId: "",
        }
      }
      const res = await ApiCreateCustomerCms(param)
      if (res.code === 200 && res.result) {
        setModalNotify({
          isOpen: true,
          title: "Thêm Thành Công",
          textBtn: 'Quay lại',
          actionReturn: () => router.push("/customer-information")
        })
        setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      } else {
        setModalNotify({
          isOpen: true,
          title: "Thêm Thất Bại",
          message: res.message || UNKNOW_ERROR,
          isError: true
        })
        setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      }
    }
  }
  const handleUpdateCustomer = async (data: IBodyDataCreate) => {

    if (!valueImg && !data.avatar) {
      toast.error("Bạn chưa thêm ảnh đại diện")
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      let param: IBodyDataCreate = INIT_VALUE_DATA_BODY_CREATE
      if (data.customerTypeId === ENTERPRISE) {
        param = {
          ...data,
          avatar: valueImg || data.avatar
        }
      } else {
        param = {
          ...param,
          id: data.id,
          avatar: valueImg || renderUrlImageUpload(data.avatar),
          fullName: data.fullName,
          phoneNumber: data.phoneNumber,
          gender: data.gender,
          birthday: data.birthday,
          email: data.email,
          personalTaxCode: data.personalTaxCode,
          address: data.address,
          wardId: data.wardId,
          districtId: data.districtId,
          provinceId: data.provinceId,
          customerTypeId: data.customerTypeId,
          status: data.status,
        }
      }
      const res = await ApiCustomerUpdateCms(param)
      if (res.code === 200 && res.result) {
        setModalNotify({
          isOpen: true,
          title: "Cập nhật thông tin Thành Công",
        })
        setModalConfirm(INIT_STATE_MODAL_CONFIRM)

        dispatch(fetchDataGetCustomer(store.param))
      } else {
        setModalNotify({
          isOpen: true,
          title: "Cập nhật thông tin Thất Bại",
          message: res.message || UNKNOW_ERROR
        })
        setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      }
    }

  }


  return (
    <>
      <Grid item xs={12} md={5} lg={4} p={0}>
        <CreateCustomerLeft setValueImg={setValueImg} />
      </Grid>
      <Grid item xs={12} md={7} lg={8}>
        <CreateCustomerRight
          handleCreateCustomer={handleCreateCustomer}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdateCustomer={handleUpdateCustomer}

        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateCustomerMain