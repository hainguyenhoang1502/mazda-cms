import styled from '@emotion/styled'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import TextField from '@mui/material/TextField'
import { IBodyDataCreate } from './interface'

const MuiTextField = styled(TextField)({
  flex: '0 0 45% '
})
const MuiInputLabel = styled(InputLabel)({
  display: 'flex',
  gap: '5px'
})
const MuiFormControlTextField = styled(FormControl)({
  
  flex: '0 0 45% '
})
const MuiFormControl = styled(FormControl)({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingBottom: '30px'
})

const INIT_VALUE_DATA_BODY_CREATE:IBodyDataCreate={
  avatar: "",
  fullName: "",
  phoneNumber: "",
  gender: "",
  birthday: "",
  email: "",
  personalTaxCode: "",
  address: "",
  wardId: "",
  districtId: "",
  provinceId:  "",
  customerTypeId: "",
  status:  "",
  companyName: "",
  companyPhoneNumber:  "",
  companyTaxCode: "",
  companyWebsite: "",
  companyAddress: "",
  companyWardId:  "",
  companyDistrictId:  "",
  companyProvinceId:  "",
}


export{
    MuiTextField,
    MuiInputLabel,
    MuiFormControlTextField,
    MuiFormControl,
    INIT_VALUE_DATA_BODY_CREATE,
    
}
