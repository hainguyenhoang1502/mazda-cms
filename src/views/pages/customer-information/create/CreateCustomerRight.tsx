
// ** MUI Imports
import Card from "@mui/material/Card"
import CardContent from "@mui/material/CardContent"
import Typography from "@mui/material/Typography"
import Button from "@mui/material/Button"
import Grid from "@mui/material/Grid";

// ** React Imports
import { useContext, useEffect, useState } from "react"

// ** Next Imports
import router from "next/router"

// ** Third Party Import
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';

// ** Components Imports
import ModalConfirm from "src/views/component/modalConfirm"

import { SelectMUI, InputRequiredMUI, DatePickerMUI } from "src/views/component/theme";

// ** Config Imports
import { OPTIONS_GENDER, OPTIONS_STATUS, OPTIONS_TYPE_CUSTOMER } from "src/configs/functionConfig"
import { ENTERPRISE, ERROR_EMAIL_VALUE_INPUT, ERROR_NULL_VALUE_INPUT, ERROR_PHONE_VALUE_INPUT, PERSONAL, regexPhone } from "src/configs/initValueConfig"
import { IOptionsFilter } from "src/configs/typeOption";

// ** Interface Services Imports 
import { INIT_VALUE_DATA_BODY_CREATE } from "./service"
import { CreateCustomerRightProps } from "./interface"

// ** Api Imports
import { ApiGetLocationDistrictByProvinceId, ApiGetLocationWardByDistrictId } from "src/api/refData";

// ** Context Imports
import { EditCustomerContext } from "src/pages/customer-information/edit/[slug]";

// ** Store Imports
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "src/store";
import { fetchDataGetListFilterAllProvince } from "src/store/apps/category/province";




const CreateCustomerRight = (props: CreateCustomerRightProps) => {
  /* Hooks */
  const context = useContext(EditCustomerContext);
  const isEdit = context && context.isEdit;

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.province)

  const { handleCreateCustomer, modalConfirm, setModalConfirm, handleUpdateCustomer } = props
  const [listDistrictPersonal, setListDistrictPersonal] = useState<Array<IOptionsFilter>>([])
  const [listDistrictCompany, setListDistrictCompany] = useState<Array<IOptionsFilter>>([])
  const [listWardPersonal, setListWardPersonal] = useState<Array<IOptionsFilter>>([])
  const [listWardCompany, setListWardCompany] = useState<Array<IOptionsFilter>>([])

  useEffect(() => {
    if (store && (!store.arrFilter || store.arrFilter.length === 0)) {
      dispatch(fetchDataGetListFilterAllProvince())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (context && context.dataCustomer) {
      getListDistricts(context.dataCustomer.provinceId, PERSONAL)
      getListWards(context.dataCustomer.districtId, PERSONAL)
    }
    if (context && context.dataCustomer && context.dataCustomer.customerTypeId === ENTERPRISE) {
      getListDistricts(context.dataCustomer.companyProvinceId, ENTERPRISE)
      getListWards(context.dataCustomer.companyDistrictId, ENTERPRISE)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context && context.dataCustomer])

  const getListDistricts = async (id: number | string, type: string) => {
    const res = await ApiGetLocationDistrictByProvinceId(id)
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      const arr = res.data.map(item => ({
        label: item.locationName,
        value: item.code
      }));
      if (type === PERSONAL) {
        setListDistrictPersonal(arr)
      } else {
        setListDistrictCompany(arr)
      }
    } else {
      if (type === PERSONAL) {
        setListDistrictPersonal([])
      } else {
        setListDistrictCompany([])
      }
    }
  }
  const getListWards = async (id: number | string, type: string) => {
    const res = await ApiGetLocationWardByDistrictId(id)
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      const arr = res.data.map(item => ({
        label: item.locationName,
        value: item.code
      }));
      if (type === PERSONAL) {
        setListWardPersonal(arr)
      } else {
        setListWardCompany(arr)
      }
    } else {
      if (type === PERSONAL) {
        setListWardPersonal([])
      } else {
        setListWardCompany([])
      }
    }
  }



  const onChangeSelectProvince = (event, field) => {
    if (field === "provinceId") {
      getListDistricts(event, PERSONAL)
    } else {
      getListDistricts(event, ENTERPRISE)
    }

  }

  const onChangeSelectDistricts = (event, field) => {
    if (field === "districtId") {
      getListWards(event, PERSONAL)
    } else {
      getListWards(event, ENTERPRISE)
    }
  }

  const SignupSchema = Yup.object().shape({
    fullName: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
    phoneNumber: Yup.string().required(ERROR_NULL_VALUE_INPUT).matches(regexPhone, ERROR_PHONE_VALUE_INPUT).nullable(),
    gender: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT).nullable(),
    birthday: Yup.date().required(ERROR_NULL_VALUE_INPUT).nullable(),
    email: Yup.string().required(ERROR_NULL_VALUE_INPUT).email(ERROR_EMAIL_VALUE_INPUT).nullable(),
    address: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
    wardId: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    districtId: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    provinceId: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    customerTypeId: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    status: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    companyName: Yup.string().when('customerTypeId', {
      is: (val) => val === ENTERPRISE,
      then: Yup.string().required(ERROR_NULL_VALUE_INPUT),
      otherwise: Yup.string().nullable()
    }),
    companyPhoneNumber: Yup.string().when('customerTypeId', {
      is: (val) => val === ENTERPRISE,
      then: Yup.string().required(ERROR_NULL_VALUE_INPUT),
      otherwise: Yup.string().nullable()
    }),
    companyTaxCode: Yup.string().when('customerTypeId', {
      is: (val) => val === ENTERPRISE,
      then: Yup.string().nullable().required(ERROR_NULL_VALUE_INPUT),
      otherwise: Yup.string().nullable()
    }),
    companyAddress: Yup.string().when('customerTypeId', {
      is: (val) => val === ENTERPRISE,
      then: Yup.string().required(ERROR_NULL_VALUE_INPUT),
      otherwise: Yup.string().nullable()
    }),
    companyWardId: Yup.mixed().nullable().when('customerTypeId', {
      is: (val) => val === ENTERPRISE,
      then: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
      otherwise: Yup.string().nullable()
    }),
    companyDistrictId: Yup.mixed().nullable().when('customerTypeId', {
      is: (val) => val === ENTERPRISE,
      then: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
      otherwise: Yup.string().nullable()
    }),
    companyProvinceId: Yup.mixed().nullable().when('customerTypeId', {
      is: (val) => val === ENTERPRISE,
      then: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
      otherwise: Yup.string().nullable()
    }),

  });

  const handleSubmit = async (values) => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? "Bạn đang thao tác Cập nhật thông tin khách hàng" : "Bạn đang thao tác Thêm khách hàng",
      action: () => {
        if (isEdit) {
          return handleUpdateCustomer(values)
        } else {
          return handleCreateCustomer(values)
        }
      }
    })
  }
  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? "Bạn đang thao tác Huỷ cập nhật thông tin khách hàng" : "Bạn đang thao tác Huỷ thêm khách hàng",
      action: () => router.push("/customer-information")
    })
  }

  return (
    <Card>
      <CardContent>
        <Formik
          initialValues={context && context.dataCustomer || INIT_VALUE_DATA_BODY_CREATE}
          onSubmit={values => handleSubmit(values)}
          validationSchema={SignupSchema}
          validateOnBlur
        >
          {(props) => {

            return (
              <Form>
                <Typography noWrap sx={{ fontWeight: 500, textTransform: 'capitalize', padding: "20px 0", fontSize: "24px" }}>
                  Thông Tin Khách Hàng
                </Typography>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="my-fullName"
                      label="Họ và Tên"
                      name="fullName"
                      component={InputRequiredMUI}
                      texterror={props.touched.fullName && props.errors.fullName}
                      texticon="*"
                    />
                  </Grid>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="my-birthday"
                      label="Ngày sinh"
                      name="birthday"
                      component={DatePickerMUI}
                      texterror={props.touched.birthday && props.errors.birthday}
                      texticon="*"
                      value={props.values.birthday}
                      onChange={(event) => {
                        props.setFieldValue("birthday", event);
                      }}
                    />
                  </Grid>
                </Grid>
                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="input-phoneNumber"
                      label="Số điện thoại"
                      name="phoneNumber"
                      component={InputRequiredMUI}
                      disabled={isEdit}
                      texterror={props.touched.phoneNumber && props.errors.phoneNumber}
                      texticon="*"
                    />
                  </Grid>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="input-email"
                      label="Email"
                      name="email"
                      component={InputRequiredMUI}
                      texterror={props.touched.email && props.errors.email}
                      texticon="*"
                    />
                  </Grid>
                </Grid>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="gender"
                      label="Giới tính"
                      as="select"
                      component={SelectMUI}
                      texticon="*"
                      name="gender"
                      texterror={props.touched.gender && props.errors.gender}
                      value={props.values.gender}
                      onChange={(event) => {
                        props.setFieldValue("gender", event.target.value);
                      }}
                      options={OPTIONS_GENDER}
                    />
                  </Grid>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="input-personalTaxCode"
                      label="Mã số thuế cá nhân"
                      name="personalTaxCode"
                      component={InputRequiredMUI}
                    />
                  </Grid>
                </Grid>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="provinceId"
                      label="Tỉnh/Thành phố"
                      component={SelectMUI}
                      texticon="*"
                      name="provinceId"
                      texterror={props.touched.provinceId && props.errors.provinceId}
                      options={store.arrFilter}
                      value={props.values.provinceId}
                      onChange={(event) => {
                        props.setFieldValue("provinceId", event.target.value);
                        props.setFieldValue("districtId", "");
                        props.setFieldValue("wardId", "");
                        onChangeSelectProvince(event.target.value, "provinceId")
                      }}
                    />
                  </Grid>

                  <Grid item lg={6} xs={12}>
                    <Field
                      id="districtId"
                      label="Quận/huyện"
                      component={SelectMUI}
                      texticon="*"
                      name="districtId"
                      texterror={props.touched.districtId && props.errors.districtId}
                      options={listDistrictPersonal}
                      value={props.values.districtId}
                      onChange={(event) => {
                        props.setFieldValue("districtId", event.target.value);
                        props.setFieldValue("wardId", "");
                        onChangeSelectDistricts(event.target.value, "districtId")
                      }}
                    />
                  </Grid>
                </Grid>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="wardId"
                      label="Phường/xã"
                      component={SelectMUI}
                      texticon="*"
                      name="wardId"
                      texterror={props.touched.wardId && props.errors.wardId}
                      options={listWardPersonal}
                      value={props.values.wardId}
                      onChange={(event) => {
                        props.setFieldValue("wardId", event.target.value);
                      }}
                    />
                  </Grid>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="input-address"
                      label="Địa chỉ"
                      name="address"
                      component={InputRequiredMUI}
                      texterror={props.touched.address && props.errors.address}
                      texticon="*"
                    />
                  </Grid>

                </Grid>
                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="customerTypeId"
                      label="Khách hàng"
                      component={SelectMUI}
                      texticon="*"
                      name="customerTypeId"
                      texterror={props.touched.customerTypeId && props.errors.customerTypeId}
                      options={OPTIONS_TYPE_CUSTOMER}
                      value={props.values.customerTypeId}
                      onChange={(event) => {
                        props.setFieldValue("customerTypeId", event.target.value);
                      }}
                    />
                  </Grid>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="status"
                      label="Trạng thái"
                      component={SelectMUI}
                      texticon="*"
                      name="status"
                      texterror={props.touched.status && props.errors.status}
                      options={OPTIONS_STATUS}
                      value={props.values.status}
                      onChange={(event) => {
                        props.setFieldValue("status", event.target.value);
                      }}
                    />
                  </Grid>
                </Grid>
                {
                  props.values.customerTypeId === ENTERPRISE &&
                  <>
                    <Typography noWrap sx={{ fontWeight: 500, textTransform: 'capitalize', padding: "20px 0", fontSize: "24px" }}>
                      Thông Tin Doanh Nghiệp
                    </Typography>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-companyName"
                          label="Tên công ty"
                          name="companyName"
                          component={InputRequiredMUI}
                          texterror={props.touched.companyName && props.errors.companyName}
                          texticon="*"
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-companyPhoneNumber"
                          label="Số điện thoại công ty"
                          name="companyPhoneNumber"
                          component={InputRequiredMUI}
                          texterror={props.touched.companyPhoneNumber && props.errors.companyPhoneNumber}
                          texticon="*"
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-companyTaxCode"
                          label="Mã số thuế công ty"
                          name="companyTaxCode"
                          component={InputRequiredMUI}
                          texterror={props.touched.companyTaxCode && props.errors.companyTaxCode}
                          texticon="*"

                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-website"
                          label="Website"
                          name="companyWebsite"
                          component={InputRequiredMUI}
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="companyProvinceId"
                          label="Tỉnh/Thành phố"
                          component={SelectMUI}
                          texticon="*"
                          name="companyProvinceId"
                          texterror={props.touched.companyProvinceId && props.errors.companyProvinceId}
                          options={store.arrFilter}

                          value={props.values.companyProvinceId}
                          onChange={(event) => {
                            props.setFieldValue("companyProvinceId", event.target.value);
                            props.setFieldValue("companyDistrictId", "");
                            props.setFieldValue("companyWardId", "");

                            onChangeSelectProvince(event.target.value, "companyProvinceId")
                          }}
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="companyDistrictId"
                          label="Quận/huyện"
                          component={SelectMUI}
                          texticon="*"
                          name="companyDistrictId"
                          texterror={props.touched.companyDistrictId && props.errors.companyDistrictId}
                          options={listDistrictCompany}
                          value={props.values.companyDistrictId}
                          onChange={(event) => {
                            props.setFieldValue("companyDistrictId", event.target.value);
                            props.setFieldValue("companyWardId", "");
                            onChangeSelectDistricts(event.target.value, "companyDistrictId")
                          }}
                        />
                      </Grid>

                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="companyWardId"
                          label="Phường/xã"
                          component={SelectMUI}
                          texticon="*"
                          name="companyWardId"
                          texterror={props.touched.companyWardId && props.errors.companyWardId}
                          options={listWardCompany}
                          value={props.values.companyWardId}
                          onChange={(event) => {
                            props.setFieldValue("companyWardId", event.target.value);
                          }}
                        />
                      </Grid>

                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-website"
                          label="Địa chỉ"
                          name="companyAddress"
                          component={InputRequiredMUI}
                          texterror={props.touched.companyAddress && props.errors.companyAddress}
                          texticon="*"

                        />
                      </Grid>
                    </Grid>
                  </>
                }
                <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
                  <Button variant="contained" type="submit">Lưu</Button>
                  <Button color="error" variant="outlined" onClick={handleCancel} >Hủy</Button>
                </CardContent>
              </Form>
            )
          }
          }
        </Formik>

      </CardContent>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Card>

  )
}
export default CreateCustomerRight