import { ModalConfirmState } from "src/configs/typeOption"

interface IBodyDataCreate {
  id?:string
  avatar: string
  fullName: string
  phoneNumber: number | string
  gender: string
  birthday: string
  email: string
  personalTaxCode: string
  address: string
  wardId: string
  districtId: string
  provinceId: string
  customerTypeId: string
  status: string
  companyName: string
  companyPhoneNumber: string
  companyTaxCode: string
  companyWebsite: string
  companyAddress: string
  companyWardId: string
  companyDistrictId: string
  companyProvinceId: string
}
interface IBodyDataCreateError {
  avatar: string
  fullName: string
  phoneNumber: string
  gender: string
  birthday: string
  email: string
  address: string
  wardId: string
  districtId: string
  provinceId: string
  customerTypeId: string
  status: string
  companyName: string
  companyPhoneNumber: string
  companyTaxCode: string
  companyAddress: string
  companyWardId: string
  companyDistrictId: string
  companyProvinceId: string
}

interface PickerProps {
  label?: string
  error?: boolean
  registername?: string
}
interface CreateCustomerRightProps {
  handleCreateCustomer:Function
  setModalConfirm:Function,
  modalConfirm:ModalConfirmState
  handleUpdateCustomer:Function
}
interface CreateCustomerLeftProps {
  setValueImg:Function
}
export type {
  IBodyDataCreate,
  PickerProps,
  IBodyDataCreateError,
  CreateCustomerRightProps,
  CreateCustomerLeftProps
}
