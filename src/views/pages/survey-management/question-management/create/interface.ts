// import { ModalConfirmState } from "src/configs/typeOption"

interface IBodyDataCreate {
  status: string,
  document: string,
}
export type {
  IBodyDataCreate,
}
