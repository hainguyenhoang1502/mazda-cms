import { Box, Button, Card, CardContent, Grid, Typography } from '@mui/material';
import { Field, Form, Formik } from 'formik';
import React, { useState } from 'react';
import { SelectMUI } from 'src/views/component/theme'
import { INIT_VALUE_DATA_BODY_CREATE } from './service';
import * as Yup from 'yup'
import { ERROR_NULL_VALUE_INPUT } from 'src/configs/initValueConfig';
import { OPTIONS_DOCUMENT, OPTIONS_STATUS } from 'src/configs/functionConfig';

import EditorControlled from 'src/components/Editor';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { EditorState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import { toast } from 'react-hot-toast';
import IconifyIcon from 'src/@core/components/icon';

const handleCancel = () => {
    console.log('cancel');
}

const CreateQuestionSchema = Yup.object().shape({
    status: Yup.mixed().required(ERROR_NULL_VALUE_INPUT),
    document: Yup.mixed().required(ERROR_NULL_VALUE_INPUT)
})

const ansItem = {
    'question-id': new Date().getTime(),
    contentEditor: EditorState.createEmpty(),
    placeholder: "Câu trả lời",
    content: ''
}

const CreateQuestionMainForm = () => {
    const [valueEdit, setValueEdit] = useState<EditorState>(EditorState.createEmpty())
    const [markupValue, setMarkupValue] = useState<string>("")
    const [typeQuestion, setTypeQuestion] = useState<string>("VANBAN");
    const [answerList, setAnswerList] = useState([ansItem])

    const handleChangeInputContentItem = (value: EditorState, index: number) => {
        const arr = [...answerList]
        arr[index].contentEditor = value
        const rawContentState = convertToRaw(value.getCurrentContent());
        const markup = draftToHtml(rawContentState);
        arr[index].content = markup
        setAnswerList(arr)
    }


    const handleAddQuestion = () => {
        const arr = [...answerList]
        const value = { ...ansItem }
        value['question-id'] = new Date().getTime();
        arr.push(value)
        setAnswerList(arr)
    }

    const handleDeleteQuestionItem = (id: number) => {
        const arr = answerList.filter(e => e['question-id'] !== id);
        if (arr.length === 0) {
            return toast.error("Phải có ít nhất 1 câu trả lời")
        }
        setAnswerList(arr)
    }

    const handleSubmit = () => {
        const rawValueEdit = convertToRaw(valueEdit.getCurrentContent());
        console.log(markupValue);
        for (let i = 0; i < answerList.length; i++) {
            const rawValueAnswer = convertToRaw(answerList[i].contentEditor.getCurrentContent())
            if (!rawValueAnswer.blocks[0].text && typeQuestion !== "VANBAN") {
                toast.error("Chưa đặt câu trả lời")
                break;
            }
        }
        if (!rawValueEdit.blocks[0].text) {
            toast.error("Chưa đặt câu hỏi")
        }
    }

    const handleChangeInputContent = (value: EditorState) => {
        const rawContentState = convertToRaw(value.getCurrentContent());
        const markup = draftToHtml(rawContentState);
        setValueEdit(value)
        setMarkupValue(markup)
    }

    return (
        <Card>
            <Formik
                initialValues={INIT_VALUE_DATA_BODY_CREATE}
                onSubmit={() => handleSubmit()}
                validationSchema={CreateQuestionSchema}
                validateOnBlur
            >
                {props => {
                    return (
                        <Form>
                            <CardContent>
                                <Grid item xs={12} >
                                    <EditorControlled value={valueEdit} setValue={(e) => handleChangeInputContent(e)} placeholder='Câu 1: Anh/ Chị Vui lòng cho biết khu vực anh chị đang sống?' />
                                </Grid>

                                <Grid item container spacing={6} marginBottom={5}>
                                    {/* questtion type */}
                                    <Grid item lg={12} xs={12}>
                                        <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '16px' }}>
                                            Loại câu hỏi
                                        </Typography>
                                        <Field
                                            id='my-document'
                                            label='Văn bản'
                                            component={SelectMUI}
                                            texticon='*'
                                            name='document'
                                            texterror={props.touched.document && props.errors.document}
                                            options={OPTIONS_DOCUMENT}
                                            value={props.values.document}
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                                props.setFieldValue('document', event.target.value)
                                                setTypeQuestion(event.target.value)
                                            }}
                                        />
                                    </Grid>

                                    {/* questrion list (flexible) */}

                                    {typeQuestion !== "VANBAN" &&
                                        <Grid item lg={12} xs={12}>
                                            <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '16px' }}>
                                                Danh sách câu trả lời
                                            </Typography>
                                            <Box>
                                                {answerList.map((child, index) => (
                                                    <Box sx={{ display: 'flex', alignItems: 'center', gap: 5, userSelect: 'none' }} key={child['question-id']}>
                                                        <EditorControlled value={child.contentEditor} setValue={(e) => handleChangeInputContentItem(e, index)} placeholder={child.placeholder} />
                                                        <IconifyIcon icon='zondicons:minus-outline' style={{ flexShrink: 0 }} cursor='pointer' onClick={() => handleDeleteQuestionItem(child['question-id'])} />
                                                    </Box>
                                                ))}

                                                <Button sx={{ background: 'transparent', width: '100%', color: "#32475C", display: 'flex', alignItems: 'center', marginTop: 5, gap: 2, border: '1px solid #757575', borderRadius: '3px' }} onClick={handleAddQuestion}>
                                                    <IconifyIcon icon='akar-icons:plus' style={{ flexShrink: 0 }} cursor='pointer' />
                                                    Thêm câu trả lời
                                                </Button>
                                            </Box>
                                        </Grid>}



                                    {/*=== status === */}
                                    <Grid item lg={12} xs={12}>
                                        <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '16px' }}>
                                            Trạng thái
                                        </Typography>
                                        <Field
                                            id='status'
                                            label='Trạng thái'
                                            component={SelectMUI}
                                            texticon='*'
                                            name='status'
                                            texterror={props.touched.status && props.errors.status}
                                            options={OPTIONS_STATUS}
                                            value={props.values.status}
                                            onChange={event => {
                                                props.setFieldValue('status', event.target.value)
                                            }}
                                        />
                                    </Grid>
                                </Grid>
                            </CardContent>

                            <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                                <Button variant='contained' type='submit'>
                                    Lưu
                                </Button>
                                <Button color='error' variant='outlined' onClick={handleCancel}>
                                    Hủy
                                </Button>
                            </CardContent>
                        </Form>
                    )
                }}
            </Formik>

            {/* <ModalConfirm
                isOpen={modalConfirm.isOpen}
                message={modalConfirm.title}
                toggle={() => handleToggleModal()}
                actionReturn={modalConfirm.action}
            /> */}

        </Card>
    );
};

export default CreateQuestionMainForm;