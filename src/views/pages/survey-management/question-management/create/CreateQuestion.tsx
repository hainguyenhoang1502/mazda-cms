import React from 'react';
import CreateQuestionMainForm from './CreateQuestionMainForm';
import { Grid } from '@mui/material';

const CreateQuestionMain = () => {
    return (
        <Grid item xs={12} p={0}>
           <CreateQuestionMainForm />
        </Grid>
    );
};

export default CreateQuestionMain;