import { Box, Button, Card, CardContent, Grid, InputAdornment, TextField, Typography } from '@mui/material';
import { Field, Form, Formik } from 'formik';
import React, { forwardRef, useState } from 'react';
import { InputRequiredMUI, SelectMUI, TypographyDataGrid } from 'src/views/component/theme'
import { INIT_VALUE_DATA_BODY_CREATE } from './service';
import * as Yup from 'yup'
import { ERROR_NULL_VALUE_INPUT, INIT_STATE_MODAL_CONFIRM } from 'src/configs/initValueConfig';
import { OPTIONS_STATUS } from 'src/configs/functionConfig';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker';
import DatePicker from 'react-datepicker'
import { ModalConfirmState, PickerProps } from 'src/configs/typeOption';
import Icon from 'src/@core/components/icon'
import { IParamValuesFilter } from '../list/interface';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { checkUserPermission } from 'src/@core/utils/permission';
import ModalSurvey from 'src/views/component/modalSurvey';

const handleCancel = () => {
    console.log('cancel');
}

const CreateQuestionSchema = Yup.object().shape({
    status: Yup.mixed().required(ERROR_NULL_VALUE_INPUT),
    document: Yup.mixed().required(ERROR_NULL_VALUE_INPUT)
})

// const ansItem = {
//     'question-id': new Date().getTime(),
//     contentEditor: EditorState.createEmpty(),
//     placeholder: "Câu trả lời",
//     content: ''
// }

const CreateSurveyForm = () => {
    // const [valueEdit, setValueEdit] = useState<EditorState>(EditorState.createEmpty())
    // const [markupValue, setMarkupValue] = useState<string>("")
    // const [typeQuestion, setTypeQuestion] = useState<string>("VANBAN");
    // const [answerList, setAnswerList] = useState([ansItem])

    const [valuesFilter, setValuesFilter] = useState<IParamValuesFilter>({
        keyword: "",
        startDate: null,
        endDate: null
    })
    const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

    // const handleChangeInputContentItem = (value: EditorState, index: number) => {
    //     const arr = [...answerList]
    //     arr[index].contentEditor = value
    //     const rawContentState = convertToRaw(value.getCurrentContent());
    //     const markup = draftToHtml(rawContentState);
    //     arr[index].content = markup
    //     setAnswerList(arr)
    // }


    // const handleAddQuestion = () => {
    //     const arr = [...answerList]
    //     const value = { ...ansItem }
    //     value['question-id'] = new Date().getTime();
    //     arr.push(value)
    //     setAnswerList(arr)
    // }

    // const handleDeleteQuestionItem = (id: number) => {
    //     const arr = answerList.filter(e => e['question-id'] !== id);
    //     if (arr.length === 0) {
    //         return toast.error("Phải có ít nhất 1 câu trả lời")
    //     }
    //     setAnswerList(arr)
    // }

    const handleSubmit = () => {

        // const rawValueEdit = convertToRaw(valueEdit.getCurrentContent());
        // console.log(markupValue);
        // for (let i = 0; i < answerList.length; i++) {
        //     const rawValueAnswer = convertToRaw(answerList[i].contentEditor.getCurrentContent())
        //     if (!rawValueAnswer.blocks[0].text && typeQuestion !== "VANBAN") {
        //         toast.error("Chưa đặt câu trả lời")
        //         break;
        //     }
        // }
        // if (!rawValueEdit.blocks[0].text) {
        //     toast.error("Chưa đặt câu hỏi")
        // }
    }

    // const handleChangeInputContent = (value: EditorState) => {
    //     const rawContentState = convertToRaw(value.getCurrentContent());
    //     const markup = draftToHtml(rawContentState);
    //     setValueEdit(value)
    //     setMarkupValue(markup)
    // }

    const PickersComponent = forwardRef(({ ...props }: PickerProps, ref) => {
        return (
            <TextField
                size='small'
                inputRef={ref}
                fullWidth
                {...props}
                label={props.label || ''}
                error={props.error}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <Icon icon='bx:calendar' fontSize={20} />
                        </InputAdornment>
                    ),
                }}
            />
        )
    })

    const handleChangeInput = (value, field) => {
        const body = { ...valuesFilter }
        body[field] = value
        setValuesFilter(body)
    }

    const canDelete = checkUserPermission("")

    const handleDelete = () => {
        console.log('cancel');

    }
    const handleToggleModal = () => {
        setModalConfirm({
            isOpen: !modalConfirm.isOpen,
            title: '',
            action: () => {
                return
            }
        })
    }

    const columns: GridColDef[] = [
        {
            flex: 0.25,
            minWidth: 400,
            field: 'question',
            sortable: false,
            headerName: 'Câu hỏi',
            renderCell: ({ row }) => {
                return (
                    <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px', flexWrap: "wrap", whiteSpace: 'pre-wrap' }}>
                        <TypographyDataGrid wrap='wrap'>
                            {row.question}
                        </TypographyDataGrid>
                    </Box>
                )
            }
        },
        {
            flex: 0.25,
            minWidth: 150,
            field: 'questionType',
            headerName: 'Loại câu hỏi',
            sortable: false,
            renderCell: ({ row }) => {
                return (
                    <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
                        <TypographyDataGrid noWrap>
                            {row.questionType}
                        </TypographyDataGrid>
                    </Box>
                )
            }
        },
        {
            flex: 0.5,
            minWidth: 200,
            field: 'questionList',
            sortable: false,
            headerName: 'danh sách câu trả lời',
            align: 'left',
            renderCell: ({ row }: any) => {
                return (
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        <TypographyDataGrid
                            noWrap
                        >
                            {row.questionList}
                        </TypographyDataGrid>
                    </Box>
                )
            }
        },
        {
            flex: 0.25,
            minWidth: 200,
            field: 'action',
            headerName: 'hành động',
            sortable: false,
            renderCell: () => {
                return (
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        {
                            canDelete &&
                            <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                                onClick={() => handleDelete()}>
                                <Icon icon='bx:trash' fontSize={20} color="#32475CDE" />
                            </Button>
                        }
                    </Box>
                )
            }
        },
    ]

    const rows = [
        { id: 1, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống ?', questionType: 'Văn bản', questionList: 'Văn bản' },
        { id: 2, question: 'Anh/ Chị có dự định tham khảo dòng xe nào từ Mazda không?', questionType: 'Văn bản', questionList: 'Văn bản' },
        { id: 3, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
        { id: 4, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
        { id: 5, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
        { id: 6, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
        { id: 7, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
        { id: 8, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
        { id: 9, question: 'Anh/ Chị vui lòng cho biết khu vực anh chị đang sống', questionType: 'Văn bản', questionList: 'Văn bản' },
    ];

    return (
        <Box>
            <Grid container xs={12} spacing={6}>
                <Grid item xs={6}>
                    <Card style={{ height: '100%' }}>
                        <Formik
                            initialValues={INIT_VALUE_DATA_BODY_CREATE}
                            onSubmit={() => handleSubmit()}
                            validationSchema={CreateQuestionSchema}
                            validateOnBlur
                        >
                            {props => {
                                return (
                                    <Form>
                                        <Typography noWrap sx={{ fontWeight: 500, padding: '20px', fontSize: '24px' }}>
                                            Thông Tin Khảo Sát
                                        </Typography>

                                        <CardContent>
                                            <Grid container xs={12} spacing={6}>
                                                <Grid item lg={12} xs={12}>
                                                    <Field
                                                        id='my-locationName'
                                                        label='Tiêu đề khảo sát'
                                                        name='surveyTitle'
                                                        component={InputRequiredMUI}
                                                        texticon='*'

                                                    // texterror={props.touched.locationName && props.errors.locationName}
                                                    />
                                                </Grid>
                                                <Grid xs={12} item>
                                                    <DatePickerWrapper sx={{ display: 'flex', gap: "1rem", width: "100%", mr: 4, maxWidth: "100%" }}>
                                                        <DatePicker
                                                            selectsStart
                                                            id='event-start-date'
                                                            selected={valuesFilter.startDate ? new Date(valuesFilter.startDate) : null}
                                                            dateFormat="dd/MM/yyyy"
                                                            maxDate={valuesFilter.endDate && new Date(valuesFilter.endDate)}
                                                            customInput={<PickersComponent label='Từ ngày' registername='startDate' />}
                                                            onChange={(date: Date) => handleChangeInput(new Date(date), "startDate")}
                                                        />
                                                        <DatePicker
                                                            selectsStart
                                                            id='event-start-date'
                                                            selected={valuesFilter.endDate ? new Date(valuesFilter.endDate) : null}
                                                            dateFormat="dd/MM/yyyy"
                                                            minDate={valuesFilter.startDate && new Date(valuesFilter.startDate)}
                                                            customInput={<PickersComponent label='Đến ngày' registername='endDate' />}
                                                            onChange={(date: Date) => handleChangeInput(new Date(date), "endDate")}
                                                        />
                                                    </DatePickerWrapper>
                                                </Grid>
                                                <Grid item lg={12} xs={12}>
                                                    <Field
                                                        id='status'
                                                        label='Trạng thái'
                                                        component={SelectMUI}
                                                        texticon='*'
                                                        name='status'
                                                        texterror={props.touched.status && props.errors.status}
                                                        options={OPTIONS_STATUS}
                                                        value={props.values.status}
                                                        onChange={event => {
                                                            props.setFieldValue('status', event.target.value)
                                                        }}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </CardContent>


                                    </Form>
                                )
                            }}
                        </Formik>
                    </Card>
                </Grid>
                <Grid container item xs={6} spacing={6}>
                    <Grid item xs={12}>
                        <Card style={{ padding: "15px" }}>
                            <Button variant="contained" onClick={() => setModalConfirm((modalConfirm) => ({ ...modalConfirm, isOpen: true }))}>Thêm câu hỏi</Button>
                        </Card>
                    </Grid>

                    <Grid item xs={12}>
                        <Card>
                            <DataGrid
                                autoHeight
                                rows={(rows || [])}
                                disableColumnFilter={true}
                                columns={columns}

                                // loading={store.isLoading}
                                checkboxSelection
                                rowCount={10}
                                hideFooterPagination
                                sx={{
                                    '& .MuiDataGrid-virtualScrollerContent': {
                                        marginBottom: '10px'
                                    },
                                    '& .MuiDataGrid-footerContainer': {
                                        display: 'none'
                                    }
                                }}
                            />
                        </Card>
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                    <Card sx={{ display: 'flex', justifyContent: 'center', gap: '25px', padding: 4 }}>
                        <Button variant='contained' type='submit'>
                            Lưu
                        </Button>
                        <Button color="secondary" variant='outlined' onClick={handleCancel}>
                            Hủy
                        </Button>
                    </Card>
                </Grid>
            </Grid>

            {/* <ModalConfirm
                isOpen={modalConfirm.isOpen}
                message={modalConfirm.title}
                toggle={() => handleToggleModal()}
                actionReturn={modalConfirm.action}
            /> */}

            <ModalSurvey
                isOpen={modalConfirm.isOpen}
                message={modalConfirm.title}
                toggle={() => handleToggleModal()}
                actionReturn={modalConfirm.action}
                rows={rows}
                columns={columns}

            />
        </Box>
    );
};

export default CreateSurveyForm;