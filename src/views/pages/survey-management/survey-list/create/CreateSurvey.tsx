import React from 'react';
import CreateSurveyForm from './CreateSurveyForm';
import { Grid } from '@mui/material';

const CreateSurveyMain = () => {
    return (
        <Grid item xs={12} p={0}>
           <CreateSurveyForm />
        </Grid>
    );
};

export default CreateSurveyMain;