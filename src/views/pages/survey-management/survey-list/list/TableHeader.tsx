
// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment, Typography } from '@mui/material'

// ** React Imports
import { forwardRef, useState } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** Third Party Import
import toast from 'react-hot-toast';

// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Styles
import DatePicker from 'react-datepicker'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'

// ** Interface Services Imports 
import { IParamValuesFilter, TableHeaderProps } from './interface'

// ** Utils
import { checkUserPermission } from 'src/@core/utils/permission'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetCustomer } from 'src/store/apps/customer-information'
import { SelectMUI } from 'src/views/component/theme'
import { OPTIONS_STATUS } from 'src/configs/functionConfig'
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import { PickerProps } from 'src/configs/typeOption'


const TableHeader = (props: TableHeaderProps) => {

  /* Props */
  const { arrRowId, setModalConfirm, getDeleteCustomer } = props


  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.customer)


  const [valuesFilter, setValuesFilter] = useState<IParamValuesFilter>({
    keyword: "",
    startDate: null,
    endDate: null
  })

  const OPTIONS_STATUS_FILTER = [
    {
      value: null,
      label: 'Tất cả'
    },
    ...OPTIONS_STATUS,
  ]
  const handleChangeInput = (value, field) => {
    const body = { ...valuesFilter }
    body[field] = value
    setValuesFilter(body)
  }
  const handleFilterDate = () => {
    const param = {
      ...store.param,
      ...valuesFilter,
    }
    dispatch(fetchDataGetCustomer(param))
  }
  const handleClearFilter = () => {
    const param = {
      ...store.param,
      keyword: "",
      startDate: null,
      endDate: null
    }
    setValuesFilter(param)
    dispatch(fetchDataGetCustomer(param))

  }
  const handleDelete = () => {
    if (arrRowId.length <= 0) {
      toast.error("Bạn chưa chọn khách hàng cần xóa")
    } else {
      setModalConfirm({
        isOpen: true,
        title: `Bạn đang thao tác Xóa ${arrRowId.length} khách hàng `,
        action: () => { getDeleteCustomer(arrRowId.toString()) }
      })
    }
  }

  const PickersComponent = forwardRef(({ ...props }: PickerProps, ref) => {
    return (
      <TextField
        size='small'
        inputRef={ref}
        fullWidth
        {...props}
        label={props.label || ''}
        error={props.error}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Icon icon='bx:calendar' fontSize={20} />
            </InputAdornment>
          ),
        }}
      />
    )
  })

  return (
    <Box sx={{
      p: 5,
      pb: 3,
      width: '100%',
    }}>
      <Box sx={{ width: '100%', mb: 4 }}>
        <Typography noWrap sx={{ fontWeight: 500, padding: '5px 0', fontSize: '20px' }}>
          Thông tin khảo sát
        </Typography>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          justifyContent: 'space-between',
          gap: "25px",
          mb: 4
        }}
      >
        <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
          <TextField
            size='small'
            value={valuesFilter.keyword}
            sx={{ mr: 4, width: "20%" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Icon icon='bx:search' fontSize={20} />
                </InputAdornment>
              ),
            }}
            placeholder='Tìm kiếm…'
            onChange={(e) => handleChangeInput(e.target.value, "keyword")}
          />
          <DatePickerWrapper sx={{ display: 'flex', gap: "1rem", width: "60%", mr: 4, maxWidth: "320px" }}>
            <DatePicker
              selectsStart
              id='event-start-date'
              selected={valuesFilter.startDate ? new Date(valuesFilter.startDate) : null}
              dateFormat="dd/MM/yyyy"
              maxDate={valuesFilter.endDate && new Date(valuesFilter.endDate)}
              customInput={<PickersComponent label='Từ ngày' registername='startDate' />}
              onChange={(date: Date) => handleChangeInput(new Date(date), "startDate")}
            />
            <DatePicker
              selectsStart
              id='event-start-date'
              selected={valuesFilter.endDate ? new Date(valuesFilter.endDate) : null}
              dateFormat="dd/MM/yyyy"
              minDate={valuesFilter.startDate && new Date(valuesFilter.startDate)}
              customInput={<PickersComponent label='Đến ngày' registername='endDate' />}
              onChange={(date: Date) => handleChangeInput(new Date(date), "endDate")}
            />
          </DatePickerWrapper>
          <Box sx={{ mr: 4 }}>
            <SelectMUI
              label='Trạng thái'
              options={OPTIONS_STATUS_FILTER}
              size='small'
              onChange={e => {
                handleChangeInput(e.target.value, 'status')
              }}
            />
          </Box>
          <Button variant="outlined" onClick={handleFilterDate} sx={{ mr: 4 }} >
            Lọc
          </Button>
          <Button variant="outlined" onClick={handleClearFilter} color='error'>
            Xóa bộ lọc
          </Button>
        </Box>
        <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', gap: "1rem", justifyContent: "end", maxWidth: "100%" }}>
          {
            checkUserPermission(permissonConfig.CUSTOMER_ADD) &&
            <Link href="survey-list/create" as={"survey-list/create"}>
              <Button variant="contained">
               TẠO KHẢO SÁT
              </Button>
            </Link>
          }
          {
            checkUserPermission(permissonConfig.CUSTOMER_DELETE) &&
            <Button variant="contained" color='error' onClick={handleDelete}>
              Xóa
            </Button>
          }
        </Box>
      </Box>
    </Box>
  )
}

export default TableHeader
