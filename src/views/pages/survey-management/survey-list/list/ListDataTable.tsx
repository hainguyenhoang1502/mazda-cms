import { Box, Button, TablePagination, Typography } from "@mui/material"
import CustomChip from 'src/@core/components/mui/chip'
import Icon from 'src/@core/components/icon'
import router from "next/router"
import { ListDataTableProps } from "./interface"
import { IParamFilterListCustomer, IResultDataDetailCustomer } from "src/api/customer/type"
import { renderNameGender, renderNameStatus } from "src/configs/functionConfig"
import { DataGrid, GridColDef } from "@mui/x-data-grid"
import { checkUserPermission } from "src/@core/utils/permission"
import { permissonConfig } from "src/configs/roleConfig"

// ** Components 
import CustomAvatar from 'src/@core/components/mui/avatar'
import { useDispatch, useSelector } from "react-redux"
import { AppDispatch, RootState } from "src/store"
import { fetchDataGetCustomer } from "src/store/apps/customer-information"



const ListDataTable = (props: ListDataTableProps) => {

  const { setModalConfirm, getDeleteCustomer, setArrRowId } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.customer)

  const handleChangePage = (e, page) => {
    const param: IParamFilterListCustomer = {
      ...store.param,
      pageIndex: page + 1,
    }
    dispatch(fetchDataGetCustomer(param))
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamFilterListCustomer = {
      ...store.param,
      pageSize: Number(event.target.value),
    }
    dispatch(fetchDataGetCustomer(param))
  }

  const handleDelete = (item: IResultDataDetailCustomer) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xóa khách hàng ' + `${item.fullName ? item.fullName : item.phoneNumber}`,
      action: () => { getDeleteCustomer(item.id.toString()) }
    })
  }

  // ** Permission 
  const canEdit = checkUserPermission(permissonConfig.CUSTOMER_EDIT)
  const canDelete = checkUserPermission(permissonConfig.CUSTOMER_DELETE)
  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }) => {

        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            {
              canEdit &&
              <Button
                sx={{ p: 0, minWidth: "30px" }}
                onClick={() => router.push(`/customer-information/edit/${row.id}`)}
              >
                <Icon icon='bx:pencil' fontSize={20} color="#32475CDE" />
              </Button >
            }
            {
              canDelete &&
              <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                onClick={() => handleDelete(row)}>
                <Icon icon='bx:trash' fontSize={20} color="#32475CDE" />
              </Button>
            }
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'fullName',
      headerName: 'Tiêu đề',
      sortable: false,
      renderCell: ({ row }) => {

        return (
          <Box sx={{ display: "flex", alignItems: "center", gap: "10px" }}>
            {row.avatar ? (
              <CustomAvatar
                src={row.avatar}
                variant='rounded'
                alt={row.fullName}
                sx={{ width: 30, height: 30, objectFit: "cover" }}
              />
            ) : (
              <CustomAvatar
                skin='light'
                variant='rounded'
                sx={{ width: 30, height: 30, fontWeight: 600, fontSize: '3rem' }}
              >
              </CustomAvatar>
            )}
            <Typography noWrap sx={{ fontWeight: 400, color: 'text.secondary', textTransform: 'capitalize' }}>
              {row.fullName}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'numberOfCarsOwned ',
      sortable: false,
      headerName: 'Thời gian bắt đầu',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.numberOfCarsOwned || 0}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'gender',
      headerName: 'Thời gian kết thúc',
      sortable: false,
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {renderNameGender(row.gender)}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'phoneNumber',
      sortable: false,
      headerName: 'số lượng câu hỏi',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.phoneNumber}
            </Typography>
          </Box>
        )
      }
    }, 
    {
      flex: 0.25,
      minWidth: 200,
      field: 'status',
      sortable: false,
      headerName: 'Trạng thái',
      renderCell: ({ row }: any) => {
        return <CustomChip rounded skin='light' size='small' label={renderNameStatus(row.status)} color={row.status === "ACTIVE" ? "success" : "error"} />
      }
    }

  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={store.data || []}
        disableColumnFilter={true}
        columns={columns}
        checkboxSelection
        loading={store.isLoading}
        rowCount={store.totalRecords}
        onRowSelectionModelChange={item => setArrRowId(item)}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          },
          '& .MuiDataGrid-footerContainer': {
            display: "none"
          }
        }}
      />
      <TablePagination
        component="div"
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>


  )
}
export default ListDataTable