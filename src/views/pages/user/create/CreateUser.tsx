// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React  
import { useState } from 'react'
import router from 'next/router'

// ** Components 
import CreateLeft from './CreateUserLeft'
import ModalNotify from 'src/views/component/modalNotify'

// ** Api  
import { ApiCreateUserCms, ApiUpdateUserCms } from 'src/api/authen'

// ** Config 
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Interface
import { IBodyDataCreate } from './interface'
import CreateUserRight from './CreateUserRight'
import { handleSetReloadDataUser } from 'src/store/apps/user'
import { useDispatch } from 'react-redux'
import { AppDispatch } from 'src/store'
import { renderUrlImageUpload } from 'src/configs/functionConfig'

const CreateMain = () => {

  const [valueImg, setValueImg] = useState<string>("")
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()

  const handleCreate = async (data: IBodyDataCreate, arrOrg: Array<string>) => {
    const param: IBodyDataCreate = {
      ...data,
      avatar: valueImg,
      organizationCodes: arrOrg
    }
    const res = await ApiCreateUserCms(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thành Công",
        textBtn: 'Quay lại',
        actionReturn: () => router.push("/system/user")
      })
      dispatch(handleSetReloadDataUser())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (data: IBodyDataCreate, arrOrg: Array<string>) => {
    const param: IBodyDataCreate = {
      ...data,
      avatar: valueImg || renderUrlImageUpload(data.avatar),
      organizationCodes: arrOrg
    }
    const res = await ApiUpdateUserCms(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thành Công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(handleSetReloadDataUser())
    } else {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }

  }
  
  return (
    <>
      <Grid item xs={12} md={5} lg={4} p={0}>
        <CreateLeft setValueImg={setValueImg} />
      </Grid>
      <Grid item xs={12} md={7} lg={8} p={0}>
        <CreateUserRight
          handleCreateUser={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdateUser={handleUpdate}

        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateMain