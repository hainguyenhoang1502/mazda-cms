import styled from '@emotion/styled'
import FormControl from '@mui/material/FormControl'
import { IBodyDataCreate } from './interface'
import * as Yup from 'yup';
import { ERROR_EMAIL_VALUE_INPUT, ERROR_NULL_VALUE_INPUT, ERROR_PASSSWORD_VALUE_INPUT, ERROR_PHONE_VALUE_INPUT, ERROR_USERNAME_VALUE_INPUT, regexPassword, regexPhone, regexUserName } from "src/configs/initValueConfig";

export const MuiFormControl = styled(FormControl)({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingBottom: '30px'
})

export const INIT_VALUE_DATA_BODY_CREATE: IBodyDataCreate = {
  avatar: null,
  userName: "",
  password: "",
  email: null,
  fullName: null,
  phoneNumber: null,
  roleId: null,
  organizationCodes: []
}

export const UserSchema = Yup.object().shape({
  userName: Yup.string().required(ERROR_NULL_VALUE_INPUT).matches(regexUserName, ERROR_USERNAME_VALUE_INPUT),
  email: Yup.string().nullable().email(ERROR_EMAIL_VALUE_INPUT),
  phoneNumber: Yup.string().nullable().matches(regexPhone, ERROR_PHONE_VALUE_INPUT),
  password: Yup.string().when('id', {
    is: (val) => !val,
    then: Yup.string().required(ERROR_NULL_VALUE_INPUT).min(6, ERROR_PASSSWORD_VALUE_INPUT).matches(regexPassword, ERROR_PASSSWORD_VALUE_INPUT),
    otherwise: Yup.string().nullable()
  }),
});

export function generateRandomPassword(length) {
  const uppercaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
  const numbers = '0123456789';
  const specialCharacters = '!@#$%^&*()_+-=[]{}|;:,.<>?';

  let password = '';

  // Ensure at least one uppercase letter
  password += specialCharacters.charAt(Math.floor(Math.random() * specialCharacters.length));
  password += specialCharacters.charAt(Math.floor(Math.random() * specialCharacters.length));

  // Ensure at least one uppercase letter
  password += lowercaseLetters.charAt(Math.floor(Math.random() * lowercaseLetters.length));
  password += lowercaseLetters.charAt(Math.floor(Math.random() * lowercaseLetters.length));

  // Ensure at least one number
  password += numbers.charAt(Math.floor(Math.random() * numbers.length));
  password += numbers.charAt(Math.floor(Math.random() * numbers.length));

  // Generate remaining characters
  for (let i = 0; i < length - 2; i++) {
    const randomIndex = Math.floor(Math.random() * uppercaseLetters.length);
    password += uppercaseLetters.charAt(randomIndex);
  }

  // Shuffle the password characters
  password = shuffleString(password);

  return password;
}

// Function to shuffle a string
function shuffleString(string) {
  let shuffledString = '';
  const charArray = string.split('');

  while (charArray.length > 0) {
    const randomIndex = Math.floor(Math.random() * charArray.length);
    shuffledString += charArray.splice(randomIndex, 1)[0];
  }

  return shuffledString;
}