import { ModalConfirmState } from "src/configs/typeOption"

export interface IBodyDataCreate {
  avatar: string,
  userName: string,
  password: string,
  email: string,
  fullName: string,
  phoneNumber: string | number,
  roleId: string,
  organizationCodes: Array<string>
}
export interface CreateUserRightProps {
  handleCreateUser: Function
  setModalConfirm: Function,
  modalConfirm: ModalConfirmState
  handleUpdateUser: Function
}
export interface CreateUserLeftProps {
  setValueImg: Function
}
