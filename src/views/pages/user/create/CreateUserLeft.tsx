
// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import IconifyIcon from 'src/@core/components/icon';

// ** React  
import { useContext, useState } from 'react';

// ** Components 
import CustomAvatar from 'src/@core/components/mui/avatar'
import { getInitials } from 'src/@core/utils/get-initials';

// ** Api  
import { ApiUploadFileImage } from 'src/api/refData';

// ** Interface
import { CreateUserLeftProps } from './interface';

// ** Context
import { EditCustomerContext } from 'src/pages/customer-information/edit/[slug]';

const CreateCustomerLeft = (props: CreateUserLeftProps) => {

  const { setValueImg } = props

  const user = useContext(EditCustomerContext);

  const [uploadImage, setUploadImage] = useState<any>(null)

  const handleFileSelected = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const reader = new FileReader();
    const files = Array.from(e.target.files)
    reader.onloadend = () => {
      setUploadImage(reader.result)
    }
    reader.readAsDataURL(files[0]);
    const data = new FormData();
    for (let i = 0; i < files.length; i++) {
      data.append('avatar', files[i]);
    }
    const res = await ApiUploadFileImage(data)
    if (res.code === 200 && res.result) {
      setValueImg(res.data.filePath)
    } else {
      setValueImg(null)
    }
  }

  return (

    <Card>
      <CardContent
        sx={{
          pt: 12,
          display: 'flex',
          padding: "24px 18px",
          alignItems: 'center',
          flexDirection: 'column',
          position: 'relative'
        }}>
        {
          uploadImage ?
            <CustomAvatar
              src={uploadImage}
              variant='rounded'
              alt={"img-upload"}
              sx={{ width: 150, height: 150, mb: 6 }}
            /> :
            user && user.isEdit ?
              <CustomAvatar
                src={user && user.dataCustomer && user.dataCustomer.avatar}
                variant='rounded'
                alt={"img-upload"}
                sx={{ width: 150, height: 150, mb: 6 }}
              /> :
              <CustomAvatar
                skin='light'
                variant='rounded'
                sx={{ width: 150, height: 150, fontWeight: 600, mb: 6, fontSize: '3rem', position: "relative" }}
              >
                {user && user.isEdit && getInitials(user && user.dataCustomer && user.dataCustomer.fullName)}
              </CustomAvatar>
        }
        <Stack
          direction="row"
          alignItems="center"
          spacing={2}
          sx={{
            position: "absolute",
            width: '150px',
            justifyContent: "center",
            top: "135px",
            background: "#00000026"
          }}>
          <IconButton color="primary" aria-label="upload picture" component="label">
            <input hidden accept="image/*" type="file" onChange={handleFileSelected} />
            <IconifyIcon icon={"material-symbols:photo-camera-outline-rounded"} />
          </IconButton>
        </Stack>
      </CardContent>
    </Card>
  )
}
export default CreateCustomerLeft