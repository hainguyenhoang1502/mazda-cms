
// ** MUI Imports
import Card from "@mui/material/Card"
import Grid from "@mui/material/Grid";
import TreeView from '@mui/lab/TreeView';
import TreeItem from '@mui/lab/TreeItem';
import Button from "@mui/material/Button"
import Checkbox from "@mui/material/Checkbox";
import Typography from "@mui/material/Typography"
import IconButton from "@mui/material/IconButton";
import CardContent from "@mui/material/CardContent"
import InputAdornment from "@mui/material/InputAdornment";
import FormControlLabel from "@mui/material/FormControlLabel";

// ** React Imports
import { useContext, useEffect, useState } from "react"

// ** Next Imports
import router from "next/router"

// ** Third Party Import
import { Formik, Form, Field } from 'formik';

// ** Components Imports
import ModalConfirm from "src/views/component/modalConfirm"

import { SelectMUI, InputRequiredMUI } from "src/views/component/theme";

// ** Interface Services Imports 
import { CreateUserRightProps } from "./interface"
import { INIT_VALUE_DATA_BODY_CREATE, UserSchema, generateRandomPassword } from "./service"

// ** Context Imports
import { EditUserContext } from "src/pages/system/user/edit/[slug]";

//** Store Imports
import { AppDispatch, RootState } from "src/store";
import { useDispatch, useSelector } from "react-redux";

// ** Icon Imports
import Icon from 'src/@core/components/icon'

// ** Api Imports
import { IResponeOrgTree } from "src/api/organization/type";
import { fetchDataFilterGetRole } from "src/store/apps/role";
import { ChevronRightIcon, ExpandMoreIcon } from "src/configs/iconConfig";
import { fetchDataGetOrganizationTree } from "src/store/apps/category/organization";


const CreateUserRight = (props: CreateUserRightProps) => {

  // ** Props
  const { handleCreateUser, modalConfirm, setModalConfirm, handleUpdateUser } = props

  // ** States
  const [checkedCompanyDealer, setCheckedCompanyDealer] = useState<Array<string>>([]);
  const [checkedRegionArea, setCheckedRegionArea] = useState<Array<string>>([]);
  const [arrExpanded, setArrExpanded] = useState<Array<string>>([])
  const [isCheckAll, setIsCheckAll] = useState<boolean>(false)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.role)
  const storeOrg = useSelector((state: RootState) => state.organization)

  // ** Hooks 
  const context = useContext(EditUserContext);
  const isEdit = context && context.isEdit;

  useEffect(() => {
    if ((store && !store.arrFilter) || (store && store.arrFilter && store.arrFilter.length === 0)) {
      dispatch(fetchDataFilterGetRole({
        ...store.param,
        pageSize: 9999
      }))
    }
    if (storeOrg && (!storeOrg.dataOrgTree || storeOrg.dataOrgTree.length === 0)) {
      dispatch(fetchDataGetOrganizationTree())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (context && context.dataUser) {
      setCheckedCompanyDealer(context.dataUser.organizationCodes || [])
      const arr = []
      if (storeOrg.dataOrgTree && storeOrg.dataOrgTree.length > 0) {
        storeOrg.dataOrgTree.forEach((item: IResponeOrgTree["data"][0]) => {
          item.areas && item.areas.length > 0 && item.areas.forEach(area => {
            if (area.allChild.every(ar => context.dataUser.organizationCodes.includes(ar))) {
              arr.push(area.areaCode)
            }
          })

          if (item.allChild.every(ar => context.dataUser.organizationCodes.includes(ar))) {
            arr.push(item.regionCode)
          }
        })
      }
      setCheckedRegionArea(arr)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context && context.dataUser, storeOrg.dataOrgTree])

  useEffect(() => {
    if(checkedCompanyDealer.length>0){
      setIsCheckAll(storeOrg.dataOrgTree.every((item) => checkedRegionArea.includes(item.regionCode)))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checkedCompanyDealer, checkedRegionArea])


  const handleSubmit = async (values) => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? "Bạn đang thao tác Cập nhật thông tin Người dùng" : "Bạn đang thao tác Thêm Người dùng",
      action: () => {
        if (isEdit) {
          return handleUpdateUser(values, checkedCompanyDealer)
        } else {
          return handleCreateUser(values, checkedCompanyDealer)
        }
      }
    })
  }

  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? "Bạn đang thao tác Huỷ cập nhật thông tin Người dùng" : "Bạn đang thao tác Huỷ thêm Người dùng",
      action: () => router.push("/system/user")
    })
  }

  const handleHideShowItem = (id: string) => {
    setTimeout(
      () => {
        const arr = [...arrExpanded]
        const index = arr.indexOf(id)
        if (index > -1) {
          arr.splice(index, 1)
        } else {
          arr.push(id)
        }
        setArrExpanded(arr)
      }, 100
    )
  }



  const handleCheckRegion = (item: IResponeOrgTree["data"][0]) => {
    let arrRegionArea = [...checkedRegionArea]
    const arrCompanyDealer = [...checkedCompanyDealer]

    // ** Get all code Company and Dealer
    let arr = []
    item.areas.forEach(area => {
      arr = [
        ...arr,
        ...area.allChild
      ]
      area.companies.forEach(company => {
        arr = [
          ...arr,
          ...company.allChild
        ]
      })
    })

    // ** Check ArrRegionArea include regionCode
    const index = arrRegionArea.indexOf(item.regionCode)
    if (index === -1) {
      arrRegionArea = [
        ...arrRegionArea,
        item.regionCode,
        ...item.allChild
      ]

      setCheckedCompanyDealer([
        ...checkedCompanyDealer,
        ...arr,
      ])
      setCheckedRegionArea(arrRegionArea)

    } else {

      // ** Filter Array Region and Area
      const arrFilter = arrRegionArea.filter(elm => {
        if (item.allChild.includes(elm) || elm === item.regionCode) {
          return false
        }

        return true
      });

      // ** Filter Array Company and Dealer
      const arrFilterCompany = arrCompanyDealer.filter(elm => !arr.includes(elm));
      setCheckedCompanyDealer(arrFilterCompany)
      setCheckedRegionArea(arrFilter)
    }
  }

  const handleCheckArea = (area: IResponeOrgTree["data"][0]["areas"][0], region: IResponeOrgTree["data"][0]) => {
    const arrRegionArea = [...checkedRegionArea]
    const arrCompanyDealer = [...checkedCompanyDealer]

    // ** Get all code Company and Dealer
    let arr = []
    area.companies.forEach(company => {
      arr = [
        ...arr,
        ...area.allChild,
        ...company.allChild
      ]
    })

    // ** Check ArrRegionArea include regionCode
    const index = arrRegionArea.indexOf(area.areaCode)
    if (index === -1) {
      arrRegionArea.push(area.areaCode)

      // ** Check ArrRegionArea has include all Area code 
      if (region.allChild.every(v => arrRegionArea.includes(v))) {
        arrRegionArea.push(region.regionCode)
      }

      setCheckedCompanyDealer([
        ...checkedCompanyDealer,
        ...arr,
      ])
      setCheckedRegionArea(arrRegionArea)

    } else {

      // ** Filter Array Region and Area
      const arrFilter = arrRegionArea.filter(elm => {
        if (elm === area.areaCode || elm === region.regionCode) {

          return false
        }

        return true
      });

      // ** Filter Array Company and Dealer
      const arrFilterCompany = arrCompanyDealer.filter(elm => !arr.includes(elm));
      setCheckedCompanyDealer(arrFilterCompany)
      setCheckedRegionArea(arrFilter)
    }
  }

  const handleCheckCompany = (
    company: IResponeOrgTree["data"][0]["areas"][0]["companies"][0],
    area: IResponeOrgTree["data"][0]["areas"][0],
    region: IResponeOrgTree["data"][0],
  ) => {
    const arrRegionArea = [...checkedRegionArea]
    const arrCompanyDealer = [...checkedCompanyDealer]

    // ** Check ArrRegionArea include regionCode
    const index = arrCompanyDealer.indexOf(company.companyCode)
    if (index === -1) {
      arrCompanyDealer.push(company.companyCode)

      // ** Check ArrRegionArea has include all Area code 
      if (area.allChild.every(ar => arrCompanyDealer.includes(ar))) {
        arrRegionArea.push(area.areaCode)
      }
      if (region.allChild.every(reg => arrRegionArea.includes(reg))) {
        arrRegionArea.push(region.regionCode)
      }

      setCheckedCompanyDealer([
        ...arrCompanyDealer,
        ...company.allChild,

      ])
      setCheckedRegionArea(arrRegionArea)

    } else {

      // ** Filter Array Region and Area
      const arrFilter = arrRegionArea.filter(elm => {
        if (elm === area.areaCode || elm === region.regionCode) {
          return false
        }

        return true
      });

      // ** Filter Array Company and Dealer
      const arrFilterCompany = arrCompanyDealer.filter(elm => {
        if (company.allChild.includes(elm) || elm === company.companyCode) {
          return false
        }

        return true
      });

      setCheckedCompanyDealer(arrFilterCompany)
      setCheckedRegionArea(arrFilter)
    }
  }

  const handleCheckDealer = (
    dealer: IResponeOrgTree["data"][0]["areas"][0]["companies"][0]["dealers"][0],
    company: IResponeOrgTree["data"][0]["areas"][0]["companies"][0],
    area: IResponeOrgTree["data"][0]["areas"][0],
    region: IResponeOrgTree["data"][0],
  ) => {
    const arrRegionArea = [...checkedRegionArea]
    const arrCompanyDealer = [...checkedCompanyDealer]

    // ** Check ArrRegionArea include regionCode
    const index = arrCompanyDealer.indexOf(dealer.dealerCode)
    if (index === -1) {
      arrCompanyDealer.push(dealer.dealerCode)

      // ** Check ArrRegionArea has include all Area code 
      if (company.allChild.every(com => arrCompanyDealer.includes(com))) {
        arrCompanyDealer.push(company.companyCode)
      }
      if (area.allChild.every(ar => arrCompanyDealer.includes(ar))) {
        arrRegionArea.push(area.areaCode)
      }
      if (region.allChild.every(reg => arrRegionArea.includes(reg))) {
        arrRegionArea.push(region.regionCode)
      }

      setCheckedCompanyDealer(arrCompanyDealer)
      setCheckedRegionArea(arrRegionArea)

    } else {

      // ** Filter Array Region and Area
      const arrFilter = arrRegionArea.filter(elm => {
        if (elm === area.areaCode || elm === region.regionCode) {
          return false
        }

        return true
      });

      // ** Filter Array Company and Dealer
      const arrFilterCompany = arrCompanyDealer.filter(elm => {
        if (company.allChild.includes(elm) || elm === company.companyCode) {
          return false
        }

        return true
      });

      setCheckedCompanyDealer(arrFilterCompany)
      setCheckedRegionArea(arrFilter)
    }
  }

  const handleCheckAllOrg = () => {
    if (isCheckAll) {

      setCheckedCompanyDealer([])
      setCheckedRegionArea([])
    } else {
      let arrRegionArea = []
      let arrCompanyDealer = []
      storeOrg.dataOrgTree && storeOrg.dataOrgTree.forEach(item => {
        arrRegionArea = [...arrRegionArea, ...item.allCodeRegionArea]
        arrCompanyDealer = [...arrCompanyDealer, ...item.allComapnyDealer]
      })
      setCheckedCompanyDealer(arrCompanyDealer)
      setCheckedRegionArea(arrRegionArea)
    }

    setIsCheckAll(!isCheckAll)
  }

  const OrganizationTree = () => {

    return (
      <TreeView
        aria-label="file system navigator"
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}
        expanded={arrExpanded}
        sx={{ maxHeight: "60vh", flexGrow: 1, overflowY: 'auto' }}
      >
        <FormControlLabel
          label={"Văn phòng điều hành"}
          control={
            <Checkbox
              onClick={(e) => e.stopPropagation()}
              checked={isCheckAll}
              key={`root-1`}
              onChange={handleCheckAllOrg}
            />
          }
        />
        {
          storeOrg.dataOrgTree && storeOrg.dataOrgTree.length > 0 && storeOrg.dataOrgTree.map((item: IResponeOrgTree["data"][0], index) => (
            <TreeItem
              nodeId={`regionCode-${item.regionCode}`}
              key={item.regionCode}
              sx={{ ml: "20px" }}
              label={
                <FormControlLabel
                  label={item.regionName}
                  control={
                    <Checkbox
                      onClick={(e) => e.stopPropagation()}
                      checked={checkedRegionArea.includes(item.regionCode)}
                      key={`regionCode-${index}`}
                      onChange={() => {
                        handleCheckRegion(item)
                      }}
                    />
                  }
                />
              }
              onClick={() => handleHideShowItem(`regionCode-${item.regionCode}`)}
            >
              {item.areas && item.areas.map((area, indexArea) => (
                <TreeItem
                  key={indexArea}
                  sx={{ ml: "20px" }}
                  nodeId={`area-${area.areaCode}`}
                  label={
                    <FormControlLabel
                      label={area.areaName}
                      control={
                        <Checkbox
                          onClick={(e) => e.stopPropagation()}
                          checked={checkedRegionArea.includes(area.areaCode)}
                          key={`areaCode-${index}`}
                          onChange={() => handleCheckArea(area, item)}
                        />
                      }
                    />
                  }
                  onClick={() => handleHideShowItem(`area-${area.areaCode}`)}
                >
                  {area.companies && area.companies.map((company, indexCompany) => (
                    <TreeItem
                      key={indexCompany}
                      sx={{ ml: "20px" }}
                      nodeId={`company-${company.companyCode}`}
                      label={
                        <FormControlLabel
                          label={company.companyName}
                          control={
                            <Checkbox
                              onClick={(e) => e.stopPropagation()}
                              checked={checkedCompanyDealer.includes(company.companyCode)}
                              key={`companyCode-${index}`}
                              onChange={() => handleCheckCompany(company, area, item)}
                            />
                          }
                        />
                      }
                      onClick={() => handleHideShowItem(`company-${company.companyCode}`)}
                    >
                      {company.dealers && company.dealers.map((dealer, indexDealers) => (
                        <TreeItem
                          key={indexDealers}
                          sx={{ ml: "20px" }}
                          nodeId={`dealerCode-${dealer.dealerCode}`}
                          label={
                            <FormControlLabel
                              label={dealer.dealerName}
                              control={
                                <Checkbox
                                  onClick={(e) => e.stopPropagation()}
                                  checked={checkedCompanyDealer.includes(dealer.dealerCode)}
                                  key={`dealerCode-${index}`}
                                  onChange={() => handleCheckDealer(dealer, company, area, item)}
                                />
                              }
                            />
                          }
                        >
                        </TreeItem>
                      ))}
                    </TreeItem>
                  ))}
                </TreeItem>
              ))}
            </TreeItem>
          ))
        }
      </TreeView >
    )
  };

  return (
    <Card>
      <Formik
        initialValues={context && context.dataUser || INIT_VALUE_DATA_BODY_CREATE}
        onSubmit={values => handleSubmit(values)}
        validationSchema={UserSchema}
        validateOnBlur
      >
        {(props) => {

          return (
            <Form>
              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px" }}>
                  Thông tin tài khoản
                </Typography>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="my-userName"
                      label="Tên tài khoản"
                      name="userName"
                      component={InputRequiredMUI}
                      texterror={props.touched.userName && props.errors.userName}
                      texticon="*"
                    />
                  </Grid>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="my-email"
                      label="Email"
                      name="email"
                      component={InputRequiredMUI}
                      texterror={props.touched.email && props.errors.email}
                    />
                  </Grid>
                  {
                    !isEdit &&
                    <Grid item lg={6} xs={12}>
                      <Field
                        id="my-password"
                        label="Mật khẩu"
                        name="password"
                        component={InputRequiredMUI}
                        texterror={props.touched.password && props.errors.password}
                        texticon="*"
                        type={"text"}
                        endAdornment={
                          <InputAdornment position='end'>
                            <IconButton
                              edge='end'
                              onMouseDown={e => e.preventDefault()}
                              onClick={() => props.setFieldValue("password", generateRandomPassword(10))}
                            >
                              <Icon fontSize={20} icon={"fe:random"} />
                            </IconButton>
                          </InputAdornment>
                        }
                      />
                    </Grid>
                  }
                </Grid>
              </CardContent>
              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, paddingBottom: "20px", fontSize: "24px" }}>
                  Thông tin người dùng
                </Typography>
                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="my-fullName"
                      label="Họ và tên"
                      name="fullName"
                      component={InputRequiredMUI}
                      texterror={props.touched.fullName && props.errors.fullName}
                    />
                  </Grid>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="my-phoneNumber"
                      label="Số điện thoại"
                      name="phoneNumber"
                      component={InputRequiredMUI}
                      texterror={props.touched.phoneNumber && props.errors.phoneNumber}
                    />
                  </Grid>
                </Grid>
              </CardContent>

              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, paddingBottom: "20px", fontSize: "24px" }}>
                  Phân quyền
                </Typography>
                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={6} xs={12}>
                    <Field
                      id="roleId"
                      label="Nhóm quyền"
                      as="select"
                      component={SelectMUI}
                      name="roleId"
                      texterror={props.touched.roleId && props.errors.roleId}
                      value={props.values.roleId}
                      onChange={(event) => {
                        props.setFieldValue("roleId", event.target.value);
                      }}
                      options={store.arrFilter}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, paddingBottom: "20px", fontSize: "24px" }}>
                  Phụ trách
                </Typography>
                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item xs={12}>
                    {OrganizationTree()}
                  </Grid>
                </Grid>
              </CardContent>
              <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
                <Button variant="contained" type="submit">Lưu</Button>
                <Button color="error" variant="outlined" onClick={handleCancel} >Hủy</Button>
              </CardContent>
            </Form>
          )
        }
        }
      </Formik>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />

    </Card>

  )
}
export default CreateUserRight