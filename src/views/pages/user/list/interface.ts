import { GridRowId } from '@mui/x-data-grid'

interface TableHeaderProps {
  setModalConfirm: Function
  arrRowId: Array<GridRowId>
  getDeleteUser: Function
}
interface IParamValuesFilter {
  keyword: string
  startDate: string
  endDate: string
}
export type { TableHeaderProps, IParamValuesFilter }
