// ** MUI Imports
import Box from '@mui/system/Box'
import Tooltip from '@mui/material/Tooltip'
import Button from '@mui/material/Button'
import CustomChip from 'src/@core/components/mui/chip'
import TablePagination from '@mui/material/TablePagination'
import { DataGrid, GridColDef } from "@mui/x-data-grid"

// ** Next Imports
import router from "next/router"

// ** Config Imports
import { permissonConfig } from "src/configs/roleConfig"
import { renderNameStatus } from "src/configs/functionConfig"

// ** Components 
import Icon from 'src/@core/components/icon'
import { TypographyDataGrid } from "src/views/component/theme"

// ** Store Redux Imports
import { AppDispatch, RootState } from "src/store"
import { fetchDataGetUser } from "src/store/apps/user"
import { useDispatch, useSelector } from "react-redux"

// ** Type Interface Imports
import { IParamListUser } from "src/api/authen/type"
import { EditDataGrid } from 'src/views/component/theme/customEditDelete'

const ListDataTable = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.user)

  const handleChangePage = (e, page) => {
    const param: IParamListUser = {
      ...store.param,
      pageIndex: page + 1,
    }
    dispatch(fetchDataGetUser(param))
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamListUser = {
      ...store.param,
      pageSize: Number(event.target.value),
    }
    dispatch(fetchDataGetUser(param))
  }


  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 130,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }) => {

        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission={permissonConfig.USER_EDIT} slug={(`/system/user/edit/${row.userName}`)} />
            <Tooltip title="Xem chi tiết" placement='top'>
              <Button color='error' sx={{ p: 0, minWidth: "30px" }} onClick={() => router.push(`/system/user/${row.userName}`)}>
                <Icon icon='mdi:eye-outline' fontSize={20} color="#32475CDE" />
              </Button >
            </Tooltip>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'userName',
      sortable: false,
      headerName: 'Tên tài khoản',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.userName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'fullName',
      headerName: 'Tên người dùng',
      sortable: false,
      renderCell: ({ row }) => {

        return (
          <Box sx={{ display: "flex", alignItems: "center", gap: "10px" }}>
            <TypographyDataGrid >
              {row.fullName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'email',
      sortable: false,
      headerName: 'Email',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.email}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'roleName',
      sortable: false,
      headerName: 'nhóm quyền',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.roleName}
            </TypographyDataGrid>
          </Box>
        )
      }
    }
    , {
      flex: 0.25,
      minWidth: 200,
      field: 'status',
      sortable: false,
      headerName: 'Trạng thái',
      renderCell: ({ row }: any) => {

        return row.status && <CustomChip rounded skin='light' size='small' label={renderNameStatus(row.status)} color={row.status === "ACTIVE" ? "success" : "error"} />
      }
    }

  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={store.data || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterSelectedRowCount
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          }
        }}
      />
      <TablePagination
        component="div"
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>


  )
}
export default ListDataTable