
// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment } from '@mui/material'

// ** React Imports
import { useState } from 'react'

// ** Components Imports
import Icon from 'src/@core/components/icon'
import { CreateButtonDataGrid } from 'src/views/component/theme/customEditDelete'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'

// ** Interface Services Imports 
import { IParamValuesFilter } from './interface'

// ** Utils

// ** Redux Store
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetUser } from 'src/store/apps/user'

const TableHeader = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.user)

  const [valuesFilter, setValuesFilter] = useState<IParamValuesFilter>({
    keyword: "",
    startDate: null,
    endDate: null
  })

  const handleChangeInput = (value, field) => {
    const body = { ...valuesFilter }
    body[field] = value
    setValuesFilter(body)
  }
  const handleFilterData = () => {
    const param = {
      ...store.param,
      ...valuesFilter,
      pageIndex: 1,
    }
    dispatch(fetchDataGetUser(param))
  }
  const handleClearFilter = () => {
    const param = {
      ...store.param,
      keyword: "",
    }
    setValuesFilter(param)
    dispatch(fetchDataGetUser(param))
  }

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: "25px"
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', minWidth: "600px" }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ mr: 4, width: "50%", }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            ),
          }}
          placeholder='Tìm kiếm…'
          onChange={(e) => handleChangeInput(e.target.value, "keyword")}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              handleFilterData()
            }
          }}

        />
        <Button variant="outlined" onClick={handleFilterData} sx={{ mr: 4 }} >
          Lọc
        </Button>
        <Button variant="outlined" onClick={handleClearFilter} color='error'>
          Xóa bộ lọc
        </Button>
      </Box>

      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', gap: "1rem", justifyContent: "end", maxWidth: "100%" }}>
        <CreateButtonDataGrid permission={permissonConfig.USER_CREATE} slug="/system/user/create" />
      </Box>
    </Box>
  )
}

export default TableHeader
