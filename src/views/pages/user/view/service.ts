
// ** Config Imports
import {renderNameStatus } from 'src/configs/functionConfig'

// ** Interface Types Imports 
import { IDataTableProps } from './interface'
import { IResponeDetailUser } from 'src/api/authen/type'


const dataTable = (data: IResponeDetailUser["data"]) => {
  const arr: Array<IDataTableProps> = [
    {
      title: 'Thông tin chi tiết',
      data: [
        {
          title: 'Tên tài khoản:',
          value: data.userName
        },
        {
          title: 'Email:',
          value: data.email
        },
        {
          title: 'Họ tên:',
          value: data.fullName
        },
        {
          title: 'Số điện thoại:',
          value: data.phoneNumber
        },
        {
          title: 'Nhóm quyền:',
          value: data.roleName
        },
        {
          title: 'Trạng thái:',
          value: renderNameStatus(data.status),
          color: data.status === 'ACTIVE' ? 'success' : 'error'
        }
      ]
    },
  ]

  return arr
}

export { dataTable }
