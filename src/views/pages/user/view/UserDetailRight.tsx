
// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography';
import TreeView from '@mui/lab/TreeView';
import TreeItem from '@mui/lab/TreeItem';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';

// ** React Imports
import { useContext, useEffect, useState } from 'react';

// ** Config Imports
import { DetailUserContext } from 'src/pages/system/user/[slug]';
import { ChevronRightIcon, ExpandMoreIcon } from 'src/configs/iconConfig';

// ** Store Redux Imports
import { AppDispatch, RootState } from 'src/store';
import { useDispatch, useSelector } from 'react-redux';
import { IResponeOrgTree } from 'src/api/organization/type';
import { fetchDataGetOrganizationTree } from 'src/store/apps/category/organization';

const CustomerDetailRight = () => {

  const [arrExpanded, setArrExpanded] = useState<Array<string>>([])
  const [checkedRegionArea, setCheckedRegionArea] = useState<Array<string>>([]);

  // ** Context
  const context = useContext(DetailUserContext);
  const { dataDetail } = context

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.organization)

  useEffect(() => {

    if (store && (!store.dataOrgTree || store.dataOrgTree.length === 0)) {
      dispatch(fetchDataGetOrganizationTree())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (context && context.dataDetail && store.dataOrgTree && store.dataOrgTree.length > 0) {
      const arr = []
      store.dataOrgTree.forEach((item: IResponeOrgTree["data"][0]) => {
        item.areas && item.areas.length > 0 && item.areas.forEach(area => {
          if (area.allChild.every(ar => context.dataDetail.organizationCodes.includes(ar))) {
            arr.push(area.areaCode)
          }
        })
        if (item.allChild.every(ar => context.dataDetail.organizationCodes.includes(ar))) {
          arr.push(item.regionCode)
        }
      })

      setCheckedRegionArea(arr)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context && context.dataDetail, store.dataOrgTree])

  const handleHideShowItem = (id: string) => {
    setTimeout(
      () => {
        const arr = [...arrExpanded]
        const index = arr.indexOf(id)
        if (index > -1) {
          arr.splice(index, 1)
        } else {
          arr.push(id)
        }
        setArrExpanded(arr)
      }, 100
    )
  }


  return (
    <>
      <Card sx={{ height: "100%" }}>
        <CardContent sx={{ pt: 12, flexDirection: 'column' }}>
          <Typography noWrap sx={{ fontWeight: 500, textTransform: 'capitalize', paddingLeft: "20px", fontSize: "24px" }}>
            Công ty phụ trách
          </Typography>
        </CardContent>
        <CardContent>
          <TreeView
            aria-label="file system navigator"
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpandIcon={<ChevronRightIcon />}
            expanded={arrExpanded}
            sx={{ maxHeight: "100vh", flexGrow: 1, overflowY: 'auto' }}
          >

            <Typography>Văn phòng điều hành</Typography>
            {
              store.dataOrgTree && store.dataOrgTree.length > 0 && store.dataOrgTree.map((item: IResponeOrgTree["data"][0], index) => (
                <TreeItem
                  nodeId={`regionCode-${item.regionCode}`}
                  key={item.regionCode}
                  label={
                    <FormControlLabel
                      label={item.regionName}
                      onChange={(e) => e.preventDefault()}
                      control={
                        <Checkbox
                          checked={checkedRegionArea.includes(item.regionCode)}
                          key={`regionCode-${index}`}
                        />
                      }
                    />
                  }
                  onClick={() => handleHideShowItem(`regionCode-${item.regionCode}`)}
                >
                  {item.areas && item.areas.map((area, indexArea) => (
                    <TreeItem
                      key={indexArea}
                      sx={{ ml: "20px" }}
                      nodeId={`area-${area.areaCode}`}
                      label={
                        <FormControlLabel
                          label={area.areaName}
                          control={
                            <Checkbox
                              checked={checkedRegionArea.includes(area.areaCode)}
                              key={`areaCode-${index}`}
                            />
                          }
                        />
                      }
                      onClick={() => handleHideShowItem(`area-${area.areaCode}`)}
                    >
                      {area.companies && area.companies.map((company, indexCompany) => (
                        <TreeItem
                          key={indexCompany}
                          sx={{ ml: "20px" }}
                          nodeId={`company-${company.companyCode}`}
                          label={
                            <FormControlLabel
                              label={company.companyName}
                              control={
                                <Checkbox
                                  checked={dataDetail.organizationCodes.includes(company.companyCode)}
                                  key={`companyCode-${index}`}
                                />
                              }
                            />
                          }
                          onClick={() => handleHideShowItem(`company-${company.companyCode}`)}
                        >
                          {company.dealers && company.dealers.map((dealer, indexDealers) => (
                            <TreeItem
                              key={indexDealers}
                              sx={{ ml: "20px" }}
                              nodeId={`dealerCode-${dealer.dealerCode}`}
                              label={
                                <FormControlLabel
                                  label={dealer.dealerName}
                                  control={
                                    <Checkbox
                                      checked={dataDetail.organizationCodes.includes(dealer.dealerCode)}
                                      key={`dealerCode-${index}`} />
                                  }
                                />
                              }
                            >
                            </TreeItem>
                          ))}
                        </TreeItem>
                      ))}
                    </TreeItem>
                  ))}
                </TreeItem>
              ))
            }
          </TreeView >
        </CardContent>
      </Card>
    </>
  )
}
export default CustomerDetailRight
