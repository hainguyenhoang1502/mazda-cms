
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** Components Imports
import UserDetailLeft from './UserDetailLeft'
import UserDetailRight from './UserDetailRight'

const UserInformationDetail = () => {

  return (
    <>
      <Grid item xs={12} md={5} lg={4}>
        <UserDetailLeft />
      </Grid>
      <Grid item xs={12} md={7} lg={8}>

        <UserDetailRight/>
      </Grid>
    </>
  )
}
export default UserInformationDetail
