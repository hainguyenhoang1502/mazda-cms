
interface SMSFormProps {
    setModalConfirm: Function;
    setModalNotify: Function
}

interface INIT_VALUE_SMS_PROPS {
    id: number;
    brandCode: string;
    brandName: string;
    apiKey: string;
    secretKey: string;
    isActive: 0 | 1;
}

export type { SMSFormProps, INIT_VALUE_SMS_PROPS }