import { INIT_VALUE_SMS_PROPS } from "./interface"

const INIT_VALUE_SMS: INIT_VALUE_SMS_PROPS = {
    id: null,
    brandCode: '',
    brandName: '',
    apiKey: '',
    secretKey: '',
    isActive: 0,
}

export { INIT_VALUE_SMS }