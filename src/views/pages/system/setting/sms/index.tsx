// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useState } from 'react'

// ** Third Party Import
import * as Yup from 'yup'
import { Formik, Form, Field } from 'formik'

// ** Components Imports
import { InputRequiredMUI } from 'src/views/component/theme'

import { ERROR_NULL_VALUE_INPUT, INIT_STATE_MODAL_CONFIRM, UNKNOW_ERROR } from 'src/configs/initValueConfig'

import { SMSFormProps } from './interface'
import { Checkbox, FormControlLabel } from '@mui/material'
import { INIT_VALUE_SMS } from './service'
import { useSettingSystem } from 'src/pages/system/setting/[slug]'
import { ApiPostSendSMS } from 'src/api/refData'
import { useRouter } from 'next/router'
import { ModalConfirmState } from 'src/configs/typeOption'
import { useDispatch } from 'react-redux'
import { AppDispatch } from 'src/store'
import { setValueBackrop } from 'src/store/utils/loadingBackdrop'

const SMSForm = (props: SMSFormProps) => {
    // ** Props
    const { setModalConfirm, setModalNotify } = props
    const router = useRouter();
    const [showID] = useState(false)
    const dispatch = useDispatch<AppDispatch>()

    // ** Hooks context API
    const context = useSettingSystem()
    const isEdit = !!context?.data?.smsConfig?.id

    const SMSFormSchema = Yup.object().shape({
        brandName: Yup.string().nullable().required(ERROR_NULL_VALUE_INPUT),
        secretKey: Yup.string().nullable().required(ERROR_NULL_VALUE_INPUT),
        apiKey: Yup.string().nullable().required(ERROR_NULL_VALUE_INPUT),
        isActive: Yup.number().oneOf([0, 1]),
    })

    const handleSubmitSMS = async (values: typeof context.data.smsConfig) => {
        const param = { ...values, brandCode: router.query.slug as string }

        dispatch(setValueBackrop(true))
        const res = await ApiPostSendSMS(param)
        dispatch(setValueBackrop(false))

        if (res.code === 200 && res.result) {
            setModalNotify({
                isOpen: true,
                title: 'Đã gửi thành công',
                textBtn: 'Quay lại',
                actionReturn: () => {
                    router.push(window.location.pathname);
                    setModalNotify((prev: ModalConfirmState) => ({ ...prev, isOpen: false }))
                }
            })
            setModalConfirm(INIT_STATE_MODAL_CONFIRM)

        } else {
            setModalNotify({
                isOpen: true,
                title: 'Thêm Thất Bại',
                message: res.message || UNKNOW_ERROR,
                isError:true
            })
            setModalConfirm(INIT_STATE_MODAL_CONFIRM)
        }
    }

    const handleSubmit = async (values: typeof context.data.smsConfig) => {

        setModalConfirm({
            isOpen: true,
            title: isEdit ? 'Bạn đang thao tác Cập nhật thông tin SMS' : 'Bạn đang thao tác thêm Thông tin SMS',
            action: () => {
                return handleSubmitSMS(values)
            }
        })
    }

    return (
        <Card>
            <Formik
                initialValues={context?.data?.smsConfig || INIT_VALUE_SMS}
                onSubmit={(values: typeof context.data.smsConfig) => handleSubmit(values)}
                validationSchema={SMSFormSchema}
                validateOnBlur
            >
                {props => {

                    return (
                        <Form>
                            <CardContent>
                                <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                                    Cấu hình gửi SMS
                                </Typography>

                                <Grid item container spacing={6} marginBottom={5}>
                                    <Grid item xs={12}>
                                        {showID && <Field
                                            id='my-id'
                                            label='id'
                                            name='id'
                                            component={InputRequiredMUI}
                                            texterror={props.touched.id && props.errors.id}
                                            texticon='*'
                                        />}

                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field
                                            id='my-brandName'
                                            label='BrandName'
                                            name='brandName'
                                            component={InputRequiredMUI}
                                            texterror={props.touched.brandName && props.errors.brandName}
                                            texticon='*'
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field
                                            id='my-apikey'
                                            label='ApiKey'
                                            name='apiKey'
                                            component={InputRequiredMUI}
                                            texticon='*'
                                            texterror={props.touched.apiKey && props.errors.apiKey}
                                        />
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Field
                                            id='my-secretkey'
                                            label='SecretKey'
                                            texticon='*'
                                            name='secretKey'
                                            component={InputRequiredMUI}
                                            texterror={props.touched.secretKey && props.errors.secretKey}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControlLabel
                                            style={{ userSelect: 'none' }}
                                            control={
                                                <Checkbox checked={!!props.values.isActive} onChange={() => props.setFieldValue("isActive", props.values.isActive ? 0 : 1)} name="isActive" />
                                            }
                                            label="Cho phép gửi SMS"
                                        />
                                    </Grid>

                                </Grid>
                            </CardContent>

                            <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                                <Button variant='contained' type='submit'>
                                    Lưu
                                </Button>
                            </CardContent>
                        </Form>
                    )
                }}
            </Formik>
        </Card>
    )
}
export default SMSForm
