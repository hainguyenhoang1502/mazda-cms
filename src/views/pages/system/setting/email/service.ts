import { IINIT_VALUE_EMAIL } from "./interface"

const INIT_VALUE_EMAIL: IINIT_VALUE_EMAIL = {
    id: null,
    host: '',
    port: '',
    userName: '',
    password: '',
    displayName: '',
    brandCode: '',
    isActive: 0,
}

export { INIT_VALUE_EMAIL }