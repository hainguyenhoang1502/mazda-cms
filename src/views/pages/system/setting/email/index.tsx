// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'


// ** Third Party Import
import * as Yup from 'yup'
import { Formik, Form, Field } from 'formik'

// ** Components Imports
import { InputRequiredMUI } from 'src/views/component/theme'


import { ERROR_NULL_VALUE_INPUT, INIT_STATE_MODAL_CONFIRM, UNKNOW_ERROR } from 'src/configs/initValueConfig'

import { Checkbox, FormControlLabel } from '@mui/material'
import { INIT_VALUE_EMAIL } from './service'
import { useSettingSystem } from 'src/pages/system/setting/[slug]'
import { EmailFormProps } from './interface'
import { useRouter } from 'next/router'
import { ApiPostSendEmail } from 'src/api/refData'
import { IParamEmail } from 'src/api/refData/type'
import { ModalConfirmState } from 'src/configs/typeOption'
import { useDispatch } from 'react-redux'
import { AppDispatch } from 'src/store'
import { setValueBackrop } from 'src/store/utils/loadingBackdrop'

const EmailForm = (props: EmailFormProps) => {
    // ** Props
    const { setModalConfirm, setModalNotify } = props
    const router = useRouter()
    const dispatch = useDispatch<AppDispatch>()

    // ** Hooks
    const context = useSettingSystem()
    const isEdit = !!context?.data?.emailConfig?.id

    const EmailFormSchema = Yup.object().shape({
        host: Yup.string().nullable().required(ERROR_NULL_VALUE_INPUT),
        port: Yup.string().nullable().required(ERROR_NULL_VALUE_INPUT),
        userName: Yup.string().nullable().required(ERROR_NULL_VALUE_INPUT),
        password: Yup.string().nullable().required(ERROR_NULL_VALUE_INPUT),
        displayName: Yup.string().nullable().required(ERROR_NULL_VALUE_INPUT),
        isActive: Yup.number().oneOf([0, 1]),
    })

    const handleSubmitEmail = async (values: typeof context.data.emailConfig) => {
        const param: IParamEmail = { ...values, brandCode: router.query.slug as string }
        
        dispatch(setValueBackrop(true))
        const res = await ApiPostSendEmail(param)
        dispatch(setValueBackrop(false))

        if (res.code === 200 && res.result) {
            setModalNotify({
                isOpen: true,
                title: 'Gửi thành công',
                textBtn: 'Quay lại',
                actionReturn: () => {
                    router.push(window.location.pathname)
                    setModalNotify((prev: ModalConfirmState) => ({ ...prev, isOpen: false }))
                }
            })
            setModalConfirm(INIT_STATE_MODAL_CONFIRM)

        } else {
            setModalNotify({
                isOpen: true,
                title: 'Thêm Thất Bại',
                message: res.message || UNKNOW_ERROR,
                isError: true,
            })
            setModalConfirm(INIT_STATE_MODAL_CONFIRM)
        }

    }
    const handleSubmit = async (values: typeof context.data.emailConfig) => {

        setModalConfirm({
            isOpen: true,
            title: isEdit ? 'Bạn đang thao tác cập nhật cấu hình Email' : "Bạn đang thao tác gửi cấu hình Email",
            action: () => {
                return handleSubmitEmail(values)
            }
        })
    }

    return (
        <Card>
            <Formik
                initialValues={context?.data?.emailConfig || INIT_VALUE_EMAIL}
                onSubmit={(values: typeof context.data.emailConfig) => handleSubmit(values)}
                validationSchema={EmailFormSchema}
                validateOnBlur
            >
                {props => {

                    return (
                        <Form>
                            <CardContent>
                                <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                                    Cấu hình gửi Email
                                </Typography>

                                <Grid item container spacing={6} marginBottom={5}>
                                    <Grid item xs={12}>
                                        <Field
                                            id='my-host'
                                            label='Host'
                                            name='host'
                                            component={InputRequiredMUI}
                                            texterror={props.touched.host && props.errors.host}
                                            texticon='*'
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field
                                            id='my-port'
                                            label='Port'
                                            name='port'
                                            component={InputRequiredMUI}
                                            texticon='*'
                                            texterror={props.touched.port && props.errors.port}
                                        />
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Field
                                            id='my-username'
                                            label='UserName'
                                            texticon='*'
                                            name='userName'
                                            component={InputRequiredMUI}
                                            texterror={props.touched.userName && props.errors.userName}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field
                                            id='my-password'
                                            label='Password'
                                            texticon='*'
                                            type="password"
                                            name='password'
                                            component={InputRequiredMUI}
                                            texterror={props.touched.password && props.errors.password}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field
                                            id='my-displayname'
                                            label='DisplayName'
                                            texticon='*'
                                            name='displayName'
                                            component={InputRequiredMUI}
                                            texterror={props.touched.displayName && props.errors.displayName}
                                        />
                                    </Grid>


                                    <Grid item xs={12}>
                                        <FormControlLabel
                                            style={{ userSelect: 'none' }}
                                            control={
                                                <Checkbox checked={!!props.values.isActive} onChange={() => props.setFieldValue("isActive", props.values.isActive ? 0 : 1)} name="isActive" />
                                            }
                                            label="Cho phép gửi Email"
                                        />
                                    </Grid>

                                </Grid>
                            </CardContent>

                            <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                                <Button variant='contained' type='submit'>
                                    Lưu
                                </Button>
                            </CardContent>
                        </Form>
                    )
                }}
            </Formik>
        </Card>
    )
}
export default EmailForm
