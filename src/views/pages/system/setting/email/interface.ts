interface EmailFormProps {
    setModalConfirm: Function;
    setModalNotify: Function
}
 interface IINIT_VALUE_EMAIL {
    id: number;
    host: string;
    port: string;
    userName: string;
    password: string;
    displayName: string;
    brandCode: string;
    isActive: 0 | 1;
}

export type { EmailFormProps, IINIT_VALUE_EMAIL }