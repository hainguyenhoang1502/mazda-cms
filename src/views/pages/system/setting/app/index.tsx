// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'

// ** Third Party Import
import * as Yup from 'yup'
import { Formik, Form, Field } from 'formik'

// ** Components Imports
import { ERROR_NULL_VALUE_INPUT, INIT_STATE_MODAL_CONFIRM, UNKNOW_ERROR } from 'src/configs/initValueConfig'

import { SMSFormProps } from './interface'

// import { Checkbox, FormControlLabel } from '@mui/material'
import { INIT_VALUE_APP } from './service'
import { useSettingSystem } from 'src/pages/system/setting/[slug]'
import { useRef } from 'react'
import { InputRequiredMUI } from 'src/views/component/theme'
import { toast } from 'react-hot-toast'
import { ApiPostSendLogoApp } from 'src/api/refData'
import { IParamLogoApp } from 'src/api/refData/type'
import { useRouter } from 'next/router'
import DropZone from './DropZone'
import { useDispatch } from 'react-redux'
import { AppDispatch } from 'src/store'
import { setValueBackrop } from 'src/store/utils/loadingBackdrop'
import { ModalConfirmState } from 'src/configs/typeOption'
import { WWW_SERVICE_IMAGE } from 'src/configs/envConfig'

const AppForm = (props: SMSFormProps) => {
    // ** Props
    const { setModalConfirm, setModalNotify } = props
    const logoRef = useRef<string>()
    const splashRef = useRef<string>()
    const router = useRouter()
    const dispatch = useDispatch<AppDispatch>()

    // ** Hooks context API
    const context = useSettingSystem()
    const isEdit = !!context?.data?.appConfig?.id

    const AppFormSchema = Yup.object().shape({
        textIntro: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    })

    async function handleSubmitApp(values: typeof context.data.appConfig) {
        const param: IParamLogoApp = {
            ...values, brandCode: router.query.slug as string, logo: logoRef.current, splashcreen: splashRef.current
        }

        dispatch(setValueBackrop(true))
        const res = await ApiPostSendLogoApp(param)
        dispatch(setValueBackrop(false))

        if (res.code === 200 && res.result) {
            setModalNotify({
                isOpen: true,
                title: 'Thêm Thành Công',
                textBtn: 'Quay lại',
                actionReturn: () => {
                    router.push(window.location.pathname)
                    setModalNotify((prev: ModalConfirmState) => ({ ...prev, isOpen: false }))
                }
            })
            setModalConfirm(INIT_STATE_MODAL_CONFIRM)

        } else {
            setModalNotify({
                isOpen: true,
                title: 'Thêm Thất Bại',
                message: res.message || UNKNOW_ERROR,
                isError: true
            })
            setModalConfirm(INIT_STATE_MODAL_CONFIRM)
        }
    }

    const handleSubmit = async (values: typeof context.data.appConfig) => {
        if (!logoRef.current && !splashRef.current) {
            return toast.error("Chưa upload logo và splashscreen")
        } else if (!logoRef.current) {
            return toast.error("Chưa upload logo")
        } else if (!splashRef.current) {
            return toast.error("Chưa upload hình splashscreen")
        }

        setModalConfirm({
            isOpen: true,
            title: isEdit ? 'Bạn đang thao tác Cập nhật thông tin Cấu hình App' : 'Bạn đang thao tác thêm Thông tin Cấu hình App',
            action: () => {
                return handleSubmitApp(values)
            }
        })
    }



    return (
        <Card>
            <Formik
                initialValues={context?.data?.appConfig || INIT_VALUE_APP}
                onSubmit={values => handleSubmit(values)}
                validationSchema={AppFormSchema}
                validateOnBlur
            >
                {props => {

                    return (
                        <Form>
                            <CardContent>
                                <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                                    Cấu hình App
                                </Typography>
                                <Grid item spacing={6} marginBottom={5}>
                                    <DropZone title='Logo' ref={logoRef} imgField='logo' imageLink={`${context?.data?.appConfig?.logo ? WWW_SERVICE_IMAGE + context?.data?.appConfig?.logo : ''}`} />
                                </Grid>
                                <Grid item spacing={6} marginBottom={5}>
                                    <DropZone title='SplashScreen' ref={splashRef} imgField='splash' imageLink={`${context?.data?.appConfig?.splashcreen ? WWW_SERVICE_IMAGE + context?.data?.appConfig?.splashcreen : ''}`} />
                                </Grid>
                                <Grid item xs={12}>
                                    <Field
                                        id='my-textintro'
                                        label='TextIntro'
                                        texticon='*'
                                        name='textIntro'
                                        component={InputRequiredMUI}
                                        texterror={props.touched.textIntro && props.errors.textIntro}
                                    />
                                </Grid>
                            </CardContent>

                            <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                                <Button variant='contained' type='submit'>
                                    Lưu
                                </Button>
                            </CardContent>
                        </Form>
                    )
                }}
            </Formik>

        </Card>
    )
}
export default AppForm
