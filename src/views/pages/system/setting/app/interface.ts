
interface SMSFormProps {
    setModalConfirm: Function;
    setModalNotify: Function
}



interface INIT_VALUE_APP_PROPS {
    id: number;
    brandCode: string;
    logo: string;
    splashcreen: string;
    textIntro: string;
}

export type { SMSFormProps, INIT_VALUE_APP_PROPS }