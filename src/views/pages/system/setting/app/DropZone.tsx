import { Box, Button, SxProps, Typography } from '@mui/material'
import React, { forwardRef, useCallback, useImperativeHandle, useState } from 'react'
import { useDropzone } from 'react-dropzone'
import { useDispatch } from 'react-redux'
import { AppDispatch } from 'src/store'
import { ApiUploadFileImage } from 'src/api/refData'
import { setValueBackrop } from 'src/store/utils/loadingBackdrop'
import { toast } from 'react-hot-toast'
import IconifyIcon from 'src/@core/components/icon'
import { renderUrlImageUpload } from 'src/configs/functionConfig'

interface DropZone {
    title: string;
    imgField: string;
    imageLink: string;
}

const BoxSxImg: SxProps = {
    width: 208,
    height: 180,
    borderRadius: "5px",
    overflow: "hidden"
}

const BoxSxWrapper: SxProps = {
    width: 208,
    height: 180,
    border: "1px solid #D99BFF",
    borderRadius: "5px",
    padding: "12px"
}

const BoxSxContent: SxProps = {
    height: "100%",
    border: "1px dashed #9A9AB0",
    borderRadius: "inherit",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    textAlign: "center",
    padding: "15px"
}

const DropZone = forwardRef(({ title, imgField, imageLink = '' }: DropZone, ref) => {
    const [selectedImages, setSelectedImages] = useState([])
    const [path, setPath] = useState<string>("")
    
    const onDrop = useCallback((acceptedFiles: Array<File>) => {
        //Do sth with files
        setSelectedImages(acceptedFiles.map((file) => Object.assign(file, {
            preview: URL.createObjectURL(file)
        })))


        handleUploadImg(acceptedFiles)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const { getRootProps, getInputProps } = useDropzone({
        onDrop,
        accept: {
            'image/*': ['.jpeg', '.png', '.jpg']
        },
        maxFiles: 1, // Giới hạn chỉ chọn một tệp tin

    })
    useImperativeHandle(ref, () => path ? path : renderUrlImageUpload(imageLink), [path, imageLink])

    const dispatch = useDispatch<AppDispatch>()

    const handleUploadImg = async (files = []) => {
        const data = new FormData();
        for (let i = 0; i < files.length; i++) {
            data.append(`${imgField}Img`, files[i]);
        }
        dispatch(setValueBackrop(true))
        console.log('data :>> ', data);

        const res = await ApiUploadFileImage(data)
        dispatch(setValueBackrop(false))

        if (res.code === 200 && res.result) {
            setPath(res.data.filePath)
        } else {
            toast.error("Đăng tải ảnh thất bại. Vui lòng thử lại")
        }
    }

    return (
        <>
            <Typography sx={{ marginBottom: 5 }}>{title}</Typography>
            <Box sx={{ display: "flex", gap: "40px" }} >
                <Box {...getRootProps()} sx={BoxSxWrapper}>
                    <Box sx={BoxSxContent}>
                        <IconifyIcon icon="cil:cloud-upload" fontSize={40} style={{ flexShrink: 0 }} />
                        <Typography sx={{ fontSize: "15px" }}>Drag&Drop files here</Typography>
                        <Typography>or</Typography>
                        <Button variant='outlined' sx={{ border: "1px solid #15B3C9", fontSize: "8px", color: "#15B3C9" }}>Browse Files</Button>
                        <input {...getInputProps()} accept="image/*" hidden />
                    </Box>
                </Box>
                {
                    (selectedImages.length > 0 || imageLink) &&
                        Array.isArray(selectedImages) && selectedImages.length > 0 ? selectedImages.map((file, index) =>
                            <Box key={index} sx={BoxSxImg}>
                                <img src={file.preview} alt="img" style={{ width: "100%", height: "100%" }} />
                            </Box>) : imageLink && <Box sx={BoxSxImg}>
                                <img src={imageLink} alt="img" style={{ width: "100%", height: "100%" }} />
                            </Box>
                }

            </Box>
        </>
    )
})

export default DropZone