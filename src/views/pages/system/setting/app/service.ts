import { INIT_VALUE_APP_PROPS } from "./interface"

const INIT_VALUE_APP: INIT_VALUE_APP_PROPS = {
    id: null,
    brandCode: "",
    logo: "",
    splashcreen: "",
    textIntro: "",
}

export { INIT_VALUE_APP  }