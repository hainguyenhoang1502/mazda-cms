import { IResponeListUserManual } from "src/api/document/type"

export type IParamValuesFilter = {
  keyword: string
  startDate: string
  endDate: string
}

export type IPopupCreateUserManualProps = {
  isOpen: boolean,
  setToggle: Function
}

export type IRowDetailData = {
  row: IResponeListUserManual["data"]["details"][0]
}

export type IListDataTableProps = {
  getDeleteData: Function
  setModalConfirm: Function
}
