// ** MUI Imports
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import { useTheme } from '@mui/material/styles'
import Typography from "@mui/material/Typography";
import CardContent from "@mui/material/CardContent";

// ** React Imports
import { useEffect, useState } from "react";

// ** Component Imports
import ModalNotify from "src/views/component/modalNotify";
import { InputRequiredMUI, SelectMUI } from "src/views/component/theme";

// ** Config Imports
import { ModalNotifyState } from "src/configs/typeOption";
import { ERROR_NULL_VALUE_INPUT, INIT_STATE_MODAL_NOTIFY } from "src/configs/initValueConfig";

// ** Third Party Import
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';

// ** Style Imports
import { IPopupCreateUserManualProps } from "./interface";

// ** Api imports
import { ApiGetCreateUserManualCMS } from "src/api/document";

// ** Store Redux Imports
import { AppDispatch, RootState } from "src/store";
import { useDispatch, useSelector } from "react-redux";
import { fetchDataGetUserManual } from "src/store/apps/user-manual";
import { fetchDataGetFilterVehicleData } from "src/store/apps/vehicle/model";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 10,
};

const defaultValues = {
  modelCode: "",
  title: ""
}

const UserManualSchema = Yup.object().shape({
  title: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  modelCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT),
});

function PopupCreateUserManual(props: IPopupCreateUserManualProps) {

  // ** Props
  const { isOpen, setToggle } = props

  // ** States
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.userManual)
  const storeModel = useSelector((state: RootState) => state.vehicleData)

  useEffect(() => {
    if (storeModel && (!storeModel.arrFilter || storeModel.arrFilter.length === 0)) {
      dispatch(fetchDataGetFilterVehicleData())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const theme = useTheme()

  const handleCreate = async (value) => {
    const res = await ApiGetCreateUserManualCMS(value)
    if (res && res.code === 200 && res.result) {
      setModalNotify({
        title: "Thêm thành công",
        isOpen: true,
      })
      dispatch(fetchDataGetUserManual(store.param))
      setToggle()
    } else {
      setModalNotify({
        title: "Thêm thất bại",
        isOpen: true,
        message: res.message,
        isError: true
      })
    }
  }

  return (
    <>
      <Modal
        open={isOpen}
        onClose={() => setToggle()}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        sx={{ zIndex: theme.zIndex.modal }}
      >
        <Card sx={style}>
          <Formik
            initialValues={defaultValues}
            onSubmit={values => handleCreate(values)}
            validationSchema={UserManualSchema}
            validateOnBlur
          >
            {(props) => {

              return (
                <Form>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 500, paddingBottom: "20px", fontSize: "24px" }}>
                      Thông tin hướng dẫn sử dụng
                    </Typography>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={12} >
                        <Field
                          id="my-modelCode"
                          label="Dòng xe"
                          name="modelCode"
                          component={SelectMUI}
                          texterror={props.touched.modelCode && props.errors.modelCode}
                          texticon="*"
                          value={props.values.modelCode}
                          onChange={(event) => {
                            props.setFieldValue("modelCode", event.target.value);
                          }}
                          options={storeModel.arrFilter}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Field
                          id="my-title"
                          label="Tiêu đề"
                          name="title"
                          texticon="*"
                          component={InputRequiredMUI}
                          texterror={props.touched.title && props.errors.title}
                        />
                      </Grid>
                    </Grid>

                  </CardContent>
                  <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
                    <Button variant="contained" type="submit">Lưu</Button>
                    <Button color="error" variant="outlined" onClick={() => setToggle()} >Hủy</Button>
                  </CardContent>
                </Form>
              )
            }
            }
          </Formik>
        </Card>
      </Modal>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />

    </>
  );
}

export default PopupCreateUserManual;