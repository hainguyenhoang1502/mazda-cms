import { Box, TablePagination } from "@mui/material"
import { DataGrid, GridColDef } from "@mui/x-data-grid"

// ** Components 
import { IParamListUser } from "src/api/authen/type"

import { useDispatch, useSelector } from "react-redux"
import { AppDispatch, RootState } from "src/store"
import { IListDataTableProps, IRowDetailData } from "./interface"
import { fetchDataGetUserManual } from "src/store/apps/user-manual"
import { TypographyDataGrid } from "src/views/component/theme"
import { DeleteDataGrid, EditDataGrid } from "src/views/component/theme/customEditDelete"

const ListDataTable = (props: IListDataTableProps) => {

  // ** Props
  const { setModalConfirm, getDeleteData } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.userManual)

  const handleChangePage = (e, page) => {
    const param: IParamListUser = {
      ...store.param,
      pageIndex: page + 1,
    }
    dispatch(fetchDataGetUserManual(param))
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamListUser = {
      ...store.param,
      pageSize: Number(event.target.value),
    }
    dispatch(fetchDataGetUserManual(param))
  }

  const handleDelete = (item: IRowDetailData["row"]) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xóa hướng dẫn sử dụng ' + item.title,
      action: () => { getDeleteData(item.id) }
    })
  }

  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 100,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }: IRowDetailData) => {

        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission='' slug={`/documents-management/user-manual/edit/${row.id}`} />
            <DeleteDataGrid permission='' handleDelete={() => handleDelete(row)} />
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'fullName',
      headerName: 'dòng xe',
      sortable: false,
      renderCell: ({ row }: IRowDetailData) => {

        return (
          <Box sx={{ display: "flex", alignItems: "center", gap: "10px" }}>
            <TypographyDataGrid noWrap >
              {row.modelCode}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'email',
      sortable: false,
      headerName: 'Tiêu đề',
      renderCell: ({ row }: IRowDetailData) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.title}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 300,
      field: 'thumbnail',
      sortable: false,
      headerName: 'hình ảnh',
      renderCell: ({ row }: IRowDetailData) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center', height: "100%", }}>
            <img src={row.thumbnail} alt={row.thumbnail} width="150px" height="auto" style={{ objectFit: "cover" }} />
          </Box>
        )
      }
    }

  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={store.data || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterSelectedRowCount
        hideFooterPagination
        rowHeight={70}
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          }
        }}
      />
      <TablePagination
        component="div"
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  )
}
export default ListDataTable