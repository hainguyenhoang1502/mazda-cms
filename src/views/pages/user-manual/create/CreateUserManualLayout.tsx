
// ** MUI Imports
import Card from "@mui/material/Card";

// ** React Imports
import { useContext, useState } from "react";

// ** Next Imports
import router from "next/router";

// ** Component Imports
import ModalNotify from "src/views/component/modalNotify";
import ModalConfirm from "src/views/component/modalConfirm";

// ** Config Imports
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from "src/configs/initValueConfig";
import { ModalConfirmState, ModalNotifyState } from "src/configs/typeOption";

// ** Styles
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

// ** Store Redux Imports
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "src/store";
import { setValueBackrop } from "src/store/utils/loadingBackdrop";

// ** Api Imports
import { IParamCreateDetailPointUserManual, IParamCreatePointUserManual, IParamUpdateDetailPointUserManual, IParamUpdatePointUserManual } from "src/api/document/type";
import { ApiGetCreatePointDetailUserManualCMS, ApiGetCreatePointUserManualCMS, ApiGetDeletePointDetailUserManualCMS, ApiGetUpdatePointDetailUserManualCMS, ApiGetUpdatePointUserManualCMS } from "src/api/document";
import CreateUserManualLayoutForm from "./CreateUserManualLayoutForm";
import { EditPointUserManualContext } from "src/pages/documents-management/user-manual/layout/edit/[slug]";
import { Grid } from "@mui/material";

function CreateUserManualLayout() {

  // ** Store
  const store = useSelector((state: RootState) => state.userManual)
  const dispatch = useDispatch<AppDispatch>()

  // ** State
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Hooks
  const context = useContext(EditPointUserManualContext);

  const handleAddPoint = async (param: IParamCreatePointUserManual) => {
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    dispatch(setValueBackrop(true))
    const res = await ApiGetCreatePointUserManualCMS(param)
    dispatch(setValueBackrop(false))
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Tạo thành công",
      })
      router.push(`/documents-management/user-manual/edit/${store.id}`)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Tạo thất bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
    }
  }

  const handleUpdatePoint = async (param: IParamUpdatePointUserManual) => {
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    dispatch(setValueBackrop(true))
    const res = await ApiGetUpdatePointUserManualCMS(param)
    dispatch(setValueBackrop(false))
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thành công",
      })
    } else {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thất bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
    }
  }

  const getDeletePointDetail = async (id: number) => {
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    dispatch(setValueBackrop(true))
    const res = await ApiGetDeletePointDetailUserManualCMS(id)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Xóa thành công",
        message: res.message || UNKNOW_ERROR
      })
      await context.handleGetDetail()
    } else {
      setModalNotify({
        isOpen: true,
        title: "Xóa thất bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
    }
    dispatch(setValueBackrop(false))
  }

  const getUpdatePointDetail = async (param: IParamUpdateDetailPointUserManual) => {
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    dispatch(setValueBackrop(true))
    const res = await ApiGetUpdatePointDetailUserManualCMS(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thành công",
        message: res.message || UNKNOW_ERROR
      })
      await context.handleGetDetail()
    } else {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thất bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
    }
    dispatch(setValueBackrop(false))
  }

  const getCreatePointDetail = async (param: IParamCreateDetailPointUserManual) => {
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    dispatch(setValueBackrop(true))
    const res = await ApiGetCreatePointDetailUserManualCMS(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thành công",
        message: res.message || UNKNOW_ERROR
      })
      await context.handleGetDetail()
    } else {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thất bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
    }
    dispatch(setValueBackrop(false))
  }

  return (
    <Grid item>
      <Card>
        <CreateUserManualLayoutForm
          setModalConfirm={setModalConfirm}
          handleAddPoint={handleAddPoint}
          handleUpdatePoint={handleUpdatePoint}
          getDeletePointDetail={getDeletePointDetail}
          getUpdatePointDetail={getUpdatePointDetail}
          getCreatePointDetail={getCreatePointDetail}
        />
        <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
        <ModalNotify {...modalNotify} toggle={setModalNotify} />
      </Card>
    </Grid>
  );
}

export default CreateUserManualLayout;