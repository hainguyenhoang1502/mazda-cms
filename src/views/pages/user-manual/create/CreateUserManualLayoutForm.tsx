
// ** MUI Imports
import Button from "@mui/material/Button";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/system/Box";

// ** React Imports
import { useContext, useEffect, useRef, useState } from "react";

// ** Next Imports
import router from "next/router";

// ** Component Imports
import { InputRequiredMUI } from "src/views/component/theme";
import IconifyIcon from "src/@core/components/icon";
import EditorControlled from "src/components/Editor";

// ** Third Party Import
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';
import { ContentState, EditorState, convertToRaw } from 'draft-js'
import draftToHtml from "draftjs-to-html";
import { toast } from "react-hot-toast";

// ** Config Imports
import { ERROR_NULL_VALUE_INPUT, ERROR_VALUE_INPUT_IMAGE, MAX_SIZE_IMAGE } from "src/configs/initValueConfig";

// ** Styles
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

// ** Serivce Interface Type Imports
import { MuiBox, MuiCustomBox } from "./service";
import { CreateUserManualLayoutFormProps, IInitValueChild } from "./interface";

// ** Store Redux Imports
import { useSelector } from "react-redux";
import { RootState } from "src/store";

// ** Api Imports
import { IParamCreateDetailPointUserManual, IParamCreatePointUserManual, IParamUpdateDetailPointUserManual, IParamUpdatePointUserManual, IResponeDetailPointUserManual } from "src/api/document/type";
import { ApiUploadFileImage } from "src/api/refData";
import { EditPointUserManualContext } from "src/pages/documents-management/user-manual/layout/edit/[slug]";
import { renderUrlImageUpload } from "src/configs/functionConfig";
import htmlToDraft from "html-to-draftjs";




const initValuesChild = {
  title: "",
  image: "",
  urlImg: "",
  contentEditor: EditorState.createEmpty(),
  content: "",
  convertContent: "",
  userManualId: null,
  childId: "childern-0",
  isEdit: false
}

const userManualSchema = Yup.object().shape({
  title: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  coordinateX: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  coordinateY: Yup.string().required(ERROR_NULL_VALUE_INPUT),
});

function CreateUserManualLayoutForm(props: CreateUserManualLayoutFormProps) {

  // ** Props
  const { setModalConfirm, handleAddPoint, handleUpdatePoint, getDeletePointDetail, getCreatePointDetail, getUpdatePointDetail } = props

  // ** Hooks
  const context = useContext(EditPointUserManualContext);
  const isEdit = context && context.isEdit;
  const data = context && context.data

  // ** Store
  const store = useSelector((state: RootState) => state.userManual)

  // ** States Ref
  const imgRef = useRef(null);

  const [defaultValues, setDefaultValues] = useState({
    title: "",
    coordinateX: 20,
    coordinateY: 20,
  });
  const [children, setChildren] = useState<Array<IInitValueChild>>([initValuesChild])
  const [renderSize, setRenderSize] = useState({ width: 0, height: 0, render: 0, return: 0 });

  useEffect(() => {
    setDefaultValues({
      title: "",
      coordinateX: 20,
      coordinateY: 20,
    })
    setChildren([initValuesChild])
    const handleImageLoad = () => {
      const { width, height } = imgRef.current?.getBoundingClientRect();
      setRenderSize({
        width,
        height,
        render: (width / imgRef.current?.naturalWidth) || 1,
        return: (imgRef.current?.naturalWidth / width) || 1,
      });
    };

    // Add event listener to the image load event
    imgRef.current && imgRef.current.addEventListener('load', handleImageLoad);

    // Clean up the event listener when the component unmounts
    return () => {

      // eslint-disable-next-line react-hooks/exhaustive-deps
      imgRef.current && imgRef.current.removeEventListener('load', handleImageLoad);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const handleResize = () => {
      // Get the current size of the image
      const { width, height } = imgRef.current.getBoundingClientRect();
      setRenderSize({
        width,
        height,
        render: (width / imgRef.current?.naturalWidth) || 1,
        return: (imgRef.current?.naturalWidth / width) || 1,
      });

    };

    // Create a new ResizeObserver instance
    const resizeObserver = new ResizeObserver(handleResize);

    // Observe the image element for size changes
    if (imgRef.current) {
      resizeObserver.observe(imgRef.current);
    }

    // Cleanup the observer on component unmount
    return () => {
      resizeObserver.disconnect();
    };
  }, []);


  useEffect(() => {
    if (data) {
      convertData(data)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data])

  useEffect(() => {
    if (data) {
      setDefaultValues({
        title: data.title,
        coordinateX: Math.floor(data.coordinateX),
        coordinateY: Math.floor(data.coordinateY)
      })
    }
  }, [data, renderSize])

  const convertData = (data: IResponeDetailPointUserManual["data"]) => {
    const arr = []
    data.details.map((detail) => {
      const item: IInitValueChild = {
        childId: detail.id,
        image: detail.image,
        content: detail.content,
        contentEditor: converHtmlToEditor(detail.content),
        userManualId: store.id,
        title: detail.title,
        urlImg: detail.image,
      }
      arr.push(item)
    })
    setChildren(arr)
  }

  const converHtmlToEditor = (content: string) => {
    const blocksFromHtml = htmlToDraft(content);
    const { contentBlocks, entityMap } = blocksFromHtml;
    const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
    const editorState = EditorState.createWithContent(contentState);
    
    return editorState;
  }

  const handleChangInputContent = (value: EditorState, index: number) => {
    const arr = [...children]
    arr[index].contentEditor = value
    arr[index].isEdit = true
    const rawContentState = convertToRaw(value.getCurrentContent());
    const markup = draftToHtml(rawContentState);
    arr[index].content = markup
    setChildren(arr)
  }

  // ** Handle Click Img get location
  const handleClick = (event) => {
    const rect = event.target.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    setDefaultValues({
      ...defaultValues,
      coordinateX: Math.floor(x * renderSize.return),
      coordinateY: Math.floor(y * renderSize.return),
    });
  };

  const handleChangInput = (value: string, field: string, index: number) => {
    const arr = [...children]
    arr[index][field] = value
    arr[index].isEdit = true
    setChildren(arr)
  }

  // ** Handle Upload File
  const handleFileSelected = async (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const files = Array.from(e.target.files)
    if (!files[0]) return
    if (files[0]?.size > MAX_SIZE_IMAGE) {
      toast.error(ERROR_VALUE_INPUT_IMAGE)

      return
    } else {
      const reader = new FileReader();
      const arr = [...children]
      reader.onloadend = () => {
        arr[index].urlImg = reader.result
      }
      reader.readAsDataURL(files[0]);
      const data = new FormData();
      for (let i = 0; i < files.length; i++) {
        data.append('avatar', files[i]);
      }
      const res = await ApiUploadFileImage(data)
      if (res.code === 200 && res.result) {
        arr[index].image = res.data.filePath
      } else {
        toast.error("Đăng tải ảnh thất bại. Vui lòng thử lại")
      }

      // ** Check update content when update point 
      arr[index].isEdit = true
      setChildren(arr)
    }
  }

  const handleAddChild = () => {
    const arr = [...children]
    const value = { ...initValuesChild }
    value.childId = 'children-' + new Date().toISOString()
    arr.push(value)
    setChildren(arr)
  }

  const handleRemove = (id: string | number) => {
    const arr = [...children]
    const newArr = arr.filter((item) => item.childId !== id)
    setChildren(newArr)
  }

  const renderNumberValueInput = (value: number, max: number) => {
    if (value > max) return max
    if (value < 0) return 0

    return value
  }

  const handleSumbit = async () => {
    let isCanCreate = true
    const pointDetails = []

    // ** Check Value Content
    children.forEach((item) => {
      if (!item.content) {
        isCanCreate = false

        return
      } else {
        const param = {
          title: item.title,
          image: item.image,
          content: item.content,
          userManualId: store.id,
        }
        pointDetails.push(param)
      }
    })

    // ** Notify Error
    if (!isCanCreate) {
      toast.error("Vui lòng nhập đầy đủ nội dung ở thông tin chi tiết")
    } else {
      const param: IParamCreatePointUserManual = {
        title: defaultValues.title,
        coordinateX: Math.floor(defaultValues.coordinateX),
        coordinateY: Math.floor(defaultValues.coordinateY),
        userManualId: store.id,
        pointDetails,
      }

      // ** Modal Confirm action
      setModalConfirm({
        isOpen: true,
        title: "Bạn đang thao tác Tạo bố cục hiển thị mới",
        action: () => {
          return handleAddPoint(param)
        }
      })
    }
  }

  const handleSubmitEdit = () => {
    const param: IParamUpdatePointUserManual = {
      id: data?.id,
      title: defaultValues.title,
      coordinateX: Math.floor(defaultValues.coordinateX),
      coordinateY: Math.floor(defaultValues.coordinateY),
      userManualId: store.id
    }
    setModalConfirm({
      isOpen: true,
      title: "Bạn đang thao tác Cập nhật bố cục hiển thị. Lưu ý thao tác này chỉ cập nhật Thông tin tổng quan.",
      action: () => {
        return handleUpdatePoint(param)
      }
    })
  }

  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: "Bạn đang thao tác Huỷ cập nhật thông tin hướng dẫn sử dụng",
      action: () => router.push(`/documents-management/user-manual/edit/${store.id}`)
    })
  }

  const handleDeletePoint = (id: number | string) => {
    setModalConfirm({
      isOpen: true,
      title: "Bạn đang thao tác Xóa chức năng. Lưu ý thao tác này sẽ không thể khôi phục! ",
      action: () => getDeletePointDetail(Number(id))
    })
  }

  const handleUpdatePointDetail = (child: IInitValueChild) => {

    if (Number(child.childId)) {
      const param: IParamUpdateDetailPointUserManual = {
        id: Number(child.childId),
        title: child.title,
        image: renderUrlImageUpload(child.image),
        content: child.content,
        pointId: data.id,
        userManualId: store.id
      }
      setModalConfirm({
        isOpen: true,
        title: "Bạn đang thao tác cập nhật thông tin  chức năng",
        action: () => getUpdatePointDetail(param)
      })
    } else {
      if (!child.content) {
        toast.error("Vui lòng nhập đầy đủ nội dung ở thông tin chi tiết")
      } else {
        const param: IParamCreateDetailPointUserManual = {
          title: child.title,
          image: child.image,
          content: child.content,
          pointId: data.id,
          userManualId: store.id
        }
        setModalConfirm({
          isOpen: true,
          title: "Bạn đang thao tác tạo mới thông tin chức năng",
          action: () => getCreatePointDetail(param)
        })
      }
    }
  }

  return (
    <Formik
      initialValues={defaultValues}
      onSubmit={() => isEdit ? handleSubmitEdit() : handleSumbit()}
      enableReinitialize={true}
      validationSchema={userManualSchema}
      validateOnBlur
    >
      {(props) => {
        return (
          <Form>
            <CardContent>
              <Typography noWrap sx={{ fontWeight: 500, paddingBottom: "20px", fontSize: "24px", textAlign: "center" }}>
                THÔNG TIN TỔNG QUAN
              </Typography>
              <Grid item container spacing={6} marginBottom={5}>
                <Grid item xs={12} >
                  <Field
                    id="my-title"
                    label="Nhóm chức năng"
                    name="title"
                    component={InputRequiredMUI}
                    texterror={props.touched.title && props.errors.title}
                    texticon="*"
                    value={props.values.title}
                    onChange={(e) => {
                      props.setFieldValue("title", e.target.value)
                      setDefaultValues({ ...defaultValues, title: e.target.value })
                    }}
                  />
                </Grid>
              </Grid>
              <Grid item container spacing={6} marginBottom={5}>
                <Grid item xs={6} >
                  <Field
                    id="my-name"
                    label="Tọa độ x"
                    name="coordinateX"
                    type="number"
                    component={InputRequiredMUI}
                    texterror={props.touched.coordinateX && props.errors.coordinateX}
                    texticon="*"
                    value={props.values.coordinateX}
                    onChange={(e) => {
                      props.setFieldValue("coordinateX", renderNumberValueInput(e.target.value, renderSize.width))
                      setDefaultValues({ ...defaultValues, coordinateX: renderNumberValueInput(e.target.value, renderSize.width) })
                    }}
                  />
                </Grid>
                <Grid item xs={6} >
                  <Field
                    id="my-coordinateY"
                    label="Tọa độ y"
                    name="coordinateY"
                    type="number"
                    component={InputRequiredMUI}
                    texterror={props.touched.coordinateY && props.errors.coordinateY}
                    texticon="*"
                    value={props.values.coordinateY}
                    onChange={(e) => {
                      props.setFieldValue("coordinateY", renderNumberValueInput(e.target.value, renderSize.height))
                      setDefaultValues({ ...defaultValues, coordinateY: renderNumberValueInput(e.target.value, renderSize.height) })
                    }}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={6} marginBottom={5}>
                <Grid item xs={12} sx={{ width: '100%', height: '100%' }}>
                  <Box position="relative" >
                    <img
                      ref={imgRef}
                      src={store.imageAddLayout}
                      alt={"imageAddLayout"}
                      onClick={handleClick}
                      style={{ width: '100%', height: "100%", objectFit: "cover" }} />

                    <MuiCustomBox
                      sx={{
                        left: `${Math.floor(defaultValues.coordinateX * renderSize.render)}px`,
                        top: `${Math.floor(defaultValues.coordinateY * renderSize.render)}px`,
                      }}
                    />
                  </Box>
                </Grid>
              </Grid>
            </CardContent>
            <CardContent >
              <Box sx={{ border: "1px solid rgba(50, 71, 92, 0.2)" }}>
                <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px", textAlign: "center" }}>
                  THÔNG TIN CHI TIẾT
                </Typography>
                {
                  children.map((child, index: number) => (
                    <MuiBox key={index}>
                      <Grid container spacing={6} marginBottom={5}>
                        <Grid item xs={10} >
                          <Grid container spacing={6} marginBottom={5}>
                            <Grid item xs={7} >
                              <InputRequiredMUI
                                id="my-title"
                                label="Tên chức năng"
                                field={{
                                  value: child.title,
                                  onChange: (e) => handleChangInput(e.target.value, "title", index),
                                }}
                              />
                            </Grid>
                            <Grid item xs={5} >
                              <Box
                                sx={{
                                  border: "1px dashed #9A9AB0",
                                  borderRadius: "5px",
                                  height: '100%',
                                  justifyContent: "center",
                                  alignItems: 'center',
                                  display: "flex"
                                }}
                              >
                                <Button aria-label="upload picture" component="label" sx={{ p: 0 }}>
                                  {
                                    child.urlImg ?
                                      <img src={child.urlImg.toString()} alt={child.childId.toString()} width="100%" height="50px" />
                                      : "Upload hình ảnh"
                                  }
                                  <input hidden accept="image/gif, image/jpg, image/jpeg, image/png" type="file" onChange={(e) => handleFileSelected(e, index)} />
                                </Button>
                              </Box>
                            </Grid>
                          </Grid>
                          <Grid container spacing={6} marginBottom={5}>
                            <Grid item xs={12} >
                              <EditorControlled value={child.contentEditor} setValue={(e) => handleChangInputContent(e, index)} />
                            </Grid>
                          </Grid>
                        </Grid>
                        <Grid item xs={2} >
                          <Box display="flex" justifyContent="center" alignItems="center" height="100%" gap="10px">
                            {
                              index + 1 === children.length &&
                              <IconifyIcon icon="teenyicons:plus-circle-outline" onClick={handleAddChild} fontSize="22px" cursor="pointer" />
                            }
                            {
                              children.length > 1 &&
                              <IconifyIcon icon="teenyicons:minus-circle-outline" onClick={() => { Number(child.childId) ? handleDeletePoint(child.childId) : handleRemove(child.childId) }} fontSize="22px" cursor="pointer" />
                            }
                            {
                              isEdit && child.isEdit &&
                              <IconifyIcon icon="teenyicons:tick-circle-outline" onClick={() => handleUpdatePointDetail(child)} fontSize="22px" cursor="pointer" />
                            }
                          </Box>
                        </Grid>
                      </Grid>
                    </MuiBox>
                  ))
                }
              </Box>
            </CardContent>
            <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
              <Button variant="contained" type="submit">Lưu</Button>
              <Button color="error" variant="outlined" onClick={() => handleCancel()} >Hủy</Button>
            </CardContent>
          </Form>
        )
      }}
    </Formik>

  );
}

export default CreateUserManualLayoutForm;