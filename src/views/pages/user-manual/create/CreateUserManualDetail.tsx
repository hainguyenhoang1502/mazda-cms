
// ** MUI Imports
import { Box } from "@mui/system";
import Card from "@mui/material/Card"
import Grid from "@mui/material/Grid";
import Table from "@mui/material/Table";
import Button from "@mui/material/Button"
import TableBody from "@mui/material/TableBody";
import Typography from "@mui/material/Typography"
import CardContent from "@mui/material/CardContent"
import { TableHead, TableRow } from "@mui/material";
import TableContainer from "@mui/material/TableContainer";

// ** React Imports
import { useContext, useEffect, useRef, useState } from "react"

// ** Next Imports
import router from "next/router"

// ** Third Party Import
import * as Yup from 'yup';
import { Formik, Form, Field, FormikProps } from 'formik';
import { toast } from "react-hot-toast";

// ** Components Imports
import ModalConfirm from "src/views/component/modalConfirm"
import FileUploaderSingle from "src/components/FileUpload/FileUploaderSingle";
import Icon from "src/@core/components/icon";

import { InputRequiredMUI, SelectMUI } from "src/views/component/theme";

// ** Interface Services Imports 
import { CustomTableCell, INIT_VALUE_DATA_BODY_CREATE, MuiCustomBox, MuiInputLabel, getFractionFromRatio } from "./service"
import { CreateUserManualDetailProps } from "./interface"

// ** Context Imports
import { EditUserManualContext } from "src/pages/documents-management/user-manual/edit/[slug]";

// ** Config Imports
import { ERROR_NULL_VALUE_INPUT } from "src/configs/initValueConfig";
import { IResponeDetailUserManual } from "src/api/document/type";

// ** Store Redux
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "src/store";
import { handleSetAddLayout } from "src/store/apps/user-manual";

// ** Api Imports
import { ApiUploadFileImage } from "src/api/refData";
import { setValueBackrop } from "src/store/utils/loadingBackdrop";
import { fetchDataGetFilterVehicleData } from "src/store/apps/vehicle/model";

const UserManualDetailSchema = Yup.object().shape({
  title: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  modelCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT),

});

const CreateUserManualDetail = (props: CreateUserManualDetailProps) => {

  // ** Hooks
  const context = useContext(EditUserManualContext);
  const isEdit = context && context.isEdit;

  // ** Props
  const { modalConfirm, setModalConfirm, handleUpdate, handleDelete } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const storeModel = useSelector((state: RootState) => state.vehicleData)

  //** State
  const [isViewDemo, setIsViewDemo] = useState<boolean>(false)
  const imgRef = useRef(null);
  const [renderSize, setRenderSize] = useState(0);

  useEffect(() => {
    if (context.data && imgRef.current) {
      imgRef.current.src = context.data.image
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context && context.data])

  useEffect(() => {
    if (storeModel && (!storeModel.arrFilter || storeModel.arrFilter.length === 0)) {
      dispatch(fetchDataGetFilterVehicleData())
    }
    const handleImageLoad = () => {
      const { width } = imgRef.current.getBoundingClientRect();
      setRenderSize((width / imgRef.current?.naturalWidth) || 1);
    };

    // Add event listener to the image load event
    imgRef.current && imgRef.current.addEventListener('load', handleImageLoad);

    // Clean up the event listener when the component unmounts
    return () => {

      // eslint-disable-next-line react-hooks/exhaustive-deps
      imgRef.current && imgRef.current.removeEventListener('load', handleImageLoad);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const handleResize = () => {
      // Get the current size of the image
      const { width } = imgRef.current.getBoundingClientRect();
      setRenderSize(width / imgRef.current?.naturalWidth || 1);
    };

    // Create a new ResizeObserver instance
    const resizeObserver = new ResizeObserver(handleResize);

    // Observe the image element for size changes
    if (imgRef.current) {
      resizeObserver.observe(imgRef.current);
    }

    // Cleanup the observer on component unmount
    return () => {
      resizeObserver.disconnect();
    };
  }, []);


  const handleSubmit = async (values: IResponeDetailUserManual["data"]) => {
    if (!values.thumbnail) {
      toast.error("Bạn chưa chọn hình ảnh Thumbnail")
    } else {
      if (!values.image) {
        toast.error("Bạn chưa chọn hình chi tiết");
      } else {
        setModalConfirm({
          isOpen: true,
          title: "Bạn đang thao tác Cập nhật thông tin hướng dẫn sử dụng",
          action: () => {
            return handleUpdate(values, context.data.id)
          }
        })
      }
    }
  }

  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: "Bạn đang thao tác Huỷ cập nhật thông tin hướng dẫn sử dụng",
      action: () => router.push("/documents-management/user-manual")
    })
  }


  const handleUploadImg = async (props: FormikProps<IResponeDetailUserManual["data"]>, value: any, field: string) => {
    
    const files: any = Array.from(value)
    const reader = new FileReader();
    reader.onloadend = () => {
      if (field === "image") {
        imgRef.current.src = reader.result
      }
    }
    reader.readAsDataURL(files[0]);
    const data = new FormData();
    for (let i = 0; i < files.length; i++) {
      data.append('avatar', files[i]);
    }
    dispatch(setValueBackrop(true))
    const res = await ApiUploadFileImage(data)
    dispatch(setValueBackrop(false))

    if (res.code === 200 && res.result) {
      props.setFieldValue(field, res.data.filePath)
      if (field === "image") {
        props.setFieldValue("imageWidth", imgRef.current?.naturalWidth)
        props.setFieldValue("imageHeight", imgRef.current?.naturalHeight)
        props.setFieldValue("imageRatio", getFractionFromRatio(imgRef.current?.naturalWidth, imgRef.current?.naturalHeight))
      }
    } else {
      toast.error("Đăng tải ảnh thất bại. Vui lòng thử lại")
    }
  }

  const renderDataForm = (data: IResponeDetailUserManual["data"]) => {
    if (!data) return INIT_VALUE_DATA_BODY_CREATE

    return {
      ...data,
      thumbnailFiles: data.thumbnail ? [data.thumbnail] : [],
      imageFiles: data.image ? [data.image] : []
    }
  }

  const handleAddLayout = (img: any) => {

    if (img && img.length > 0) {
      const param = {
        id: context.id,
        imageAddLayout: imgRef.current.src
      }
      dispatch(handleSetAddLayout(param))
      router.push(`/documents-management/user-manual/layout/create#${context.id}`)
    } else {
      toast.error("Bạn chưa chọn hình chi tiết để thêm bố cục mới. Vui lòng thêm hình chi tiết")
    }
  }

  const handleEditLayout = (id: number) => {
    if (imgRef.current && imgRef.current.src) {
      const param = {
        id: context.id,
        imageAddLayout: imgRef.current.src
      }
      dispatch(handleSetAddLayout(param))
      router.push(`/documents-management/user-manual/layout/edit/${id}#${context.id}`)
    } else {
      toast.error("Bạn chưa chọn hình chi tiết để thêm bố cục mới. Vui lòng thêm hình chi tiết")
    }
  }

  const handleViewDemo = (img: any) => {
    if (img && img.length > 0) {
      setIsViewDemo(!isViewDemo)
    } else {
      toast.error("Bạn chưa chọn hình chi tiết để xem mô phỏng. Vui lòng thêm hình chi tiết")
    }
  }

  // ** Handle Delete
  const handleDeletePoint = (item: IResponeDetailUserManual["data"]["points"][0]) => {
    setModalConfirm({
      isOpen: true,
      title: "Bạn đang thao tác Xóa nhóm chức năng" + item.title,
      action: () => handleDelete(item.id)
    })
  }

  return (
    <Card>
      <Formik
        initialValues={isEdit && renderDataForm(context.data)}
        onSubmit={values => handleSubmit(values)}
        validationSchema={UserManualDetailSchema}
        validateOnBlur
      >
        {(propsForm) => {

          return (
            <Form>
              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, paddingBottom: "20px", fontSize: "24px" }}>
                  Thông tin hướng dẫn sử dụng
                </Typography>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item xs={12} >
                    <Field
                      id="my-modelCode"
                      label="Dòng xe"
                      name="modelCode"
                      component={SelectMUI}
                      texterror={propsForm.touched.modelCode && propsForm.errors.modelCode}
                      texticon="*"
                      value={propsForm.values.modelCode}
                      onChange={(event) => {
                        propsForm.setFieldValue("modelCode", event.target.value);
                      }}
                      options={storeModel.arrFilter}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      id="my-title"
                      label="Tiêu đề"
                      name="title"
                      component={InputRequiredMUI}
                      texterror={propsForm.touched.title && propsForm.errors.title}
                    />
                  </Grid>
                </Grid>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item xs={6} >
                    <Box height="225px">
                      <MuiInputLabel>Chọn hình ảnh Thumbnail</MuiInputLabel>
                      <Field
                        name="thumbnailFiles"
                        component={FileUploaderSingle}
                        texterror={propsForm.touched.thumbnailFiles && propsForm.errors.thumbnailFiles}
                        texticon="*"
                        files={propsForm.values.thumbnailFiles || []}
                        setFiles={(e) => {
                          propsForm.setFieldValue("thumbnailFiles", e)
                          handleUploadImg(propsForm, e, "thumbnail")
                        }}
                      />
                    </Box>
                  </Grid>
                  <Grid item xs={6} >
                    <Box height="225px">
                      <MuiInputLabel>Chọn hình chi tiết</MuiInputLabel>
                      <Field
                        id="my-imageFiles"
                        label="Dòng xe"
                        name="imageFiles"
                        component={FileUploaderSingle}
                        texterror={propsForm.touched.imageFiles && propsForm.errors.imageFiles}
                        texticon="*"
                        files={propsForm.values.imageFiles || []}
                        setFiles={(e) => {
                          propsForm.setFieldValue("imageFiles", e),
                            handleUploadImg(propsForm, e, "image")
                        }}
                      />
                    </Box>
                  </Grid>
                </Grid>
              </CardContent>
              <CardContent>
                <Box
                  sx={{
                    visibility: isViewDemo ? "visible" : "hidden",
                    opacity: isViewDemo ? 1 : 0,
                    transition: "all linear 0.3s",
                    overflow: "hidden",
                    height: isViewDemo ? "100%" : 0,
                    position: "relative"
                  }}>
                  <img
                    ref={imgRef}
                    alt={"imageAddLayout"}
                    style={{
                      width: '100%',
                      height: "100%",
                      objectFit: "cover",
                    }}
                  />
                  {
                    context.data && context.data.points && context.data.points.length > 0 && context.data.points.map((item) => (
                      <MuiCustomBox
                        key={item.coordinateX}
                        sx={{
                          left: `${Math.floor(item.coordinateX * renderSize)}px`,
                          top: `${Math.floor(item.coordinateY * renderSize)}px`,
                        }}
                      />
                    ))
                  }
                </Box>
              </CardContent>
              <CardContent>
                <Box sx={{ display: "flex", justifyContent: "space-between", padding: "20px 0", }}>

                  <Typography noWrap sx={{ fontWeight: 500, fontSize: "24px" }}>
                    Bố cục hiển thị
                  </Typography>
                  <Button variant="outlined" onClick={() => handleViewDemo(propsForm.values.imageFiles)}>{!isViewDemo ? "Xem mô phỏng" : "Ẩn mô phỏng"}</Button>
                  <Button variant="outlined" onClick={() => handleAddLayout(propsForm.values.imageFiles)}>Thêm mới</Button>
                </Box>
                <TableContainer sx={{ paddingBottom: "20px" }}>
                  <Table size='small'>
                    <TableHead>
                      <TableRow>
                        <CustomTableCell>Nhóm chức năng</CustomTableCell>
                        <CustomTableCell>Toạ độ x</CustomTableCell>
                        <CustomTableCell>Toạ độ y</CustomTableCell>
                        <CustomTableCell>Tên chức năng</CustomTableCell>
                        <CustomTableCell>Hình ảnh</CustomTableCell>
                        {/* <CustomTableCell>Nội dung chi tiết</CustomTableCell> */}
                        <CustomTableCell>Hành động</CustomTableCell>

                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {
                        context.data && context.data.points && context.data.points.length > 0 && context.data.points.map((item) => (
                          item.details && item.details.length > 0 ? item.details.map((detail, indexDetail) => (
                            <TableRow key={item.id + indexDetail}>
                              {
                                indexDetail === 0 &&
                                <>
                                  <CustomTableCell rowSpan={item.details.length}>{item.title || "-"}</CustomTableCell>
                                  <CustomTableCell rowSpan={item.details.length}>{Math.floor(item.coordinateX)}</CustomTableCell>
                                  <CustomTableCell rowSpan={item.details.length}>{Math.floor(item.coordinateY)}</CustomTableCell>
                                </>
                              }
                              <CustomTableCell>{detail.title || "-"}</CustomTableCell>
                              <CustomTableCell>
                                {
                                  detail.image ?
                                    <Box height="100px">
                                      <img src={detail.image} alt="detail" height="100%" width="auto" />
                                    </Box>
                                    : "-"
                                }
                              </CustomTableCell>
                              {/* <CustomTableCell >
                                <Box dangerouslySetInnerHTML={{ __html: detail.content }}
                                  sx={{
                                    maxWidth: "450px",
                                    overflow: "hidden",
                                    display: '-webkit-box',
                                    WebkitLineClamp: 2,
                                    lineClamp: 2,
                                    textOverflow: "ellipsis",
                                    boxOrient: "vertical",
                                    WebkitBoxOrient: "vertical",
                                  }}
                                >
                                </Box>
                              </CustomTableCell> */}
                              {
                                indexDetail === 0 &&
                                <CustomTableCell rowSpan={item.details.length}>
                                  <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: "center" }}>
                                    <Button
                                      sx={{ p: 0, minWidth: "30px" }}
                                      onClick={() => handleEditLayout(item.id)}
                                    >
                                      <Icon icon='bx:pencil' fontSize={20} color="#32475CDE" />
                                    </Button >
                                    <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                                      onClick={() => handleDeletePoint(item)}>
                                      <Icon icon='bx:trash' fontSize={20} color="#32475CDE" />
                                    </Button>
                                  </Box>
                                </CustomTableCell>
                              }
                            </TableRow>

                          ))
                            :
                            <TableRow key={item.id}>
                              <CustomTableCell>{item.title || "-"}</CustomTableCell>
                              <CustomTableCell>{Math.floor(item.coordinateX)}</CustomTableCell>
                              <CustomTableCell>{Math.floor(item.coordinateY)}</CustomTableCell>
                              <CustomTableCell>-</CustomTableCell>
                              <CustomTableCell>-</CustomTableCell>
                              <CustomTableCell>-</CustomTableCell>
                              <CustomTableCell>
                                <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: "center" }}>
                                  <Button
                                    sx={{ p: 0, minWidth: "30px" }}
                                    onClick={() => handleEditLayout(item.id)}
                                  >
                                    <Icon icon='bx:pencil' fontSize={20} color="#32475CDE" />
                                  </Button >

                                  <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                                    onClick={() => handleDeletePoint(item)}>
                                    <Icon icon='bx:trash' fontSize={20} color="#32475CDE" />
                                  </Button>

                                </Box>
                              </CustomTableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </CardContent>
              <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
                <Button variant="contained" type="submit">Lưu</Button>
                <Button color="error" variant="outlined" onClick={handleCancel} >Hủy</Button>
              </CardContent>
            </Form>
          )
        }
        }
      </Formik>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Card>
  )
}
export default CreateUserManualDetail  