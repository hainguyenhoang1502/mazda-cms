import { EditorState } from "draft-js"
import { ModalConfirmState } from "src/configs/typeOption"
import { IParamCreateDetailPointUserManual, IParamCreatePointUserManual, IParamUpdateDetailPointUserManual, IParamUpdatePointUserManual, IParamUpdateUserManual } from 'src/api/document/type'

interface CreateUserManualDetailProps {
  setModalConfirm: Function,
  modalConfirm: ModalConfirmState
  handleUpdate: (values: IParamUpdateUserManual, id: number) => void
  handleDelete: (id: number) => void
}

interface IInitValueChild {
  title: string,
  image: string,
  urlImg: string | ArrayBuffer,
  content: string,
  contentEditor: EditorState
  userManualId: number,
  childId: string | number
  isEdit?: boolean
}

interface CreateUserManualLayoutFormProps {
  setModalConfirm: Function
  handleAddPoint: (param: IParamCreatePointUserManual) => void
  handleUpdatePoint: (param: IParamUpdatePointUserManual) => void
  getDeletePointDetail: (id: number) => void
  getUpdatePointDetail: (param: IParamUpdateDetailPointUserManual) => void
  getCreatePointDetail: (param: IParamCreateDetailPointUserManual) => void
}

export type {
  CreateUserManualDetailProps,
  CreateUserManualLayoutFormProps,
  IInitValueChild
}
