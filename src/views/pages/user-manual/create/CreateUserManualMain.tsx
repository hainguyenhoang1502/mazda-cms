
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React  
import { useContext, useState } from 'react'

// ** Components 
import ModalNotify from 'src/views/component/modalNotify'
import CreateUserManualDetail from './CreateUserManualDetail'

// ** Api  
import { IParamUpdateUserManual } from 'src/api/document/type'
import { ApiGetDeletePointUserManualCMS, ApiGetUpdateUserManualCMS } from 'src/api/document'

// ** Config 
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { renderUrlImageUpload } from 'src/configs/functionConfig'

// ** Store Redux Imports
import { useDispatch } from 'react-redux'
import { AppDispatch } from 'src/store'
import { setValueBackrop } from 'src/store/utils/loadingBackdrop'
import { handleSetReloadDataUserManual } from 'src/store/apps/user-manual'
import router from 'next/router'

// ** Context Imports
import { EditUserManualContext } from 'src/pages/documents-management/user-manual/edit/[slug]'

const CreateUserManualMain = () => {

  // ** State
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  //** Store
  const dispatch = useDispatch<AppDispatch>()

  // ** Hooks
  const context = useContext(EditUserManualContext);

  const handleUpdate = async (data: IParamUpdateUserManual, id: number) => {
    const param = {
      title: data.title,
      thumbnail: renderUrlImageUpload(data.thumbnail),
      image: renderUrlImageUpload(data.image),
      imageWidth: data.imageWidth,
      imageHeight: data.imageHeight,
      imageRatio: data.imageRatio,
      modelCode: data.modelCode,
      id: id,
    }
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    dispatch(setValueBackrop(true))
    const res = await ApiGetUpdateUserManualCMS(param)
    dispatch(setValueBackrop(false))
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thành Công",
        textBtn: "Quay lại",
        actionReturn: () => router.push("/documents-management/user-manual/")
      })
      dispatch(handleSetReloadDataUserManual())
    } else {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thất Bại",
        message: res.message || UNKNOW_ERROR
      })
    }
  }

  const getDeletePoint = async (id: number) => {
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    dispatch(setValueBackrop(true))
    const res = await ApiGetDeletePointUserManualCMS(id)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Xóa thành công",
        message: res.message || UNKNOW_ERROR
      })
      context.handleGetDetail()

    } else {
      setModalNotify({
        isOpen: true,
        title: "Xóa thất bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
    }
    dispatch(setValueBackrop(false))
  }



  return (
    <>
      <Grid item xs={12} p={0}>
        <CreateUserManualDetail
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdate={handleUpdate}
          handleDelete={getDeletePoint}
        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />

    </>
  )
}
export default CreateUserManualMain
