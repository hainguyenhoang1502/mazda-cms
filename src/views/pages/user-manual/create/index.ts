import CreateUserManualMain from "./CreateUserManualMain"
import CreateUserManualLayout from "./CreateUserManualLayout"

export {
    CreateUserManualMain,
    CreateUserManualLayout
}