import styled from '@emotion/styled'
import InputLabel from '@mui/material/InputLabel'
import TableCell from '@mui/material/TableCell'
import Box from '@mui/system/Box'
import { IResponeDetailUserManual } from 'src/api/document/type'

const MuiInputLabel = styled(InputLabel)({
  paddingBottom: "12px"
})

const MuiBox = styled(Box)({
  padding: "20px",
  border: "1px solid rgba(50, 71, 92, 0.2)",
  margin: "20px"
})

const MuiCustomBox = styled(Box)({
  position: 'absolute',
  borderRadius: "50%",
  width: '40px',
  height: '40px',
  backgroundColor: 'red',
  transform: 'translate(-20px,-20px)',
  border: '5px solid #FFF'
})

const CustomTableCell = styled(TableCell)({
  border: '1px solid',
  color: "unset",
  textAlign: "start",
  padding: "6px 10px !important",
  '&.MuiTableCell-body': {
    color: "unset"
  },
  '&.MuiTableCell-head': {
    fontWeight: "700"
  },

})

const INIT_VALUE_DATA_BODY_CREATE: IResponeDetailUserManual["data"] = {
  points: [],
  id: 0,
  title: "",
  thumbnail: "",
  image: "",
  imageWidth: 0,
  imageHeight: 0,
  imageRatio: "",
  modelCode: "",
  thumbnailFiles: [],
  imageFiles: [],
}

const getFractionFromRatio = (naturalWidth: number, naturalHeight: number) => {

  // Find the greatest common divisor (GCD) between the numerator and denominator
  function gcd(a, b) {
    if (b === 0) {
      return a;
    }
    
    return gcd(b, a % b);
  }

  // Calculate the GCD
  const divisor = gcd(naturalWidth, naturalHeight);

  // Calculate the numerator and denominator
  const numerator = naturalWidth / divisor;
  const denominator = naturalHeight / divisor;

  return numerator + ":" + denominator;
}


export {
  MuiInputLabel,
  CustomTableCell,
  MuiCustomBox,
  MuiBox,
  INIT_VALUE_DATA_BODY_CREATE,
  getFractionFromRatio
}
