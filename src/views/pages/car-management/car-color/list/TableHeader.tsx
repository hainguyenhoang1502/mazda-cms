// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import { InputAdornment } from '@mui/material'
import TextField from '@mui/material/TextField'

// ** React Imports
import {  useState } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Interface Services Imports

// ** Utils Imports

// ** Store Redux Imports
import { AppDispatch, RootState } from 'src/store'
import { useDispatch, useSelector } from 'react-redux'
import { IParamVehicleListColor } from 'src/api/vehicles/type'
import { fetchDataGetListVehicleColor } from 'src/store/apps/car-management/car-color'


const TableHeader = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.listVehicleColor)

  const [valuesFilter, setValuesFilter] = useState<IParamVehicleListColor>(store.param)

  const handleChangeInput = (value: string, field: string) => {
    const body = { ...valuesFilter }

    body[field] = value
    setValuesFilter(body)
  }

  const handleFilter = () => {
    dispatch(fetchDataGetListVehicleColor(valuesFilter))
  }

  const handleClearFilter = () => {
    const param = {
      pageIndex: 1,
      pageSize: 10,
      keyword: '',
    }
    dispatch(fetchDataGetListVehicleColor(param))
    setValuesFilter(param)
  }

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: '25px'
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', minWidth: '500px', gap: 4 }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ width: '30%' }}
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            )
          }}
          placeholder='Tìm kiếm...'
          onChange={e => handleChangeInput(e.target.value, 'keyword')}
        />
        <Button variant='outlined' sx={{ mr: 2 }} onClick={handleFilter}>
          Lọc
        </Button>
        <Button variant="outlined" color='error' onClick={handleClearFilter}>
          Xóa bộ lọc
        </Button>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          gap: '1rem',
          justifyContent: 'end',
          maxWidth: '100%'
        }}
      >
        <Link href='/car-management/car-color/create/' as={'/car-management/car-color/create/'}>
          <Button variant='contained'>+THÊM MÀU XE</Button>
        </Link>
      </Box>
    </Box>
  )
}

export default TableHeader
