interface PropsDataTable {
  setModalConfirm: Function
  onDeleteCarColor: Function
}

interface IParamValuesFilter {
  code: string;
  status: string
}
export type { IParamValuesFilter, PropsDataTable }
