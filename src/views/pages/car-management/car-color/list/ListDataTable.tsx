
// ** MUI Imports
import Box from '@mui/system/Box'
import TablePagination from '@mui/material/TablePagination'
import { DataGrid, GridColDef } from '@mui/x-data-grid'

// ** Components
import { TypographyDataGrid } from 'src/views/component/theme'
import { DeleteDataGrid, EditDataGrid } from 'src/views/component/theme/customEditDelete'

// ** Config Imports

// ** Store Redux Imports
import { AppDispatch, RootState } from 'src/store'
import { useDispatch, useSelector } from 'react-redux'

// ** Interface
import { PropsDataTable } from './interface'
import { fetchDataGetListVehicleColor } from 'src/store/apps/car-management/car-color'

const ListDataTable = (props: PropsDataTable) => {
  const { setModalConfirm, onDeleteCarColor } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.listVehicleColor)

  const handleChangePage = (e: React.MouseEvent<HTMLButtonElement> | null, page: number) => {
    const param = {
      ...store.param,
      pageIndex: page + 1
    }
    dispatch(fetchDataGetListVehicleColor(param))
  }

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param = {
      ...store.param,
      pageSize: Number(event.target.value)
    }
    dispatch(fetchDataGetListVehicleColor(param))
  }

  const handleDelete = (id: number, name: string) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xoá Màu xe ' + `${name}`,
      action: () =>  onDeleteCarColor(id) 
    })
  }

  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      maxWidth: 200,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission={null} slug={`/car-management/car-color/edit/${row.id}`} />
            
            <Box sx={{ display: 'none' }}>
              <DeleteDataGrid permission={null} handleDelete={() => handleDelete(row.id, row.colorNameVi)} />
            </Box>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'groupColor',
      headerName: 'Nhóm màu',
      sortable: false,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <TypographyDataGrid noWrap>
              {row.groupColor}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'colorCode',
      headerName: 'Mã màu',
      sortable: false,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <TypographyDataGrid noWrap>
              {row.colorCode}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'colorNameVi',
      sortable: false,
      headerName: 'Tên màu',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid
              noWrap
            >
              {row.colorNameVi}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'hexcode',
      sortable: false,
      headerName: 'Hexcode',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid
              noWrap
            >
              {row.hexcode}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={(store.data) || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: '10px'
          },
          '& .MuiDataGrid-footerContainer': {
            display: 'none'
          }
        }}
      />

      <TablePagination
        component='div'
        count={store.totalRecords}
        page={store.param.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  )
}
export default ListDataTable
