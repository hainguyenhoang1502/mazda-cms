import { ModalConfirmState } from 'src/configs/typeOption'

interface IBodyDataCreate {
  colorCode: string,
  groupColor: string,
  colorNameVi: string,
  hexCode: string
}

interface PickerProps {
  label?: string
  error?: boolean
  registername?: string
}
interface CreateBrand {
  handleCreate: Function
  setModalConfirm: Function
  modalConfirm: ModalConfirmState
  handleUpdate: Function
}
interface CreateUserLeftProps {
  setValueImg: Function
}

interface IRegionDataCreate {
  code: string
  regionName: string
  status: string
}
export type IValueChecked = {
  arrReturn: Array<string>
  children: {
    childrenChecked: number
    children: Array<string>
  }[]
}
export type { IBodyDataCreate, PickerProps, CreateBrand, CreateUserLeftProps, IRegionDataCreate }
