// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useContext, useState } from 'react'

// ** Next Imports
import router from 'next/router'

// ** Third Party Import
import * as Yup from 'yup'
import { Formik, Form, Field } from 'formik'

// ** Components Imports
import ModalConfirm from 'src/views/component/modalConfirm'

import { InputRequiredMUI } from 'src/views/component/theme'

// ** Interface Services Imports
import { INIT_VALUE_DATA_BODY_CREATE } from './service'

// ** Api Imports
import { ERROR_NULL_VALUE_INPUT, INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import ModalNotify from 'src/views/component/modalNotify'
import { useDispatch } from 'react-redux'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { AppDispatch } from 'src/store'
import { ApiVehicleColorCreate, ApiVehicleColorUpdate } from 'src/api/vehicles'
import { IParamCreateColor, IParamUpdateColor } from 'src/api/vehicles/type'
import { EditVehicleColorContext } from 'src/pages/car-management/car-color/edit/[slug]'
import { handleSetReloadDataColor } from 'src/store/apps/car-management/car-color'

const CreateColorForm = () => {
  // ** Props
  // const { handleCreate, modalConfirm, setModalConfirm, handleUpdate } = props


  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()

  const handleCreate = async (param: IParamCreateColor) => {
    const res = await ApiVehicleColorCreate(param)

    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thành Công',
        textBtn: 'Quay lại',
        actionReturn: () => router.push('/car-management/car-color/')
      })

      dispatch(handleSetReloadDataColor())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (param: IParamUpdateColor) => {
    const res = await ApiVehicleColorUpdate(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thành Công'
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(handleSetReloadDataColor())
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  // ** Hooks
  const context = useContext(EditVehicleColorContext);
  const isEdit = context && context.isEdit;

  const AddVersionSchema = Yup.object().shape({
    colorCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT),
    groupColor: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    colorNameVi: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    hexCode: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  })

  const handleSubmit = async values => {

    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Cập nhật thông tin Màu xe' : 'Bạn đang thao tác thêm Màu xe',
      action: () => {
        if (isEdit) {
          return handleUpdate(values)
        } else {
          return handleCreate(values)
        }
      }
    })
  }
  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Huỷ cập nhật thông tin Màu xe' : 'Bạn đang thao tác Huỷ thêm Màu xe',
      action: () => router.push('/car-management/car-color/')
    })
  }


  return (
    <Grid item xs={12} p={0}>
      <Card>
        <Formik
          initialValues={(context && context.data) || INIT_VALUE_DATA_BODY_CREATE}
          onSubmit={values => handleSubmit(values)}
          validationSchema={AddVersionSchema}
          validateOnBlur
        >
          {props => {

            return (
              <Form>
                <CardContent>
                  <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                    Thông tin màu xe
                  </Typography>
                  <Grid item container spacing={6} marginBottom={5}>
                    <Grid item lg={12} xs={12}>
                      <Field
                        id='groupColor'
                        label='Nhóm màu'
                        name='groupColor'
                        component={InputRequiredMUI}
                        texterror={props.touched.groupColor && props.errors.groupColor}
                        texticon='*'
                      />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                      <Field
                        id='colorCode'
                        label='Mã màu'
                        name='colorCode'
                        component={InputRequiredMUI}
                        texterror={props.touched.colorCode && props.errors.colorCode}
                        texticon='*'
                        disabled={context?.data?.colorCode ? true : false}
                      />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                      <Field
                        id='colorNameVi'
                        label='Tên màu'
                        name='colorNameVi'
                        component={InputRequiredMUI}
                        texterror={props.touched.colorNameVi && props.errors.colorNameVi}
                        texticon='*'
                      />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                      <Field
                        id='hexCode'
                        label='HexCode'
                        name='hexCode'
                        component={InputRequiredMUI}
                        texterror={props.touched.hexCode && props.errors.hexCode}
                        texticon='*'
                      />
                    </Grid>
                  </Grid>
                </CardContent>
                <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                  <Button variant='contained' type='submit'>
                    Lưu
                  </Button>
                  <Button color='error' variant='outlined' onClick={handleCancel}>
                    Hủy
                  </Button>
                </CardContent>
              </Form>
            )
          }}
        </Formik>
      </Card>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </Grid>
  )
}
export default CreateColorForm
