// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useContext, useEffect, useState } from 'react'

// ** Next Imports
import router from 'next/router'

// ** Third Party Import
import * as Yup from 'yup'
import { Formik, Form, Field } from 'formik'

// ** Components Imports
import ModalConfirm from 'src/views/component/modalConfirm'

import { SelectMUI, InputRequiredMUI } from 'src/views/component/theme'

// ** Interface Services Imports
import { INIT_VALUE_DATA_BODY_CREATE } from './service'

// ** Api Imports
import { ERROR_NULL_VALUE_INPUT, INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import ModalNotify from 'src/views/component/modalNotify'
import { useDispatch } from 'react-redux'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { getListModelCodeAction } from 'src/store/apps/vehicle/listModelCode'
import { ApiVehicleVersionCreate, ApiVehicleVersionUpdate } from 'src/api/vehicles'
import { IParamCreateVersion, IParamUpdateVersion } from 'src/api/vehicles/type'
import { handleSetReloadDataVersion } from 'src/store/apps/car-management/car-version'
import { EditVehicleVersionContext } from 'src/pages/car-management/car-version/edit/[slug]'

const CreateVersionForm = () => {
  // ** Props
  // const { handleCreate, modalConfirm, setModalConfirm, handleUpdate } = props


  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  const storeListModelCode = useSelector((state: RootState) => state.listModelCode)

  useEffect(() => {
    if ((storeListModelCode && !storeListModelCode.data) || (storeListModelCode && storeListModelCode.data.length === 0) || (storeListModelCode && storeListModelCode.isNeedReloadData)) {
      dispatch(getListModelCodeAction())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()

  const handleCreate = async (param: IParamCreateVersion) => {
    const res = await ApiVehicleVersionCreate(param)

    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thành Công',
        textBtn: 'Quay lại',
        actionReturn: () => router.push('/car-management/car-version/')
      })

      dispatch(handleSetReloadDataVersion())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (param: IParamUpdateVersion) => {
    const res = await ApiVehicleVersionUpdate(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thành Công'
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(handleSetReloadDataVersion())
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  // ** Hooks
  const context = useContext(EditVehicleVersionContext);
  const isEdit = context && context.isEdit;

  const AddVersionSchema = Yup.object().shape({
    modelCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT),
    gradeCode: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    gradeName: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  })

  const handleSubmit = async values => {

    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Cập nhật thông tin Phiên bản' : 'Bạn đang thao tác thêm Phiên bản',
      action: () => {
        if (isEdit) {
          return handleUpdate(values)
        } else {
          return handleCreate(values)
        }
      }
    })
  }
  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Huỷ cập nhật thông tin Phiên bản' : 'Bạn đang thao tác Huỷ thêm Phiên bản',
      action: () => router.push('/car-management/car-version/')
    })
  }


  return (
    <Grid item xs={12} p={0}>
      <Card>
        <Formik
          initialValues={(context && context.data) || INIT_VALUE_DATA_BODY_CREATE}
          onSubmit={values => handleSubmit(values)}
          validationSchema={AddVersionSchema}
          validateOnBlur
        >
          {props => {

            return (
              <Form>
                <CardContent>
                  <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                    Thông tin phiên bản
                  </Typography>
                  <Grid item container spacing={6} marginBottom={5}>
                    <Grid item lg={12} xs={12}>
                      <Field
                        id='modelCode'
                        label='Dòng xe'
                        component={SelectMUI}
                        texticon='*'
                        name='modelCode'
                        texterror={props.touched.modelCode && props.errors.modelCode}
                        options={storeListModelCode.data || []}
                        value={props.values.modelCode}
                        onChange={event => {
                          props.setFieldValue('modelCode', event.target.value)
                        }}
                      />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                      <Field
                        id='gradeCode'
                        label='Mã phiên bản'
                        name='gradeCode'
                        component={InputRequiredMUI}
                        texterror={props.touched.gradeCode && props.errors.gradeCode}
                        texticon='*'
                        disabled={context?.data?.gradeCode ? true : false}
                      />
                    </Grid>
                    <Grid item lg={12} xs={12}>
                      <Field
                        id='gradeName'
                        label='Tên phiên bản'
                        name='gradeName'
                        component={InputRequiredMUI}
                        texterror={props.touched.gradeName && props.errors.gradeName}
                        texticon='*'
                      />
                    </Grid>
                  </Grid>
                </CardContent>
                <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                  <Button variant='contained' type='submit'>
                    Lưu
                  </Button>
                  <Button color='error' variant='outlined' onClick={handleCancel}>
                    Hủy
                  </Button>
                </CardContent>
              </Form>
            )
          }}
        </Formik>
      </Card>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </Grid>
  )
}
export default CreateVersionForm
