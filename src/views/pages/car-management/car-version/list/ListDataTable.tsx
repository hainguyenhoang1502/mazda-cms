
// ** MUI Imports
import Box from '@mui/system/Box'
import TablePagination from '@mui/material/TablePagination'
import { DataGrid, GridColDef } from '@mui/x-data-grid'

// ** Components
import { TypographyDataGrid } from 'src/views/component/theme'
import { DeleteDataGrid, EditDataGrid } from 'src/views/component/theme/customEditDelete'

// ** Store Redux Imports
import { AppDispatch, RootState } from 'src/store'
import { useDispatch, useSelector } from 'react-redux'

// ** Interface
import { PropsDataTable } from './interface'
import { fetchDataGetListVehicleVersion } from 'src/store/apps/car-management/car-version'

const ListDataTable = (props: PropsDataTable) => {
  const { setModalConfirm, onDeleteCarVerion } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.listVehicleVersion)

  const handleChangePage = (e: React.MouseEvent<HTMLButtonElement> | null, page: number) => {
    const param = {
      ...store.param,
      pageIndex: page + 1
    }
    dispatch(fetchDataGetListVehicleVersion(param))
  }

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param = {
      ...store.param,
      pageSize: Number(event.target.value)
    }
    dispatch(fetchDataGetListVehicleVersion(param))
  }

  const handleDelete = (id: number, name: string) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xoá phiên bản xe ' + `${name}`,
      action: () =>  onDeleteCarVerion(id) 
    })
  }

  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission='' slug={`/car-management/car-version/edit/${row.id}`} />
            <DeleteDataGrid permission='' handleDelete={() => handleDelete(row.id, row.gradeName)} />
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'modelName',
      headerName: 'Tên dòng xe',
      sortable: false,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <TypographyDataGrid noWrap>
              {row.modelName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'gradeCode',
      headerName: 'Mã phiên bản',
      sortable: false,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <TypographyDataGrid noWrap>
              {row.gradeCode}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'gradeName',
      sortable: false,
      headerName: 'Tên phiên bản',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid
              noWrap
            >
              {row.gradeName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
  
  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={(store.data) || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: '10px'
          },
          '& .MuiDataGrid-footerContainer': {
            display: 'none'
          }
        }}
      />

      <TablePagination
        component='div'
        count={store.totalRecords}
        page={store.param.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  )
}
export default ListDataTable
