interface PropsDataTable {
  setModalConfirm: Function
  onDeleteCarVerion: Function
}

interface IParamValuesFilter {
  code: string;
  status: string
}
export type { IParamValuesFilter, PropsDataTable }
