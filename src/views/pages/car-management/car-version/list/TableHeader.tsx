// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import { InputAdornment, OutlinedInput, SelectChangeEvent } from '@mui/material'
import TextField from '@mui/material/TextField'

// ** React Imports
import { useMemo, useState } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** Components Imports
import Icon from 'src/@core/components/icon'
import { SelectMUI } from 'src/views/component/theme'

// ** Interface Services Imports

// ** Utils Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'

// ** Store Redux Imports
import { AppDispatch, RootState } from 'src/store'
import { useDispatch, useSelector } from 'react-redux'
import { IParamVehicleList } from 'src/api/vehicles/type'
import { fetchDataGetListVehicleVersion } from 'src/store/apps/car-management/car-version'


const TableHeader = () => {
  const storeListModelCode = useSelector((state: RootState) => state.listModelCode)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.listVehicleVersion)

  const [valuesFilter, setValuesFilter] = useState<IParamVehicleList>(store.param)

  const handleChangeInput = (value: string, field: string) => {
    const body = { ...valuesFilter }

    body[field] = value
    setValuesFilter(body)
  }

  const OPTIONS_CAR = useMemo(() => {
    return [
      {
        value: null,
        label: "Tất cả"
      },
      ...storeListModelCode.data,
    ]
  }, [storeListModelCode])

  const handleFilter = () => {
    dispatch(fetchDataGetListVehicleVersion(valuesFilter))
  }

  const handleClearFilter = () => {
    const param = {
      pageIndex: 1,
      pageSize: 10,
      keyword: '',
      modelCode: null,
    }
    dispatch(fetchDataGetListVehicleVersion(param))
    setValuesFilter(param)
  }

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: '25px'
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', minWidth: '500px', gap: 4 }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ width: '30%' }}
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            )
          }}
          placeholder='Tìm kiếm...'
          onChange={e => handleChangeInput(e.target.value, 'keyword')}
        />
        <Box>
          <SelectMUI
            label='Dòng xe'
            options={OPTIONS_CAR || []}
            shrink
            displayEmpty
            input={<OutlinedInput notched label="Dòng xe" />}
            size='small'
            value={valuesFilter.modelCode}
            onChange={(e: SelectChangeEvent<any>) => {
              handleChangeInput(e.target.value, 'modelCode')
            }}
          />
        </Box>
        <Button variant='outlined' sx={{ mr: 2 }} onClick={handleFilter}>
          Lọc
        </Button>
        <Button variant="outlined" color='error' onClick={handleClearFilter}>
          Xóa bộ lọc
        </Button>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          gap: '1rem',
          justifyContent: 'end',
          maxWidth: '100%'
        }}
      >
        {checkUserPermission(permissonConfig.BRAND_CREATE) && (
          <Link href='/car-management/car-version/create/' as={'/car-management/car-version/create/'}>
            <Button variant='contained'>+THÊM PHIÊN BẢN</Button>
          </Link>
        )}
      </Box>
    </Box>
  )
}

export default TableHeader
