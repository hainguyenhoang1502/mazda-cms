

import { Box, TablePagination } from '@mui/material'
import { DataGrid, GridColDef } from '@mui/x-data-grid'

// ** Components
import { PropsDataTable } from './interface'

import { AppDispatch, RootState } from 'src/store'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { fetchDataGetVehicleData } from 'src/store/apps/vehicle/model'
import { TypographyDataGrid } from 'src/views/component/theme'
import { DeleteDataGrid, EditDataGrid } from 'src/views/component/theme/customEditDelete'

const ListDataTable = (props: PropsDataTable) => {
  const { setModalConfirm, getDeleteModel } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.vehicleData)

  // ** Permission

  const handleChangePage = (e: React.MouseEvent<HTMLButtonElement> | null, page: number) => {
    const param = {
      ...store.param,
      pageIndex: page + 1
    }
    dispatch(fetchDataGetVehicleData(param))
  }

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param = {
      ...store.param,
      pageSize: Number(event.target.value)
    }
    dispatch(fetchDataGetVehicleData(param))
  }

  const handleDelete = (code: string, name: string) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Dòng xe ' + `${name}`,
      action: () => { getDeleteModel(code) }
    })
  }

  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission='' slug={`/car-management/model/edit/${row.id}`} />
            <DeleteDataGrid permission='' handleDelete={() => handleDelete(row.id, row.modelName)} />
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'modelCode',
      headerName: 'Mã dòng xe',
      sortable: false,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <TypographyDataGrid noWrap sx={{ fontWeight: 400, color: 'text.secondary' }}>
              {row.modelCode}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'modelName',
      sortable: false,
      headerName: 'Tên dòng xe',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid
              noWrap
              sx={{ fontWeight: 400, color: 'text.secondary', textTransform: 'capitalize', textAlign: 'center' }}
            >
              {row.modelName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={store.data || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: '10px'
          },
          '& .MuiDataGrid-footerContainer': {
            display: 'none'
          }
        }}
      />

      <TablePagination
        component='div'
        count={store.totalRecords}
        page={store.param.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  )
}
export default ListDataTable
