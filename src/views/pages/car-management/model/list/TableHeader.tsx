// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment } from '@mui/material'

// ** React Imports
import { useState } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Utils
import { checkUserPermission } from 'src/@core/utils/permission'

import { useDispatch } from 'react-redux'
import { AppDispatch } from 'src/store'

import { fetchDataGetVehicleData } from 'src/store/apps/vehicle/model'
import { IParamModelList } from 'src/api/vehicles/type'

const TableHeader = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()

  const [valuesFilter, setValuesFilter] = useState<IParamModelList>({
    keyword: '',
    pageIndex: 1,
    pageSize: 10
  })

  const handleChangeInput = (value, field) => {

    const body = { ...valuesFilter }
    body[field] = value
    setValuesFilter(body)
  }

  const handleFilter = () => {
    const param = {
      ...valuesFilter,
    }

    dispatch(fetchDataGetVehicleData(param))
  }

  const handleClearFilter = () => {
    
    const param = {
      keyword: '',
      pageIndex: 1,
      pageSize: 10
    }

    setValuesFilter(param)
    dispatch(fetchDataGetVehicleData(param))
  }

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: '25px'
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ mr: 4, width: "30%" }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            ),
          }}
          placeholder='Tìm kiếm…'
          onChange={(e) => handleChangeInput(e.target.value, "keyword")}

        />
        <Button variant="outlined" onClick={handleFilter} sx={{ mr: 4 }} >
          Lọc
        </Button>
        <Button variant="outlined" onClick={handleClearFilter} color='error'>
          Xóa bộ lọc
        </Button>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          gap: '1rem',
          justifyContent: 'end',
          maxWidth: '100%'
        }}
      >
        {checkUserPermission('') && (
          <Link href='/car-management/model/create/' as={'/car-management/model/create/'}>
            <Button variant='contained'>+THÊM MỚI</Button>
          </Link>
        )}
      </Box>
    </Box>
  )
}

export default TableHeader
