
interface RowDataDetail {
  id: number
}

interface PropsDataTable {
  setModalConfirm: Function
  getDeleteModel: Function
}

interface IParamValuesFilter {
  keyword: string;
  status: string
}
export type { RowDataDetail, IParamValuesFilter, PropsDataTable }
