// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React
import { useState } from 'react'
import router from 'next/router'

// ** Components
import ModalNotify from 'src/views/component/modalNotify'

// ** Api

// ** Config
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Interface
import CreateModelForm from './CreateModelForm'
import { useDispatch } from 'react-redux'
import { AppDispatch } from 'src/store'
import { handleSetReloadDataModel } from 'src/store/apps/vehicle/model'
import { ApiVehicleModelCreate, ApiVehicleVersionUpdate } from 'src/api/vehicles'

const CreateBrand = () => {
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()

  const handleCreate = async (param) => {
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)

    const res = await ApiVehicleModelCreate(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thành Công',
        textBtn: 'Quay lại',
        actionReturn: () => router.push('/car-management/model/')
      })
      dispatch(handleSetReloadDataModel())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (param) => {
    const res = await ApiVehicleVersionUpdate(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thành Công',
        textBtn: 'Quay lại',
        actionReturn: () => router.push('/car-management/model/')
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(handleSetReloadDataModel())
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true

      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  return (
    <>
      <Grid item xs={12} p={0}>
        <CreateModelForm
          handleCreate={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdate={handleUpdate}
        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateBrand
