import styled from '@emotion/styled'
import FormControl from '@mui/material/FormControl'

const MuiFormControl = styled(FormControl)({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingBottom: '30px'
})

const INIT_VALUE_DATA_BODY_CREATE = {
  brandCode: "",
  modelName: "",
  modelCode: "",
}

export { MuiFormControl, INIT_VALUE_DATA_BODY_CREATE }
