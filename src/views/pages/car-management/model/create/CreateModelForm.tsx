// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useContext } from 'react'

// ** Next Imports
import router from 'next/router'

// ** Third Party Import
import * as Yup from 'yup'
import { Formik, Form, Field } from 'formik'

// ** Components Imports
import ModalConfirm from 'src/views/component/modalConfirm'

import { InputRequiredMUI } from 'src/views/component/theme'

// ** Interface Services Imports
import { INIT_VALUE_DATA_BODY_CREATE } from './service'

// ** Api Imports
import { ERROR_NULL_VALUE_INPUT } from 'src/configs/initValueConfig'
import { CreateBrand } from './interface'
import { EditModelContext } from 'src/pages/car-management/model/edit/[slug]'


// import { useDispatch } from 'react-redux'
// import { AppDispatch, RootState } from 'src/store'
// import { useSelector } from 'react-redux'
// import { fetchDataGetListBrand } from 'src/store/apps/category/brand'

const CreateModelForm = (props: CreateBrand) => {
  // ** Props
  const { handleCreate, modalConfirm, setModalConfirm, handleUpdate } = props

  // ** Hooks
  const context = useContext(EditModelContext);
  const isEdit = context && context.isEdit;
  
  // ** Redux
  // const dispatch = useDispatch<AppDispatch>()
  // const storeBrand = useSelector((state: RootState) => state.brand)

  // useEffect(() => {
  //   const param = {
  //     pageIndex: 1,
  //     pageSize: 9999,
  //     code: "",
  //     status: "ACTIVE",
  //   }
  //   if (JSON.stringify(storeBrand.param) !== JSON.stringify(param)) {
  //     dispatch(fetchDataGetListBrand(param))
  //   }
  // }, [])

  const AddModelSchema = Yup.object().shape({
    modelCode: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    modelName: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  })

  const handleSubmit = async values => {

    values.brandCode = "3"
    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Cập nhật thông tin Dòng xe' : 'Bạn đang thao tác thêm Dòng xe',
      action: () => {
        if (isEdit) {
          return handleUpdate(values)
        } else {
          return handleCreate(values)
        }
      }
    })
  }
  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Huỷ cập nhật thông tin Dòng xe' : 'Bạn đang thao tác Huỷ thêm Dòng xe',
      action: () => router.push('/car-management/model')
    })
  }
  
  return (
    <Card>
      <Formik
        initialValues={(context && context.data) || INIT_VALUE_DATA_BODY_CREATE}
        onSubmit={values => handleSubmit(values)}
        validationSchema={AddModelSchema}
        validateOnBlur
      >
        {props => {

          return (
            <Form>
              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                  Thông tin Dòng xe
                </Typography>

                {/* <Grid item lg={12} xs={12} marginBottom={5}>
                    <Field
                      id='brandCode'
                      label='Thương hiệu'
                      component={SelectMUI}
                      texticon='*'
                      name='brandCode'
                      texterror={props.touched.brandCode && props.errors.brandCode}
                      options={storeBrand.arrFilter}
                      value={props.values.brandCode}
                      onChange={event => {
                        props.setFieldValue('brandCode', event.target.value)
                      }}
                    />
                </Grid> */}
                <Grid item container spacing={6}>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='modelCode'
                      label='Mã Dòng xe'
                      name='modelCode'
                      component={InputRequiredMUI}
                      texterror={props.touched.modelCode && props.errors.modelCode}
                      texticon='*'
                      disabled= { isEdit ? true : false}
                    />
                  </Grid>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='modelName'
                      label='Tên Dòng xe'
                      name='modelName'
                      component={InputRequiredMUI}
                      texterror={props.touched.modelName && props.errors.modelName}
                      texticon='*'
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                <Button variant='contained' type='submit'>
                  Lưu
                </Button>
                <Button color='error' variant='outlined' onClick={handleCancel}>
                  Hủy
                </Button>
              </CardContent>
            </Form>
          )
        }}
      </Formik>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Card>
  )
}
export default CreateModelForm
