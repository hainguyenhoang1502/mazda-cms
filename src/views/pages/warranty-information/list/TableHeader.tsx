
// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment } from '@mui/material'

// ** React Imports
import { forwardRef, useState } from 'react'

// ** Third Party Import
import DatePicker from 'react-datepicker'

// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Styles
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'

// ** Config Imports
import { PickerProps } from 'src/configs/typeOption'
import { permissonConfig } from 'src/configs/roleConfig'

// ** Interface Services Imports 
import { IParamValuesFilter } from './interface'

// ** Utils
import { checkUserPermission } from 'src/@core/utils/permission'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetCustomer } from 'src/store/apps/customer-information'
import ModalFile, { useModalFile } from 'src/components/Modal/ModalFile'
import ContentFile from 'src/views/component/ContentFile'
import { WarrantyPolicysApi } from 'src/api-client/warranty'

// import ContentFile from './ContentFile'


const TableHeader = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.customer)

  const { open, handleClose, handleOpen } = useModalFile()

  const [valuesFilter, setValuesFilter] = useState<IParamValuesFilter>({
    keyword: "",
    startDate: null,
    endDate: null
  })

  const handleChangeInput = (value, field) => {
    const body = { ...valuesFilter }
    body[field] = value
    setValuesFilter(body)
  }
  const handleFilterDate = () => {
    const param = {
      ...store.param,
      ...valuesFilter,
    }
    dispatch(fetchDataGetCustomer(param))
  }
  const handleClearFilter = () => {
    const param = {
      ...store.param,
      keyword: "",
      startDate: null,
      endDate: null
    }
    setValuesFilter(param)
    dispatch(fetchDataGetCustomer(param))

  }
  const PickersComponent = forwardRef(({ ...props }: PickerProps, ref) => {

    return (
      <TextField
        size='small'
        inputRef={ref}
        fullWidth
        {...props}
        label={props.label || ''}
        error={props.error}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Icon icon='bx:calendar' fontSize={20} />
            </InputAdornment>
          ),
        }}
      />
    )
  })

  return (
    <>
      <Box
        sx={{
          p: 5,
          pb: 3,
          width: '100%',
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          justifyContent: 'space-between',
          gap: "25px"
        }}
      >
        <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
          <TextField
            size='small'
            value={valuesFilter.keyword}
            sx={{ mr: 4, width: "30%" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Icon icon='bx:search' fontSize={20} />
                </InputAdornment>
              ),
            }}
            placeholder='Tìm kiếm…'
            onChange={(e) => handleChangeInput(e.target.value, "keyword")}
          />
          <DatePickerWrapper sx={{ display: 'flex', gap: "1rem", width: "60%", mr: 4, maxWidth: "320px" }}>
            <DatePicker
              selectsStart
              id='event-start-date'
              selected={valuesFilter.startDate ? new Date(valuesFilter.startDate) : null}
              dateFormat="dd/MM/yyyy"
              maxDate={valuesFilter.endDate && new Date(valuesFilter.endDate)}
              customInput={<PickersComponent label='Từ ngày' registername='startDate' />}
              onChange={(date: Date) => handleChangeInput(new Date(date), "startDate")}
            />
            <DatePicker
              selectsStart
              id='event-start-date'
              selected={valuesFilter.endDate ? new Date(valuesFilter.endDate) : null}
              dateFormat="dd/MM/yyyy"
              minDate={valuesFilter.startDate && new Date(valuesFilter.startDate)}
              customInput={<PickersComponent label='Đến ngày' registername='endDate' />}
              onChange={(date: Date) => handleChangeInput(new Date(date), "endDate")}
            />
          </DatePickerWrapper>
          <Button variant="outlined" onClick={handleFilterDate} sx={{ mr: 4 }} >
            Lọc
          </Button>
          <Button variant="outlined" onClick={handleClearFilter} color='error'>
            Xóa bộ lọc
          </Button>
        </Box>
        <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', gap: "1rem", justifyContent: "end", maxWidth: "100%" }}>
          {
            checkUserPermission(permissonConfig.CUSTOMER_ADD) &&
            <>
              <Button variant='outlined' onClick={handleOpen}>Import</Button>
            </>
          }
        </Box>
      </Box>
      <ModalFile open={open} handleClose={handleClose}>
        <ContentFile functionGetFile={WarrantyPolicysApi.postFileWarranty} title='Import Thông tin bảo hành' linkDownload='https://kong-gateway.toponseek.com/ref-data/files/2023/6/admin/ImportWarrantyTemplate.xlsx' />
      </ModalFile>
    </>
  )
}

export default TableHeader
