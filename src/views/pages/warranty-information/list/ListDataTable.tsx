import { Box, TablePagination } from "@mui/material"
import { ListDataTableProps } from "./interface"
import { DataGrid, GridColDef } from "@mui/x-data-grid"
import { permissonConfig } from "src/configs/roleConfig"
import { TypographyDataGrid } from "src/views/component/theme"

// ** Components 
import { useSelector } from "react-redux"
import { AppDispatch, RootState } from "src/store"
import { renderDate } from "src/configs/functionConfig"
import { fetchDataGetWarrantyInformation } from "src/store/apps/warranty-information"
import { useDispatch } from "react-redux"
import { IParamListWarrantyInformation } from "src/api/warranty/type"
import { DeleteDataGrid, EditDataGrid } from "src/views/component/theme/customEditDelete"



const ListDataTable = (props: ListDataTableProps) => {

  const { setModalConfirm, getDelete } = props

  // ** Redux
  const store = useSelector((state: RootState) => state.warrantyInformation)

  const dispatch = useDispatch<AppDispatch>()
  const handleDelete = (item: any) => {

    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác xoá thông tin bảo hành',
      action: () => { getDelete(item.id.toString()) }
    })
  }

  const handleChangePage = (e, page) => {
    const param: IParamListWarrantyInformation = {
      ...store.param,
      pageIndex: page + 1,
    }
    dispatch(fetchDataGetWarrantyInformation(param))
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamListWarrantyInformation = {
      ...store.param,
      pageSize: Number(event.target.value),
    }
    dispatch(fetchDataGetWarrantyInformation(param))
  }

  // ** Permission 
  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }) => {

        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission={permissonConfig.CUSTOMER_EDIT} slug={`/car-management/model/edit/${row.id}`} />
            <DeleteDataGrid permission={permissonConfig.CUSTOMER_DELETE} handleDelete={() => handleDelete(row)} />
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'customerName',
      headerName: 'Tên khách hàng',
      sortable: false,
      renderCell: ({ row }) => {

        return (
          <Box sx={{ display: "flex", alignItems: "center", gap: "10px" }}>
            {/* {row.avatar ? (
              <CustomAvatar
                src={row.avatar}
                variant='rounded'
                alt={row.fullName}
                sx={{ width: 30, height: 30, objectFit: "cover" }}
              />
            ) : (
              <CustomAvatar
                skin='light'
                variant='rounded'
                sx={{ width: 30, height: 30, fontWeight: 600, fontSize: '3rem' }}
              >
              </CustomAvatar>
            )} */}
            <TypographyDataGrid noWrap>
              {row.customerName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'phoneNumber',
      sortable: false,
      headerName: 'Số điện thoại',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.phoneNumber}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'carModel',
      sortable: false,
      headerName: 'Dòng xe',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.carModel}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'carColor',
      sortable: false,
      headerName: 'Màu xe',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.carColor}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'carPlate',
      sortable: false,
      headerName: 'Biển số',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.carPlate}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'vinNumber',
      sortable: false,
      headerName: 'Số khung',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.vinNumber}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 180,
      field: 'engineNumber',
      sortable: false,
      headerName: 'Số máy',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.engineNumber}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'warrantyRegDate',
      sortable: false,
      headerName: 'Ngày đăng ký bảo hành',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {renderDate(row.warrantyRegDate)}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'warrantyEndDate',
      sortable: false,
      headerName: 'Ngày kết thúc bảo hành',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {renderDate(row.warrantyEndDate)}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 300,
      field: 'carDealer',
      sortable: false,
      headerName: 'Điểm bán hàng',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.carDealer}
            </TypographyDataGrid>
          </Box>
        )
      }
    },

  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={store.data || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          },
          '& .MuiDataGrid-footerContainer': {
            display: "none"
          }
        }}
      />
      <TablePagination
        component="div"
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>


  )
}
export default ListDataTable