

interface ListDataTableProps {
  setModalConfirm:Function
  getDelete:Function
}
interface RowDataDetail {
  id: number
  customerName: string
  gender: number
  phoneNumber: string
  email: string
  address: string
  numberOfCarsOwned: string
  status: boolean
}
interface IParamValuesFilter {
  keyword: string
  startDate: string
  endDate: string
}
export type { ListDataTableProps, RowDataDetail, IParamValuesFilter }
