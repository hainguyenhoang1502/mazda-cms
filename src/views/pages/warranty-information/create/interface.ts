import { ModalConfirmState } from 'src/configs/typeOption'

interface IBodyDataCreate {
  customerName: string
  birthday: string
  phoneNumber: string
  email: string
  provinceId: string
  districtId: string
  wardId: string
  address: string
  modelName: string
  carColor: string
  numberPlate: string
  vehicleIdentificationNumber: string
  engineNumber: string
  signupDate: string
  endDate: string
  agency: string
}
interface PickerProps {
  label?: string
  error?: boolean
  registername?: string
}
interface CreateProvince {
  handleCreate: Function
  setModalConfirm: Function
  modalConfirm: ModalConfirmState
  handleUpdate: Function
}
interface CreateUserLeftProps {
  setValueImg: Function
}

interface IRegionDataCreate {
  code: string
  regionName: string
  status: string
}
export type IValueChecked = {
  arrReturn: Array<string>
  children: {
    childrenChecked: number
    children: Array<string>
  }[]
}
export type { IBodyDataCreate, PickerProps, CreateProvince, CreateUserLeftProps, IRegionDataCreate }
