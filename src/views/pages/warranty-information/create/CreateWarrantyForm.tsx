// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useContext, useEffect, useState } from 'react'

// ** Next Imports
import router from 'next/router'

// ** Third Party Import
import * as Yup from 'yup'
import { Formik, Form, Field } from 'formik'

// ** Components Imports
import ModalConfirm from 'src/views/component/modalConfirm'

import { SelectMUI, InputRequiredMUI, DatePickerMUI } from 'src/views/component/theme'

// ** Interface Services Imports
import { INIT_VALUE_DATA_BODY_CREATE } from './service'
import { CreateProvince } from './interface'

//** Store Imports
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'

// ** Api Imports
import { ApiGetLocationDistrictByProvinceId, ApiGetLocationWardByDistrictId } from "src/api/refData";

// ** Config Imports
import { ERROR_EMAIL_VALUE_INPUT, ERROR_NULL_VALUE_INPUT, ERROR_PHONE_VALUE_INPUT, regexPhone } from 'src/configs/initValueConfig'

import { fetchDataGetListRegion } from 'src/store/apps/category/region'
import { EditRegionContext } from 'src/pages/category/region/edit/[slug]'
import { IOptionsFilter } from 'src/configs/typeOption'
import { fetchDataGetListProvince } from 'src/store/apps/category/province'
import { fetchDataGetVehicleCode } from 'src/store/apps/vehicle/modelCode'
import { fetchDataGetListFilterDealers } from 'src/store/apps/category/company-dealer/dealer'

const CreateRegionContent = (props: CreateProvince) => {
  
  // ** Props
  const { handleCreate, modalConfirm, setModalConfirm, handleUpdate } = props

  // ** States
  const [listDistrict, setListDistrict] = useState<Array<IOptionsFilter>>([])
  const [listWard, setListWard] = useState<Array<IOptionsFilter>>([])

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const regionData = useSelector((state: RootState) => state.region)
  const storeProvince = useSelector((state: RootState) => state.province)
  const storeModelName = useSelector((state: RootState) => state.vehicleCode)
  const storeDealer = useSelector((state: RootState) => state.dealer)

  // ** Hooks
  const context = useContext(EditRegionContext);
  const isEdit = context && context.isEdit;

  const listCarColor = [
    {
      value: 1,
      label: "Trắng"
    },
    {
      value: 2,
      label: "Đen"
    },
    {
      value: 3,
      label: "Xám"
    },
  ]

  useEffect(() => {
    const param = {
      ...regionData.param,
      status: 'ACTIVE'
    }
    dispatch(fetchDataGetListRegion(param))

    if (storeProvince && (!storeProvince.data || storeProvince.data.length === 0 || storeProvince.isNeedReloadData)) {
      dispatch(fetchDataGetListProvince(storeProvince.param))
    }

    if (storeModelName && (!storeModelName.data || storeModelName.data.length === 0)) {
      dispatch(fetchDataGetVehicleCode())
    }


    if (!storeDealer.arrFilter || storeDealer.arrFilter.length === 0) {
      dispatch(fetchDataGetListFilterDealers())
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  // change options

  const onChangeSelectProvince = (event) => {
    getListLocationDistricts(event)
  }

  const onChangeSelectDistricts = (event) => {
    getListLocationWards(event)
  }

  const getListLocationDistricts = async (id: number | string) => {
    const res = await ApiGetLocationDistrictByProvinceId(id)
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      const arr = res.data.map(item => ({
        label: item.locationName,
        value: item.code
      }));
      setListDistrict(arr)
    } else {
      setListDistrict([])
    }
  }

  const getListLocationWards = async (id: number | string) => {
    const res = await ApiGetLocationWardByDistrictId(id)
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      const arr = res.data.map(item => ({
        label: item.locationName,
        value: item.code
      }));
      setListWard(arr)
    } else {
      setListWard([])

    }
  }

  const handleSubmit = async values => {

    console.log(values)

    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Cập nhật thông tin Bảo hành' : 'Bạn đang thao tác Thêm thông tin Bảo hành',
      action: () => {
        if (isEdit) {
          return handleUpdate(values)
        } else {
          return handleCreate(values)
        }
      }
    })
  }
  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Huỷ cập nhật thông tin Bảo hành' : 'Bạn đang thao tác Huỷ thêm thông tin Bảo hành',
      action: () => router.push('/warranty-management/warranty-information/')
    })
  }

  // validate

  const SignupSchema = Yup.object().shape({
    customerName: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    phoneNumber: Yup.string().required(ERROR_NULL_VALUE_INPUT).matches(regexPhone, ERROR_PHONE_VALUE_INPUT),
    birthday: Yup.date().required(ERROR_NULL_VALUE_INPUT),
    email: Yup.string().required(ERROR_NULL_VALUE_INPUT).email(ERROR_EMAIL_VALUE_INPUT),
    address: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    wardId: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    districtId: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    provinceId: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    modelName: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    carColor: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    numberPlate: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    vehicleIdentificationNumber: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    engineNumber: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    signupDate: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
    endDate: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),

  });

  return (
    <>
      <Formik

        // initialValues={(context && context.data) || INIT_VALUE_DATA_BODY_CREATE}

        initialValues={INIT_VALUE_DATA_BODY_CREATE}
        onSubmit={values => handleSubmit(values)}
        validationSchema={SignupSchema}
        validateOnBlur
      >
        {props => {

          return (
            <Form>
              <Card sx={{ marginBottom: '25px' }}>
                <CardContent>
                  <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                    Thông tin khách hàng
                  </Typography>

                  <Grid item container spacing={6} marginBottom={5}>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id='customerName'
                        label='Họ và Tên'
                        name='customerName'
                        component={InputRequiredMUI}
                        texterror={props.touched.customerName && props.errors.customerName}
                        texticon='*'
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id='birthday'
                        label='Ngày sinh'
                        name='birthday'
                        component={DatePickerMUI}
                        texterror={props.touched.birthday && props.errors.birthday}
                        texticon='*'
                        value={props.values.birthday}
                        onChange={(event) => {
                          props.setFieldValue("birthday", event);
                        }}
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id='phoneNumber'
                        label='Số điện thoại'
                        name='phoneNumber'
                        component={InputRequiredMUI}
                        texterror={props.touched.phoneNumber && props.errors.phoneNumber}
                        texticon='*'
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id='email'
                        label='Email'
                        name='email'
                        component={InputRequiredMUI}
                        texterror={props.touched.email && props.errors.email}
                        texticon='*'
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id="provinceId"
                        label="Tỉnh/Thành phố"
                        component={SelectMUI}
                        texticon="*"
                        name="provinceId"
                        texterror={props.touched.provinceId && props.errors.provinceId}
                        options={storeProvince.arrFilter}
                        value={props.values.provinceId}
                        onChange={(event) => {
                          props.setFieldValue("provinceId", event.target.value);
                          props.setFieldValue("districtId", "");
                          props.setFieldValue("wardId", "");
                          onChangeSelectProvince(event.target.value)
                        }}
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id="districtId"
                        label="Quận/huyện"
                        component={SelectMUI}
                        texticon="*"
                        name="districtId"
                        texterror={props.touched.districtId && props.errors.districtId}
                        options={listDistrict}
                        value={props.values.districtId}
                        onChange={(event) => {
                          props.setFieldValue("districtId", event.target.value);
                          props.setFieldValue("wardId", "");
                          onChangeSelectDistricts(event.target.value)
                        }}
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id="wardId"
                        label="Phường/xã"
                        component={SelectMUI}
                        texticon="*"
                        name="wardId"
                        texterror={props.touched.wardId && props.errors.wardId}
                        options={listWard}
                        value={props.values.wardId}
                        onChange={(event) => {
                          props.setFieldValue("wardId", event.target.value);
                        }}
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id="input-address"
                        label="Địa chỉ"
                        name="address"
                        component={InputRequiredMUI}
                        texterror={props.touched.address && props.errors.address}
                        texticon="*"
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
              <Card sx={{ marginBottom: '25px' }}>
                <CardContent>
                  <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                    Thông tin xe
                  </Typography>
                  <Grid item container spacing={6} marginBottom={5}>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id="modelName"
                        label="Dòng xe"
                        component={SelectMUI}
                        texticon="*"
                        name="modelName"
                        texterror={props.touched.modelName && props.errors.modelName}
                        options={storeModelName.arrFilter}
                        value={props.values.modelName}
                        onChange={(event) => {
                          props.setFieldValue("modelName", event.target.value);
                        }}
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id="carColor"
                        label="Màu xe"
                        component={SelectMUI}
                        texticon="*"
                        name="carColor"
                        texterror={props.touched.carColor && props.errors.carColor}
                        options={listCarColor}
                        value={props.values.carColor}
                        onChange={(event) => {
                          props.setFieldValue("carColor", event.target.value);
                        }}
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id='numberPlate'
                        label='Biển số'
                        name='numberPlate'
                        component={InputRequiredMUI}
                        texterror={props.touched.numberPlate && props.errors.numberPlate}
                        texticon='*'
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id='vehicleIdentificationNumber'
                        label='Số khung'
                        name='vehicleIdentificationNumber'
                        component={InputRequiredMUI}
                        texterror={props.touched.vehicleIdentificationNumber && props.errors.vehicleIdentificationNumber}
                        texticon='*'
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id='engineNumber'
                        label='Số máy'
                        name='engineNumber'
                        component={InputRequiredMUI}
                        texterror={props.touched.engineNumber && props.errors.engineNumber}
                        texticon='*'
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
              <Card sx={{ marginBottom: '25px' }}>
                <CardContent>
                  <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                    Thông tin bảo hành
                  </Typography>
                  <Grid item container spacing={6} marginBottom={5}>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id="signupDate"
                        label="Ngày đăng ký bảo hành"
                        name="signupDate"
                        component={DatePickerMUI}
                        texterror={props.touched.signupDate && props.errors.signupDate}
                        texticon="*"
                        value={props.values.signupDate}
                        onChange={(event) => {
                          props.setFieldValue("signupDate", event);
                        }}
                      />
                    </Grid>
                    <Grid item lg={6} xs={6}>
                      <Field
                        id="endDate"
                        label="Ngày kết thúc bảo hành"
                        name="endDate"
                        component={DatePickerMUI}
                        texterror={props.touched.endDate && props.errors.endDate}
                        texticon="*"
                        value={props.values.endDate}
                        onChange={(event) => {
                          props.setFieldValue("endDate", event);
                        }}
                      />
                    </Grid>

                    <Grid item lg={6} xs={6}>
                      <Field
                        id="agency"
                        label="Đại lý bán xe"
                        component={SelectMUI}
                        texticon="*"
                        name="agency"
                        texterror={props.touched.agency && props.errors.agency}
                        options={storeDealer.arrFilter}
                        value={props.values.agency}
                        onChange={(event) => {
                          props.setFieldValue("agency", event.target.value);
                        }}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
              <Card sx={{ marginBottom: '25px' }}>
                <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                  <Button variant='contained' type='submit'>
                    Lưu
                  </Button>
                  <Button color='error' variant='outlined' onClick={handleCancel}>
                    Hủy
                  </Button>
                </CardContent>
              </Card>
            </Form>
          )
        }}
      </Formik>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </>
  )
}
export default CreateRegionContent
