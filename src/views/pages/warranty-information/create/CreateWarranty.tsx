// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React
import { useState } from 'react'
import router from 'next/router'

// ** Components
import ModalNotify from 'src/views/component/modalNotify'

// ** Api

// ** Config
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Interface
import CreateWarrantyContent from './CreateWarrantyForm'

// import { useDispatch } from 'react-redux'
// import { AppDispatch } from 'src/store'

const CreateWarranty = () => {
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux
  // const dispatch = useDispatch<AppDispatch>()

  // const handleCreate = async (param: IParamRegionCreate) => {

  const handleCreate = async () => {

    // const res = await ApiPostCreateRegions(param)
    // if (res.code === 200 && res.result) {

    setModalNotify({
      isOpen: true,
      title: 'Thêm Thành Công',
      textBtn: 'Quay lại',
      actionReturn: () => router.push('/warranty-management/warranty-information/')
    })

    // dispatch(handleSetReloadDataRegion())

    setModalConfirm(INIT_STATE_MODAL_CONFIRM)

    // } else {
    //   setModalNotify({
    //     isOpen: true,
    //     title: 'Thêm Thất Bại',
    //     message: res.message || UNKNOW_ERROR
    //   })
    //   setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    // }
  }

  // const handleUpdate = async (param: IParamRegionCreate) => {

  const handleUpdate = async () => {

    // const res = await ApiUpdateRegionByCode(param)
    // if (res.code === 200 && res.result) {

    setModalNotify({
      isOpen: true,
      title: 'Cập nhật thông tin Thành Công'
    })
    setModalConfirm(INIT_STATE_MODAL_CONFIRM)

    //   dispatch(handleSetReloadDataRegion())
    // } else {
    //   setModalNotify({
    //     isOpen: true,
    //     title: 'Cập nhật thông tin Thất Bại',
    //     message: res.message || UNKNOW_ERROR
    //   })
    //   setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    // }
  }

  return (
    <>
      <Grid item xs={12} p={0}>
        <CreateWarrantyContent
          handleCreate={handleCreate}
          handleUpdate={handleUpdate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateWarranty
