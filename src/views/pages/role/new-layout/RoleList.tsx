// ** MUI Imports
import Box from "@mui/system/Box";
import Card from "@mui/material/Card";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import { DataGrid, GridColDef } from "@mui/x-data-grid";

// ** React Imports
import { useState } from "react";

// ** Redux Store Imports
import { AppDispatch, RootState } from "src/store"
import { useDispatch, useSelector } from "react-redux"
import { fetchDataGetRole } from "src/store/apps/role";
import { setListPermissionRoleEdit } from "src/store/apps/permission";

// ** Config Imports
import { permissonConfig } from "src/configs/roleConfig";
import { ModalConfirmState, ModalNotifyState } from "src/configs/typeOption";
import { ERROR_NULL_VALUE_INPUT, INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from "src/configs/initValueConfig";

// ** Util Imports
import { checkUserPermission } from "src/@core/utils/permission";
import { setValueBackrop } from "src/store/utils/loadingBackdrop";

// ** Icon Imports
import Icon from "src/@core/components/icon";

// ** Api Type Interface Imports
import { IDataRow } from "../list/interface";
import { ApiCreateRoleCms, ApiDeleteRoleCms, ApiDetailRoleCms } from "src/api/authen";

// ** Component Imports
import ModalConfirm from "src/views/component/modalConfirm";

// ** Thirst Part Imports
import * as Yup from 'yup';
import { toast } from "react-hot-toast";
import { InputRequiredMUI, TypographyDataGrid } from "src/views/component/theme";

import ModalClonePermission, { useModalClonePermission } from "src/components/Modal/ModalClonePermission";
import { CardContent, Grid, SxProps, Typography } from "@mui/material";
import { Field, Form, Formik } from "formik";
import { INIT_VALUE_DATA_BODY_CLONE } from "../create/service";
import ModalNotify from "src/views/component/modalNotify";

const BoxStyle: SxProps = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '50%',
  height: 'auto',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
  padding: "40px 50px"

}

function RoleList() {

  // ** State
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [rolePermission, setRolePermission] = useState(null)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.role)

  const { open, handleClose, handleOpen } = useModalClonePermission()

  // ** Permission 
  const canEdit = checkUserPermission(permissonConfig.ROLE_EDIT)
  const canDelete = checkUserPermission(permissonConfig.ROLE_DELETE)

  // show popup clone permission

  const handleShowPopup = async (id: string) => {

    await handleGetDetail(id)
    handleOpen()

  }

  // submit form

  const handleSubmit = async (values) => {

    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Sao chép nhóm quyền từ ' + rolePermission?.param.name,
      action: () => { handleCreateRolePermission(values) }
    })

  }

  // create role permission

  const handleCreateRolePermission = async (values) => {
    const param = {...values, permissionCodes: rolePermission?.data}

    dispatch(setValueBackrop(true))

    const res = await ApiCreateRoleCms(param)
    dispatch(setValueBackrop(false))
    handleClose()
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thành Công",
      })
      dispatch(fetchDataGetRole(store.param))
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }


  // validation

  const SignupSchema = Yup.object().shape({
    name: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  });

  const getDeleteData = async (id: string) => {
    dispatch(setValueBackrop(true))
    const res = await ApiDeleteRoleCms(id)
    dispatch(setValueBackrop(false))
    if (res.code == 200 && res.result) {
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      toast.success("Xóa thành công")
      dispatch(fetchDataGetRole(store.param))
    } else {
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      toast.success("Xóa thất bại")
    }

  }

  const handleDelete = (item) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xóa vai trò ' + item.name,
      action: () => { getDeleteData(item.id) }
    })
  }

  const handleGetDetail = async (id: string) => {
    dispatch(setValueBackrop(true))
    const res = await ApiDetailRoleCms(id)
    dispatch(setValueBackrop(false))
    if (res && res.code == 200 && res.result) {
      const param = {
        data: [],
        param: {
          name: res.data.name,
          id: res.data.id,
          description: res.data.description
        }
      }
      res.data.claims.forEach(item => {
        param.data.push(item.claimValue)
      })

      setRolePermission(param)
      dispatch(setListPermissionRoleEdit(param))
    }
  }

  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 250,
      field: 'name',
      sortable: false,
      headerName: 'Tên Vai Trò',
      renderCell: ({ row }: IDataRow) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Tooltip title={row.description} placement="top-start">
              <TypographyDataGrid noWrap >
                {row.name}
              </TypographyDataGrid>
            </Tooltip>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 120,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }: IDataRow) => {

        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: "center" }}>
            {
              canEdit &&
              <Button
                sx={{ p: 0, minWidth: "30px" }}
                onClick={() => handleGetDetail(row.id)}
              >
                <Icon icon='bx:pencil' fontSize={20} color="#32475CDE" />
              </Button >
            }
            {
              canEdit &&
              <Button
                sx={{ p: 0, minWidth: "30px" }}
                onClick={() => { handleShowPopup(row.id) }}
              >
                <Icon icon='charm:copy' fontSize={20} color="#32475CDE" />
              </Button >
            }
            {
              canDelete &&
              <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                onClick={() => handleDelete(row)}>
                <Icon icon='bx:trash' fontSize={20} color="#32475CDE" />
              </Button>
            }
          </Box>
        )
      }
    },

  ]

  return (
    <Card sx={{ marginTop: "24px" }}>
      <DataGrid
        autoHeight
        rows={store.data || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-main': {
            overflowY: "auto",
            height: "400px",
            overflowX: "hidden"
          },
          '& .MuiDataGrid-main::-webkit-scrollbar': {
            display: "none"
          },
          '& .MuiDataGrid-columnHeaders': {
            zIndex: 99,
            background: "#FFF",
            position: "sticky",
            top: 0,
            left: 0
          },
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          },
          '& .MuiDataGrid-footerContainer': {
            display: "none"
          }
        }}
      />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalClonePermission open={open} handleClose={handleClose}>
        <Box sx={BoxStyle}>
              <Formik

                initialValues={INIT_VALUE_DATA_BODY_CLONE}
                onSubmit={values => handleSubmit(values)}
                validationSchema={SignupSchema}
                validateOnBlur
              >
                {(props) => {

                  return (
                    <Form>
                      <CardContent sx={{ p: 0 }}>
                        <Typography sx={{ fontWeight: 500, paddingBottom: "20px", fontSize: "24px", textAlign: "center" }}>
                          Sao chép nhóm quyền từ <br /> {rolePermission?.param.name}
                        </Typography>

                        <Grid item container spacing={6} marginBottom={5}>
                          <Grid item xs={12}>
                            <Field
                              id="my-name"
                              label="Tên vai trò"
                              name="name"
                              component={InputRequiredMUI}
                              texterror={props.touched.name && props.errors.name}
                              texticon="*"
                            />
                          </Grid>
                          <Grid item xs={12}>
                            <Field
                              id="my-description"
                              label="Mô tả"
                              name="description"
                              component={InputRequiredMUI}
                              texterror={props.touched.description && props.errors.description}
                            />
                          </Grid>
                        </Grid>
                      </CardContent>
                      <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px", padding: "0 !important" }}>
                        <Button variant="contained" type="submit">Lưu</Button>
                      </CardContent>
                    </Form>
                  )
                }
                }
              </Formik>
        </Box>
      </ModalClonePermission>
    </Card>
  );
}

export default RoleList;