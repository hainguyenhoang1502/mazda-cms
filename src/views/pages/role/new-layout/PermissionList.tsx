// ** MUI Imports
import styled from "@emotion/styled";
import Card from "@mui/material/Card";
import Table from "@mui/material/Table";
import Checkbox from "@mui/material/Checkbox";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import FormControlLabel from "@mui/material/FormControlLabel";
import { Button, CardContent, Typography } from "@mui/material";

// ** Component Imports
import ModalConfirm from "src/views/component/modalConfirm";
import ModalNotify from "src/views/component/modalNotify";

// ** React Imports 
import { useEffect, useState } from "react";

//** Store Redux Imports
import { AppDispatch, RootState } from "src/store";
import { useDispatch, useSelector } from "react-redux";
import { setValueBackrop } from "src/store/utils/loadingBackdrop";
import { fetchDataGetPermission } from "src/store/apps/permission";

// ** Api Imports
import { IDataPermission } from "src/api/authen/type";
import { ApiUpdateRoleCms } from "src/api/authen";

// ** Config Imports
import { ModalConfirmState, ModalNotifyState } from "src/configs/typeOption";
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from "src/configs/initValueConfig";

const TableCellSticky = styled(TableCell)({
  fontSize: 16,
  fontWeight: 600,
  whiteSpace: 'nowrap',
  position: "sticky",
  left: 0,
  top: 0,
  background: "#FFF",
  zIndex: "99"
})
function RolePermissionList() {

  const [listPermission, setListPermission] = useState<Array<string>>([])
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)
  const [listAllRequired, setListAllRequired] = useState([])

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.permission)

  useEffect(() => {
    if (!store.data) {
      dispatch(fetchDataGetPermission())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (Array.isArray(store.dataPermissionRole)) {
      setListPermission(store.dataPermissionRole)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [store.dataPermissionRole])

  useEffect(() => {
    handleGetAllRequired()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [store.data])

  const renderChecked = (permission: string) => {

    if (permission === 'admin' && listAllRequired.length === listPermission.length) {
      return true
    }

    const itemChecked = listPermission.find(it => it === permission)

    return itemChecked ? true : false
  }

  const handleGetAllRequired = () => {

    if (store.data) {
      const codes = store.data.flatMap(item => item.permissions.map(permission => permission.code));

      setListAllRequired(codes)
    }

  }



  const handleCheckded = (permission: string, groupCode: string | number) => {

    const arr = [...listPermission]
    let arrFinal = []
    const indexChecked = listPermission.findIndex(it => it === permission)

    if (permission === 'admin') {

      if (listPermission.length === listAllRequired.length) {
        arrFinal = []
      }

      else {
        arrFinal = [...listAllRequired]
      }

      setListPermission(arrFinal)

      return
    }

    if (indexChecked === -1) {

      arr.push(permission)
      arrFinal = handleCheckReuired(permission, groupCode, arr)

    } else {

      arr.splice(indexChecked, 1)
      arrFinal = handleUnCheckReuired(permission, groupCode, arr)

    }

    setListPermission(arrFinal)
  }

  const handleCheckReuired = (permission, groupCode, arr) => {
    const targetGroup = store.data.find(obj => obj.groupCode === groupCode);

    if (targetGroup) {

      const targetCode = targetGroup.permissions.find(obj => obj.code === permission);

      if (targetCode) {
        const arrRequired = targetCode.required;
        const arrListPer = arr;

        // Gộp hai mảng
        const mergedArray = arrRequired.concat(arrListPer);

        // Lọc và loại bỏ các phần tử trùng nhau
        const uniqueArray = [...new Set(mergedArray)];

        return uniqueArray
      }
    }

    return arr;
  }

  const handleUnCheckReuired = (permission, groupCode, arr) => {
    const targetGroup = store.data.find(obj => obj.groupCode === groupCode);

    if (targetGroup) {

      const filteredData = targetGroup.permissions.filter(obj => obj.required.includes(permission));

      const arrCode = filteredData.map(obj => obj.code);
      const arrListPer = arr;

      const filteredArray = arrListPer.filter(item => !arrCode.includes(item));

      return filteredArray
    }

    return arr
  }

  const handlConfirmSave = () => {
    setModalConfirm({
      isOpen: true,
      title: "Bạn đang thao tác Cập nhật thông tin nhóm quyền",
      action: () => handleSavePermissionRole()
    })
  }

  const handleSavePermissionRole = async () => {
    const param = {
      ...store.param,
      permissionCodes: listPermission
    }
    dispatch(setValueBackrop(true))

    const res = await ApiUpdateRoleCms(param)
    dispatch(setValueBackrop(false))
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin thành công",
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Thêm thất bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  return (
    <Card>
      {
        store.param?.name &&
        <CardContent>
          <Typography sx={{ fontSize: '1.375rem', fontWeight: 700, color: '#696CFF' }}>{store.param?.name}</Typography>
        </CardContent>
      }
      <CardContent>
        <TableContainer sx={{ paddingBottom: "20px" }}>
          <Table size='small'>
            <TableBody>
              <TableRow sx={{ '& .MuiTableCell-root:first-of-type': { pl: '0 !important', minWidth: "100px" } }}>
                <TableCellSticky>
                  Quyền truy cập của quản trị viên
                </TableCellSticky>
                <TableCell>
                  <FormControlLabel
                    label={<Typography style={{ fontSize: "13px" }}>Chọn tất cả</Typography>}
                    sx={{ my: -1, whiteSpace: 'nowrap', fontSize: "12px !important" }}
                    control={
                      <Checkbox
                        id={`admin-read`}
                        checked={renderChecked('admin')}
                        onChange={() => handleCheckded('admin', 0)}
                      />
                    }
                  />
                </TableCell>
              </TableRow>
              {store.data && store.data.length > 0 && store.data.map((item: IDataPermission, index: number) => (
                <TableRow key={index} sx={{ '& .MuiTableCell-root:first-of-type': { pl: '0 !important', minWidth: "100px" } }}>
                  <TableCellSticky>
                    {item.groupName}
                  </TableCellSticky>
                  {item.permissions && item.permissions.length > 0 && item.permissions.map((permission, indexPer) => (
                    <TableCell key={indexPer} sx={{ fontSize: "12px" }}>
                      <FormControlLabel
                        label={<Typography style={{ fontSize: "13px" }}>{permission.name}</Typography>}
                        sx={{ my: -1, whiteSpace: 'nowrap', fontSize: "12px !important" }}
                        control={
                          <Checkbox
                            id={`${indexPer}-read`}
                            checked={renderChecked(permission.code)}
                            onChange={() => handleCheckded(permission.code, item.groupCode)}
                          />
                        }
                      />
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </CardContent>
      <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
        <Button variant="contained" onClick={() => handlConfirmSave()}>Lưu</Button>
      </CardContent>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />

    </Card>

  );
}

export default RolePermissionList;