
// ** MUI Imports
import Card from "@mui/material/Card"
import CardContent from "@mui/material/CardContent"
import Typography from "@mui/material/Typography"
import Button from "@mui/material/Button"
import Grid from "@mui/material/Grid";

// ** Third Party Import
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';

// ** React Imports
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

// ** Config Imports
import { ERROR_NULL_VALUE_INPUT, INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from "src/configs/initValueConfig";
import { ModalConfirmState, ModalNotifyState } from "src/configs/typeOption";

// ** Component Imports
import { InputRequiredMUI } from "src/views/component/theme";
import ModalConfirm from "src/views/component/modalConfirm";
import ModalNotify from "src/views/component/modalNotify";

// ** Store Imports
import { AppDispatch, RootState } from "src/store";

// ** Service and Type Interface Imports
import { IBodyDataCreate } from "../create/interface";
import { INIT_VALUE_DATA_BODY_CREATE } from "../create/service";

// ** Api Imports
import { ApiCreateRoleCms } from "src/api/authen";
import { fetchDataGetRole } from "src/store/apps/role";

function RoleCreate() {

  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.role)

  const handleSubmit = async (values) => {

    setModalConfirm({
      isOpen: true,
      title: "Bạn đang thao tác Thêm nhóm quyền",
      action: () => {
        return handleCreate(values)
      }
    })
  }

  const handleCreate = async (data: IBodyDataCreate) => {
    const res = await ApiCreateRoleCms(data)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thành Công",
      })
      dispatch(fetchDataGetRole(store.param))
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const SignupSchema = Yup.object().shape({
    name: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  });

  return (
    <Card>
      <CardContent>
        <Formik

          initialValues={INIT_VALUE_DATA_BODY_CREATE}
          onSubmit={values => handleSubmit(values)}
          validationSchema={SignupSchema}
          validateOnBlur
        >
          {(props) => {

            return (
              <Form>
                <CardContent sx={{ p: 0 }}>
                  <Typography noWrap sx={{ fontWeight: 500, paddingBottom: "20px", fontSize: "24px" }}>
                    Thông tin vai trò
                  </Typography>

                  <Grid item container spacing={6} marginBottom={5}>
                    <Grid item xs={12}>
                      <Field
                        id="my-name"
                        label="Tên vai trò"
                        name="name"
                        component={InputRequiredMUI}
                        texterror={props.touched.name && props.errors.name}
                        texticon="*"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Field
                        id="my-description"
                        label="Mô tả"
                        name="description"
                        component={InputRequiredMUI}
                        texterror={props.touched.description && props.errors.description}
                      />
                    </Grid>
                  </Grid>
                </CardContent>
                <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px", padding: "0 !important" }}>
                  <Button variant="contained" type="submit">Lưu</Button>
                </CardContent>
              </Form>
            )
          }
          }
        </Formik>
      </CardContent>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
      <ModalNotify {...modalNotify} toggle={setModalNotify} />

    </Card>
  );
}

export default RoleCreate;