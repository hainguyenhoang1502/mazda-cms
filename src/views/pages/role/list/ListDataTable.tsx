// ** MUI Imports
import Box from '@mui/system/Box'
import Button from '@mui/material/Button'
import { DataGrid, GridColDef } from "@mui/x-data-grid"
import TablePagination from '@mui/material/TablePagination'

// ** Next Imports
import router from "next/router"

// ** Component Imports
import Icon from 'src/@core/components/icon'
import { TypographyDataGrid } from "src/views/component/theme"

// ** Config Imports
import { permissonConfig } from "src/configs/roleConfig"

// ** Type Interface Imports
import { IDataRow, ListDataTableProps } from "./interface"
import { IParamFilterListRole } from 'src/api/authen/type'

// ** Utils Import
import { checkUserPermission } from "src/@core/utils/permission"

// ** Store Redux Imports
import { AppDispatch, RootState } from "src/store"
import { useDispatch, useSelector } from "react-redux"
import { fetchDataGetRole } from 'src/store/apps/role'

const ListDataTable = (props: ListDataTableProps) => {

  const { setModalConfirm, getDeleteData } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.role)

  const handleChangePage = (e, page) => {
    const param: IParamFilterListRole = {
      ...store.param,
      pageIndex: page + 1,
    }
    dispatch(fetchDataGetRole(param))
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamFilterListRole = {
      ...store.param,
      pageSize: Number(event.target.value),
    }
    dispatch(fetchDataGetRole(param))
  }

  const handleDelete = (item) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xóa vai trò ' + item.name,
      action: () => { getDeleteData(item.id) }
    })
  }

  // ** Permission 
  const canEdit = checkUserPermission(permissonConfig.ROLE_CREATE)
  const canDelete = checkUserPermission(permissonConfig.ROLE_DELETE)
  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }: IDataRow) => {

        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            {
              canEdit &&
              <Button
                sx={{ p: 0, minWidth: "30px" }}
                onClick={() => router.push(`/system/role/edit/${row.id}`)}
              >
                <Icon icon='bx:pencil' fontSize={20} color="#32475CDE" />
              </Button >
            }
            {
              canDelete &&
              <Button color='primary' sx={{ p: 0, minWidth: "30px" }}
                onClick={() => handleDelete(row)}>
                <Icon icon='bx:trash' fontSize={20} color="#32475CDE" />
              </Button>
            }
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'name',
      sortable: false,
      headerName: 'Tên Vai Trò',
      renderCell: ({ row }: IDataRow) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.name}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'fullName',
      headerName: 'Mô tả',
      sortable: false,
      renderCell: ({ row }: IDataRow) => {

        return (
          <TypographyDataGrid noWrap >
            {row.description}
          </TypographyDataGrid>
        )
      }
    }

  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={store.data || []}
        disableColumnFilter={true}
        columns={columns}

        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          },
          '& .MuiDataGrid-footerContainer': {
            display: "none"
          }
        }}
      />
      <TablePagination
        component="div"
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>


  )
}
export default ListDataTable