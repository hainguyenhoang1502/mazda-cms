
// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment } from '@mui/material'

// ** React Imports
import { useState } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'

// ** Interface Services Imports 
import { IParamValuesFilter } from './interface'

// ** Utils
import { checkUserPermission } from 'src/@core/utils/permission'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetRole } from 'src/store/apps/role'


const TableHeader = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.role)

  const [valuesFilter, setValuesFilter] = useState<IParamValuesFilter>({
    keyword: "",
    startDate: null,
    endDate: null
  })

  const handleChangeInput = (value, field) => {
    const body = { ...valuesFilter }
    body[field] = value
    setValuesFilter(body)
  }
  const handleFilterDate = () => {
    const param = {
      ...store.param,
      ...valuesFilter,
    }
    dispatch(fetchDataGetRole(param))
  }

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: "25px"
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', minWidth: "500px" }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ mr: 4, width: "70%", }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            ),
          }}
          placeholder='Tìm kiếm…'
          onChange={(e) => handleChangeInput(e.target.value, "keyword")}

        />
        <Button variant="outlined" onClick={handleFilterDate} sx={{ mr: 4 }} >
          Tìm Kiếm
        </Button>
      </Box>
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', gap: "1rem", justifyContent: "end", maxWidth: "100%" }}>
        {
          checkUserPermission(permissonConfig.ROLE_CREATE) &&
          <Link href="/system/role/create" as={"/system/role/create"}>
            <Button variant="contained">
              + thêm vai trò
            </Button>
          </Link>
        }
      </Box>
    </Box>
  )
}

export default TableHeader
