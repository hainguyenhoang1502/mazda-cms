import { GridRowId } from '@mui/x-data-grid'
import { IResultDataDetailRole } from 'src/api/authen/type'

interface ListDataTableProps {
  setModalConfirm:Function
  getDeleteData:Function
}
interface RowDataDetail {
  id: number
  customerName: string
  gender: number
  phoneNumber: string
  email: string
  address: string
  numberOfCarsOwned: string
  status: boolean
}
interface TableHeaderProps {
  setModalConfirm:Function
  arrRowId:Array<GridRowId>
  getDeleteData:Function
}
interface IParamValuesFilter {
  keyword: string
  startDate: string
  endDate: string
}

interface IDataRow{
  row:IResultDataDetailRole
}
export type { ListDataTableProps, RowDataDetail, TableHeaderProps, IParamValuesFilter,IDataRow }
