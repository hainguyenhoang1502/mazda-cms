import { ModalConfirmState } from "src/configs/typeOption"

interface IBodyDataCreate {
  name: string,
  description: string
}

interface IBodyDataClone {
  name: string,
  description: string,
  permissionCodes: Array<any>
}

interface PickerProps {
  label?: string
  error?: boolean
  registername?: string
}
interface CreatePermissionRightProps {
  handleCreate: Function
  setModalConfirm: Function,
  modalConfirm: ModalConfirmState
  handleUpdate: Function
}

export type {
  IBodyDataCreate,
  PickerProps,
  CreatePermissionRightProps,
  IBodyDataClone
}
