
// ** MUI Imports
import Card from "@mui/material/Card"
import CardContent from "@mui/material/CardContent"
import Typography from "@mui/material/Typography"
import Button from "@mui/material/Button"
import Grid from "@mui/material/Grid";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";

// ** React Imports
import { useContext, useEffect, useState } from "react"

// ** Next Imports
import router from "next/router"

// ** Third Party Import
import * as Yup from 'yup';
import { Formik, Form, Field } from 'formik';

// ** Components Imports
import ModalConfirm from "src/views/component/modalConfirm"

import { InputRequiredMUI } from "src/views/component/theme";

// ** Interface Services Imports 
import { INIT_VALUE_DATA_BODY_CREATE } from "./service"
import { CreatePermissionRightProps } from "./interface"

// ** Context Imports
import { EditRoleContext } from "src/pages/system/role/edit/[slug]";

//** Store Imports
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "src/store";
import { fetchDataGetPermission } from "src/store/apps/permission";

import { IDataPermission } from "src/api/authen/type";
import styled from '@emotion/styled'
import { ERROR_NULL_VALUE_INPUT } from "src/configs/initValueConfig";


const TableCellSticky = styled(TableCell)({
  fontSize: 20,
  fontWeight: 600,
  whiteSpace: 'nowrap',
  position: "sticky",
  left: 0,
  top: 0,
  background: "#FFF",
  zIndex: "99"
})

const CreatePermissionRight = (props: CreatePermissionRightProps) => {


  // ** Hooks
  const context = useContext(EditRoleContext);
  const isEdit = context && context.isEdit;

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.permission)

  // ** Props
  const { handleCreate, modalConfirm, setModalConfirm, handleUpdate } = props

  // ** States
  const [listPermission, setListPermission] = useState<Array<string>>([])


  useEffect(() => {
    if (!store.data) {
      dispatch(fetchDataGetPermission())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (context && context.data && context.data.claims) {
      const arr = []
      context.data.claims.forEach(item => {
        arr.push(item.claimValue)
      })
      setListPermission(arr)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context && context.data])

  const SignupSchema = Yup.object().shape({
    name: Yup.string().required(ERROR_NULL_VALUE_INPUT),
  });

  const handleSubmit = async (values) => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? "Bạn đang thao tác Cập nhật thông tin nhóm quyền" : "Bạn đang thao tác Thêm nhóm quyền",
      action: () => {
        if (isEdit) {
          return handleUpdate(values, listPermission, context.data && context.data.id)
        } else {
          return handleCreate(values, listPermission)
        }
      }
    })
  }
  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? "Bạn đang thao tác Huỷ cập nhật thông tin nhóm quyền" : "Bạn đang thao tác Huỷ thêm nhóm quyền",
      action: () => router.push("/permission")
    })
  }

  const renderChecked = (permission: string) => {
    const itemChecked = listPermission.find(it => it === permission)

    return itemChecked ? true : false
  }

  const handleCheckded = (permission: string) => {
    const arr = [...listPermission]
    const indexChecked = listPermission.findIndex(it => it === permission)
    if (indexChecked === -1) {
      arr.push(permission)
    } else {
      arr.splice(indexChecked, 1)
    }
    setListPermission(arr)
  }

  return (
    <Card>
      <Formik

        initialValues={isEdit ? context.data : INIT_VALUE_DATA_BODY_CREATE}
        onSubmit={values => handleSubmit(values)}
        validationSchema={SignupSchema}
        validateOnBlur
      >
        {(props) => {

          return (
            <Form>
              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px" }}>
                  Thông tin vai trò
                </Typography>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item xs={12}>
                    <Field
                      id="my-name"
                      label="Tên vai trò"
                      name="name"
                      component={InputRequiredMUI}
                      texterror={props.touched.name && props.errors.name}
                      texticon="*"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      id="my-description"
                      label="Mô tả"
                      name="description"
                      component={InputRequiredMUI}
                      texterror={props.touched.description && props.errors.description}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, textTransform: 'capitalize', padding: "20px 0", fontSize: "24px" }}>
                  Thông Tin Nhóm quyền
                </Typography>
                <TableContainer sx={{ paddingBottom: "20px" }}>
                  <Table size='small'>
                    <TableBody>
                      {store.data && store.data.length > 0 && store.data.map((item: IDataPermission, index: number) => {
                        return (
                          <TableRow key={index} sx={{ '& .MuiTableCell-root:first-of-type': { pl: '0 !important', minWidth: "100px" } }}>
                            <TableCellSticky
                              sx={{
                                fontWeight: 600,
                                whiteSpace: 'nowrap',
                              }}
                            >
                              {item.groupName}
                            </TableCellSticky>
                            {
                              item.permissions && item.permissions.length > 0 && item.permissions.map((permission, indexPer) => (
                                <>
                                  <TableCell>
                                    <FormControlLabel
                                      label={permission.name}
                                      sx={{ my: -1, whiteSpace: 'nowrap', }}
                                      control={
                                        <Checkbox
                                          id={`${indexPer}-read`}
                                          checked={renderChecked(permission.code)}
                                          onChange={() => handleCheckded(permission.code)}
                                        />
                                      }
                                    />
                                  </TableCell>
                                </>
                              ))
                            }

                          </TableRow>
                        )
                      })}
                    </TableBody>
                  </Table>
                </TableContainer>
              </CardContent>

              <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
                <Button variant="contained" type="submit">Lưu</Button>
                <Button color="error" variant="outlined" onClick={handleCancel} >Hủy</Button>
              </CardContent>
            </Form>
          )
        }
        }
      </Formik>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Card>

  )
}
export default CreatePermissionRight  