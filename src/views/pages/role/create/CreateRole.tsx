
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React  
import { useState } from 'react'

import router from 'next/router'

// ** Components 
import ModalNotify from 'src/views/component/modalNotify'

// ** Api  
import { ApiCreateRoleCms, ApiUpdateRoleCms } from 'src/api/authen'

// ** Config 
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Interface
import { IBodyDataCreate } from './interface'

import CreatePermissionRight from './CreateRoleRight'
import { handleSetReloadDataRole } from 'src/store/apps/role'
import { useDispatch } from 'react-redux'
import { AppDispatch } from 'src/store'

const CreateMain = () => {


  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  const dispatch = useDispatch<AppDispatch>()


  const handleCreate = async (data: IBodyDataCreate, listPermission: Array<string>) => {
    const param = {
      ...data,
      permissionCodes: listPermission
    }
    const res = await ApiCreateRoleCms(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thành Công",
        textBtn: 'Quay lại',
        actionReturn: () => router.push("/role")
      })
      dispatch(handleSetReloadDataRole())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (data: IBodyDataCreate, listPermission: Array<string>, id: string) => {
    const param = {
      ...data,
      id: id,
      permissionCodes: listPermission
    }
    const res = await ApiUpdateRoleCms(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thành Công",
      })
      dispatch(handleSetReloadDataRole())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }


  return (
    <>
      <Grid item xs={12} p={0}>
        <CreatePermissionRight
          handleCreate={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdate={handleUpdate}

        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateMain