import styled from '@emotion/styled'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import TextField from '@mui/material/TextField'
import { IBodyDataClone, IBodyDataCreate } from './interface'

const MuiTextField = styled(TextField)({
  flex: '0 0 45% '
})
const MuiInputLabel = styled(InputLabel)({
  display: 'flex',
  gap: '5px'
})
const MuiFormControlTextField = styled(FormControl)({
  
  flex: '0 0 45% '
})
const MuiFormControl = styled(FormControl)({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingBottom: '30px'
})

const INIT_VALUE_DATA_BODY_CREATE:IBodyDataCreate={
  name:"",
  description:""
}

const INIT_VALUE_DATA_BODY_CLONE:IBodyDataClone={
  name:"",
  description:"",
  permissionCodes:[]
}

export{
    MuiTextField,
    MuiInputLabel,
    MuiFormControlTextField,
    MuiFormControl,
    INIT_VALUE_DATA_BODY_CREATE,
    INIT_VALUE_DATA_BODY_CLONE
}
