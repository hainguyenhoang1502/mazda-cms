
import { ERROR_NULL_VALUE_INPUT } from 'src/configs/initValueConfig';
import * as Yup from 'yup';


export const INIT_VALUE_DATA_BODY_CREATE = {
  vehicleId: 0,
  customerId: 0,
  roNo: "",
  roDate: "",
  maintenanceType: "",
  presentKm: "",
  dealerCode: "",
  receiveDate: "",
  deliveryDate: ""
}

export const MaintenanceSchema = Yup.object().shape({
  roNo: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
  roDate: Yup.date().required(ERROR_NULL_VALUE_INPUT).nullable(),
  receiveDate: Yup.date().required(ERROR_NULL_VALUE_INPUT).nullable(),
  deliveryDate: Yup.date().required(ERROR_NULL_VALUE_INPUT).nullable(),
  dealerCode: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT),
  presentKm: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
});   