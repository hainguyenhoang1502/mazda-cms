
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Import 
import { useState } from 'react'

// ** Next Import
import router from 'next/router'

// ** Component Imports
import CreateMaintenanceForm from './CreateMaintenanceForm'
import ModalNotify from 'src/views/component/modalNotify'

// ** Api  
import { ApiGetCreateMaintenanceHistory } from 'src/api/warranty'

// ** Config 
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'

// ** Store Redux Imports
import { AppDispatch } from 'src/store'
import { useDispatch } from 'react-redux'

// ** Type Interface Import
import { IParamCreateMaintenanceHistory, IResponeMaintenanceSuggestInfo } from 'src/api/warranty/type'
import { handleSetReloadDataMaintenance } from 'src/store/apps/maintenance'

const CreateMain = () => {

  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  const dispatch = useDispatch<AppDispatch>()


  const handleCreate = async (data: IParamCreateMaintenanceHistory, customer: IResponeMaintenanceSuggestInfo["data"][0]) => {
    const param = {
      ...data,
      vehicleId: customer.vehicleId,
      customerId: customer.customerId,
    }
    const res = await ApiGetCreateMaintenanceHistory(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thành Công",
        textBtn: 'Quay lại',
        actionReturn: () => router.push("/maintenance")
      })
      dispatch(handleSetReloadDataMaintenance())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }


  return (
    <>
      <Grid item xs={12} p={0}>
        <CreateMaintenanceForm
          handleCreate={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}

        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />

    </>
  )
}
export default CreateMain