
// ** MUI Imports
import Card from "@mui/material/Card"
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button"
import Typography from "@mui/material/Typography"
import CardContent from "@mui/material/CardContent"


// ** React Imports
import { useEffect, useState } from "react"

// ** Next Imports
import router from "next/router"

// ** Third Party Import
import { Formik, Form, Field } from 'formik';

// ** Components Imports
import ModalConfirm from "src/views/component/modalConfirm"

import { DatePickerMUI, InputRequiredMUI, SelectMUI } from "src/views/component/theme";

// ** Interface Services Imports 
import { MaintenanceSchema, INIT_VALUE_DATA_BODY_CREATE } from "./service"

//** Store Imports
import { AppDispatch, RootState } from "src/store";
import { useDispatch, useSelector } from "react-redux";

// ** Config Imports
import { IOptionsFilter, ModalConfirmState } from "src/configs/typeOption";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import { ApiListMaintenanceSuggestInfoCms, ApiListMaintenanceTypeCms } from "src/api/warranty";
import { fetchDataGetListFilterDealers } from "src/store/apps/category/company-dealer/dealer";
import { toast } from "react-hot-toast";

// ** Interface Type
interface CreateMaintenanceFormProps {
  handleCreate: Function
  setModalConfirm: Function,
  modalConfirm: ModalConfirmState
}

const CreateMaintenanceForm = (props: CreateMaintenanceFormProps) => {

  // ** Redux Store
  const dispatch = useDispatch<AppDispatch>()
  const storeDealer = useSelector((state: RootState) => state.dealer)

  // ** States
  const [listSuggest, setListSuggest] = useState([])
  const [timeoutId, setTimeoutId] = useState(null);
  const [customer, setCustomer] = useState(null)
  const [optionsMaintenanceType, setOptionsMaintenanceType] = useState<Array<IOptionsFilter>>([])

  // ** Props
  const { handleCreate, modalConfirm, setModalConfirm } = props

  useEffect(() => {
    getListMaintenanceType()
    if (!storeDealer.arrFilter || storeDealer.arrFilter.length === 0) {
      dispatch(fetchDataGetListFilterDealers())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleGetListSuggest = (event) => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }

    if (event && event.target.value && event.target.value.length > 0) {

      const newTimeoutId = setTimeout(() => {
        getListSuggestOption(event.target.value)
      }, 1000);

      setTimeoutId(newTimeoutId);

    } else {
      setListSuggest([])
    }
  }

  const getListSuggestOption = async (value: string) => {
    const res = await ApiListMaintenanceSuggestInfoCms(value)
    const arr = []
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      res.data.forEach(item => {
        arr.push({
          ...item,
          title: `${item.vinCode} - ${item.licensePlate} - ${item.modelName} - ${item.fullName} -${item.phoneNumber}`
        })

      })
    }
    setListSuggest(arr)
  }

  const getListMaintenanceType = async () => {
    const res = await ApiListMaintenanceTypeCms()
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      const option = res.data.map(item => {
        return {
          value: item.code,
          label: item.name
        }
      })
      setOptionsMaintenanceType(option)
    }
  }
  const handleSubmit = async (values) => {
    if (!customer || !customer.vehicleId || !customer.customerId) {
      toast.error("Bạn chưa chọn khách hàng")
    } else {

      setModalConfirm({
        isOpen: true,
        title: "Bạn đang thao tác Thêm thông tin bảo dưỡng",
        action: () => {
          return handleCreate(values, customer)
        }
      })
    }
  }

  const handleCancel = () => {

    setModalConfirm({
      isOpen: true,
      title: "Bạn đang thao tác Huỷ thêm điểm bán hàng",
      action: () => router.push("/maintenance")
    })
  }

  return (
    <Grid>
      <Formik
        initialValues={INIT_VALUE_DATA_BODY_CREATE}
        onSubmit={values => handleSubmit(values)}
        validationSchema={MaintenanceSchema}
        validateOnBlur
      >
        {(props) => {

          return (
            <Form>
              <Grid item marginBottom={5}>
                <Card>
                  <CardContent sx={{ width: "50%" }}>
                    <Autocomplete
                      options={listSuggest}
                      onChange={(event, value) => {
                        setCustomer(value)
                      }}
                      onKeyDown={(e) => {
                        if (e.key === 'Enter') {
                          e.preventDefault();
                        }
                      }}
                      getOptionLabel={option => option.title}
                      renderInput={params => (
                        <TextField
                          fullWidth
                          {...params}
                          label="Nhập số khung hoặc số điện thoại"
                          onChange={handleGetListSuggest}
                        />
                      )}
                    />
                  </CardContent>
                </Card>
              </Grid>

              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px" }}>
                      Thông tin xe
                    </Typography>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={6}>
                        <TextField
                          fullWidth
                          label="Biển số "
                          disabled
                          InputLabelProps={{ shrink: customer && customer.licensePlate }}
                          value={customer ? customer.licensePlate : ""}
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <TextField
                          fullWidth
                          label="Số vin"
                          disabled
                          InputLabelProps={{ shrink: customer && customer.vinCode }}
                          value={customer ? customer.vinCode : ""}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={6}>

                        <TextField
                          fullWidth
                          label="Dòng xe"
                          disabled
                          InputLabelProps={{ shrink: customer && customer.modelName }}
                          value={customer ? customer.modelName : ""}
                        />
                      </Grid>
                      <Grid item xs={6}>

                        <TextField
                          fullWidth
                          label="Màu xe"
                          disabled
                          InputLabelProps={{ shrink: customer && customer.colorNameVi }}
                          value={customer ? customer.colorNameVi : ""}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={6}>
                        <TextField
                          fullWidth
                          label="Số khung"
                          disabled
                          InputLabelProps={{ shrink: customer && customer.frameNumber }}
                          value={customer ? customer.frameNumbe : ""}
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <TextField
                          fullWidth
                          label="Năm sản xuất"
                          disabled
                          InputLabelProps={{ shrink: customer && customer.year }}
                          value={customer ? customer.year : ""}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={6}>
                        <TextField
                          fullWidth
                          label="Số máy"
                          disabled
                          InputLabelProps={{ shrink: customer && customer.engineNumber }}
                          value={customer ? customer.engineNumber : ""}
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>

              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px" }}>
                      Thông tin khách hàng
                    </Typography>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Chủ xe"
                          disabled
                          InputLabelProps={{ shrink: customer && customer.fullName }}
                          value={customer ? customer.fullName : ""}
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Số điện thoại chủ xe "
                          InputLabelProps={{ shrink: customer && customer.phoneNumber }}
                          value={customer ? customer.phoneNumber : ""}
                          disabled
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Tên liên hệ"
                          InputLabelProps={{ shrink: customer && customer.fullName }}
                          value={customer ? customer.fullName : ""}
                          disabled
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <TextField
                          fullWidth
                          label="Số điện thoại tài xế"
                          InputLabelProps={{ shrink: customer && customer.phoneNumber }}
                          value={customer ? customer.phoneNumber : ""}
                          disabled
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>

              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px" }}>
                      Thông tin bảo dưỡng
                    </Typography>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-roNo"
                          label="Mã RO"
                          name="roNo"
                          component={InputRequiredMUI}
                          texticon="*"
                          texterror={props.touched.roNo && props.errors.roNo}

                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-receiveDate"
                          label="Ngày vào xưởng"
                          name="receiveDate"
                          texticon="*"
                          texterror={props.touched.receiveDate && props.errors.receiveDate}
                          maxDate={props.values.deliveryDate && new Date(props.values.deliveryDate)}
                          component={DatePickerMUI}
                          value={props.values.receiveDate}
                          onChange={(event) => {
                            props.setFieldValue("receiveDate", event);
                          }}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-roDate"
                          label="RO date"
                          name="roDate"
                          component={DatePickerMUI}
                          texticon="*"
                          texterror={props.touched.roDate && props.errors.roDate}
                          value={props.values.roDate}
                          onChange={(event) => {
                            props.setFieldValue("roDate", event);
                          }}
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-deliveryDate"
                          label="Ngày giao xe "
                          name="deliveryDate"
                          component={DatePickerMUI}
                          minDate={props.values.receiveDate && new Date(props.values.receiveDate)}
                          texticon="*"
                          texterror={props.touched.deliveryDate && props.errors.deliveryDate}
                          value={props.values.deliveryDate}
                          onChange={(event) => {
                            props.setFieldValue("deliveryDate", event);
                          }}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-maintenanceType"
                          label="Loại bảo dưỡng"
                          name="maintenanceType"
                          texticon="*"
                          component={SelectMUI}
                          texterror={props.touched.maintenanceType && props.errors.maintenanceType}
                          options={optionsMaintenanceType}
                          value={props.values.maintenanceType}
                          onChange={(event) => {
                            props.setFieldValue("maintenanceType", event.target.value);
                          }}
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-dealerCode"
                          label="Điểm bán hàng"
                          name="dealerCode"
                          component={SelectMUI}
                          texticon="*"
                          texterror={props.touched.dealerCode && props.errors.dealerCode}
                          options={storeDealer.arrFilter}
                          value={props.values.dealerCode}
                          onChange={(event) => {
                            props.setFieldValue("dealerCode", event.target.value);
                          }}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-presentKm"
                          label="Số Km"
                          name="presentKm"
                          texticon="*"
                          component={InputRequiredMUI}
                          texterror={props.touched.presentKm && props.errors.presentKm}
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>

              <Grid item marginBottom={5}>
                <Card>
                  <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
                    <Button variant="contained" type="submit">Lưu</Button>
                    <Button color="error" variant="outlined" onClick={handleCancel} >Hủy</Button>
                  </CardContent>
                </Card>
              </Grid>
            </Form>
          )
        }
        }
      </Formik>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Grid>

  )
}
export default CreateMaintenanceForm  