
// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { Autocomplete, InputAdornment } from '@mui/material'

// ** React Imports
import { useEffect, useState } from 'react'

// ** Next Imports
// ** Third Party Import

// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Styles

// ** Config Imports

// ** Interface Services Imports 
import { IParamValuesFilter } from './interface'

// ** Utils
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetMaintenanceData } from 'src/store/apps/maintenance'
import { fetchDataGetFilterVehicleData } from 'src/store/apps/vehicle/model'
import { checkUserPermission } from 'src/@core/utils/permission'
import Link from 'next/link'
import ModalFile, { useModalFile } from 'src/components/Modal/ModalFile'
import ContentFile from 'src/views/component/ContentFile'
import { WarrantyPolicysApi } from 'src/api-client/warranty'


const TableHeader = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.maintenanceHistory)
  const storeModel = useSelector((state: RootState) => state.vehicleData)
  const { open, handleClose, handleOpen } = useModalFile()

  useEffect(() => {
    if (storeModel && (!storeModel.arrFilter || storeModel.arrFilter.length === 0)) {
      dispatch(fetchDataGetFilterVehicleData())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  const [valuesFilter, setValuesFilter] = useState<IParamValuesFilter>({
    keyword: "",
    modelCode: null,
    pageIndex: 1,
    pageSize: 10
  })

  const handleChangeInput = (value, field) => {
    const body = { ...valuesFilter }
    body[field] = value
    setValuesFilter(body)
  }
  const handleFilterDate = () => {
    const param = {
      ...store.param,
      ...valuesFilter,
    }
    dispatch(fetchDataGetMaintenanceData(param))
  }

  const handleClearFilter = () => {
    const param = {
      keyword: "",
      modelCode: "",
      pageIndex: 1,
      pageSize: 10
    }
    setValuesFilter(param)
    dispatch(fetchDataGetMaintenanceData(param))
  }

  return (
    <Box sx={{ p: 5 }}>
      <Box
        sx={{
          pb: 3,
          width: '100%',
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          justifyContent: 'space-between',
          gap: "25px"
        }}
      >

        <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
          <TextField
            size='small'
            value={valuesFilter.keyword}
            sx={{ mr: 4, width: "25%" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Icon icon='bx:search' fontSize={20} />
                </InputAdornment>
              ),
            }}
            placeholder='Tìm kiếm…'
            onChange={(e) => handleChangeInput(e.target.value, "keyword")}
          />
          <Box sx={{ mr: 4 }}>
            {/* <SelectMUI
              label='Dòng xe'
              options={storeModel.arrFilter}
              size='small'
              value={valuesFilter.modelCode}
              onChange={e => {
                handleChangeInput(e.target.value, 'modelCode')
              }}
            /> */}
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={storeModel.arrFilter}
              size='small'
              sx={{ width: 130 }}
              renderInput={(params) => <TextField {...params} label='Dòng xe' />}
              onChange={(e: any, newValue: string | null) => {
                handleChangeInput(newValue, 'modelCode')
              }}
            />
          </Box>
          <Button variant="outlined" onClick={handleFilterDate} sx={{ mr: 4 }} >
            Lọc
          </Button>
          <Button variant="outlined" onClick={handleClearFilter} color='error'>
            Xóa bộ lọc
          </Button>

        </Box>
        <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', gap: "1rem", justifyContent: "end", maxWidth: "100%" }}>
          {
            checkUserPermission("") &&
            <>
              <Link href="/maintenance/create" as={"/maintenance/create"}>
                <Button variant="contained">
                  + thêm mới
                </Button>
              </Link>

              <Button variant='outlined' onClick={handleOpen}>Import</Button>
            </>
          }
        </Box>
      </Box>
      <ModalFile open={open} handleClose={handleClose}>
        <ContentFile functionGetFile={WarrantyPolicysApi.postFileMaintainence} title='Import Lịch sử bảo dưỡng' linkDownload='https://kong-gateway.toponseek.com/ref-data/files/2023/6/admin/ImportMaintenanceHistoryTemplate.xlsx' />
      </ModalFile>
    </Box>
  )
}

export default TableHeader
