
import { GridRowId } from '@mui/x-data-grid'

interface ListDataTableProps {
  setModalConfirm:Function
  getDeleteCustomer:Function
  setArrRowId:Function
}
interface RowDataDetail {
  id: number
  customerName: string
  gender: number
  phoneNumber: string
  email: string
  address: string
  numberOfCarsOwned: string
  status: boolean
}
interface TableHeaderProps {
  setModalConfirm:Function
  arrRowId:Array<GridRowId>
  getDeleteCustomer:Function
}
interface IParamValuesFilter {
  keyword: string
  modelCode: string
  pageIndex: number,
  pageSize: number
}
export type { ListDataTableProps, RowDataDetail, TableHeaderProps, IParamValuesFilter }
