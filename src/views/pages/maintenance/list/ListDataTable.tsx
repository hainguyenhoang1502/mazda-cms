import { Box, TablePagination } from "@mui/material"

// import { renderNameGender } from "src/configs/functionConfig"
import { DataGrid, GridColDef } from "@mui/x-data-grid"

// ** Components 
import { useDispatch, useSelector } from "react-redux"
import { AppDispatch, RootState } from "src/store"
import { IParamListMaintenaceHistoryAll } from "src/api/warranty/type"
import { fetchDataGetMaintenanceData } from "src/store/apps/maintenance"

import { renderDate } from 'src/configs/functionConfig'
import { TypographyDataGrid } from "src/views/component/theme"

const ListDataTable = () => {
  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.maintenanceHistory)

  const handleChangePage = (e, page) => {
    const param: IParamListMaintenaceHistoryAll = {
      ...store.param,
      pageIndex: page + 1,
    }
    dispatch(fetchDataGetMaintenanceData(param))
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamListMaintenaceHistoryAll = {
      ...store.param,
      pageSize: Number(event.target.value),
    }
    dispatch(fetchDataGetMaintenanceData(param))
  }


  // ** Permission 
  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'roNo',
      headerName: 'mã ro',
      sortable: false,
      renderCell: ({ row }) => {

        return (
          <Box sx={{display:"flex",alignItems:"center"}}>
            <TypographyDataGrid noWrap>
              {row.roNo}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'roDate',
      sortable: false,
      headerName: 'rodate',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {renderDate(row.roDate)}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'licensePlate',
      headerName: 'Biển số',
      sortable: false,
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.licensePlate}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 180,
      field: 'modelName',
      sortable: false,
      headerName: 'dòng xe',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.modelName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 250,
      field: 'maintenanceType',
      sortable: false,
      headerName: 'Nội dung công việc',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.maintenanceType}
            </TypographyDataGrid>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 150,
      field: 'numberKm',
      sortable: false,
      headerName: 'số km',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.numberKm}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'receiveDate',
      sortable: false,
      headerName: 'ngày vào xưởng',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {renderDate(row.receiveDate)}
            </TypographyDataGrid>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 200,
      field: 'deliveryDate',
      sortable: false,
      headerName: 'ngày giao xe',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              { renderDate(row.deliveryDate)}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 250,
      field: 'customerName',
      sortable: false,
      headerName: 'Tên khách hàng',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.customerName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'phoneNumber',
      sortable: false,
      headerName: 'số điện thoại',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.phoneNumber}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'frameNumber',
      sortable: false,
      headerName: 'số khung',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.frameNumber}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 300,
      field: 'dealerName',
      sortable: false,
      headerName: 'Đại lý',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap >
              {row.dealerName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={store.data || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          },
          '& .MuiDataGrid-footerContainer': {
            display: "none"
          }
        }}
      />
      <TablePagination
        component="div"
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>


  )
}
export default ListDataTable