// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React
import { useState } from 'react'

// ** Components
import ModalNotify from 'src/views/component/modalNotify'

// ** Config
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Interface
import { IBodyDataCreate } from './interface'
import CreateDistrictContent from './CreateDisctrictForm'

const CreateDistrict = () => {
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux
  // const dispatch = useDispatch<AppDispatch>()

  const handleCreate = async (data: IBodyDataCreate, arrOrg: Array<string>) => {
    console.log({ data, arrOrg })

    // const param: IBodyDataCreate = {
    //   ...data,
    //   avatar: valueImg,
    //   organizationCodes: arrOrg
    // }
    // const res = await ApiCreateUserCms(param)
    // if (res.code === 200 && res.result) {
    //   setModalNotify({
    //     isOpen: true,
    //     title: 'Thêm Thành Công',
    //     textBtn: 'Quay lại',
    //     actionReturn: () => router.push('/user')
    //   })
    //   dispatch(handleSetReloadDataUser())
    //   setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    // } else {
    //   setModalNotify({
    //     isOpen: true,
    //     title: 'Thêm Thất Bại',
    //     message: res.message || UNKNOW_ERROR
    //   })
    //   setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    // }
  }

  const handleUpdate = async (data: IBodyDataCreate, arrOrg: Array<string>) => {
    console.log({ data, arrOrg })
    
    // const res = await ApiUpdateUserCms(param)
    // if (res.code === 200 && res.result) {
    //   setModalNotify({
    //     isOpen: true,
    //     title: 'Cập nhật thông tin Thành Công'
    //   })
    //   setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    //   dispatch(handleSetReloadDataUser())
    // } else {
    //   setModalNotify({
    //     isOpen: true,
    //     title: 'Cập nhật thông tin Thất Bại',
    //     message: res.message || UNKNOW_ERROR
    //   })
    //   setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    // }
  }

  return (
    <>
      <Grid item xs={12} p={0}>
        <CreateDistrictContent
          handleCreate={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdate={handleUpdate}
        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateDistrict
