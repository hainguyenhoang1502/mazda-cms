import { ModalConfirmState } from 'src/configs/typeOption'

interface IBodyDataCreate {
  code: string
  provincename: string
  region: string
  area: string
  status: string
}
interface PickerProps {
  label?: string
  error?: boolean
  registername?: string
}
interface CreateProvince {
  handleCreate: Function
  setModalConfirm: Function
  modalConfirm: ModalConfirmState
  handleUpdate: Function
}
interface CreateUserLeftProps {
  setValueImg: Function
}

export type IValueChecked = {
  arrReturn: Array<string>
  children: {
    childrenChecked: number
    children: Array<string>
  }[]
}
export type { IBodyDataCreate, PickerProps, CreateProvince, CreateUserLeftProps }
