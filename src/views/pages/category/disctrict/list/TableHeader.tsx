// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment } from '@mui/material'

// ** React Imports

// ** Next Imports
import Link from 'next/link'


// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Interface Services Imports

// ** Utils
import { checkUserPermission } from 'src/@core/utils/permission'
import { SelectMUI } from 'src/views/component/theme'
import { OPTIONS_STATUS } from 'src/configs/functionConfig'

// redux
import { RootState } from 'src/store'

import { fetchDataGetListDistrict } from 'src/store/apps/category/district'
import { useSelector } from 'react-redux'
import useFilter from 'src/hooks/useFilter'

const TableHeader = () => {
  // ** Redux
  const storeDistrict = useSelector((state: RootState) => state.district);

  const OPTION_VALUE = [
    ...OPTIONS_STATUS,
    {
      value: null,
      label: 'Tất cả'
    }
  ]

  const {
    valuesFilter,
    handleChangeInput,
    handleFilter,
    handleClearFilter
  } = useFilter({ fetchDataGetList: fetchDataGetListDistrict })

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: '25px'
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', minWidth: '500px', gap: 4 }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ width: '30%' }}
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            )
          }}
          placeholder='Tìm kiếm…'
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => handleChangeInput(e.target.value, 'keyword')}
        />
        <Box>
          <SelectMUI
            label='Trạng thái'
            options={OPTION_VALUE}
            value={valuesFilter.status}
            size='small'
            onChange={(e: React.ChangeEvent<HTMLSelectElement>) => handleChangeInput(e.target.value, 'status')}
          />
        </Box>

        <Button variant='outlined' onClick={() => handleFilter(storeDistrict.param.keyword,storeDistrict.param.status)} sx={{ mr: 2 }}>
          Lọc
        </Button>
        <Button variant="outlined" onClick={handleClearFilter} color='error'>
          Xóa bộ lọc
        </Button>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          gap: '1rem',
          justifyContent: 'end',
          maxWidth: '100%'
        }}
      >
        {checkUserPermission('') && (
          <Link href='/category/district/create' as={'/category/district/create'}>
            <Button variant='contained'>+thêm quận / huyện</Button>
          </Link>
        )}
      </Box>
    </Box>
  )
}

export default TableHeader
