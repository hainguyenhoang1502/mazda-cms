import { Box, TablePagination } from '@mui/material'
import { DataGrid, GridColDef } from '@mui/x-data-grid'

// ** Components
import { useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { TypographyDataGrid } from 'src/views/component/theme'
import CustomChip from 'src/@core/components/mui/chip'
import { renderNameStatus } from 'src/configs/functionConfig'
import { useDispatch } from 'react-redux'
import { fetchDataGetListDistrict } from 'src/store/apps/category/district'
import { IParamFilter } from 'src/api/type'
import { EditDataGrid } from 'src/views/component/theme/customEditDelete'
import { permissonConfig } from 'src/configs/roleConfig'

const ListDataTable = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.district)

  const handleChangePage = (e: React.MouseEvent<HTMLButtonElement> | null, page: number) => {
    const param: IParamFilter = {
      ...store.param,
      pageIndex: page + 1
    }
    dispatch(fetchDataGetListDistrict(param))
  }

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamFilter = {
      ...store.param,
      pageSize: Number(event.target.value)
    }
    dispatch(fetchDataGetListDistrict(param))
  }


  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission={permissonConfig.LOCATION_EDIT} slug={`/category/district/edit/${row.id}`} />

          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'districtFullName',
      sortable: false,
      headerName: 'Tên quận / huyện',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.districtFullName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 200,
      field: 'provincesFullName',
      sortable: false,
      headerName: 'Tên tỉnh / thành',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.provincesFullName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },

    
    {
      flex: 0.5,
      minWidth: 200,
      field: 'status',
      sortable: false,
      headerName: 'trạng thái',
      renderCell: ({ row }: any) => {
        return <CustomChip rounded skin='light' size='small' label={renderNameStatus(row.status)} color={row.status === "ACTIVE" ? "success" : "error"} />
      }
    }
  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={(store.data || [])}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: '10px'
          },
          '& .MuiDataGrid-footerContainer': {
            display: 'none'
          }
        }}
      />
      <TablePagination
        component='div'
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  )
}
export default ListDataTable
