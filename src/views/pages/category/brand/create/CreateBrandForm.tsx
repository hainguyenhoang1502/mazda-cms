// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useContext } from 'react'

// ** Next Imports
import router from 'next/router'

// ** Third Party Import
import * as Yup from 'yup'
import { Formik, Form, Field } from 'formik'

// ** Components Imports
import ModalConfirm from 'src/views/component/modalConfirm'

import { SelectMUI, InputRequiredMUI } from 'src/views/component/theme'

// ** Interface Services Imports
import { INIT_VALUE_DATA_BODY_CREATE } from './service'

// ** Api Imports
import { ERROR_NULL_VALUE_INPUT } from 'src/configs/initValueConfig'
import { OPTIONS_STATUS } from 'src/configs/functionConfig'
import { CreateBrand } from './interface'
import { EditBrandContext } from 'src/pages/category/brand/edit/[slug]'

const CreateBrandForm = (props: CreateBrand) => {
  // ** Props
  const { handleCreate, modalConfirm, setModalConfirm, handleUpdate } = props

  // ** Hooks
  const context = useContext(EditBrandContext);
  const isEdit = context && context.isEdit;

  const AddBrandSchema = Yup.object().shape({
    code: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    name: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    status: Yup.mixed().required(ERROR_NULL_VALUE_INPUT),
  })

  const handleSubmit = async values => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Cập nhật thông tin Thương hiệu' : 'Bạn đang thao tác thêm Thương hiệu',
      action: () => {
        if (isEdit) {
          return handleUpdate(values)
        } else {
          return handleCreate(values)
        }
      }
    })
  }
  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Huỷ cập nhật thông tin Thương hiệu' : 'Bạn đang thao tác Huỷ thêm Thương hiệu',
      action: () => router.push('/category/brand')
    })
  }


  return (
    <Card>
      <Formik
        initialValues={(context && context.data) || INIT_VALUE_DATA_BODY_CREATE}
        onSubmit={values => handleSubmit(values)}
        validationSchema={AddBrandSchema}
        validateOnBlur
      >
        {props => {
          return (
            <Form>
              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                  Thông tin thương hiệu
                </Typography>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='my-code'
                      label='Mã Thương Hiệu'
                      name='code'
                      component={InputRequiredMUI}
                      texterror={props.touched.code && props.errors.code}
                      texticon='*'
                    />
                  </Grid>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='my-brandname'
                      label='Tên Thương Hiệu'
                      name='name'
                      component={InputRequiredMUI}
                      texterror={props.touched.name && props.errors.name}
                      texticon='*'
                    />
                  </Grid>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='status'
                      label='Trạng thái'
                      component={SelectMUI}
                      texticon='*'
                      name='status'
                      texterror={props.touched.status && props.errors.status}
                      options={OPTIONS_STATUS}
                      value={props.values.status}
                      onChange={event => {
                        props.setFieldValue('status', event.target.value)
                      }}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                <Button variant='contained' type='submit'>
                  Lưu
                </Button>
                <Button color='error' variant='outlined' onClick={handleCancel}>
                  Hủy
                </Button>
              </CardContent>
            </Form>
          )
        }}
      </Formik>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Card>
  )
}
export default CreateBrandForm
