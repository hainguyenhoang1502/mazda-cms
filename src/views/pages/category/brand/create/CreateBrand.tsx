// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React
import { useState } from 'react'
import router from 'next/router'

// ** Components
import ModalNotify from 'src/views/component/modalNotify'

// ** Api

// ** Config
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Interface
import CreateBrandForm from './CreateBrandForm'
import { useDispatch } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { ApiOrganizationBrandUpdate, ApiOrganizationPostCreateBrand } from 'src/api/organization'
import { IBodyBrand } from 'src/api/organization/type'
import { fetchDataGetListBrand, handleSetReloadDataBrand } from 'src/store/apps/category/brand'
import { useSelector } from 'react-redux'

const CreateBrand = () => {
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  const store = useSelector((state: RootState) => state.brand)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()

  const handleCreate = async (param: IBodyBrand) => {
    const res = await ApiOrganizationPostCreateBrand(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thành Công',
        textBtn: 'Quay lại',
        actionReturn: () => router.push('/category/brand/')
      })
      dispatch(fetchDataGetListBrand(store.param))
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (param: IBodyBrand) => {
    const res = await ApiOrganizationBrandUpdate(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thành Công'
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(handleSetReloadDataBrand())
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError:true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  return (
    <>
      <Grid item xs={12} p={0}>
        <CreateBrandForm
          handleCreate={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdate={handleUpdate}
        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />

    </>
  )
}
export default CreateBrand
