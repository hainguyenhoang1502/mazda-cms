
// ** MUI Imports
import Box from '@mui/system/Box'
import TablePagination from '@mui/material/TablePagination'
import { DataGrid, GridColDef } from '@mui/x-data-grid'

// ** Components
import CustomChip from 'src/@core/components/mui/chip'
import { TypographyDataGrid } from 'src/views/component/theme'
import { DeleteDataGrid, EditDataGrid } from 'src/views/component/theme/customEditDelete'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'
import { renderNameStatus } from 'src/configs/functionConfig'

// ** Store Redux Imports
import { AppDispatch, RootState } from 'src/store'
import { useDispatch, useSelector } from 'react-redux'
import { fetchDataGetListBrand } from 'src/store/apps/category/brand'

// ** Interface
import { PropsDataTable } from './interface'

const ListDataTable = (props: PropsDataTable) => {
  const { setModalConfirm, getDeleteBrand } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.brand)

  const handleChangePage = (e: React.MouseEvent<HTMLButtonElement> | null, page: number) => {
    const param = {
      ...store.param,
      pageIndex: page + 1
    }
    dispatch(fetchDataGetListBrand(param))
  }

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param = {
      ...store.param,
      pageSize: Number(event.target.value)
    }
    dispatch(fetchDataGetListBrand(param))
  }

  const renderRow = (dataRow = []) => {
    const data = dataRow.map(item => ({ ...item, id: item.code }))

    return data
  }

  const handleDelete = (code: string, name: string) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xoá thương hiệu ' + `${name}`,
      action: () => { getDeleteBrand(code) }
    })
  }

  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission={permissonConfig.BRAND_EDIT} slug={`/category/brand/edit/${row.id}`} />
            <DeleteDataGrid permission={permissonConfig.BRAND_DELETE} handleDelete={() => handleDelete(row.code, row.name)} />
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'code',
      headerName: 'Mã thương hiệu',
      sortable: false,
      renderCell: ({ row }) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <TypographyDataGrid noWrap>
              {row.code}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'name',
      sortable: false,
      headerName: 'Tên thương hiệu',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid
              noWrap
            >
              {row.name}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'status',
      sortable: false,
      headerName: 'trạng thái',
      renderCell: ({ row }: any) => {
        return <CustomChip rounded skin='light' size='small' label={renderNameStatus(row.status)} color={row.status === "ACTIVE" ? "success" : "error"} />
      }
    }
  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={(renderRow(store.data)) || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: '10px'
          },
          '& .MuiDataGrid-footerContainer': {
            display: 'none'
          }
        }}
      />

      <TablePagination
        component='div'
        count={store.totalRecords}
        page={store.param.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  )
}
export default ListDataTable
