interface PropsDataTable {
  setModalConfirm: Function
  getDeleteBrand: Function
}

interface IParamValuesFilter {
  code: string;
  status: string
}
export type { IParamValuesFilter, PropsDataTable }
