// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import { InputAdornment, OutlinedInput } from '@mui/material'
import TextField from '@mui/material/TextField'

// ** React Imports
import { useRef, useState } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** Components Imports
import Icon from 'src/@core/components/icon'
import { SelectMUI } from 'src/views/component/theme'


// ** Utils Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'
import { OPTIONS_STATUS } from 'src/configs/functionConfig'

// ** Store Redux Imports
import { AppDispatch, RootState } from 'src/store'
import { useDispatch, useSelector } from 'react-redux'
import { fetchDataGetListBrand } from 'src/store/apps/category/brand'
import { IParamBrandList } from 'src/api/organization/type'

const OPTIONS_STATUS_FILTER = [
  {
    value: null,
    label: 'Tất cả'
  },
  ...OPTIONS_STATUS,
]

const TableHeader = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.brand)
  const filterRef = useRef<boolean>(false)

  const [valuesFilter, setValuesFilter] = useState<IParamBrandList>({
    pageIndex: 1,
    pageSize: 10,
    code: '',
    status: null,
  })

  const handleChangeInput = (value, field) => {
    const body = { ...valuesFilter }

    body[field] = value
    setValuesFilter(body)

  }

  const handleFilterBrand = () => {
    const initCode = store.param.code;
    const initStatus = store.param.status;

    if (valuesFilter.code !== initCode || valuesFilter.status !== initStatus) {
      dispatch(fetchDataGetListBrand(valuesFilter))
      filterRef.current = true
    }
  }

  const handleClearFilter = () => {
    const param = {
      code: '',
      pageIndex: 1,
      pageSize: 10,
      status: null
    }
    if (filterRef.current) {
      setValuesFilter(param)
      dispatch(fetchDataGetListBrand(param))
      filterRef.current = false
    }
  }

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: '25px'
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', minWidth: '500px', gap: 4 }}>
        <TextField
          size='small'
          value={valuesFilter.code}
          sx={{ width: '30%' }}
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            )
          }}
          placeholder='Tìm kiếm...'
          onChange={e => handleChangeInput(e.target.value, 'code')}
        />
        <Box>
          <SelectMUI
            label='Trạng thái'
            options={OPTIONS_STATUS_FILTER}
            shrink
            displayEmpty
            input={<OutlinedInput notched label="Trạng thái" />}
            size='small'
            value={valuesFilter.status}
            onChange={e => {
              handleChangeInput(e.target.value, 'status')
            }}
          />
        </Box>
        <Button variant='outlined' onClick={handleFilterBrand} sx={{ mr: 2 }}>
          Lọc
        </Button>
        <Button variant="outlined" onClick={handleClearFilter} color='error'>
          Xóa bộ lọc
        </Button>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          gap: '1rem',
          justifyContent: 'end',
          maxWidth: '100%'
        }}
      >
        {checkUserPermission(permissonConfig.BRAND_CREATE) && (
          <Link href='/category/brand/create/' as={'/category/brand/create/'}>
            <Button variant='contained'>+THÊM THƯƠNG HIỆU</Button>
          </Link>
        )}
      </Box>
    </Box>
  )
}

export default TableHeader
