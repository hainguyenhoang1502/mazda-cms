import styled from '@emotion/styled'
import FormControl from '@mui/material/FormControl'
import { IBodyDataCreate } from './interface'

const MuiFormControl = styled(FormControl)({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingBottom: '30px'
})

const INIT_VALUE_DATA_BODY_CREATE: IBodyDataCreate = {
  code: '',
  regionName: '',
  status: 'ACTIVE'
}

export { MuiFormControl, INIT_VALUE_DATA_BODY_CREATE }
