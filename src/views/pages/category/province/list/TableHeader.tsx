// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment } from '@mui/material'

// ** Next Imports
import Link from 'next/link'


// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Interface Services Imports

// ** Utils
import { checkUserPermission } from 'src/@core/utils/permission'
import { SelectMUI } from 'src/views/component/theme'
import { OPTIONS_STATUS } from 'src/configs/functionConfig'
import { RootState } from 'src/store'
import { useSelector } from 'react-redux'
import { fetchDataGetListProvince } from 'src/store/apps/category/province'
import useFilter from 'src/hooks/useFilter'

const TableHeader = () => {
  // ** Redux
  const store = useSelector((state: RootState) => state.province)

  const OPTIONS_STATUS_FILTER = [
    {
      value: null,
      label: 'Tất cả'
    },
    ...OPTIONS_STATUS,
  ]

  const {
    valuesFilter,
    handleChangeInput,
    handleFilter,
    handleClearFilter
  } = useFilter({ fetchDataGetList: fetchDataGetListProvince })

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: '25px'
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', minWidth: '500px', gap: 4 }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ width: '25%' }}
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            )
          }}
          placeholder='Tìm kiếm…'
          onChange={e => handleChangeInput(e.target.value, 'keyword')}
        />
        <Box>
          <SelectMUI
            label='Trạng thái'
            options={OPTIONS_STATUS_FILTER}
            size='small'
            value={valuesFilter.status}
            onChange={e => handleChangeInput(e.target.value, 'status')}
          />
        </Box>
        <Button variant='outlined' onClick={() => handleFilter(store.param.keyword, store.param.status)} sx={{ mr: 2 }}>
          Lọc
        </Button>
        <Button variant="outlined" onClick={handleClearFilter} color='error'>
          Xóa bộ lọc
        </Button>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          gap: '1rem',
          justifyContent: 'end',
          maxWidth: '100%'
        }}
      >
        {checkUserPermission('') && (
          <Link href='/category/province/create' as={'/category/province/create'}>
            <Button variant='contained'>+thêm tỉnh / thành</Button>
          </Link>
        )}
      </Box>
    </Box>
  )
}

export default TableHeader
