// ** MUI Imports
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'

// ** React Imports
import { useContext, useEffect } from 'react'

// ** Next Imports
import router from 'next/router'

// ** Third Party Import
import * as Yup from 'yup'
import { Formik, Form, Field } from 'formik'

// ** Components Imports
import ModalConfirm from 'src/views/component/modalConfirm'

import { SelectMUI, InputRequiredMUI } from 'src/views/component/theme'

// ** Interface Services Imports
import { INIT_VALUE_DATA_BODY_CREATE } from './service'
import { CreateProvince } from './interface'

// ** Context Imports

//** Store Imports
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'

// ** Api Imports

import { ERROR_NULL_VALUE_INPUT } from 'src/configs/initValueConfig'
import { OPTIONS_STATUS } from 'src/configs/functionConfig'
import { fetchDataGetListRegion } from 'src/store/apps/category/region'
import { fetchDataGetListArea } from 'src/store/apps/category/area'
import { EditProvinceContext } from 'src/pages/category/province/edit/[slug]'

const CreateProvinceForm = (props: CreateProvince) => {
  // ** Props
  const { handleCreate, modalConfirm, setModalConfirm, handleUpdate } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()

  const areaData = useSelector((state: RootState) => state.area)
  const regionData = useSelector((state: RootState) => state.region)

  // ** Hooks
  const context = useContext(EditProvinceContext)
  const isEdit = context && context.isEdit

  useEffect(() => {
    const param = {
      ...regionData.param,
      status: 'ACTIVE'
    }
    dispatch(fetchDataGetListRegion(param))

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  // ===== area ====
  useEffect(() => {
    const param = {
      ...areaData.param,
      status: 'ACTIVE'
    }
    dispatch(fetchDataGetListArea(param))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const AddProvinceSchema = Yup.object().shape({
    code: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    locationName: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    regionCode: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    areaCode: Yup.string().required(ERROR_NULL_VALUE_INPUT),
    status: Yup.mixed().nullable().required(ERROR_NULL_VALUE_INPUT)
  })

  const handleSubmit = async values => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Cập nhật thông tin tỉnh / thành' : 'Bạn đang thao tác Tỉnh / thành',
      action: () => {
        if (isEdit) {
          return handleUpdate(values)
        } else {
          return handleCreate(values)
        }
      }
    })
  }

  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? 'Bạn đang thao tác Huỷ cập nhật thông tin Người dùng' : 'Bạn đang thao tác Huỷ thêm Người dùng',
      action: () => router.push('/category/province')
    })
  }

  return (
    <Card>
      <Formik
        initialValues={(context && context.dataProvince) || INIT_VALUE_DATA_BODY_CREATE}
        onSubmit={values => handleSubmit(values)}
        validationSchema={AddProvinceSchema}
        validateOnBlur
      >
        {props => {
          return (
            <Form>
              <CardContent>
                <Typography noWrap sx={{ fontWeight: 500, padding: '20px 0', fontSize: '24px' }}>
                  Thông Tin Tỉnh / thành
                </Typography>

                <Grid item container spacing={6} marginBottom={5}>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='my-code'
                      label='Mã Tỉnh / Thành'
                      name='code'
                      component={InputRequiredMUI}
                      texterror={props.touched.code && props.errors.code}
                      texticon='*'
                    />
                  </Grid>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='my-locationName'
                      label='Tên tỉnh thành'
                      name='locationName'
                      component={InputRequiredMUI}
                      texterror={props.touched.locationName && props.errors.locationName}
                    />
                  </Grid>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='my-region'
                      label='Vùng miền'
                      component={SelectMUI}
                      texticon='*'
                      name='regionCode'
                      texterror={props.touched.regionCode && props.errors.regionCode}
                      options={regionData.arrFilter || []}
                      value={props.values.regionCode}
                      onChange={event => {
                        props.setFieldValue('regionCode', event.target.value)
                      }}
                    />
                  </Grid>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='my-area'
                      label='Khu vực'
                      component={SelectMUI}
                      texticon='*'
                      name='areaCode'
                      texterror={props.touched.areaCode && props.errors.areaCode}
                      options={areaData.arrFilter || []}
                      value={props.values.areaCode}
                      onChange={event => {
                        props.setFieldValue('areaCode', event.target.value)
                      }}
                    />
                  </Grid>
                  <Grid item lg={12} xs={12}>
                    <Field
                      id='status'
                      label='Trạng thái'
                      component={SelectMUI}
                      texticon='*'
                      name='status'
                      texterror={props.touched.status && props.errors.status}
                      options={OPTIONS_STATUS}
                      value={props.values.status}
                      onChange={event => {
                        props.setFieldValue('status', event.target.value)
                      }}
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <CardContent sx={{ display: 'flex', justifyContent: 'center', gap: '25px' }}>
                <Button variant='contained' type='submit'>
                  Lưu
                </Button>
                <Button color='error' variant='outlined' onClick={handleCancel}>
                  Hủy
                </Button>
              </CardContent>
            </Form>
          )
        }}
      </Formik>
      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Card>
  )
}
export default CreateProvinceForm
