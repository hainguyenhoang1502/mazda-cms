// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React
import { useState } from 'react'

// ** Components
import ModalNotify from 'src/views/component/modalNotify'

// ** Api

// ** Config
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Interface
import CreateProvinceForm from './CreateProvinceForm'
import { ApiPostCreateProvince, ApiPutUpdateProvince } from 'src/api/refData'
import { IParamProvinceCreate } from 'src/api/refData/type'
import router from 'next/router'
import { useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { useDispatch } from 'react-redux'
import { fetchDataGetListProvince, handleSetReloadDataProvince } from 'src/store/apps/category/province'

const CreateProvinceMain = () => {
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux
  const store = useSelector((state: RootState) => state.province)
  const dispatch = useDispatch<AppDispatch>()

  const handleCreate = async (param: IParamProvinceCreate) => {

    const res = await ApiPostCreateProvince(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thành Công',
        textBtn: 'Quay lại',
        actionReturn: () => router.push('/category/province')
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)

      dispatch(fetchDataGetListProvince(store.param))
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true,
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (data: IParamProvinceCreate, arrOrg: Array<string>) => {
    console.log({ data, arrOrg })

    const res = await ApiPutUpdateProvince(data)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thành Công'
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(handleSetReloadDataProvince())
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError:true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  return (
    <>
      <Grid item xs={12} p={0}>
        <CreateProvinceForm
          handleCreate={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdate={handleUpdate}
        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateProvinceMain
