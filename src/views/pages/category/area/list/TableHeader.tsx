// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import { InputAdornment } from '@mui/material'
import TextField from '@mui/material/TextField'

// ** React Imports

// ** Next Imports
import Link from 'next/link'

// ** Components Imports
import Icon from 'src/@core/components/icon'
import { SelectMUI } from 'src/views/component/theme'

// ** Interface Services Imports

// ** Utils Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'
import { OPTIONS_STATUS } from 'src/configs/functionConfig'

// ** Store Redux Imports
import { RootState } from 'src/store'
import { useSelector } from 'react-redux'
import { fetchDataGetListArea } from 'src/store/apps/category/area'
import useFilter from 'src/hooks/useFilter'

const OPTIONS_STATUS_FILTER = [
  {
    value: null,
    label: 'Tất cả'
  },
  ...OPTIONS_STATUS,
]

const TableHeader = () => {
  // ** Redux
  const store = useSelector((state: RootState) => state.region)
  const {
    valuesFilter,
    handleChangeInput,
    handleFilter,
    handleClearFilter
  } = useFilter({ fetchDataGetList: fetchDataGetListArea })

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: '25px'
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', minWidth: '500px', gap: 4 }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ width: '25%' }}
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            )
          }}
          placeholder='Tìm kiếm khu vực...'
          onChange={e => handleChangeInput(e.target.value, 'keyword')}
        />
        <Box>
          <SelectMUI
            label='Trạng thái'
            options={OPTIONS_STATUS_FILTER}
            value={valuesFilter.status}
            size='small'
            onChange={e => {
              handleChangeInput(e.target.value, 'status')
            }}
          />
        </Box>
        <Button variant='outlined' onClick={() => handleFilter(store.param.keyword, store.param.status)} sx={{ mr: 2 }}>
          Lọc
        </Button>
        <Button variant="outlined" onClick={handleClearFilter} color='error'>
          Xóa bộ lọc
        </Button>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          gap: '1rem',
          justifyContent: 'end',
          maxWidth: '100%'
        }}
      >
        {checkUserPermission(permissonConfig.LOCATION_CREATE) && (
          <Link href='/category/area/create' as={'/category/area/create/'}>
            <Button variant='contained'>+THÊM KHU VỰC</Button>
          </Link>
        )}
      </Box>
    </Box>
  )
}

export default TableHeader
