
interface RowDataDetail {
  id: number
}

interface IParamValuesFilter {
  keyword: string
  status: string
}
export type {  RowDataDetail, IParamValuesFilter }
