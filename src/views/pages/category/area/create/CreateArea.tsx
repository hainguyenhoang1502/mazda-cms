// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React
import { useState } from 'react'
import router from 'next/router'

// ** Components
import ModalNotify from 'src/views/component/modalNotify'

// ** Api

// ** Config
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'

// ** Interface
import CreateAreaForm from './CreateAreaForm'
import { useDispatch } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { ApiPostCreateArea, ApiPostUpdateArea } from 'src/api/refData'
import { IParamAreaCreate } from 'src/api/refData/type'
import { handleSetReloadDataRegion } from 'src/store/apps/category/region'
import { fetchDataGetListArea } from 'src/store/apps/category/area'
import { useSelector } from 'react-redux'

const CreateArea = () => {
  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.area)

  const handleCreate = async (param: IParamAreaCreate) => {
    const res = await ApiPostCreateArea(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thành Công',
        textBtn: 'Quay lại',
        actionReturn: () => router.push('/category/area/')
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(fetchDataGetListArea(store.param))
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Thêm Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (param: IParamAreaCreate) => {
    const res = await ApiPostUpdateArea(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thành Công'
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
      dispatch(handleSetReloadDataRegion())
    } else {
      setModalNotify({
        isOpen: true,
        title: 'Cập nhật thông tin Thất Bại',
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  return (
    <>
      <Grid item xs={12} p={0}>
        <CreateAreaForm
          handleCreate={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdate={handleUpdate}
        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateArea
