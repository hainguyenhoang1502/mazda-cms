import { IResponeListDealer } from "src/api/organization/type"

interface RowDataDetail {
  row: IResponeListDealer["data"]["data"]["0"]
}

interface IParamValuesFilter {
  keyword: string
  status: string
}

interface ListDataTableProps {
  setModalConfirm: Function
  getDelete: Function
}

export type { RowDataDetail, IParamValuesFilter, ListDataTableProps }
