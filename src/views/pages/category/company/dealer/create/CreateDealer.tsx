
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Import 
import { useState } from 'react'

// ** Next Import
import router from 'next/router'

// ** Component Imports
import CreateDealerForm from './CreateDealerForm'
import ModalNotify from 'src/views/component/modalNotify'

// ** Api  
import { ApiCreateDealerCms, ApiUpdateDealerCms } from 'src/api/organization'

// ** Config 
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'

// ** Store Redux Imports
import { AppDispatch } from 'src/store'
import { useDispatch } from 'react-redux'
import { handleSetReloadDataRole } from 'src/store/apps/role'

// ** Type Interface Import
import { IParamCreateDealer } from 'src/api/organization/type'

const CreateMain = () => {

  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  const dispatch = useDispatch<AppDispatch>()


  const handleCreate = async (data: IParamCreateDealer) => {
    const res = await ApiCreateDealerCms(data)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thành Công",
        textBtn: 'Quay lại',
        actionReturn: () => router.push("/category/company/dealer/")
      })
      dispatch(handleSetReloadDataRole())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (data: IParamCreateDealer) => {

    const res = await ApiUpdateDealerCms(data)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thành Công",
      })
      dispatch(handleSetReloadDataRole())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }


  return (
    <>
      <Grid item xs={12} p={0}>
        <CreateDealerForm
          handleCreate={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdate={handleUpdate}

        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateMain