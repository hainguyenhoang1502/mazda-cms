
// ** MUI Imports
import Box from "@mui/system/Box";
import Card from "@mui/material/Card"
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button"
import Checkbox from "@mui/material/Checkbox";
import Typography from "@mui/material/Typography"
import CardContent from "@mui/material/CardContent"
import OutlinedInput from "@mui/material/OutlinedInput";
import FormHelperText from "@mui/material/FormHelperText";
import FormControlLabel from "@mui/material/FormControlLabel";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";

// ** React Imports
import { useContext, useEffect, useState } from "react"

// ** Next Imports
import router from "next/router"

// ** Third Party Import
import { toast } from "react-hot-toast";
import { Formik, Form, Field, FormikProps } from 'formik';

// ** Components Imports
import ModalConfirm from "src/views/component/modalConfirm"
import { CustomMultiBox } from "src/views/component/theme/customMultiBox";

import { InputRequiredMUI, MuiFormControlTextField, SelectMUI } from "src/views/component/theme";

// ** Interface Services Imports 
import { DealerSchema, INIT_VALUE_DATA_BODY_CREATE } from "./service"

// ** Context Imports
import { EditDealerContext } from "src/pages/category/company/dealer/edit/[slug]";

//** Store Imports
import { AppDispatch, RootState } from "src/store";
import { useDispatch, useSelector } from "react-redux";
import { fetchDataGetListBrand } from "src/store/apps/category/brand";
import { fetchDataGetListFilterAllProvince } from "src/store/apps/category/province";
import { fetchDataGetListBussinessType } from "src/store/apps/category/organization";
import { fetchDataGetListCompany } from "src/store/apps/category/company-dealer/company";

// ** Config Imports
import { OPTIONS_STATUS, renderUrlImageUpload } from "src/configs/functionConfig";
import { IOptionsFilter, ModalConfirmState } from "src/configs/typeOption";
import { ERROR_EMAIL_VALUE_INPUT, ERROR_VALUE_INPUT_IMAGE, MAX_SIZE_IMAGE, regexEmail } from "src/configs/initValueConfig";

// ** Api Imports
import { ApiGetLocationDistrictByProvinceId, ApiGetLocationWardByDistrictId, ApiUploadFileImage } from "src/api/refData";
import { IParamCreateDealer, IResponeDetailDealer } from "src/api/organization/type";
import Table from "@mui/material/Table";
import styled from "@emotion/styled";
import { TableHead, TextField } from "@mui/material";
import IconifyIcon from "src/@core/components/icon";


// ** Interface Type
type CreateDealerFormProps = {
  handleCreate: Function
  setModalConfirm: Function,
  modalConfirm: ModalConfirmState
  handleUpdate: Function
}

type ValueJsonPhone = {
  key: string
  value: string,
  childId?: string
}

const TableCellCustom = styled(TableCell)({
  border: '1px solid',
  color: "unset",
  textAlign: "center",
  padding: "6px 10px !important",
  '&.MuiTableCell-body': {
    color: "unset"
  },
})

const initValuePhone: ValueJsonPhone = {
  key: "",
  value: "",
  childId: "child-1"
}
const CreateDealerForm = (props: CreateDealerFormProps) => {

  // ** Hooks
  const context = useContext(EditDealerContext);
  const isEdit = context && context.isEdit;

  // ** Redux Store
  const dispatch = useDispatch<AppDispatch>()
  const storeProvince = useSelector((state: RootState) => state.province)
  const storeOrganization = useSelector((state: RootState) => state.organization)
  const storeBrand = useSelector((state: RootState) => state.brand)
  const storeCompany = useSelector((state: RootState) => state.company)

  // ** Props
  const { handleCreate, modalConfirm, setModalConfirm, handleUpdate } = props

  // ** States
  const [listDistrict, setListDistrict] = useState<Array<IOptionsFilter>>([])
  const [listWard, setListWard] = useState<Array<IOptionsFilter>>([])
  const [listBussinessType, setListBussinessType] = useState<Array<string>>([])
  const [listBrand, setListBrand] = useState<Array<string>>([])
  const [listEmail, setListEmail] = useState<Array<string>>([])
  const [valueEmail, setValueEmail] = useState<string>("")
  const [errorValueEmail, setErrorValueEmail] = useState<boolean>(false)
  const [listPhone, setListPhone] = useState<Array<ValueJsonPhone>>([initValuePhone])

  useEffect(() => {

    dispatch(fetchDataGetListFilterAllProvince())

    if (storeOrganization && (!storeOrganization.dataBussinessType || storeOrganization.dataBussinessType.length === 0)) {
      dispatch(fetchDataGetListBussinessType())
    }

    const param = {
      pageIndex: 1,
      pageSize: 9999,
      code: "",
      status: "ACTIVE",
    }
    if (JSON.stringify(storeBrand.param) !== JSON.stringify(param)) {
      dispatch(fetchDataGetListBrand(param))
    }

    const paramCompany = {
      pageIndex: 1,
      pageSize: 9999,
      keyword: "",
      status: "ACTIVE",
    }
    if (JSON.stringify(storeCompany.param) !== JSON.stringify(paramCompany)) {
      dispatch(fetchDataGetListCompany(paramCompany))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  useEffect(() => {
    if (context && context.data) {
      setListEmail(context.data.emailReceiveNotify)
      setListBrand(context.data.brandCodes)
      setListBussinessType(context.data.businessTypeCodes)
      getListLocationDistricts(context.data.provinceCode)
      getListLocationWards(context.data.districtCode)
      setListPhone(context.data.jsonPhone)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context && context.data])


  // ** Fetch data District
  const getListLocationDistricts = async (id: number | string) => {
    const res = await ApiGetLocationDistrictByProvinceId(id)
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      const arr = res.data.map(item => ({
        label: item.locationName,
        value: item.code
      }));
      setListDistrict(arr)
    } else {
      setListDistrict([])
    }
  }

  // ** Fetch data Ward
  const getListLocationWards = async (id: number | string) => {
    const res = await ApiGetLocationWardByDistrictId(id)
    if (res.code === 200 && res.result && res.data && res.data.length > 0) {
      const arr = res.data.map(item => ({
        label: item.locationName,
        value: item.code
      }));
      setListWard(arr)
    } else {
      setListWard([])
    }
  }

  const onChangeSelectProvince = (event) => {
    getListLocationDistricts(event)

  }

  const onChangeSelectDistricts = (event) => {
    getListLocationWards(event)
  }

  // ** handle check Bussiness Type
  const handleCheckded = (code: string) => {
    const arr = [...listBussinessType]
    const indexChecked = arr.findIndex(it => it === code)
    if (indexChecked === -1) {
      arr.push(code)
    } else {
      arr.splice(indexChecked, 1)
    }
    setListBussinessType(arr)
  }

  // ** handle check Brand
  const handleCheckdedBrand = (code: string) => {
    const arr = [...listBrand]
    const indexChecked = arr.findIndex(it => it === code)
    if (indexChecked === -1) {
      arr.push(code)
    } else {
      arr.splice(indexChecked, 1)
    }
    setListBrand(arr)
  }

  // **  Input Email
  const handleInputEmail = (event) => {
    if (event.key === 'Enter') {
      if (regexEmail.test(valueEmail)) {
        const arr = [...listEmail]
        arr.push(valueEmail)
        setValueEmail("")
        setListEmail(arr)
        setErrorValueEmail(false)
      } else {
        setErrorValueEmail(true)
      }

      return event.preventDefault()
    }
  }

  const handleDelte = (id: number) => {
    const arr = [...listEmail]
    arr.splice(id, 1)
    setListEmail(arr)
  }


  // ** Handle Upload File
  const handleFileSelected = async (e: React.ChangeEvent<HTMLInputElement>, props: FormikProps<IParamCreateDealer>) => {
    const files = Array.from(e.target.files)
    if (!files[0]) return
    if (files[0]?.size > MAX_SIZE_IMAGE) {
      toast.error(ERROR_VALUE_INPUT_IMAGE)

      return
    } else {
      const reader = new FileReader();
      reader.onloadend = () => {
        props.setFieldValue("urlImg", reader.result)
      }
      reader.readAsDataURL(files[0]);
      const data = new FormData();
      for (let i = 0; i < files.length; i++) {
        data.append('avatar', files[i]);
      }
      const res = await ApiUploadFileImage(data)
      if (res.code === 200 && res.result) {
        props.setFieldValue("image", res.data.filePath)
      } else {
        toast.error("Đăng tải ảnh thất bại. Vui lòng thử lại")
      }
    }
  }

  const handleAddPhone = () => {
    const arr = [...listPhone]
    const value = {
      key: "",
      value: "",
      childId: 'children-' + new Date().toISOString()
    }
    arr.push(value)
    setListPhone(arr)
  }

  const handleRemove = (id: string) => {
    const arr = [...listPhone]
    const newArr = arr.filter(item => item.childId !== id)
    setListPhone(newArr)
  }

  const handleChangeInput = (field: string, value: string, id: string) => {
    const arr = [...listPhone]
    const index = arr.findIndex(it => it.childId === id)
    if (index > -1) {
      arr[index][field] = value
    }
    setListPhone(arr)
  }

  const renderDataForm = (data: IResponeDetailDealer["data"]) => {
    if (!data) return INIT_VALUE_DATA_BODY_CREATE

    return {
      ...data,
      urlImg: data.image,
    }
  }

  const handleSubmit = async (values) => {
    if (listBussinessType.length === 0) {
      toast.error("Bạn chưa chọn Loại hình kinh doanh")
    } else {
      if (listBrand.length === 0) {
        toast.error("Bạn chưa chọn Thông tin thương hiệu")
      } else {
        let validPhone = true
        const arrPhone = []
        listPhone.forEach(item => {
          if (!item.key || !item.value) {
            validPhone = false
          } else {
            arrPhone.push({
              key: item.key,
              value: item.value
            })
          }
        })

        if (!validPhone) {
          toast.error("Vui lòng nhập đầy đủ thông tin số điện thoại")
        } else {
          const param = {
            ...values,
            image: renderUrlImageUpload(values.image),
            businessTypeCodes: listBussinessType,
            brandCodes: listBrand,
            emailReceiveNotify: listEmail,
            jsonPhone: arrPhone,
            urlImg: ""
          }
          setModalConfirm({
            isOpen: true,
            title: isEdit ? "Bạn đang thao tác Cập nhật thông tin điểm bán hàng" : "Bạn đang thao tác Thêm điểm bán hàng",
            action: () => {
              if (isEdit) {
                return handleUpdate(param)
              } else {
                return handleCreate(param)
              }
            }
          })
        }
      }
    }
  }

  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? "Bạn đang thao tác Huỷ cập nhật thông tin điểm bán hàng" : "Bạn đang thao tác Huỷ thêm điểm bán hàng",
      action: () => router.push("/permission")
    })
  }

  return (
    <Grid>
      <Formik
        initialValues={isEdit ? renderDataForm(context.data) : INIT_VALUE_DATA_BODY_CREATE}
        onSubmit={values => handleSubmit(values)}
        validationSchema={DealerSchema}
        validateOnBlur
      >
        {(props) => {

          return (
            <Form>
              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px" }}>
                      Thông tin điểm bán hàng
                    </Typography>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={6}>
                        <Field
                          id="my-companyCode"
                          label="Tên pháp nhân"
                          name="companyCode"
                          component={SelectMUI}
                          texterror={props.touched.companyCode && props.errors.companyCode}
                          texticon="*"
                          options={storeCompany.arrFilter}
                          value={props.values.companyCode}
                          onChange={(event) => {
                            props.setFieldValue("companyCode", event.target.value);
                          }}
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Field
                          id="my-name"
                          label="Tên điểm bán hàng"
                          name="name"
                          component={InputRequiredMUI}
                          texterror={props.touched.name && props.errors.name}
                          texticon="*"
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={12}>
                        <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "18px" }}>
                          Loại hình kinh doanh
                        </Typography>
                        {
                          storeOrganization.dataBussinessType && storeOrganization.dataBussinessType.length > 0 && storeOrganization.dataBussinessType.map((item, index) => (
                            <FormControlLabel
                              label={item.name}
                              sx={{ my: -1, whiteSpace: 'nowrap', }}
                              key={index}
                              control={
                                <Checkbox
                                  id={`${index}-read`}
                                  checked={listBussinessType.includes(item.code)}
                                  onChange={() => handleCheckded(item.code)}
                                />
                              }
                            />
                          ))
                        }

                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={12}>
                        <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "18px" }}>
                          Thông tin thương hiệu
                        </Typography>
                        {
                          storeBrand.data && storeBrand.data.length > 0 && storeBrand.data.map((item, index) => (
                            <FormControlLabel
                              label={item.name}
                              sx={{ my: -1, whiteSpace: 'nowrap', }}
                              key={index}
                              control={
                                <Checkbox
                                  id={`${index}-read`}
                                  checked={listBrand.includes(item.code)}
                                  onChange={() => handleCheckdedBrand(item.code)}
                                />
                              }
                            />
                          ))
                        }

                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>

              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px" }}>
                      Thông Tin Địa chỉ
                    </Typography>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="provinceCode"
                          label="Tỉnh/Thành phố"
                          component={SelectMUI}
                          texticon="*"
                          name="provinceCode"
                          texterror={props.touched.provinceCode && props.errors.provinceCode}
                          options={storeProvince.arrFilter}
                          value={props.values.provinceCode}
                          onChange={(event) => {
                            props.setFieldValue("provinceCode", event.target.value);
                            props.setFieldValue("districtCode", "");
                            props.setFieldValue("wardCode", "");
                            onChangeSelectProvince(event.target.value)
                          }}
                        />
                      </Grid>

                      <Grid item lg={6} xs={12}>
                        <Field
                          id="districtCode"
                          label="Quận/huyện"
                          component={SelectMUI}
                          texticon="*"
                          name="districtCode"
                          texterror={props.touched.districtCode && props.errors.districtCode}
                          options={listDistrict}
                          value={props.values.districtCode}
                          onChange={(event) => {

                            props.setFieldValue("districtCode", event.target.value);
                            props.setFieldValue("wardCode", "");
                            onChangeSelectDistricts(event.target.value)
                          }}
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="wardCode"
                          label="Phường/xã"
                          component={SelectMUI}
                          texticon="*"
                          name="wardCode"
                          texterror={props.touched.wardCode && props.errors.wardCode}
                          options={listWard}
                          value={props.values.wardCode}
                          onChange={(event) => {
                            props.setFieldValue("wardCode", event.target.value);
                          }}
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="input-address"
                          label="Địa chỉ"
                          name="address"
                          component={InputRequiredMUI}
                          texterror={props.touched.address && props.errors.address}
                          texticon="*"
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-latitude"
                          label="LAT"
                          name="latitude"
                          component={InputRequiredMUI}
                          texterror={props.touched.latitude && props.errors.latitude}
                          texticon="*"
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-longitude"
                          label="LONG"
                          name="longitude"
                          component={InputRequiredMUI}
                          texterror={props.touched.longitude && props.errors.longitude}
                          texticon="*"
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-mapURL"
                          label="Google Map "
                          name="mapURL"
                          component={InputRequiredMUI}

                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-status"
                          label="Trạng thái"
                          name="status"
                          component={SelectMUI}
                          texterror={props.touched.status && props.errors.status}
                          texticon="*"
                          options={OPTIONS_STATUS}
                          value={props.values.status}
                          onChange={(event) => {
                            props.setFieldValue("status", event.target.value);
                          }}
                        />
                      </Grid>
                    </Grid>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-website"
                          label="Website"
                          name="website"
                          component={InputRequiredMUI}
                          texterror={props.touched.website && props.errors.website}
                        />
                      </Grid>
                      <Grid item lg={6} xs={12}>
                        <Box
                          sx={{
                            border: "1px dashed #9A9AB0",
                            borderRadius: "5px",
                            height: '100%',
                            justifyContent: "center",
                            alignItems: 'center',
                            display: "flex"
                          }}
                        >
                          <Button aria-label="upload picture" component="label" sx={{ p: 0 }}>
                            {
                              props.values.urlImg ?
                                <img src={props.values.urlImg.toString()} alt={props.values.urlImg.toString()} width="100%" height="50px" />
                                : "Upload hình ảnh"
                            }
                            <input hidden accept="image/gif, image/jpg, image/jpeg, image/png" type="file" onChange={(e) => handleFileSelected(e, props)} />
                          </Button>
                        </Box>
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>

              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 400, fontSize: "18px", paddingBottom: "10px" }}>
                      Số điện thoại
                    </Typography>
                    <TableContainer sx={{ paddingBottom: "20px" }}>
                      <Table size='small'>
                        <TableHead>
                          <TableRow sx={{ '& .MuiTableCell-root:first-of-type': { pl: '0 !important', } }}>
                            <TableCellCustom>
                              STT
                            </TableCellCustom>
                            <TableCellCustom>
                              Tên số điện thoại
                            </TableCellCustom>
                            <TableCellCustom>
                              Số điện thoại
                            </TableCellCustom>
                            <TableCellCustom>
                            </TableCellCustom>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {
                            listPhone && listPhone.length > 0 && listPhone.map((item, index) => (
                              <TableRow sx={{ '& .MuiTableCell-root:first-of-type': { pl: '0 !important', } }} key={index}>
                                <TableCellCustom>
                                  {index + 1}
                                </TableCellCustom>
                                <TableCellCustom>
                                  <TextField
                                    fullWidth
                                    value={item.key}
                                    onChange={(event) => handleChangeInput("key", event.target.value, item.childId)}
                                  />
                                </TableCellCustom>
                                <TableCellCustom>
                                  <TextField
                                    fullWidth
                                    value={item.value}
                                    onChange={(event) => handleChangeInput("value", event.target.value, item.childId)}
                                  />
                                </TableCellCustom>
                                <TableCellCustom>
                                  <IconifyIcon icon="teenyicons:minus-circle-outline" onClick={() => handleRemove(item.childId)} fontSize="18px" cursor="pointer" />
                                </TableCellCustom>
                              </TableRow>
                            ))
                          }
                        </TableBody>
                      </Table>
                    </TableContainer>
                    <Box sx={{ textAlign: "end" }}>
                      <Button variant="contained" onClick={handleAddPhone}>Thêm mới</Button>
                    </Box>

                  </CardContent>
                </Card>
              </Grid>

              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 400, fontSize: "18px", paddingBottom: "10px" }}>
                      Email nhận thông báo
                    </Typography>
                    {
                      <CustomMultiBox list={listEmail} handelDelte={handleDelte} />

                    }
                  </CardContent>
                  <CardContent>
                    <MuiFormControlTextField fullWidth>
                      <OutlinedInput
                        placeholder="Nhập Email..."
                        onKeyDown={handleInputEmail}
                        value={valueEmail}
                        onChange={(e) => setValueEmail(e.target.value)}
                      />
                      {
                        errorValueEmail &&
                        <Box sx={{ display: "flex", color: "red", alignItems: "center" }}>
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2a1 1 0 0 0 0-2z" /></svg>
                          <FormHelperText sx={{ color: "#f4071e", marginLeft: "3px", fontSize: '0.8rem' }} id="component-error-text">{ERROR_EMAIL_VALUE_INPUT}</FormHelperText>
                        </Box >
                      }
                    </MuiFormControlTextField>
                  </CardContent>
                </Card>
              </Grid>

              <Grid item marginBottom={5}>
                <Card>
                  <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
                    <Button variant="contained" type="submit">Lưu</Button>
                    <Button color="error" variant="outlined" onClick={handleCancel} >Hủy</Button>
                  </CardContent>
                </Card>
              </Grid>
            </Form>
          )
        }
        }
      </Formik>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Grid>

  )
}
export default CreateDealerForm  