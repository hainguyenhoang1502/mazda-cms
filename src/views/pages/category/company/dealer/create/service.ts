import { IParamCreateDealer } from 'src/api/organization/type'
import { ERROR_NULL_VALUE_INPUT, ERROR_WEBSITE_VALUE_INPUT, regexWebsite } from 'src/configs/initValueConfig';
import * as Yup from 'yup';


export const INIT_VALUE_DATA_BODY_CREATE: IParamCreateDealer = {
  code: "1",
  name: "",
  provinceCode: "",
  districtCode: "",
  wardCode: "",
  address: "",
  longitude: null,
  latitude: null,
  mapURL: "",
  status: "ACTIVE",
  emailReceiveNotify: [],
  companyCode: "",
  brandCodes: [],
  businessTypeCodes: [],
  website: "",
  image: "",
  urlImg: "",
}

export const DealerSchema = Yup.object().shape({
  name: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
  provinceCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
  districtCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
  wardCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
  address: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
  longitude: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
  latitude: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
  status: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
  companyCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
  website: Yup.string().nullable().matches(regexWebsite, ERROR_WEBSITE_VALUE_INPUT),
});