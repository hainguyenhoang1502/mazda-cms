import { IParamCreateCompanyAgency } from 'src/api/organization/type'
import { ERROR_NULL_VALUE_INPUT } from 'src/configs/initValueConfig';
import * as Yup from 'yup';

export const INIT_VALUE_DATA_BODY_CREATE: IParamCreateCompanyAgency = {
  code: "",
  name: "",
  provinceCode: "1",
  districtCode: "1",
  wardCode: "1",
  address: "1",
  longitude: 0,
  latitude: 0,
  mapURL: "",
  status: "ACTIVE",
  emailReceiveNotify: [],
  legalEntityName: "",
  companyType: 'COMPANY',
  regionCode: "",
  areaCode: ""
}

export const ComapnySchema = Yup.object().shape({
  name: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
  code: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
  legalEntityName: Yup.string().required(ERROR_NULL_VALUE_INPUT).nullable(),
  companyType: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
  regionCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
  status: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
  areaCode: Yup.mixed().required(ERROR_NULL_VALUE_INPUT).nullable(),
});


export const OPTION_COMPANY_TYPE = [
  {
    value: "COMPANY",
    label: "Công ty trực thuộc"
  },
  {
    value: "AGENCY",
    label: "Đại lý"
  }
]