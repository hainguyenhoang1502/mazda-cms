
// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** React Import
import { useState } from 'react'

// ** Next Import
import router from 'next/router'

// ** Components Imports
import ModalNotify from 'src/views/component/modalNotify'
import CreatePermissionRight from './CreateComanyAgencyForm'

// ** Api Imports
import { ApiCreateCompanyCms, ApiUpdateCompanyCms } from 'src/api/organization'
import { IParamCreateCompanyAgency } from 'src/api/organization/type'

// ** Config Imports
import { ModalConfirmState, ModalNotifyState } from 'src/configs/typeOption'
import { INIT_STATE_MODAL_CONFIRM, INIT_STATE_MODAL_NOTIFY, UNKNOW_ERROR } from 'src/configs/initValueConfig'

// ** Store Imports
import { AppDispatch } from 'src/store'
import { useDispatch } from 'react-redux'
import { handleSetReloadDataCompany } from 'src/store/apps/category/company-dealer/company'


const CreateMain = () => {

  const [modalNotify, setModalNotify] = useState<ModalNotifyState>(INIT_STATE_MODAL_NOTIFY)
  const [modalConfirm, setModalConfirm] = useState<ModalConfirmState>(INIT_STATE_MODAL_CONFIRM)

  const dispatch = useDispatch<AppDispatch>()


  const handleCreate = async (data: IParamCreateCompanyAgency, listEmail: Array<string>) => {
    const param = {
      ...data,
      emailReceiveNotify: listEmail
    }
    const res = await ApiCreateCompanyCms(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thành Công",
        textBtn: 'Quay lại',
        actionReturn: () => router.push("/category/company/")
      })

      dispatch(handleSetReloadDataCompany())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Thêm Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  const handleUpdate = async (data: IParamCreateCompanyAgency, listEmail: Array<string>) => {
    const param = {
      ...data,
      emailReceiveNotify: listEmail
    }
    const res = await ApiUpdateCompanyCms(param)
    if (res.code === 200 && res.result) {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thành Công",
      })

      dispatch(handleSetReloadDataCompany())
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    } else {
      setModalNotify({
        isOpen: true,
        title: "Cập nhật thông tin Thất Bại",
        message: res.message || UNKNOW_ERROR,
        isError: true
      })
      setModalConfirm(INIT_STATE_MODAL_CONFIRM)
    }
  }

  return (
    <>
      <Grid item xs={12} p={0}>
        <CreatePermissionRight
          handleCreate={handleCreate}
          setModalConfirm={setModalConfirm}
          modalConfirm={modalConfirm}
          handleUpdate={handleUpdate}

        />
      </Grid>
      <ModalNotify {...modalNotify} toggle={setModalNotify} />
    </>
  )
}
export default CreateMain