
// ** MUI Imports
import Card from "@mui/material/Card"
import CardContent from "@mui/material/CardContent"
import Typography from "@mui/material/Typography"
import Button from "@mui/material/Button"
import Grid from "@mui/material/Grid";
import { FormHelperText, OutlinedInput } from "@mui/material";
import Box from "@mui/system/Box";

// ** React Imports
import { useContext, useEffect, useState } from "react"

// ** Next Imports
import router from "next/router"

// ** Third Party Import
import { Formik, Form, Field } from 'formik';

// ** Components Imports
import ModalConfirm from "src/views/component/modalConfirm"
import { CustomMultiBox } from "src/views/component/theme/customMultiBox";
import { InputRequiredMUI, MuiFormControlTextField, RadioMUI, SelectMUI } from "src/views/component/theme";

// ** Interface Services Imports 
import { ComapnySchema, INIT_VALUE_DATA_BODY_CREATE, OPTION_COMPANY_TYPE } from "./service"

// ** Context Imports
import { EditCompanyContext } from "src/pages/category/company/edit/[slug]";

//** Store Imports
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "src/store";
import { fetchDataGetListRegion } from "src/store/apps/category/region";
import { fetchDataGetListArea } from "src/store/apps/category/area";

// **  Api Imports
import { IParamLocationRegion } from "src/api/refData/type";

// ** Config Imports
import { ERROR_EMAIL_VALUE_INPUT, regexEmail } from "src/configs/initValueConfig";
import { ModalConfirmState } from "src/configs/typeOption";
import { OPTIONS_STATUS } from "src/configs/functionConfig";



// ** Interface Typee
interface CreateComanyAgencyFormProps {
  handleCreate: Function
  setModalConfirm: Function,
  modalConfirm: ModalConfirmState
  handleUpdate: Function
}

const CreateComanyAgencyForm = (props: CreateComanyAgencyFormProps) => {

  // ** Hooks
  const context = useContext(EditCompanyContext);
  const isEdit = context && context.isEdit;

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const storeRegion = useSelector((state: RootState) => state.region)
  const storeArea = useSelector((state: RootState) => state.area)

  // ** Props
  const { handleCreate, modalConfirm, setModalConfirm, handleUpdate } = props

  // ** States
  const [listEmail, setListEmail] = useState<Array<string>>([])
  const [valueEmail, setValueEmail] = useState<string>("")
  const [errorValueEmail, setErrorValueEmail] = useState<boolean>(false)


  useEffect(() => {
    const param: IParamLocationRegion = {
      keyword: "",
      pageIndex: 1,
      pageSize: 9999,
      status: "ACTIVE"
    }
    if (storeRegion && (!storeRegion.data || storeRegion.data.length === 0 || storeRegion.isNeedReloadData)) {
      dispatch(fetchDataGetListRegion(param))
    }
    if (storeArea && (!storeArea.data || storeArea.data.length === 0 || storeArea.isNeedReloadData)) {
      dispatch(fetchDataGetListArea(storeArea.param))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (context && context.data) {
      setListEmail(context.data.emailReceiveNotify)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context && context.data])

  const handleInputEmail = (event) => {
    if (event.key === 'Enter') {
      if (regexEmail.test(valueEmail)) {
        const arr = [...listEmail]
        arr.push(valueEmail)
        setValueEmail("")
        setListEmail(arr)
        setErrorValueEmail(false)
      } else {
        setErrorValueEmail(true)
      }
      
      return event.preventDefault()
    }
  }

  const handleDelte = (id: number) => {
    const arr = [...listEmail]
    arr.splice(id, 1)
    setListEmail(arr)
  }

  const handleSubmit = async (values) => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? "Bạn đang thao tác Cập nhật thông tin công ty" : "Bạn đang thao tác Thêm công ty",
      action: () => {
        if (isEdit) {
          return handleUpdate(values, listEmail)
        } else {
          return handleCreate(values, listEmail)
        }
      }
    })
  }

  const handleCancel = () => {
    setModalConfirm({
      isOpen: true,
      title: isEdit ? "Bạn đang thao tác Huỷ cập nhật thông tin công ty" : "Bạn đang thao tác Huỷ thêm công ty",
      action: () => router.push("/category/company/")
    })
  }



  return (
    <Grid>
      <Formik
        initialValues={isEdit ? context.data : INIT_VALUE_DATA_BODY_CREATE}
        onSubmit={values => handleSubmit(values)}
        validationSchema={ComapnySchema}
        validateOnBlur
      >
        {(props) => {

          return (
            <Form>
              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px" }}>
                      Thông tin công ty / đại lý
                    </Typography>

                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={6}>
                        <Field
                          id="my-code"
                          label="Mã đơn vị"
                          name="code"
                          component={InputRequiredMUI}
                          texterror={props.touched.code && props.errors.code}
                          texticon="*"
                          disabled={isEdit}
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Field
                          id="my-legalEntityName"
                          label="Tên pháp nhân"
                          name="legalEntityName"
                          component={InputRequiredMUI}
                          texterror={props.touched.legalEntityName && props.errors.legalEntityName}
                          texticon="*"
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={6}>
                        <Field
                          id="my-name"
                          label="Tên công ty / đại lý"
                          name="name"
                          component={InputRequiredMUI}
                          texterror={props.touched.name && props.errors.name}
                          texticon="*"
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Field
                          id="my-companyType"
                          name="companyType"
                          component={RadioMUI}
                          texterror={props.touched.companyType && props.errors.companyType}
                          value={props.values.companyType}
                          onChange={(event) => props.setFieldValue("companyType", event.target.value)}
                          options={OPTION_COMPANY_TYPE}
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 500, padding: "20px 0", fontSize: "24px" }}>
                      Thông Tin Địa chỉ
                    </Typography>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item xs={6}>
                        <Field
                          id="my-regionCode"
                          label="Vùng miền"
                          name="regionCode"
                          component={SelectMUI}
                          texterror={props.touched.regionCode && props.errors.regionCode}
                          texticon="*"
                          value={props.values.regionCode}
                          onChange={(event) => props.setFieldValue("regionCode", event.target.value)}
                          options={storeRegion.arrFilter}
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Field
                          id="my-areaCode"
                          label="Khu vực"
                          name="areaCode"
                          component={SelectMUI}
                          texterror={props.touched.areaCode && props.errors.areaCode}
                          texticon="*"
                          value={props.values.areaCode}
                          onChange={(event) => props.setFieldValue("areaCode", event.target.value)}
                          options={storeArea.arrFilter}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container spacing={6} marginBottom={5}>
                      <Grid item lg={6} xs={12}>
                        <Field
                          id="my-status"
                          label="Trạng thái"
                          name="status"
                          component={SelectMUI}
                          texterror={props.touched.status && props.errors.status}
                          texticon="*"
                          options={OPTIONS_STATUS}
                          value={props.values.status}
                          onChange={(event) => {
                            props.setFieldValue("status", event.target.value);
                          }}
                        />
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item marginBottom={5}>
                <Card>
                  <CardContent>
                    <Typography noWrap sx={{ fontWeight: 400, fontSize: "18px", paddingBottom: "10px" }}>
                      Email nhận thông báo
                    </Typography>
                    {
                      <CustomMultiBox list={listEmail} handelDelte={handleDelte} />

                    }

                  </CardContent>
                  <CardContent>
                    <MuiFormControlTextField fullWidth>
                      <OutlinedInput
                        placeholder="Nhập Email..."
                        onKeyDown={handleInputEmail}
                        value={valueEmail}
                        onChange={(e) => setValueEmail(e.target.value)}
                      />
                      {
                        errorValueEmail &&
                        <Box sx={{ display: "flex", color: "red", alignItems: "center" }}>
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2a1 1 0 0 0 0-2z" /></svg>
                          <FormHelperText sx={{ color: "#f4071e", marginLeft: "3px", fontSize: '0.8rem' }} id="component-error-text">{ERROR_EMAIL_VALUE_INPUT}</FormHelperText>
                        </Box >
                      }
                    </MuiFormControlTextField>
                  </CardContent>
                </Card>
              </Grid>

              <Grid item marginBottom={5}>
                <Card>
                  <CardContent sx={{ display: "flex", justifyContent: "center", gap: "25px" }}>
                    <Button variant="contained" type="submit">Lưu</Button>
                    <Button color="error" variant="outlined" onClick={handleCancel} >Hủy</Button>
                  </CardContent>
                </Card>
              </Grid>
            </Form>
          )
        }
        }
      </Formik>

      <ModalConfirm {...modalConfirm} toggle={setModalConfirm} />
    </Grid>

  )
}
export default CreateComanyAgencyForm  