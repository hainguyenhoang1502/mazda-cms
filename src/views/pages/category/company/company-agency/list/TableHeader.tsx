// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment } from '@mui/material'

// ** React Imports
import { useState } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** Components Imports
import Icon from 'src/@core/components/icon'
import { SelectMUI } from 'src/views/component/theme'

// ** Config Imports
import { permissonConfig } from 'src/configs/roleConfig'
import { OPTIONS_STATUS } from 'src/configs/functionConfig'

// ** Interface Services Imports
import { IParamValuesFilter } from './interface'

// ** Util Imports
import { checkUserPermission } from 'src/@core/utils/permission'

// ** Store Redux Imports
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetListCompany } from 'src/store/apps/category/company-dealer/company'

const OPTIONS_STATUS_FILTER = [
  {
    value: null,
    label: 'Tất cả'
  },
  ...OPTIONS_STATUS,
]

const TableHeader = () => {
  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.region)

  const [valuesFilter, setValuesFilter] = useState<IParamValuesFilter>({
    keyword: '',
    status: null
  })

  const handleChangeInput = (value, field) => {
    const body = { ...valuesFilter }

    body[field] = value
    setValuesFilter(body)
    const param = {
      ...store.param,
      ...body
    }

    dispatch(fetchDataGetListCompany(param))
  }

  return (
    <Box
      sx={{
        p: 5,
        pb: 3,
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: '25px'
      }}
    >
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', minWidth: '600px', gap: 4 }}>
        <TextField
          size='small'
          value={valuesFilter.keyword}
          sx={{ width: '50%' }}
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <Icon icon='bx:search' fontSize={20} />
              </InputAdornment>
            )
          }}
          placeholder='Tìm kiếm ...'
          onChange={e => handleChangeInput(e.target.value, 'keyword')}
        />
        <Box sx={{ width: '40%' }}>
          <SelectMUI
            label='Trạng thái'
            options={OPTIONS_STATUS_FILTER}
            size='small'
            onChange={e => {
              handleChangeInput(e.target.value, 'status')
            }}
          />
        </Box>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          gap: '1rem',
          justifyContent: 'end',
          maxWidth: '100%'
        }}
      >
        {checkUserPermission(permissonConfig.ORG_CREATE) && (
          <Link href='/category/company/create' as={'/category/company/create/'}>
            <Button variant='contained'>+ thêm mới</Button>
          </Link>
        )}
      </Box>
    </Box>
  )
}

export default TableHeader
