//** MUI Imports
import { DataGrid, GridColDef } from '@mui/x-data-grid'
import { Box, TablePagination } from '@mui/material'

// ** Components
import CustomChip from 'src/@core/components/mui/chip'
import { TypographyDataGrid } from 'src/views/component/theme'


// ** Store Redux Imports
import { AppDispatch, RootState } from 'src/store'
import { useDispatch, useSelector } from 'react-redux'
import { fetchDataGetListCompany } from 'src/store/apps/category/company-dealer/company'

// ** Interface Imports
import { ListDataTableProps, RowDataDetail } from './interface'

// ** Config Imports
import { renderNameStatus } from 'src/configs/functionConfig'
import { DeleteDataGrid, EditDataGrid } from 'src/views/component/theme/customEditDelete'
import { permissonConfig } from 'src/configs/roleConfig'

const ListDataTable = (props: ListDataTableProps) => {

  const { getDelete, setModalConfirm } = props

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.company)

  const handleChangePage = (e: React.MouseEvent<HTMLButtonElement> | null, page: number) => {
    const param = {
      ...store.param,
      pageIndex: page + 1
    }
    dispatch(fetchDataGetListCompany(param))
  }

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param = {
      ...store.param,
      pageSize: Number(event.target.value)
    }
    dispatch(fetchDataGetListCompany(param))
  }

  const handleDelete = (item: RowDataDetail["row"]) => {
    setModalConfirm({
      isOpen: true,
      title: 'Bạn đang thao tác Xóa công ty ' + `${item.legalEntityName}`,
      action: () => { getDelete(item.code.toString()) }
    })
  }

  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 150,
      field: 'actions',
      sortable: false,
      headerName: 'Hành động',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <EditDataGrid permission={permissonConfig.ORG_EDIT} slug={`/category/company/edit/${row.code}`} />
            <DeleteDataGrid permission={permissonConfig.ORG_DELETE} handleDelete={() => handleDelete(row)} />
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 150,
      field: 'code',
      headerName: 'Mã Công ty ',
      sortable: false,
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <TypographyDataGrid noWrap >
              {row.code}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'legalEntityName',
      sortable: false,
      headerName: 'Tên pháp nhân ',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.legalEntityName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 250,
      field: 'name',
      sortable: false,
      headerName: 'công ty',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.name}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 150,
      field: 'regionName',
      sortable: false,
      headerName: 'Vùng miền',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.regionName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 200,
      field: 'areaName',
      sortable: false,
      headerName: 'Khu vực',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.areaName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 200,
      field: 'provinceName',
      sortable: false,
      headerName: 'Tỉnh/thành phố',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.provinceName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 200,
      field: 'districtName',
      sortable: false,
      headerName: 'Quận/Huyện',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.districtName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 200,
      field: 'wardName',
      sortable: false,
      headerName: 'Phường / xã',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.wardName}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 240,
      field: 'address',
      sortable: false,
      headerName: 'Địa chỉ ',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.address}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 120,
      field: 'latitude',
      sortable: false,
      headerName: 'Lat',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.latitude}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 120,
      field: 'longitude',
      sortable: false,
      headerName: 'Lng',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.longitude}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.5,
      minWidth: 300,
      field: 'mapURL',
      sortable: false,
      headerName: 'google map',
      align: 'left',
      renderCell: ({ row }: RowDataDetail) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <TypographyDataGrid noWrap>
              {row.mapURL}
            </TypographyDataGrid>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'status',
      sortable: false,
      headerName: 'trạng thái',
      renderCell: ({ row }: any) => {
        return <CustomChip rounded skin='light' size='small' label={renderNameStatus(row.status)} color={row.status === "ACTIVE" ? "success" : "error"} />
      }
    }
  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={(store.data) || []}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: '10px'
          },
          '& .MuiDataGrid-footerContainer': {
            display: 'none'
          }
        }}
      />

      <TablePagination
        component='div'
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>
  )
}
export default ListDataTable
