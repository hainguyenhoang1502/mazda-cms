// ** MUI Imports
import List from '@mui/material/List'
import { styled } from '@mui/material/styles'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import ListItem, { ListItemProps } from '@mui/material/ListItem'
import Link from 'next/link'
import Icon from 'src/@core/components/icon'
import { ElementType } from 'react'

// ** Styled Components
const ListItemStyled = styled(ListItem)<ListItemProps & { component?: ElementType; href: string }>(({ theme }) => ({
  borderLeftWidth: '3px',
  borderLeftStyle: 'solid',
  padding: theme.spacing(3),
  marginBottom: theme.spacing(1)
}))


function SidebarLeftCompany(props) {

  const handleActiveItem = (type:string) => {
    if (props.label === type) {
      
      return true
    }

    return false
  }

  return (
    <List component='div'>
      <ListItemStyled
        component={Link}
        href='/category/company/'
        sx={{ borderLeftColor: handleActiveItem("company") ? 'primary.main' : 'transparent' }}
      >
        <ListItemIcon sx={{ mr: 2.5, '& svg': { color: 'warning.main' } }}>
          <Icon icon='bxs:circle' fontSize='0.75rem' />
        </ListItemIcon>
        <ListItemText
          primary='Công ty - Đại lý'
          primaryTypographyProps={{
            noWrap: true,
            sx: {
              fontWeight: 500,
            }
          }}
        />
      </ListItemStyled>
      <ListItemStyled
        component={Link}
        href='/category/company/dealer'
        sx={{ borderLeftColor: handleActiveItem("dealer") ? 'primary.main' : 'transparent' }}
      >
        <ListItemIcon sx={{ mr: 2.5, '& svg': { color: 'error.main' } }}>
          <Icon icon='bxs:circle' fontSize='0.75rem' />
        </ListItemIcon>
        <ListItemText
          primary='Điểm bán hàng'
          primaryTypographyProps={{
            noWrap: true,
            sx: {
              fontWeight: 500,
            }
          }}
        />
      </ListItemStyled>
    </List>
  );
}

export default SidebarLeftCompany;