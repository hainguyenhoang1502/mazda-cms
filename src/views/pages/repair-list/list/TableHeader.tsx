
// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import { InputAdornment, Typography } from '@mui/material'

// ** React Imports
import { useState } from 'react'

// ** Next Imports
// ** Third Party Import

// ** Components Imports
import Icon from 'src/@core/components/icon'

// ** Styles

// ** Config Imports

// ** Interface Services Imports 
import { IParamValuesFilter } from './interface'

// ** Utils
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from 'src/store'
import { fetchDataGetCustomer } from 'src/store/apps/customer-information'
import { SelectMUI } from 'src/views/component/theme'
import { OPTIONS_STATUS } from 'src/configs/functionConfig'


const TableHeader = () => {

  // ** Redux
  const dispatch = useDispatch<AppDispatch>()
  const store = useSelector((state: RootState) => state.customer)

  const OPTIONS_STATUS_FILTER = [
    {
      value: null,
      label: 'Tất cả'
    },
    ...OPTIONS_STATUS,
  ]

  const [valuesFilter, setValuesFilter] = useState<IParamValuesFilter>({
    keyword: "",
    startDate: null,
    endDate: null
  })
  const handleClearFilter = () => {
    console.log('aaa');
  }
  
  const handleChangeInput = (value, field) => {
    const body = { ...valuesFilter }
    body[field] = value
    setValuesFilter(body)
  }
  const handleFilterDate = () => {
    const param = {
      ...store.param,
      ...valuesFilter,
    }
    dispatch(fetchDataGetCustomer(param))
  }



  return (
    <Box sx={{ p: 5 }}>
      <Box sx={{ width: '100%' }}>
        <Typography noWrap sx={{ fontWeight: 500, padding: '10px 0', fontSize: '20px' }}>
          Lệnh sửa chữa
        </Typography>
      </Box>
      <Box
        sx={{
          pb: 3,
          width: '100%',
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          justifyContent: 'space-between',
          gap: "25px"
        }}
      >

        <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
          <TextField
            size='small'
            value={valuesFilter.keyword}
            sx={{ mr: 4, width: "30%" }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Icon icon='bx:search' fontSize={20} />
                </InputAdornment>
              ),
            }}
            placeholder='Tìm kiếm…'
            onChange={(e) => handleChangeInput(e.target.value, "keyword")}
          />
          <Box sx={{ marginRight: 4 }}>
            <SelectMUI
              label='Showroom'
              options={OPTIONS_STATUS_FILTER}
              size='small'
              onChange={e => {
                handleChangeInput(e.target.value, 'status')
              }}
            />
          </Box>
          <Button variant="outlined" onClick={handleFilterDate} sx={{ mr: 4 }} >
            Lọc
          </Button>
          <Button variant="outlined" onClick={handleClearFilter} color='error'>
            Xóa bộ lọc
          </Button>
        </Box>
      </Box>
    </Box>
  )
}

export default TableHeader
