import { Box, TablePagination, Typography } from "@mui/material"
import { IParamFilterListCustomer } from "src/api/customer/type"
import { renderNameGender } from "src/configs/functionConfig"
import { DataGrid, GridColDef } from "@mui/x-data-grid"

// ** Components 
import CustomAvatar from 'src/@core/components/mui/avatar'
import { useDispatch, useSelector } from "react-redux"
import { AppDispatch, RootState } from "src/store"
import { fetchDataGetCustomer } from "src/store/apps/customer-information"



const ListDataTable = () => {
   // ** Redux
   const dispatch = useDispatch<AppDispatch>()
   const store = useSelector((state: RootState) => state.customer)
 
  const handleChangePage = (e, page) => {
    const param: IParamFilterListCustomer = {
      ...store.param,
      pageIndex: page + 1,
    }
    dispatch(fetchDataGetCustomer(param))
  }
  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const param: IParamFilterListCustomer = {
      ...store.param,
      pageSize: Number(event.target.value),
    }
    dispatch(fetchDataGetCustomer(param))
  }


  // ** Permission 
  const columns: GridColDef[] = [
    {
      flex: 0.25,
      minWidth: 240,
      field: 'fullName',
      headerName: 'Mã chứng từ',
      sortable: false,
      renderCell: ({ row }) => {

        return (
          <Box sx={{display:"flex",alignItems:"center",gap:"10px"}}>
            {row.avatar ? (
              <CustomAvatar
                src={row.avatar}
                variant='rounded'
                alt={row.fullName}
                sx={{ width: 30, height: 30,objectFit:"cover"}}
              />
            ) : (
              <CustomAvatar
                skin='light'
                variant='rounded'
                sx={{ width: 30, height: 30, fontWeight: 600, fontSize: '3rem' }}
              >
              </CustomAvatar>
            )}
            <Typography noWrap sx={{ fontWeight: 400, color: 'text.secondary', textTransform: 'capitalize' }}>
              {row.fullName}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'numberOfCarsOwned ',
      sortable: false,
      headerName: 'số chứng từ',
      align: 'left',
      renderCell: ({ row }: any) => {
        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.numberOfCarsOwned || 0}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'gender',
      headerName: 'showroom',
      sortable: false,
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {renderNameGender(row.gender)}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'phoneNumber',
      sortable: false,
      headerName: 'Khách hàng',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.phoneNumber}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 240,
      field: 'email',
      sortable: false,
      headerName: 'số điện thoại',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap sx={{ fontWeight: 400, color: 'text.secondary', textAlign: "center" }}>
              {row.email}
            </Typography>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 200,
      field: 'address',
      sortable: false,
      headerName: 'địa chỉ',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.address}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'ward',
      sortable: false,
      headerName: 'Dòng xe',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.ward}
            </Typography>
          </Box>
        )
      }
    }
    ,
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district',
      sortable: false,
      headerName: 'số vin',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district2',
      sortable: false,
      headerName: 'số máy',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district3',
      sortable: false,
      headerName: 'biển số',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district4',
      sortable: false,
      headerName: 'số km',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district5',
      sortable: false,
      headerName: 'yêu cầu khách hàng',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district6',
      sortable: false,
      headerName: 'loại bảo dưỡng',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district7',
      sortable: false,
      headerName: 'ngày bắt đầu sửa chữa',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district8',
      sortable: false,
      headerName: 'ngày kết thúc sửa chữa',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district9',
      sortable: false,
      headerName: 'ngày bắt đầu bảo hiểm',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district10',
      sortable: false,
      headerName: 'ngày kết thúc bảo hiểm',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district11',
      sortable: false,
      headerName: 'ngày mở ro',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
    {
      flex: 0.25,
      minWidth: 200,
      field: 'district12',
      sortable: false,
      headerName: 'ngày hoàn thành',
      renderCell: ({ row }: any) => {

        return (
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Typography noWrap >
              {row.district}
            </Typography>
          </Box>
        )
      }
    },
  ]

  return (
    <>
      <DataGrid
        autoHeight
        rows={store.data||[]}
        disableColumnFilter={true}
        columns={columns}
        loading={store.isLoading}
        rowCount={store.totalRecords}
        hideFooterPagination
        sx={{
          '& .MuiDataGrid-virtualScrollerContent': {
            marginBottom: "10px"
          },
          '& .MuiDataGrid-footerContainer': {
            display: "none"
          }
        }}
      />
      <TablePagination
        component="div"
        count={store.totalRecords}
        page={store.pageIndex - 1}
        onPageChange={handleChangePage}
        rowsPerPage={store.pageSize}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </>


  )
}
export default ListDataTable