import { useRouter } from 'next/router';
import React from 'react';
import { mediaApi } from 'src/api-client';
import { CarImageApi } from 'src/api-client/car-images-management';

export function formWithDetails(WrappedComponent) {
  return function FormWithDetails(props) {
    const [initialValues, setInitialValues] = React.useState(null);
    const router = useRouter(); 
    const { slugs } = router.query
    const [loading, setLoading]= React.useState(false);
    const getInitialValues = async () => {
      // Fetch data and set the initial values
      // Modify this part according to your API and data structure
      try {
        setLoading(true)
        const res = await CarImageApi.getVehiclesImageDetail(slugs);
        if (res.data) {
          const imageNotify = await mediaApi.getImage(res.data.image);
          const imageFile = new File([imageNotify], 'image.jpg', {
            type: imageNotify.type,
          });

          const newInitialValues = {
            modelCode: res.data.modelCode ? res.data.modelCode : '',
            gradeCode: res.data.gradeCode ? res.data.gradeCode : '',
            colorCode: res.data.colorCode ? res.data.colorCode : '',
            year: res.data.year ? new Date(res.data.year, 0, 1) : null,
            image: [imageFile],
            status: res.data.status,
          };

          setInitialValues(newInitialValues);
          setLoading(false)
        } else {
          // Handle error or redirect
          router.push('/404');
          setLoading(false);
        }
      } catch (error) {
        // Handle error
      }
    };

    React.useEffect(() => {
      getInitialValues();
    }, []);

    if (!initialValues||loading) { 
      // Render loading state or skeleton UI
      return null;
    }

    return <WrappedComponent {...props} initialValues={initialValues}>
        {...props.children}
    </WrappedComponent>;
  };
}
