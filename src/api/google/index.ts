import { SLUG_GOOGLE, nullRes } from "../initValue"
import { $get } from "../request"
import { LogELSError } from "../els"

export const getApiRecaptcha = async (value: string, req?: any): Promise<any> => {
  try {
    const res = await $get(req, `response=${value}`, SLUG_GOOGLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}