const nullRes = {
  result: false,
  code: 0,
  data: null,
  message: ''
}
const INIT_CONTROLLER = "cms"
const SLUG_GOOGLE = "google"

export {
  nullRes,
  INIT_CONTROLLER,
  SLUG_GOOGLE
}