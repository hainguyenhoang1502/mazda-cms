import router from 'next/router'
import toast from 'react-hot-toast'
import { eraseCookie, getCookie } from 'src/@core/utils/cookie'

// ** Config
import authConfig from 'src/configs/auth'
import {
  SLUG_AUTHEN,
  SLUG_CUSTOMER,
  SLUG_DOCUMENTS,
  SLUG_ORGANIZATION,
  SLUG_REF_DATA,
  SLUG_VEHICLE,
  SLUG_WARRANTY,
  X_API_KEY
} from 'src/configs/envConfig'

import { store } from 'src/store'
import { setAccessToken } from 'src/store/utils/accessToken'
import { SLUG_GOOGLE } from './initValue'
import { LogELSError } from './els'

const getApiUrl = (req?) => {
  if (req) {
    return process.env.WWW_API_SERVICE_HOST
  }
  try {
    return `${window.location.origin}`
  } catch (error) {
    return process.env.WWW_API_SERVICE_HOST
  }
}
const renderBaseUrl = (slugUrl, req) => {
  if (req) {
    return slugUrl + '/api'
  }
  switch (slugUrl) {
    case 'els':
      return 'api-els'
    case SLUG_GOOGLE:
      return 'api-google'
    case SLUG_CUSTOMER:
      return 'api-customer'
    case SLUG_REF_DATA:
      return 'api-ref-data'
    case SLUG_VEHICLE:
      return 'api-vehicle'
    case SLUG_WARRANTY:
      return 'api-warranty'
    case SLUG_AUTHEN:
      return 'api-authen'
    case SLUG_ORGANIZATION:
      return 'api-organization'
    case SLUG_DOCUMENTS:
      return 'api-document'
    default:
      return 'api'
  }
}

const getToken = req => {
  try {
    return getCookie(authConfig.tokenKeyName, req)
  } catch (error) {
    return store.getState().accessToken.accessToken
  }
}
const request: any = async (req?: any, uri?: string, method?: string, body?: any, slugUrl?: string) => {
  const baseURl = renderBaseUrl(slugUrl, req)
  const url = `${getApiUrl(req)}/${baseURl}/${uri}`

  const token = getToken(req)
  try {
    const res = await fetch(url, {
      method,
      body: JSON.stringify(body),
      credentials: 'include',
      headers: {
        "Authorization": token,
        "Content-Type": "application/json",
        "x-api-key": X_API_KEY
      }
    })
    if (res.status === 401) {
      toast.error('Phiên đăng nhập đã hết hạn. Vui lòng đăng nhập lại')
      store.dispatch(setAccessToken(null))
      eraseCookie(authConfig.tokenKeyName)
      router.push('/login', '/login')

      return {
        message: '',
        data: [],
        result: false
      }
    }
    if (res.headers.get('content-type').indexOf('text/html') !== -1) {
      const text = await res.json()

      return text
    } else {
      const json = await res.json()

      return json
    }
  } catch (error) {
    LogELSError(error)
    throw new Error()
  }
}
const upload: any = async (req?: any, uri?: string, method?: string, body?: any, slugUrl?: string) => {
  const baseURl = renderBaseUrl(slugUrl, req)
  let url = `${getApiUrl(req)}/${baseURl}/${uri}`
  if (/^http(s{0,1}).*/g.test(uri)) {
    url = uri
  } else {
    url = `${getApiUrl(req)}/${baseURl}/${uri}`
  }
  const token = getToken(req)
  try {
    const res = await fetch(url, {
      method,
      body: body,
      credentials: 'include',
      headers: {
        Authorization: token
      }
    })

    if (res.status === 401) {
      toast.error('Phiên đăng nhập đã hết hạn. Vui lòng đăng nhập lại')
      store.dispatch(setAccessToken(null))
      eraseCookie(authConfig.tokenKeyName)
      router.push('/login', '/login')

      return {
        message: '',
        data: [],
        result: false
      }
    }
    if (res.headers.get('content-type').indexOf('text/html') !== -1) {
      const text = await res.json()

      return text
    } else {
      const json = await res.json()

      return json
    }
  } catch (error) {
    throw new Error()
  }
}
const $get = async (req?: any, uri?: string, slugUrl?: string) => request(req, uri, 'GET', undefined, slugUrl)
const $post = async (uri: string, body?: any, req?: any, slugUrl?: string) => request(req, uri, 'POST', body, slugUrl)
const $put = async (uri: string, body?: any, req?: any, slugUrl?: string) => request(req, uri, 'PUT', body, slugUrl)
const $delete = async (uri: string, body?: any, req?: any, slugUrl?: string) => request(req, uri, 'DELETE', body, slugUrl)
const $patch = async (uri: string, body?: any, req?: any, slugUrl?: string) => request(req, uri, 'PATCH', body, slugUrl)
const $upload = async (uri: string, body?: any, req?: any, slugUrl?: string) => upload(req, uri, 'POST', body, slugUrl)

export { $get, $post, $put, $delete, $patch, $upload }
