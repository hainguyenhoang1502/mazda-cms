interface IResponeProvince {
  result: boolean
  code: number
  data: Array<ItemDataProvince>
  message: string
}

interface IResponseProvinceDetail {
  result: boolean
  code: number
  data: {
    totalRecords: number
    pageIndex: number
    pageSize: number
    totalPages: number
    result: Array<ItemDataProvinceList>
  }
  message: string
}

interface ItemDataProvinceList {
  totalRows: number;
  id: string,
  locationName: string,
  status: string,
  code: string,
  regionName: string,
  areaName: string
}

interface IResponseRegion {
  result: boolean
  code: number
  data: {
    totalRecords: number
    pageIndex: number
    pageSize: number
    totalPages: number
    result: Array<{ id: number; code: string; regionName: string; status: string }>
  }
  message: string
}
interface IResponseArea {
  result: boolean
  code: number
  data: {
    totalRecords: number
    pageIndex: number
    pageSize: number
    totalPages: number
    result: Array<{ id: number; code: string; areaName: string; status: string }>
  }
  message: string
}
interface IResponseRegionEdit {
  result: boolean
  code: number
  data: {
    id: number
    code: string
    regionName: string
    status: string
  }
  message: string
}
interface IResponseProvinceDetailByID {
  result: boolean
  code: number
  data: {
    id: string,
    code: string,
    locationName: string,
    status: string,
    regionCode: string,
    areaCode: string
  }
  message: string
}
interface IResponseAreaEdit {
  result: boolean
  code: number
  data: {
    id: number,
    code: string,
    areaName: string,
    regionName: string,
    regionCode: string,
    status: string
  }
  message: string
}
interface IResponseSMSDetail {
  result: boolean,
  code: number,
  data: {
    id: number,
    brandCode: string,
    brandName: string,
    apiKey: string,
    secretKey: string,
    isActive: number,
    createdUser?: string
  },
  message: string
}
interface IResponseSettingDetail {
  result: boolean,
  code: number,
  data: {
    smsConfig: {
      id: number,
      brandCode: string,
      brandName: string,
      apiKey: string,
      secretKey: string,
      isActive: 0 | 1
    },
    emailConfig: {
      id: number,
      host: string,
      port: string,
      userName: string,
      password: string,
      displayName: string,
      brandCode: string,
      isActive: 0 | 1
    },
    appConfig: {
      id: number,
      brandCode: string,
      logo: string,
      splashcreen: string,
      textIntro: string
    }
  },
  message: string
}
interface IResponseDistrict {
  result: boolean
  code: number
  data: {
    totalRecords: number
    pageIndex: number
    pageSize: number
    totalPages: number
    result: Array<{
      totalRows: number
      id: string
      districtFullName: string
      status: string
      code: string
      regionName: string
      areaCode: string
      regionCode: string
      provincesFullName: string
    }>
  }
  message: string
}
interface IResponseUpdateRegion {
  result: boolean
  code: number
  data: number
  message: string
}

type IResponseCreateProvince = IResponseUpdateRegion
type IResponseEmail = IResponseUpdateRegion
type IResponsePostSMS = IResponseUpdateRegion
type IResponsePostLogoApp = IResponseUpdateRegion

interface ItemDataProvince {
  code: number
  locationName: string
}

interface IResponeUploadFile {
  result: boolean
  code: number
  data: {
    filePath: string
    serverName: string
  }
  message: string
}
interface IParamLocationRegion {
  keyword: string
  pageIndex: number
  pageSize: number
  status: string
}

type IParamArea = IParamLocationRegion



interface IParamRegionCreate {
  code: string
  regionName: string
  status: string
}

interface IParamAreaCreate {
  code: string,
  areaName: string,
  status: string,
  regionCode: string
}

interface IParamProvinceCreate {
  code: string,
  fullName: string,
  status: string,
  areaCode: string,
  regionCode: string,
  createUser: string
}

interface IOptionsRegion {
  value?: string
  label?: string
}

interface IParamDistrict {
  code: string,
  locationName: string,
  status: string,
  provinceCode: string,
  createUser: string
}

interface IParamSMS {
  id: number,
  brandCode: string,
  brandName: string,
  apiKey: string,
  secretKey: string,
  isActive: 1 | 0
}

interface IParamEmail {
  id: number,
  host: string,
  port: string,
  userName: string,
  password: string,
  displayName: string,
  brandCode: string,
  isActive: 0 | 1
}
interface IParamLogoApp {
  id: number,
  brandCode: string,
  logo: string,
  splashcreen: string,
  textIntro: string
}

export type {
  IResponeProvince,
  ItemDataProvince,
  IResponseProvinceDetail,
  IResponeUploadFile,
  IParamLocationRegion,
  IOptionsRegion,
  IResponseRegion,
  IParamRegionCreate,
  IResponseUpdateRegion,
  IResponseRegionEdit,
  IParamAreaCreate,
  IResponseAreaEdit,
  IResponseArea,
  IParamArea,
  IParamProvinceCreate,
  IResponseCreateProvince,
  IResponseProvinceDetailByID,
  IResponseDistrict,
  IParamDistrict,
  IResponseSMSDetail,
  IParamSMS,
  IParamEmail,
  IResponseEmail,
  IResponsePostSMS,
  IParamLogoApp,
  IResponsePostLogoApp,
  IResponseSettingDetail
}
