import { SLUG_REF_DATA } from 'src/configs/envConfig'
import { $get, $post, $put, $upload } from '../request'
import { nullRes } from '../initValue'
import {
  IParamAreaCreate,
  IParamDistrict,
  IParamEmail,
  IParamLocationRegion,
  IParamLogoApp,
  IParamProvinceCreate,
  IParamRegionCreate,
  IParamSMS,
  IResponeProvince,
  IResponeUploadFile,
  IResponseArea,
  IResponseAreaEdit,
  IResponseCreateProvince,
  IResponseDistrict,
  IResponseEmail,
  IResponsePostLogoApp,
  IResponsePostSMS,
  IResponseProvinceDetail,
  IResponseProvinceDetailByID,
  IResponseRegion,
  IResponseRegionEdit,
  IResponseSettingDetail,
  IResponseUpdateRegion
} from './type'
import { LogELSError } from '../els'
import { IParamFilter } from '../type'

// ====== province =====
const ApiGetLocationProvince = async (req?: any): Promise<IResponeProvince> => {
  try {
    const res = await $get(req, `locations/get-provinces`, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiPostListProvince = async (param: IParamFilter, req?: any): Promise<IResponseProvinceDetail> => {
  try {
    const res = await $post(`locations/get-list-provinces-detail`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiPostCreateProvince = async (param: IParamProvinceCreate, req?: any): Promise<IResponseCreateProvince> => {
  try {
    const res = await $post(`locations/create-province`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiPutUpdateProvince = async (param: IParamProvinceCreate, req?: any): Promise<IResponseCreateProvince> => {
  try {
    const res = await $put(`locations/update-province`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiGetProvinceById = async (id: string, req?: any): Promise<IResponseProvinceDetailByID> => {
  try {
    const res = await $get(req, `locations/get-province-detail-by-id?id=${id}`, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiGetLocationDistrictByProvinceId = async (id: number | string, req?: any): Promise<IResponeProvince> => {
  try {
    const res = await $get(req, `locations/get-district-by-province-id?ProvinceId=${id}`, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiGetLocationWardByDistrictId = async (id: number | string, req?: any): Promise<IResponeProvince> => {
  try {
    const res = await $get(req, `locations/get-ward-by-district-id?DistrictId=${id}`, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiUploadAvatarCustomer = async (param: any, req?: any): Promise<IResponeUploadFile> => {
  try {
    const res = await $upload(`files/upload-avatar`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiUploadFileImage = async (param: any, req?: any): Promise<IResponeUploadFile> => {
  try {
    const res = await $upload(`files/upload-image`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

// ==== upload file ====
const ApiUploadFile = async (param: any, req?: any): Promise<IResponeUploadFile> => {
  try {
    const res = await $upload(`files/upload-file`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

// ====== region ======

const ApiGetLocationRegion = async (param: IParamLocationRegion, req?: any): Promise<IResponeProvince> => {
  try {
    const res = await $post(`locations/list-regions`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiEditRegionById = async (id: number, req?: any): Promise<IResponseRegionEdit> => {
  try {
    const res = await $get(req, `locations/region-detail-by-id?id=${id}`, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiGetListRegion = async (param: IParamLocationRegion, req?: any): Promise<IResponseRegion> => {
  try {
    const res = await $post(`locations/list-regions`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiPostCreateRegions = async (param: IParamRegionCreate, req?: any): Promise<IResponseUpdateRegion> => {
  try {
    const res = await $post(`locations/create-region`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiUpdateRegionByCode = async (param: IParamRegionCreate, req?: any): Promise<IResponseUpdateRegion> => {
  try {
    const res = await $put("locations/update-region-by-code", param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}


// ====== area ====
const ApiEditAreaById = async (id: number, req?: any): Promise<IResponseAreaEdit> => {
  try {
    const res = await $get(req, `locations/area-detail-by-id?id=${id}`, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiGetListArea = async (param: IParamLocationRegion, req?: any): Promise<IResponseArea> => {
  try {
    const res = await $post(`locations/list-areas`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiPostUpdateArea = async (param: IParamAreaCreate, req?: any): Promise<IResponseRegionEdit> => {
  try {
    const res = await $put(`locations/update-area-by-code`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiPostCreateArea = async (param: IParamAreaCreate, req?: any): Promise<IResponseUpdateRegion> => {
  try {
    const res = await $post(`locations/create-area`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

// ====== district ======
const ApiPostListDistrict = async (param: IParamFilter, req?: any): Promise<IResponseDistrict> => {
  try {
    const res = await $post(`locations/get-list-districts-detail`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiPostCreateDistrict = async (param: IParamDistrict, req?: any): Promise<IResponseDistrict> => {
  try {
    const res = await $post(`locations/create-district`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

// ====== system setting =====
const ApiDetailSystemSetting = async (code: string, req?: any): Promise<IResponseSettingDetail> => {
  try {
    const res = await $get(req, `smsconfig/get-cms-config?code=${code}`, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiPostSendSMS = async (param: IParamSMS, req?: any): Promise<IResponsePostSMS> => {
  try {
    const res = await $post(`smsconfig/save-sms-config`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

// ===== email ======
const ApiPostSendEmail = async (param: IParamEmail, req?: any): Promise<IResponseEmail> => {
  try {
    const res = await $post(`smsconfig/save-email-config`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

// ====== logo app =====

const ApiPostSendLogoApp = async (param: IParamLogoApp, req?: any): Promise<IResponsePostLogoApp> => {
  try {
    const res = await $post(`smsconfig/save-logo-app`, param, req, SLUG_REF_DATA)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}


export {
  ApiGetLocationProvince,
  ApiPostListProvince,
  ApiPutUpdateProvince,
  ApiGetLocationDistrictByProvinceId,
  ApiPostCreateProvince,
  ApiGetProvinceById,
  ApiGetLocationWardByDistrictId,
  ApiUploadAvatarCustomer,
  ApiUploadFileImage,
  ApiGetLocationRegion,
  ApiGetListArea,
  ApiGetListRegion,
  ApiPostCreateRegions,
  ApiEditRegionById,
  ApiPostCreateArea,
  ApiEditAreaById,
  ApiPostUpdateArea,
  ApiUpdateRegionByCode,
  ApiPostListDistrict,
  ApiPostCreateDistrict,
  ApiDetailSystemSetting,
  ApiPostSendSMS,
  ApiPostSendEmail,
  ApiPostSendLogoApp,
  ApiUploadFile
}
