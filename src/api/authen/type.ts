export type IParamLogin = {
  userName: string
  password: string
}
export type IResponeLogin = {
  result: boolean
  code: number
  data: {
    access_token: string
    refresh_token: string
    expires_in: number
  }
  message: string
}

export type IParamListUser = {
  keyword: string
  pageIndex: number
  pageSize: number
  startDate?: string
  endDate?: string
}
export type IResponeListUser = {
  result: boolean
  code: number
  data: Array<IResultDataDetailUser>
  message: string
}
export type IResultDataDetailUser = {
  id: number,
  avatar: string,
  userId: string,
  userName: string,
  password?: string
  email: string,
  fullName: string,
  phoneNumber: string,
  status: string,
  roleId: string,
  roleName: string,
  organizationCodes: Array<string>
}

export type IParamFilterListRole = {
  keyword: string
  pageIndex: number
  pageSize: number
  startDate?: string
  endDate?: string
}

export type IResponeListRole = {
  result: boolean
  code: number
  data: {
    totalRecords: number
    pageIndex: number
    pageSize: number
    totalPages: number
    roles: {
      description: string
      claims: {
        id: number
        roleId: string
        claimType: string
        claimValue: string
      }[]
      id: string
      name: string
      normalizedName: string
      concurrencyStamp: string
    }[]
  }
  message: string
}
export type IResponeDataRole = {
  totalRecords: 2
  pageIndex: 1
  pageSize: 10
  totalPages: 1
  roles: Array<IResultDataDetailRole>
}
export type IResultDataDetailRole = {
  description: string
  claims: [
    {
      id: number | string
      roleId: string
      claimType: string
      claimValue: string
    }
  ]
  id: string
  name: string
  normalizedName: string
  concurrencyStamp: string
}

export type IResponePermissionList = {
  result: boolean
  code: number
  data: Array<IDataPermission>
  message: string
}

export type IDataPermission = {
  groupCode: number | string
  groupName: string
  permissions: [
    {
      name: string
      code: string
      required: Array<any>
    }
  ]
}

export type IParamCreateRole = {
  name: string,
  description: string,
  permissionCodes?: Array<string>

}
export type IParamUpdateRole = {
  name?: string,
  description?: string,
  permissionCodes: Array<string>
  id: string
}

export type IParamCreateUser = {
  avatar: string
  userName: string
  password: string
  email: string
  fullName: string
  phoneNumber: string | number
  roleId: string
  organizationCodes: Array<string>
}

export type IResponeDetailUser = {
  result: boolean
  code: number
  data: {
    id: string | number,
    avatar: string,
    userName: string,
    email: string,
    fullName: string,
    phoneNumber: string | number,
    status: string,
    roleId: string,
    roleName: string,
    organizationCodes: Array<string>
    password?: string
  }
  message: string
}

export type IResponeProfileUser = {
  result: boolean
  code: number
  data: {
    roleId: string,
    roleName: string,
    grantedAuthorities: Array<string>
    id: string | number,
    avatar: string,
    userName: string,
    userId: string,
    phoneNumber: string | number,
    email: string,
    fullName: string,
    status: string
  }
  message: string

}

export type IResponeRoleDetail = {
  result: boolean
  code: number
  data: {
    description: string,
    claims:
    {
      id: number,
      roleId: string,
      claimType: string,
      claimValue: string
    }[],
    id: string,
    name: string,
    normalizedName: string,
    concurrencyStamp: string
  }
  message: string

}

export type IParamUpdateStatus = {
  userName: string,
  status: string
}