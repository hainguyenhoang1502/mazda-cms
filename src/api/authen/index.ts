import { SLUG_AUTHEN } from 'src/configs/envConfig'
import { INIT_CONTROLLER, nullRes } from '../initValue'
import { $delete, $get, $patch, $post, $put } from '../request'
import {
  IParamCreateRole,
  IParamCreateUser,
  IParamFilterListRole,
  IParamListUser,
  IParamLogin,
  IParamUpdateRole,
  IParamUpdateStatus,
  IResponeDetailUser,
  IResponeListRole,
  IResponeLogin,
  IResponePermissionList,
  IResponeProfileUser,
  IResponeRoleDetail
} from './type'
import { LogELSError } from '../els'
import { IRespone } from '../type'

export const ApiLogin = async (param: IParamLogin, req?: any): Promise<IResponeLogin> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/login`, param, req, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiLogout = async (req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/logout`, {}, req, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiCreateUserCms = async (param: IParamCreateUser, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/create-user`, param, req, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiUpdateUserCms = async (param: IParamCreateUser, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`${INIT_CONTROLLER}/update-user`, param, req, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiListUserCms = async (param: IParamListUser, req?: any): Promise<IRespone> => {
  try {
    const res = await $get(
      req,
      `${INIT_CONTROLLER}/list-users/${param.pageIndex}/${param.pageSize}?keyword=${param.keyword}`,
      SLUG_AUTHEN
    )

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiDeleteUserCms = async (param: any, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`${INIT_CONTROLLER}/delete-user`, param, req, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiListRoleCms = async (param: IParamFilterListRole, req?: any): Promise<IResponeListRole> => {
  try {
    const res = await $get(
      req,
      `${INIT_CONTROLLER}/list-roles/${param.pageIndex}/${param.pageSize}?keyword=${param.keyword}`,
      SLUG_AUTHEN
    )

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
export const ApiDeleteRoleCms = async (id: number | string, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`${INIT_CONTROLLER}/delete-role/${id}`, null, req, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiCreateRoleCms = async (param: IParamCreateRole, req?: any): Promise<any> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/create-role`, param, req, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
export const ApiUpdateRoleCms = async (param: IParamUpdateRole, req?: any): Promise<any> => {
  try {
    const res = await $put(`${INIT_CONTROLLER}/update-role`, param, req, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiListPermissionCms = async (req?: any): Promise<IResponePermissionList> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/list-permissions`, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiDetailRoleCms = async (id: number | string, req?: any): Promise<IResponeRoleDetail> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/role-detail/${id}`, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiDetailUserCms = async (id: number | string, req?: any): Promise<IResponeDetailUser> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/detail-user/${id}`, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiProfileUserCms = async (req?: any): Promise<IResponeProfileUser> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/profile`, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiUpdateStatusUserCms = async (param: IParamUpdateStatus, req?: any): Promise<IRespone> => {
  try {
    const res = await $patch(`${INIT_CONTROLLER}/update-status-user`, param, req, SLUG_AUTHEN)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}