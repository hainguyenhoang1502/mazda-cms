import { SLUG_WARRANTY } from "src/configs/envConfig"
import { $get, $post } from "../request"
import { INIT_CONTROLLER, nullRes } from "../initValue"
import {
  IParamListMaintenaceHistory,
  IParamListMaintenaceHistoryAll,
  IParamListMaintenaceHistoryByVehicle,
  IParamListWarrantyInformation,
  IResponeListMaintenaceHistory,
  IResponseListMantenanceHistoryAll,
  IResponseListMantenanceHistoryByVehicle,
  IResponseListWarrantyInformation,
  IResponeMaintenanceSuggestInfo,
  IResponeListMaintenanceType,
  IParamCreateMaintenanceHistory
}
  from "./type"
import { LogELSError } from "../els"
import { IRespone } from "../type"

export const ApiListMaintenanceHistoryByCustomerCms = async (param: IParamListMaintenaceHistory, req?: any): Promise<IResponeListMaintenaceHistory> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/list-maintenance-history-by-customer`, param, req, SLUG_WARRANTY)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiListWarrantyInformationCms = async (param: IParamListWarrantyInformation, req?: any): Promise<IResponseListWarrantyInformation> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/list-warranty-infomation`, param, req, SLUG_WARRANTY)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiListMaintenanceHistoryByVehicleCms = async (param: IParamListMaintenaceHistoryByVehicle, req?: any): Promise<IResponseListMantenanceHistoryByVehicle> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/list-maintenance-history-by-vehicle`, param, req, SLUG_WARRANTY)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiListMaintenanceHistoryAllCms = async (param: IParamListMaintenaceHistoryAll, req?: any): Promise<IResponseListMantenanceHistoryAll> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/list-maintenance-history-get-all`, param, req, SLUG_WARRANTY)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiListMaintenanceSuggestInfoCms = async (keyword: string, req?: any): Promise<IResponeMaintenanceSuggestInfo> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/list-maintenance-suggest-info?keyword=${keyword}`, SLUG_WARRANTY)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiListMaintenanceTypeCms = async (req?: any): Promise<IResponeListMaintenanceType> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/get-maintenance-type`, SLUG_WARRANTY)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}



export const ApiGetCreateMaintenanceHistory = async (param: IParamCreateMaintenanceHistory, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/create-maintenance-history`, param, req, SLUG_WARRANTY)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}