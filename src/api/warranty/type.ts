export type IParamListMaintenaceHistory = {
  keyword: string,
  pageIndex: number,
  pageSize: number,
  vehicleId: number | string
  startDate: string,
  endDate: string
}

export type IParamListWarrantyInformation = {
  id?: number,
  keyword: string,
  pageIndex: number,
  pageSize: number,
  customerId: number,
  vehicleId: number,
  startDate: string,
  endDate: string
}
export type IParamListMaintenaceHistoryByVehicle = {
  keyword: string,
  pageIndex: number,
  pageSize: number,
  customerId: number,
  vehicleId: number,
  startDate: string,
  endDate: string
}
export type IResponeListMaintenaceHistory = {
  code: number
  data: IDataResponeListMaintenaceHistory
  message: string
  result: boolean
}
export type IDataResponeListMaintenaceHistory = {
  totalRecords: number,
  pageIndex: number,
  pageSize: number,
  totalPages: number,
  result: Array<IResultDataResponeListMaintenaceHistory>
}
export type IResultDataResponeListMaintenaceHistory = {
  id: number,
  voucherCode: string,
  maintenanceDate: string,
  receivedDate: string,
  content: string,
  maintenanceType: string,
  itemsToDoSoon: string,
  completeDate: string,
  typeBillDate: string
}

export type IResponseListWarrantyInformation = {
  result: boolean,
  code: number,
  data: {
    totalRecords: number,
    pageIndex: number,
    pageSize: 10,
    totalPages: 1,
    details: [
      {
        id: string,
        customerName: string,
        phoneNumber: string,
        carModel: string,
        carColor: string,
        carPlate: string,
        vinNumber: string,
        engineNumber: string,
        warrantyRegDate: string,
        warrantyEndDate: string,
        carDealer: string
      }
    ]
  },
  message: string
}

export type IResponseListMantenanceHistoryByVehicle = {
  result: boolean,
  code: number,
  data: {
    totalRecords: number,
    pageIndex: number,
    pageSize: number,
    totalPages: number,
    result:
    {
      id: number,
      roNo: number,
      roDate: string,
      modelName: string,
      licensePlate: string,
      numberKm: number,
      dealerName: string,
      maintenanceType: string,
      receiveDate: string,
      deliveryDate: string
    }[]
  },
  message: string
}

export type IParamListMaintenaceHistoryAll = {
  keyword: string,
  pageIndex: number,
  pageSize: number,
  modelCode: string
}

export type IResponseListMantenanceHistoryAll = {
  result: boolean,
  code: number,
  data: {
    totalRecords: number,
    pageIndex: number,
    pageSize: number,
    totalPages: number,
    result: [
      {
        totalRows: number,
        customerName: string,
        phoneNumber: string,
        frameNumber: string,
        id: number,
        roNo: string,
        roDate: string,
        modelName: string,
        licensePlate: string,
        numberKm: string,
        dealerName: string,
        maintenanceType: string,
        receiveDate: string,
        deliveryDate: string
      }
    ]
  },
  message: string
}

export type IParamMaintenaceList = {
  pageIndex: number;
  pageSize: number;
  keyword: string;
  modelCode: string;
}

export type IResponeMaintenanceSuggestInfo = {
  result: boolean,
  code: number,
  data: {
    suggestId: number,
    vehicleId: number,
    customerId: number,
    fullName: string,
    licensePlate: string,
    modelName: string,
    phoneNumber: string,
    vinCode: string,
    colorNameVi: string,
    frameNumber: string,
    engineNumber: string,
    year: string
  }[]

  message: string
}

export type IResponeListMaintenanceType = {
  result: boolean,
  code: number,
  data: {
    code: string,
    name: string
  }[]
  message: string
}

export type IParamCreateMaintenanceHistory = {
  vehicleId: number,
  customerId: number,
  roNo: string,
  roDate: string,
  maintenanceType: string,
  presentKm: string,
  dealerCode: string,
  receiveDate: string,
  deliveryDate: string
}
