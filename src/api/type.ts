export type IRespone = {
  code: number
  data: any
  message: string
  result: boolean
}

export type IParamFilter = {
  pageIndex: number;
  pageSize: number;
  keyword: string;
  status: string;
}
