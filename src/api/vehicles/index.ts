import { SLUG_VEHICLE } from "src/configs/envConfig"
import { INIT_CONTROLLER, nullRes } from "../initValue"
import { $delete, $get, $post, $put } from "../request"
import { IRespone } from "../type"
import { IBodyModel, IParamCreateColor, IParamCreateVersion, IParamListVehicleByCustomer, IParamModelList, IParamUpdateColor, IParamUpdateVersion, IParamVehicleList, IParamVehicleListColor, IResponeModelCreate, IResponeModelDetail, IResponseColorDetail, IResponseListModelCode, IResponseListVehicle, IResponseListVehicleByCustomer, IResponseListVehicleColor, IResponseVehicleDetail } from "./type"
import { LogELSError } from "../els"
import queryString from "query-string"


const ApiListVehicleByCustomerCms = async (param: IParamListVehicleByCustomer, req?: any): Promise<IResponseListVehicleByCustomer> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/list-vehicle-by-customer`, param, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiDeleteVehicleCustomerCms = async (param: any, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`${INIT_CONTROLLER}/vehicle-delete`, param, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiListVehicleModelsCode = async (req?: any): Promise<IRespone> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/list-vehicle-models-code`, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiListVehicleModelsData = async (param: IParamModelList, req?: any): Promise<IRespone> => {
  try {

    const qs = queryString.stringify({ keyword: param.keyword, pageIndex: param.pageIndex, pageSize: param.pageSize })

    const res = await $get(req, `model/get-vehicle-models?${qs}`, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiVehicleModelCreate = async (param: IBodyModel, req?: any): Promise<IResponeModelCreate> => {
  try {
    const res = await $post(`model/create-vehicle-model`, param, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiVehicleModelDetail = async (code: string, req?: any): Promise<IResponeModelDetail> => {
  try {
    const res = await $get(req, `model/get-vehicle-model-detail/${code}`, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiVehicleModelUpdate = async (param: IBodyModel, req?: any): Promise<IResponeModelCreate> => {
  try {
    const res = await $put(`model/update-vehicle-model`, param, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiVehicleModelDelete = async (id: string, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`model/delete-vehicle-model/${id}`, {}, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

// ==== GRADE CODE ====
export const ApiListVehicleModelCode = async (req?: any): Promise<IResponseListModelCode> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/list-vehicle-models-code`, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

//===== CAR VERSION =====

const ApiListVehicleVersion = async (param: IParamVehicleList, req?: any): Promise<IResponseListVehicle> => {
  try {

    const qs = queryString.stringify({ ...param })
    const res = await $get(req, `grade/search?${qs}`, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiVehicleVersionDelete = async (id: number, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`grade/delete-grade-by-id`, { id }, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiVehicleVersionCreate = async (param: IParamCreateVersion, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`grade/create-grade`, param, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiListVehicleVersionDetail = async (id: number, req?: any): Promise<IResponseVehicleDetail> => {
  try {

    const res = await $get(req, `grade/get-detail-color?id=${id}`, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
export const ApiVehicleVersionUpdate = async (param: IParamUpdateVersion, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`grade/update-grade-by-id`, param, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

//===== CAR COLOR =====

const ApiListVehicleColor = async (param: IParamVehicleListColor, req?: any): Promise<IResponseListVehicleColor> => {
  try {

    const qs = queryString.stringify({ ...param })
    const res = await $get(req, `color/search-color?${qs}`, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiVehicleColorDelete = async (id: number, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`color/delete-color`, { id }, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiVehicleColorCreate = async (param: IParamCreateColor, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`color/create-color`, param, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiListVehicleColorDetail = async (id: number, req?: any): Promise<IResponseColorDetail> => {
  try {

    const res = await $get(req, `color/get-detail-color?id=${id}`, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiVehicleColorUpdate = async (param: IParamUpdateColor, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`color/update-color`, param, req, SLUG_VEHICLE)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export {
  ApiListVehicleByCustomerCms,
  ApiDeleteVehicleCustomerCms,
  ApiListVehicleModelsCode,
  ApiListVehicleModelsData,
  ApiVehicleModelCreate,
  ApiVehicleModelDelete,
  ApiListVehicleVersion,
  ApiVehicleVersionDelete,
  ApiVehicleVersionCreate,
  ApiListVehicleVersionDetail,
  ApiListVehicleColor,
  ApiVehicleColorDelete,
  ApiVehicleColorCreate,
  ApiListVehicleColorDetail,
  ApiVehicleColorUpdate
}