interface IParamListVehicleByCustomer {
  keyword: string
  pageIndex: number
  pageSize: number
  customerId: number | string | string[]
}
interface IResponseListVehicleByCustomer {
  code: number
  data: IResultDataResultVehicleByCustomer
  message: string
  result: boolean
}

interface IResultDataResultVehicleByCustomer {
  totalRecords: number
  pageIndex: number
  pageSize: number
  totalPages: number
  result: Array<IResultDataItemVehicleByCustomer>
}
interface IResultDataItemVehicleByCustomer {
  id: number
  dealerId: number
  modelId: number
  dealerName: string
  modelName: string
  licensePlate: string
  vin: string
  warrantyRegistrationDate: string
  lastMaintenanceDate: string
  engineNumber: string
  frameNumber: string
  engine: number
  numberKmCurrent: number
  modelColor: string
  yearOfManufacture: number
  batteryType: string
  tireType: string
  spareTireType: string
}

export interface IParamModelList {
  pageIndex: number;
  pageSize: number;
  keyword: string;
}
export interface IParamVehicleList {
  pageIndex: number;
  pageSize: number;
  keyword: string;
  modelCode: string;
}

export interface IParamVehicleListColor {
  pageIndex: number;
  pageSize: number;
  keyword: string;
}

export type {
  IParamListVehicleByCustomer,
  IResponseListVehicleByCustomer,
  IResultDataResultVehicleByCustomer,
  IResultDataItemVehicleByCustomer
}

export interface IBodyModel {
  brandCode: string,
  modelCode: string,
  modelName: string
}

export interface IResponeModelCreate {
  result: boolean,
  code: number,
  data: number,
  message: string
}

export interface IResponeModelDetail {
  result: boolean,
  code: number,
  data: {
    data: Array<IBodyModel>
  },
  message: string
}
export interface IResponseListModelCode {
  result: boolean;
  code: number;
  data: Array<{
    modelCode: string;
    modelName: string;
  }>;
  message: string
}
export interface IResponseListVehicle {
  result: boolean;
  code: number;
  data: {
    totalRecords: number;
    pageIndex: number;
    pageSize: number;
    totalPages: number;
    details: Array<{
      id: number;
      gradeCode: string;
      gradeName: string;
      modelName: string
    }>;
  }
  message: string
}

export interface IResponseListVehicleColor {
    result: boolean,
    code: number,
    data: {
      totalRecords: number,
      pageIndex: number,
      pageSize: number,
      totalPages: number,
      data: Array<{
        id: number,
        colorCode: string,
        colorNameEn: string,
        colorNameVi: string,
        groupColor: string,
        hexcode: string
      }>;
    },
    message: string
}

export interface IResponseVehicleDetail {
  result: boolean;
  code: number;
  data: {
    id: number;
    gradeCode: string;
    gradeName: string;
    modelCode: string
  };
  message: string
}

export interface IResponseColorDetail {
  result: boolean,
  code: number,
  data: {
    id: number,
    colorCode: string,
    groupColor: string,
    colorNameVi: string,
    hexCode: string
  },
  message: string
}
export interface IParamCreateVersion {
  modelCode: string;
  gradeCode: string;
  gradeName: string
}

export interface IParamCreateColor {
  colorCode: string,
  groupColor: string,
  colorNameVi: string,
  hexCode: string
}

export interface IParamUpdateVersion {
    id: number;
    gradeCode:string;
    gradeName: string;
    modelCode: string
}

export interface IParamUpdateColor {
    id: number,
    colorCode: string,
    groupColor: string,
    colorNameVi: string,
    hexCode: string
}