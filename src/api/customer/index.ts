import { SLUG_CUSTOMER } from 'src/configs/envConfig'
import { $get, $post, $put } from '../request'
import { IResponeCustomerByPhone, IResponseListCustomer, IResponseListCustomerCo } from './type'
import { INIT_CONTROLLER, nullRes } from '../initValue'
import { IRespone } from '../type'
import { LogELSError } from '../els'

const ApiListCustomerCms = async (param, req?: any): Promise<IResponseListCustomer> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/list-customer`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiCreateCustomerCms = async (param, req?: any): Promise<IResponseListCustomer> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/create-customer`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiEditCustomerCms = async (param, req?: any): Promise<IResponseListCustomer> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/customer-update`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiDetailCustomerCms = async (param, req?: any): Promise<IResponseListCustomer> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/customer-detail-by-id`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiCustomerUpdateStatusCms = async (param: any, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`${INIT_CONTROLLER}/customer-update-status`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiDeleteCustomerCms = async (param: any, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/customer-delete`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
const ApiCustomerUpdateCms = async (param: any, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`${INIT_CONTROLLER}/customer-update`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiCustomerByPhone = async (phoneNumber: string, req?: any): Promise<IResponeCustomerByPhone> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/find-customer-by-phone?phoneNumber=${phoneNumber}`, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiGetCustomerCo = async (param, req?: any): Promise<IResponseListCustomerCo> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/list-customer-co-owner`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiDeleteCustomerCo = async (param: any, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/delete-co-owners`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

const ApiCreateCustomerCo = async (param, req?: any): Promise<IResponseListCustomer> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/create-co-owners`, param, req, SLUG_CUSTOMER)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export {
  ApiListCustomerCms,
  ApiCreateCustomerCms,
  ApiDetailCustomerCms,
  ApiCustomerUpdateStatusCms,
  ApiEditCustomerCms,
  ApiDeleteCustomerCms,
  ApiCustomerUpdateCms,
  ApiCustomerByPhone,
  ApiGetCustomerCo,
  ApiDeleteCustomerCo,
  ApiCreateCustomerCo
}
