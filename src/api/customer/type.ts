interface IResponseListCustomer {
  code: number
  data: IDataListCustomer
  message: string
  result: boolean
}

interface IDataListCustomer {
  totalRecords: number
  pageIndex: number
  pageSize: number
  totalPages: number
  result: Array<IResultDataDetailCustomer>
}
interface IResultDataDetailCustomer {
  id: number
  avatar: string
  birthday: string
  fullName: string
  gender: string
  phoneNumber: number
  email: string
  address: string
  status: string
  personalTaxCode: number
  ward: string
  district: string
  province: string
  customerTypeId: string
  companyName: string
  companyPhoneNumber: number
  companyTaxCode: number
  companyWebsite: string
  companyAddress: string
  companyWard: string
  companyDistrict: string
  companyProvince: string
  companyWardId: string
  companyDistrictId: string
  companyProvinceId: string
  districtId: string
  provinceId: string
  wardId: string
  createdDate: string
}
interface IResultDataListCustomer {
  avatar: string
  id: number
  fullName: string
  identity: number
  phoneNumber: number
  email: string
  address: string
  numberOfCarsOwned: number
  status: number
  district: string
  gender: string
  province: string
  ward: string
}
interface IParamFilterListCustomer {
  keyword: string
  pageIndex: number
  pageSize: number
  startDate: string
  endDate: string
}

export type IResponeCustomerByPhone = {
  result: boolean,
  code: number,
  data: {
    id: number,
    fullName: string,
    gender: string,
    phoneNumber: string,
    birthday: string,
    email: string,
    address: string,
    ward: string,
    district: string,
    districtId: string,
    province: string,
    provinceId: string,
    status: string,
    personalTaxCode: string,
    relationship: string
  },
  message: string
}

interface IResponseListCustomerCo {
  result: boolean,
  code: number,
  data: {
    totalRecords: number,
    pageIndex: number,
    pageSize: number,
    totalPages: number,
    result:
    {
      id: number,
      fullName: string,
      gender: string,
      phoneNumber: string,
      birthday: string,
      email: string,
      address: string,
      ward: string,
      district: string,
      province: string,
      status: string,
      personalTaxCode: string,
      relationship: string
    }[]
  },
  message: string
}

export type {
  IResponseListCustomer,
  IDataListCustomer,
  IResultDataListCustomer,
  IParamFilterListCustomer,
  IResultDataDetailCustomer,
  IResponseListCustomerCo
}
