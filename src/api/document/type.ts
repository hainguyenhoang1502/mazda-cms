
// ** List User Manual
export type IParamListUserManual = {
  keyword: string,
  pageSize: number,
  pageIndex: number
}


export type IResponeListUserManual = {
  result: boolean,
  code: number,
  data: {
    totalRecords: number,
    pageIndex: number,
    pageSize: number,
    totalPages: number,
    details: {
      points: Array<string>,
      id: number,
      title: string,
      thumbnail: string,
      image: string,
      imageWidth: number,
      imageHeight: number,
      imageRatio: number,
      modelCode: string
    }[],
  },
  message: string
}

// ** Create User Manual
export type IParamCreateUserManual = {
  title: string,
  modelCode: string
}

// ** Detail User Manual
export type IResponeDetailUserManual = {
  result: boolean,
  code: number,
  data: {
    points: {
      id: number,
      title: string,
      coordinateX: number,
      coordinateY: number,
      details:
      {
        id: number,
        title: string,
        image: string,
        content: string
      }[]
    }[],
    id: number,
    title: string,
    thumbnail: string,
    image: string,
    imageWidth: number,
    imageHeight: number,
    imageRatio: string,
    modelCode: string,
    thumbnailFiles?: Array<string>,
    imageFiles?: Array<string>,
  }
  message: string
}

// ** Update User Manual
export type IParamUpdateUserManual = {
  id: number,
  title: string,
  thumbnail: string,
  image: string,
  imageWidth: number,
  imageHeight: number,
  imageRatio: string,
  modelCode: string
}


// ** Create Point User Manual
export type IParamCreatePointUserManual = {
  title: string,
  coordinateX: number,
  coordinateY: number,
  details?:
  {
    id: number,
    title: string,
    image: string,
    content: string
  }[],
  userManualId: number,
  pointDetails:
  {
    title: string,
    image: string,
    content: string,
    pointId: number,
    userManualId: number
  }[]
}

// ** Detail Point User Manual
export type IResponeDetailPointUserManual = {
  result: boolean,
  code: number,
  data: {
    id: number,
    title: string,
    coordinateX: number,
    coordinateY: number,
    details: {
      id: number,
      title: string,
      image: string,
      content: string
    }[]
  }
  message: string
}

// ** Update Point User Manual
export type IParamUpdatePointUserManual = {
  id: number,
  title: string,
  coordinateX: number,
  coordinateY: number,
  userManualId: number
}

// ** Update Point Detail User Manual 
export type IParamUpdateDetailPointUserManual = {
  id: number,
  title: string,
  image: string,
  content: string,
  pointId: number,
  userManualId: number
}

// ** Create Point Detail User Manual
export type IParamCreateDetailPointUserManual = {
  title: string,
  image: string,
  content: string,
  pointId: number,
  userManualId: number
}