import { SLUG_DOCUMENTS } from "src/configs/envConfig"
import { nullRes } from "../initValue"
import { $delete, $get, $post, $put } from "../request"
import { LogELSError } from "../els"
import { IParamCreateDetailPointUserManual, IParamCreatePointUserManual, IParamCreateUserManual, IParamListUserManual, IParamUpdateDetailPointUserManual, IParamUpdatePointUserManual, IParamUpdateUserManual, IResponeDetailPointUserManual, IResponeDetailUserManual, IResponeListUserManual } from "./type"
import { IRespone } from "../type"


// ** User Manual
export const ApiGetListUserManualCMS = async (param: IParamListUserManual, req?: any): Promise<IResponeListUserManual> => {
  try {
    const res = await $get(req, `usermanuals/list/${param.pageIndex}/${param.pageSize}?keyword=${param.keyword}`, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetCreateUserManualCMS = async (param: IParamCreateUserManual, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`usermanuals/create`, param, req, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetDeleteUserManualCMS = async (id: string, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`usermanuals/delete/${id}`, {}, req, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetUpdateUserManualCMS = async (param: IParamUpdateUserManual, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`usermanuals/update`, param, req, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetDetailUserManualCMS = async (id: number, req?: any): Promise<IResponeDetailUserManual> => {
  try {
    const res = await $get(req, `usermanuals/deltail/${id}`, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

// ** User Manual Point
export const ApiGetCreatePointUserManualCMS = async (param: IParamCreatePointUserManual, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`usermanuals/create-point`, param, req, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}


export const ApiGetDeletePointUserManualCMS = async (id: number, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`usermanuals/delete-point/${id}`, {}, req, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetDetailPointUserManualCMS = async (id: number, req?: any): Promise<IResponeDetailPointUserManual> => {
  try {
    const res = await $get(req, `usermanuals/point/${id}`, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetUpdatePointUserManualCMS = async (param: IParamUpdatePointUserManual, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`usermanuals/update-point`, param, req, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}


// ** User Manual Point Detail
export const ApiGetDeletePointDetailUserManualCMS = async (id: number, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`usermanuals/delete-point-detail/${id}`, {}, req, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetUpdatePointDetailUserManualCMS = async (param: IParamUpdateDetailPointUserManual, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`usermanuals/update-point-detail`, param, req, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetCreatePointDetailUserManualCMS = async (param: IParamCreateDetailPointUserManual, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`usermanuals/create-point-detail`, param, req, SLUG_DOCUMENTS)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
