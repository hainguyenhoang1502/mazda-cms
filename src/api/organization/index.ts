import { SLUG_ORGANIZATION } from "src/configs/envConfig"
import { INIT_CONTROLLER, nullRes } from "../initValue"
import { $delete, $get, $post, $put } from "../request"
import { LogELSError } from "../els"
import { IBodyBrand, IParamBrandList, IParamCreateCompanyAgency, IParamCreateDealer, IResponeBrandCreate, IResponeBussinessType, IResponeCompanyList, IResponeListDealer, IResponeOrgTree, IResponseOrganizationBrandDetail, IResponseOrganizationBrandList } from "./type"
import queryString from "query-string";
import { IParamFilter, IRespone } from "../type"


// ** Organizations
export const ApiListBussinessTypeCMS = async (req?: any): Promise<IResponeBussinessType> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/business-types-list`, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiListOrgTreeCMS = async (req?: any): Promise<IResponeOrgTree> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/org-tree`, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}



// ===== Brand ====
export const ApiOrganizationBrandList = async (param: IParamBrandList, req?: any): Promise<IResponseOrganizationBrandList> => {
  try {

    const qs = queryString.stringify({ code: param.code, status: param.status })
    const res = await $get(req, `${INIT_CONTROLLER}/brand-list/${param.pageIndex}/${param.pageSize}?${qs}`, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiOrganizationPostCreateBrand = async (param: IBodyBrand, req?: any): Promise<IResponeBrandCreate> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/create-brand`, param, req, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiOrganizationBrandDetail = async (code: string, req?: any): Promise<IResponseOrganizationBrandDetail> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/brand-detail/${code}`, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}


export const ApiOrganizationBrandUpdate = async (param: IBodyBrand, req?: any): Promise<IResponeBrandCreate> => {
  try {
    const res = await $put(`${INIT_CONTROLLER}/update-brand`, param, req, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiOrganizationBrandDelete = async (code: string, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`${INIT_CONTROLLER}/delete-brand/${code}`, {}, req, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}


// ** Company
export const ApiCreateCompanyCms = async (param: IParamCreateCompanyAgency, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/create-company`, param, req, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetListCompanyCms = async (param: IParamFilter, req?: any): Promise<IResponeCompanyList> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/companies-list/${param.pageIndex}/${param.pageSize}?keyword=${param.keyword}&status=${param.status}`, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApGetCompanyDetail = async (code: string, req?: any): Promise<IResponeCompanyList> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/company-detail/${code}`, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiUpdateCompanyCms = async (param: IParamCreateCompanyAgency, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`${INIT_CONTROLLER}/update-company`, param, req, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetDeleteCompanyCms = async (code: string, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`${INIT_CONTROLLER}/delete-company/${code}`, {}, req, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

// ** Dealer
export const ApiGetListDealerCms = async (param: IParamFilter, req?: any): Promise<IResponeListDealer> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/dealers-list/${param.pageIndex}/${param.pageSize}?keyword=${param.keyword}&status=${param.status}`, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiCreateDealerCms = async (param: IParamCreateDealer, req?: any): Promise<IRespone> => {
  try {
    const res = await $post(`${INIT_CONTROLLER}/create-dealer`, param, req, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}


export const ApiGetDeleteDealerCms = async (code: string, req?: any): Promise<IRespone> => {
  try {
    const res = await $delete(`${INIT_CONTROLLER}/delete-dealer/${code}`, {}, req, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiGetDetailDealerCms = async (code: string, req?: any): Promise<IResponeListDealer> => {
  try {
    const res = await $get(req, `${INIT_CONTROLLER}/dealer-detail/${code}`, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}

export const ApiUpdateDealerCms = async (param: IParamCreateDealer, req?: any): Promise<IRespone> => {
  try {
    const res = await $put(`${INIT_CONTROLLER}/update-dealer`, param, req, SLUG_ORGANIZATION)

    return res ? res : nullRes
  } catch (error) {
    await LogELSError(error)

    return nullRes
  }
}
