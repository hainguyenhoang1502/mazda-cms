// ** Organization
export type IResponeBussinessType = {
  result: boolean,
  code: number,
  data: {
    code: string,
    name: string
  }[],
  message: string
}

export type IResponeOrgTree = {
  result: boolean,
  code: number,
  data: {
    regionCode: string,
    regionName: string,
    allChild?: Array<string>
    areas: {
      areaCode: string,
      areaName: string,
      allChild?: Array<string>
      companies:
      {
        companyCode: string,
        companyName: string,
        allChild?: Array<string>
        dealers:
        {
          dealerCode: string,
          dealerName: string
        }[]
      }[]
    }[]
  }[]
  message: string
}
export interface IResponeBrandCreate {
  result: boolean,
  code: number,
  data: number,
  message: string
}

export interface IResponseOrganizationBrandList {
  result: boolean,
  code: number,
  data: {
    totalRecords: number,
    pageIndex: number,
    pageSize: number,
    totalPages: number,
    data: [
      {
        code: string,
        name: string,
        status: string
      },
    ]
  },
  message: string
}

export interface IResponseOrganizationBrandDetail {
  result: boolean,
  code: number,
  data: {
    totalRecords: number,
    pageIndex: number,
    pageSize: number,
    totalPages: number,
    data: Array<IBodyBrand>
  },
  message: string
}

export interface IParamBrandList {
  pageIndex: number;
  pageSize: number;
  code: string;
  status: string;
}

export interface IParamBrandDetail {
  code: string
}

export interface IBodyBrand {
  code: string,
  name: string,
  status: string
}


export type IDataDealerOrganizationTree = {
  id: number,
  code: string,
  organizationName: string,
  phoneNumber: number | string,
  hotline: number | string,
  email: string,
  organizationType: number | string,
  regionCode: number | string,
  provinceCode: number | string,
  districtCode: number | string,
  wardCode: number | string,
  longitude: number | string,
  latitude: number | string,
  mapURL: string,
  status: string,
  emailReceiveNotify: Array<string>,
  companyCode: number | string,
  comapanyName: string,
  provincesFullName: string,
  districtFullName: string,
  wardFullName: string,
  locationFullPath: string,
  branchs: Array<IDataBranchOrganizationTree>
}

export type IDataBranchOrganizationTree = {
  id: number,
  code: string,
  organizationName: string,
  phoneNumber: number | string,
  hotline: number | string,
  email: string,
  organizationType: number | string,
  regionCode: number | string,
  provinceCode: number | string,
  districtCode: number | string,
  wardCode: number | string,
  longitude: number | string,
  latitude: number | string,
  mapURL: string,
  status: string,
  emailReceiveNotify: Array<string>,
  companyCode: string,
  comapanyName: string,
  dealerCode: string,
  dealerName: string,
  provincesFullName: string,
  districtFullName: string,
  wardFullName: string,
  locationFullPath: string
}

// ** Company


export type IResponeCompanyList = {
  result: boolean,
  code: number,
  data: {
    totalRecords: number,
    pageIndex: number,
    pageSize: number,
    totalPages: number,
    data: {
      id: number,
      code: string,
      name: string,
      provinceCode: string,
      provinceName: string,
      districtCode: string,
      districtName: string,
      wardCode: string,
      wardName: string,
      address: string,
      longitude: number,
      latitude: number,
      mapURL: string,
      status: string,
      jsonPhone: {
        key: string,
        value: string
      }[],
      jsonEmail: {
        key: string,
        value: string
      }[],
      emailReceiveNotify: Array<string>,
      legalEntityName: string,
      companyType: string,
      regionCode: string,
      regionName: string,
      areaCode: string,
      areaName: string
    }[]
  },
  message: string
}


export type IParamCreateCompanyAgency = {
  code: string,
  name: string,
  provinceCode?: string,
  districtCode?: string,
  wardCode?: string,
  address?: string,
  longitude?: number,
  latitude?: number,
  mapURL: string,
  status: string,
  jsonPhone?: {
    key: string,
    value: string
  }[],
  jsonEmail?: {
    key: string,
    value: string
  }[],
  emailReceiveNotify: Array<string>,
  legalEntityName: string,
  companyType: number | string,
  regionCode: string,
  areaCode: string
}

export type IResponeDetailComanyAgency = {
  result: true,
  code: 0,
  data: {
    id: 0,
    code: string,
    name: string,
    provinceCode: string,
    provinceName: string,
    districtCode: string,
    districtName: string,
    wardCode: string,
    wardName: string,
    address: string,
    longitude: number,
    latitude: number,
    mapURL: string,
    status: string,
    jsonPhone?: {
      key: string,
      value: string
    }[],
    jsonEmail?: {
      key: string,
      value: string
    }[],
    emailReceiveNotify: Array<string>,
    legalEntityName: string,
    companyType: string,
    regionCode: string,
    regionName: string,
    areaCode: string,
    areaName: string
  },
  message: string
}

// ** Dealer 
export type IResponeListDealer = {
  result: boolean,
  code: number,
  data: {
    totalRecords: number,
    pageIndex: number,
    pageSize: number,
    totalPages: number,
    data: {
      id: number,
      code: string,
      name: string,
      provinceCode: string,
      provinceName: string,
      districtCode: string,
      districtName: string,
      wardCode: string,
      wardName: string,
      address: string,
      longitude: number,
      latitude: number,
      mapURL: string,
      status: string,
      jsonPhone: {
        key: string,
        value: string
      }[],
      jsonEmail: {
        key: string,
        value: string
      }[],
      emailReceiveNotify: Array<string>,
      companyCode: string,
      companyName: string,
      brandCodes: Array<string>,
      businessTypeCodes: Array<string>
    }[]
  },
  message: string
}

export type IParamCreateDealer = {
  code: string,
  name: string,
  provinceCode: string,
  districtCode: string,
  wardCode: string,
  address: string,
  longitude: number,
  latitude: number,
  mapURL: string,
  status: string,
  jsonPhone?: {
    key: string,
    value: string
  }[],
  jsonEmail?: {
    key: string,
    value: string
  }[],
  emailReceiveNotify: Array<string>,
  companyCode: string,
  brandCodes: Array<string>
  businessTypeCodes: Array<string>
  website: string
  image: string
  urlImg?: string
}

export type IResponeDetailDealer = {
  result: boolean,
  code: number,
  data: {
    id: number,
    code: string,
    name: string,
    provinceCode: string,
    provinceName: string,
    districtCode: string,
    districtName: string,
    wardCode: string,
    wardName: string,
    address: string,
    longitude: number,
    latitude: number,
    mapURL: string,
    status: string,
    jsonPhone?: {
      key: string,
      value: string
    }[],
    jsonEmail?: {
      key: string,
      value: string
    }[],
    emailReceiveNotify: Array<string>,
    companyCode: string,
    companyName: string,
    brandCodes: Array<string>
    businessTypeCodes: Array<string>
    website: string
    image: string
    urlImg?: string
  },
}