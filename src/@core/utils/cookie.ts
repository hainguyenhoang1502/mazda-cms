function setCookie(cname: string, cvalue: string, exdays?: number) {
  const dt = new Date();
  const getExdays = exdays || 7
  dt.setTime(dt.getTime() + (getExdays * 24 * 60 * 60 * 1000));
  const expires = "expires=" + dt.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname: string, req?: any) {
  if (req) {
    
    return req.cookies[cname]
  }else{
    if (typeof window !== "undefined") {
      const name = cname + "=";
      const decodedCookie = decodeURIComponent(document.cookie);
      const ca = decodedCookie.split(';');
      for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
  
      return "";
    }
  }
}

const eraseCookie = (name) => {
  setCookie(name, '', -1);
};

export {
  getCookie,
  setCookie,
  eraseCookie,
};
