import { store } from "src/store";

export const checkUserPermission = (role: string) => {
  if (!role) {

    return true
  } else {
    const grantedAuthorities = store.getState().profile.grantedAuthorities
    const isPermission = grantedAuthorities && grantedAuthorities.find(item => item === role)
    if (role && isPermission) {

      return true
    }

    return false
  }
}
