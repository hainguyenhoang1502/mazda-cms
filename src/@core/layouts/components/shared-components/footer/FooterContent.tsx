
// ** MUI Imports
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { Card, Grid } from '@mui/material'

const FooterContent = () => {

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <Card sx={{ p: 5 }}>
          <Box sx={{ display: "flex" }}>
            <Typography sx={{ mr: 2 }}>
              Develop by:
            </Typography>
            <Typography sx={{ fontWeight: 500, color: '#696CFF' }}>
              TopOnTech
            </Typography>
          </Box>
        </Card>
      </Grid>
    </Grid>
  )
}

export default FooterContent
