// ** MUI Imports
import Box, { BoxProps } from '@mui/material/Box'
import CircularProgress from '@mui/material/CircularProgress'
import { styled } from '@mui/material/styles'

const FallbackSpinner = ({ sx }: { sx?: BoxProps['sx'] }) => {
  // ** Hook

  const Img = styled('img')(({ theme }) => ({
    maxWidth: '100%',
    marginTop: theme.spacing(4)
  }))

  return (
    <Box
      sx={{
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        ...sx
      }}
    >

      <Img width='100px' alt='logothacoauto' src={`/images/logo-loading.png`} />

      <CircularProgress disableShrink sx={{ mt: 6 }} color='info' />
    </Box>
  )
}

export default FallbackSpinner
