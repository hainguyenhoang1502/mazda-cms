// ** React Imports
import { ReactNode, ReactElement, useEffect } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** Hooks Import
import { useAuth } from 'src/hooks/useAuth'

// ** Utils Config Import
import { getCookie } from 'src/@core/utils/cookie'
import authConfig from 'src/configs/auth'

interface AuthGuardProps {
  children: ReactNode
  fallback: ReactElement | null

}

const AuthGuard = (props: AuthGuardProps) => {
  const { children, fallback } = props
  const auth = useAuth()
  const router = useRouter()


  useEffect(
    () => {
      if (!router.isReady) {
        return
      }

      if (!auth.token && !getCookie(authConfig.tokenKeyName)) {
        if (router.asPath !== '/') {
          router.replace({
            pathname: '/login',
            query: { returnUrl: router.asPath }
          })
        } else {
          router.replace('/login')
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [router.route]
  )

  if (auth.loading || !auth.token) {
    return fallback
  }

  return <>{children}</>
}

export default AuthGuard
