import { ReactNode } from 'react'

export type PageHeaderProps = {
  title: ReactNode
  subtitle?: ReactNode
}
export type PageHeaderTitleProps = {
  title: string
}