// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** Types
import { PageHeaderProps, PageHeaderTitleProps } from './types'
import Typography from '@mui/material/Typography'
import { renderTextTitle } from 'src/configs/functionConfig'

const PageHeader = (props: PageHeaderProps) => {
  // ** Props
  const { title, subtitle } = props

  return (
    <Grid item xs={12}>
      {title}
      {subtitle || null}
    </Grid>
  )
}

export const PageHeaderTitle = (props: PageHeaderTitleProps) => {

  return <Typography sx={{ fontSize: '24px', fontWeight: 700, color: '#696CFF' }}>{renderTextTitle(props.title)}</Typography>
}

export default PageHeader
